package net.lustlab.rndr.buffers;

import com.jogamp.opengl.GL;

public enum WrapMode {

    REPEAT,
    CLAMP_TO_EDGE,
    MIRRORED_REPEAT;

    public int toGLWrapMode() {
        switch(this) {
            case REPEAT:
                return GL.GL_REPEAT;
            case CLAMP_TO_EDGE:
                return GL.GL_CLAMP_TO_EDGE;
            case MIRRORED_REPEAT:
                return GL.GL_MIRRORED_REPEAT;
            default:
                throw new RuntimeException("unsupported wrap mode");
        }
    }


}
