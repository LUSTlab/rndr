package net.lustlab.rndr.draw;

import net.lustlab.rndr.math.Matrix44;

/**
 * DrawContext is an internal class that is passed between internal drawing systems
 */
public class DrawContext {

    public int width;
    public int height;

    public Matrix44 projection;
    public Matrix44 modelView;

    public DrawContext(int width, int height, Matrix44 projection, Matrix44 modelView) {
        this.width = width;
        this.height = height;
        this.projection = projection;
        this.modelView = modelView;
    }
}
