package net.lustlab.rndr.draw;

/**
* Created by edwin on 6/3/15.
*/
public enum BlendMode {
    NORMAL,
    OVER,
    ADD,
    ADD_ALPHA,
    NONE,
    MULTIPLY

}
