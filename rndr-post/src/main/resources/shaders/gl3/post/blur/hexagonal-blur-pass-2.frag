#version 330

layout(location=0) out vec4 o_output;


in vec2 v_texCoord0;

uniform sampler2D vertical;
uniform sampler2D diagonal;

uniform vec2 direction0;
uniform vec2 direction1;

const int NUM_SAMPLES = 40;
const float PI = 3.14159265322f;

float saturate(float x) {
    return clamp(x, 0.0, 1.0);
}

void main() {
    vec2 viewport = textureSize(vertical, 0);

	vec4 curr = texture(vertical, v_texCoord0);
	vec4 sum = vec4(0.0);;

	vec2 s = vec2(0, 1) / viewport;
	vec2 p2 = v_texCoord0 + s * 0.5;
	for (int i = 1; i < NUM_SAMPLES; ++i) {
		vec4 tmp = texture(vertical, p2);
		float alpha = 1.0;//saturate(tmp.a - i);
		alpha *= saturate(curr.a - i);
		sum += 2 * tmp * alpha;
		p2 += s;
	}

	s = vec2(-sin(3.14159265 / 3), -cos(3.14159265 / 3)) / viewport;
	p2 = v_texCoord0 + s * 0.5;
	for (int i = 1; i < NUM_SAMPLES; ++i) {
		vec4 tmp = texture(diagonal, p2);
		float alpha = 1.0;//saturate(tmp.a - i);
		alpha *= saturate(curr.a - i);
		sum += tmp * alpha;
		p2 += s;
	}

	if (sum.a < 1)
		o_output  =vec4(curr.rgb / curr.a, 1);
	else
		o_output = vec4(sum.rgb / sum.a, 1);

}