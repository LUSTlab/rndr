package net.lustlab.rndr.math;

public class Linear {

    public static Matrix44 multiply(Matrix44 ... matrices) {
        Matrix44 result = Matrix44.IDENTITY;
        for (int i = 0; i < matrices.length; ++i) {
            result = result.multiply(matrices[i]);
        }
        return result;
    }

    public static Vector3 multiply(Matrix44 m, Vector3 v) {
        return v.multiply(m);
    }


    public static Vector2 vector2(double x, double y) {
        return new Vector2(x, y);
    }

    public static Vector3 vector3(double x, double y, double z) {
        return new Vector3(x, y, z);
    }

    public static Vector4 vector4(double x, double y, double z, double w) {
        return new Vector4(x, y, z, w);
    }



    /**
     * Allows you to write Vector3 a = plus(left, right);
     * @param left left vector
     * @param right right vector
     * @return
     */
    public static Vector3 add(Vector3 left, Vector3 right) {
        return left.plus(right);
    }


    public static Vector3 subtract(Vector3 left, Vector3 right) {
        return left.minus(right);
    }



}
