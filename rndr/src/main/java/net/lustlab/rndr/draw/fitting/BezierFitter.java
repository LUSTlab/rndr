package net.lustlab.rndr.draw.fitting;

import net.lustlab.rndr.draw.*;
import net.lustlab.rndr.math.Bezier;
import net.lustlab.rndr.math.Vector2;
import net.lustlab.rndr.shape.Segment;
import net.lustlab.rndr.shape.Shape;
import net.lustlab.rndr.shape.ShapeContour;

import java.util.ArrayList;
import java.util.List;

/**
 * Bezier curve fitter
 *
 * This is an online curve fitter. It expects points to come in sequentially, as would happen in for example a drawing application.
 *
 * The implemented curve fitting algorithm is based on vector distance maps.
 *
 * Implements ideas described in
 * Efficient Curve Fitting; Sarah F. Frisken, 2008
 */
public class BezierFitter {

    double maxError = 2.0;
    int maxIterations = 100;
    int sampleCount = 50;
    private double stepSize = 10.0;
    double maxAngle = 60;

    /**
     * Sets the maximum angle between last added curve segment and new input point
     * @param maxAngle the maximum angle in degrees
     */
    public BezierFitter maxAngle(double maxAngle) {
        this.maxAngle = maxAngle;
        return this;
    }

    static class CurveSegment {
        public Vector2 c0;
        public Vector2 c1;
        public Vector2 c2;
        public Vector2 c3;
        public boolean constrained = false;

        public CurveSegment copy() {
            CurveSegment copy = new CurveSegment();
            copy.c0 = c0;
            copy.c1 = c1;
            copy.c2 = c2;
            copy.c3 = c3;
            copy.constrained = constrained;
            return copy;
        }
    }

    List<CurveSegment> curve = new ArrayList<>();

    DistanceMap distanceMap;
    public BezierFitter(Drawer drawer, int width, int height) {
        distanceMap = new DistanceMap(drawer, width, height);
    }


    Vector2 previous;

    /**
     * Resets the active curve
     */
    public BezierFitter reset() {
        curve = new ArrayList<>();
        previous = null;
        distanceMap.clear();
        return this;
    }

    /**
     * Add a point to the curve
     */
    public BezierFitter addPoint(Vector2 point) {


        if (curve.size() == 0) {
            CurveSegment segment = new CurveSegment();
            segment.c0 = point.copy();
            segment.c1 = point.copy();
            segment.c2 = point.copy();
            segment.c3 = point.copy();
            curve.add(segment);
            previous = point;
            return this;
        }

        UpdateStatus status = updateCurveSegment(point, curve.get(curve.size()-1));

        if (status != UpdateStatus.SUCCESS) {
            distanceMap.clear();
            distanceMap.point(curve.get(curve.size()-1).c3);
            distanceMap.line(curve.get(curve.size() - 1).c3, point);

            CurveSegment next = new CurveSegment();
            next.c0 = curve.get(curve.size()-1).c3;
            next.c1 = point;
            next.c2 = point;
            next.c3 = point;

            if (status == UpdateStatus.CORNER) {
                next.constrained = false;
            } else {
                next.constrained = true;
            }

            curve.add(next);


        }
        previous = point;
        return this;
    }

    enum UpdateStatus {
        SUCCESS,
        CORNER,
        FAILURE
    }

    UpdateStatus updateCurveSegment(Vector2 point, CurveSegment segment) {


        Vector2 prev = segment.c3;
        {
            Vector2 tangent = segment.c2.minus(segment.c3).normalized();
            if (segment.c3.minus(segment.c2).length() > 0.01 && Math.acos((-point.minus(prev).normalized().dot(tangent))) > Math.toRadians(maxAngle)) {
                return UpdateStatus.CORNER;
            }
        }

        CurveSegment backup = segment.copy();

        segment.c3 = point.copy();
        segment.c2 = segment.c2.plus(point).minus(prev);

        distanceMap.line(prev, point);
        distanceMap.point(prev);

        double error = Double.POSITIVE_INFINITY;
        int iterations = 0;
        while (error > maxError && iterations < maxIterations) {
            Vector2 f1 = new Vector2(0, 0);
            Vector2 f2 = new Vector2(0, 0);
            for (int i = 0; i <= sampleCount; ++i) {
                double t = (double)i / sampleCount;
                double it = 1.0 - t;
                Vector2 xy = Bezier.bezier(segment.c0, segment.c1, segment.c2, segment.c3, t);
                Vector2 dxdy = distanceMap.distance(xy);

                double d = dxdy.length() * stepSize;
                f1 = f1.minus(dxdy.scale(6.0 * t * it *it * d).scale(1,1) );
                f2 = f2.minus(dxdy.scale(6.0 * t * t *it * d) ).scale(1,1);
            }
            if (segment.constrained) {
                Vector2 tangent = segment.c1.minus(segment.c0).normalized();
                f1 = tangent.scale(f1.dot(tangent));
            }

            segment.c1 = segment.c1.plus(f1);
            segment.c2 = segment.c2.plus(f2);

            error = 0;
            for (int i = 0; i <= sampleCount; ++i) {
                double t = (double)i / sampleCount;
                Vector2 xy = Bezier.bezier(segment.c0, segment.c1, segment.c2, segment.c3, t);
                Vector2 dxdy = distanceMap.distance(xy);
                error = error + dxdy.length();
            }

            iterations++;

            if (error < maxError) {
                return UpdateStatus.SUCCESS;
            }

        }
        segment.c0 = backup.c0;
        segment.c1 = backup.c1;
        segment.c2 = backup.c2;
        segment.c3 = backup.c3;
        return UpdateStatus.FAILURE;

    }

    /**
     * Returns a copy of the active curve as a Shape
     * @return a Shape instance with a single open contour
     */
    public Shape shape() {
        Shape shape = new Shape();
        ShapeContour contour = new ShapeContour();
        for (CurveSegment cs: curve) {
            contour.segments.add(new Segment(cs.c0, cs.c1, cs.c2, cs.c3));
        }

        shape.contours.add(contour);
        return shape;
    }

}

