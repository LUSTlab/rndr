#version 150

in vec2 v_texCoord0;
uniform sampler2D tex0;

out vec4 o_color;
void main() {

    vec2 s = textureSize(tex0, 0).xy;
    s = vec2(1.0/s.x, 1.0/s.y);

    vec2 v00 = texture(tex0, v_texCoord0).rg;
    vec2 v10 = texture(tex0, v_texCoord0 + s * vec2(1.0, 0.0)).rg;
    vec2 v01 = texture(tex0, v_texCoord0 + s * vec2(0.0, 1.0)).rg;
    vec2 v11 = texture(tex0, v_texCoord0 + s * vec2(1.0, 1.0)).rg;

    float l00 = length(v00);
    float l01 = length(v01);
    float l11 = length(v11);
    float l10 = length(v10);

    vec2 tv = v00;
    if (l01 >= l00 && l01 >= l11 && l01 > l10) tv = v01;
    if (l10 >= l00 && l10 >= l11 && l10 > l01) tv = v10;
    if (l11 >= l00 && l11 >= l01 && l11 > l10) tv = v11;

    o_color = vec4(tv.x, tv.y, 0.0, 1.0);

}