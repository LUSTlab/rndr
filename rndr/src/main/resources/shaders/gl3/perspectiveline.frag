#version 330

out vec4 o_color;

uniform vec4 stroke;

void main() {
    o_color = stroke;
}