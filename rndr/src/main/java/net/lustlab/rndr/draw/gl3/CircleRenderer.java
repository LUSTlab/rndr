package net.lustlab.rndr.draw.gl3;

import com.jogamp.opengl.GL3;

import com.jogamp.opengl.GLContext;

import net.lustlab.rndr.buffers.*;
import net.lustlab.rndr.draw.DrawContext;
import net.lustlab.rndr.draw.DrawStyle;

import net.lustlab.rndr.gl.GLError;

import net.lustlab.rndr.math.Vector2;
import net.lustlab.rndr.math.Vector3;
import net.lustlab.rndr.shaders.ShadeStyleManager;
import net.lustlab.rndr.shaders.Shader;

import java.nio.ByteBuffer;
import java.util.List;


public class CircleRenderer {


    static int intPropertyOrDefault(String property, int defaultValue) {

        String stringValue = System.getProperty(property);

        if (stringValue != null) {
            try {
                int realValue = Integer.parseInt(stringValue);
                System.out.println("setting the value to" + realValue);
                return realValue;
            } catch (NumberFormatException e) {
                System.out.println("error while parsing value for:" + property);
                return defaultValue;
            }
        } else {
            return defaultValue;
        }
    }

    VertexBuffer circleVBO;

    ShadeStyleManager circleShaderManager;
    ShadeStyleManager circlesShaderManager;
    BufferTexture circlePositions;

    int circleCount = intPropertyOrDefault("net.lustlab.rndr.CircleRenderer.bufferSize", 200_000);


    public CircleRenderer() {
        circleShaderManager = new ShadeStyleManager("cp:shaders/gl3/circle.vert", "cp:shaders/gl3/circle.frag");
        circlesShaderManager = new ShadeStyleManager("cp:shaders/gl3/circles.vert", "cp:shaders/gl3/circles.frag");

        VertexFormat format = new VertexFormat().textureCoordinate(2).position(3);

        circlePositions = BufferTexture.createStream(ColorFormat.RGBA, ColorType.FLOAT32, circleCount);
        {
            ByteBuffer circleBuffer = ByteBuffer.allocateDirect(format.size() * 6);
            BufferWriter bw = new BufferWriter(circleBuffer);

            Vector2 uv00 = new Vector2(0, 0);
            Vector2 uv01 = new Vector2(0, 1);
            Vector2 uv11 = new Vector2(1, 1);
            Vector2 uv10 = new Vector2(1, 0);
            Vector3 p00 = new Vector3(-1, -1, 0);
            Vector3 p01 = new Vector3(-1, 1, 0);
            Vector3 p10 = new Vector3(1, -1, 0);
            Vector3 p11 = new Vector3(1, 1, 0);

            bw.write(uv00).write(p00).write(uv01).write(p01).write(uv11).write(p11);
            bw.write(uv11).write(p11).write(uv10).write(p10).write(uv00).write(p00);

            circleBuffer.rewind();

            circleVBO = VertexBuffer.createStatic(format, 6, circleBuffer);
        }
    }

    public void drawCircle(DrawContext context, DrawStyle style, Vector3 position, double radius) {

        GL3 gl3 = GLContext.getCurrentGL().getGL3();

        StateTools.setupState(style);
        Shader circleShader = circleShaderManager.shader(style.shadeStyle, circleVBO.format());

        circleShader.begin();
        ShaderTools.setupShader(circleShader, context, style);
        circleShaderManager.setParameters(circleShader, style.shadeStyle);

        circleShader.setUniform("offset", position);
        circleShader.setUniform1f("radius", (float) radius);

        circleVBO.bind();
        VertexBufferDrawer.draw(gl3, circleShader, circleVBO, circleVBO.format(), GL3.GL_TRIANGLES, 6);
        circleShader.end();
        circleVBO.unbind();
        GLError.debug(gl3);
    }

    public void drawCircles(DrawContext context, DrawStyle style, BufferTexture positionsAndRadii, double radius, double radiusMod, int count) {
        GL3 gl3 = GLContext.getCurrentGL().getGL3();
        StateTools.setupState(style);
        Shader circlesShader = circlesShaderManager.shader(style.shadeStyle, circleVBO.format());

        circlesShader.begin();
        ShaderTools.setupShader(circlesShader, context, style);
        circlesShaderManager.setParameters(circlesShader, style.shadeStyle);
        circlesShader.setUniform1i("positions", 0);
        circleVBO.bind();

        positionsAndRadii.bindTexture(0);

        int instancesPerBatch = 32_000;

        int batches = (int) Math.ceil((double)count / instancesPerBatch);
        int processed = 0;
        for (int batch = 0; batch <  batches; ++batch) {
            circlesShader.setUniform1i("instanceOffset", processed);
            VertexBufferDrawer.drawInstances(gl3, circlesShader, circleVBO, circleVBO.format(), GL3.GL_TRIANGLES, 6, Math.min(instancesPerBatch, count - processed));
            processed += instancesPerBatch;
        }

        //circlePositions.unbindTexture();
        circlesShader.end();
        circleVBO.unbind();

        positionsAndRadii.unbindTexture();
        GLError.debug(gl3);

    }

    public void drawCircles(DrawContext context, DrawStyle style, List<Vector2> positions, List<Double> radii) {

        {
            int idx = 0;
            BufferWriter bw = circlePositions.shadow().writer();
            circlePositions.shadow().buffer().rewind();
            for (Vector2 position : positions) {
                double radius = radii.get(idx);
                bw.write(position).write(0.0f).write((float) radius);
                idx++;
            }

            circlePositions.shadow().buffer().rewind();
            circlePositions.write(circlePositions.shadow().buffer(), positions.size());
        }
        drawCircles(context, style, circlePositions, 0, 0, positions.size());
    }

    public void destroy() {
        circleVBO.destroy();
        circlePositions.destroy();
        circleShaderManager.destroy();
        circlesShaderManager.destroy();
    }
}
