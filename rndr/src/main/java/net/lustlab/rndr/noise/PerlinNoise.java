package net.lustlab.rndr.noise;


import net.lustlab.rndr.math.Vector3;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Random;

public class PerlinNoise {

    final int permSize = 8192;
    int[] P;
    double[] G;

    int bands = 8;
    double spread = 2.0;
    double decay = 0.5;
    Vector3 domain = new Vector3(1, 1, 1);

    double grad(int hash, double x, double y, double z) {
        int h = hash & 15;                      // CONVERT LO 4 BITS OF HASH CODE
        double u = h<8 ? x : y,                 // INTO 12 GRADIENT DIRECTIONS.
                v = h<4 ? y : h==12||h==14 ? x : z;
        return ((h&1) == 0 ? u : -u) + ((h&2) == 0 ? v : -v);

    }

    /**
     * Constructor. Uses System.currentTimeMillis() as seed.
     */
    public PerlinNoise() {
        this(System.currentTimeMillis());
    }

    /**
     * Constructor with user supplied seed.
     * @param seed the seed to be used in the random number generator
     */
    public PerlinNoise(long seed) {

        Random generator = new Random();
        generator.setSeed(seed);

        List<Integer> p = new ArrayList<Integer>();
        for (int i = 0; i < permSize; ++i) {
            p.add(i);
        }

        Collections.shuffle(p);
        P = new int[permSize];
        G = new double[permSize];
        for (int i = 0; i < permSize; ++i) {
            P[i] = p.get(i);
            G[i] = generator.nextDouble()*2.0 - 1.0; //Math.random()*2.0-1.0;
        }
    }


    int mod(int a, int b) {
        return ((a%b)+b)%b;
    }

    public NoiseSample noise(Vector3 position) {

        double u, v, w;

        position = new Vector3(Math.abs(position.x), Math.abs(position.y), Math.abs(position.z));

        final int i = (int) position.x;
        final int j = (int) position.y;
        final int k = (int) position.z;
        u = position.x - i;
        v = position.y - j;
        w = position.z - k;



        final double du = 30.0f*u*u*(u*(u-2.0f)+1.0f);
        final double dv = 30.0f*v*v*(v*(v-2.0f)+1.0f);
        final double dw = 30.0f*w*w*(w*(w-2.0f)+1.0f);

        u = u*u*u*(u*(u*6.0f-15.0f)+10.0f);
        v = v*v*v*(v*(v*6.0f-15.0f)+10.0f);
        w = w*w*w*(w*(w*6.0f-15.0f)+10.0f);

        final int A = P[mod(i, permSize)  ]+j, AA = P[mod(A,permSize)]+k, AB = P[mod(A+1,permSize)]+k;      // HASH COORDINATES OF
        final int B = P[mod(i+1,permSize)]+j, BA = P[mod(B,permSize)]+k, BB = P[mod(B+1,permSize)]+k;


        double a = grad(P[mod(AA, permSize)], u,v,w);
        double b = grad(P[mod(BA, permSize)], u-1,v,w);
        double c = grad(P[mod(AB, permSize)], u,v-1,w);
        double d = grad(P[mod(BB, permSize)], u-1,v-1,w);
        double e = grad(P[mod((AA+1), permSize)], u,v,w-1);
        double f = grad(P[mod((BA+1), permSize)], u-1,v,w-1);
        double g = grad(P[mod((AB+1), permSize)], u,v-1,w-1);
        double h = grad(P[mod((BB+1), permSize)], u-1,v-1,w-1);


        double k0 =   a;
        double k1 =   b - a;
        double k2 =   c - a;
        double k3 =   e - a;
        double k4 =   a - b - c + d;
        double k5 =   a - c - e + g;
        double k6 =   a - b - e + f;
        double k7 = - a + b + c - d + e - f - g + h;

        NoiseSample ns = new NoiseSample();

        ns.value = (k0 + k1*u + k2*v + k3*w + k4*u*v + k5*v*w + k6*w*u + k7*u*v*w);
        ns.gradient = new Vector3(
        (du * (k1 + k4*v + k6*w + k7*v*w)),
        (dv * (k2 + k5*w + k4*u + k7*w*u)),
        (dw * (k3 + k6*u + k5*v + k7*u*v)));
        return ns;
    }


    /**
     * Sets the domain
     * @param width the width of the domain, default value is 1.0
     * @param height the height of the domain, default value is 1.0
     * @param depth the depth of the domain, default value is 1.0
     */
    public PerlinNoise domain(double width, double height, double depth) {
        domain = new Vector3(width, height, depth);
        return this;
    }

    /**
     * Sets the frequency spreading
     * @param spread the spread amount, default value is 2.0
     */
    public PerlinNoise spread(double spread) {
        this.spread = spread;
        return this;
    }

    /**
     * Sets the decay
     * @param decay the decay amount, default value is 0.5
     */
    public PerlinNoise decay(double decay) {
        this.decay = decay;
        return this;
    }

    /**
     * Sets the number of bands to be used in sum()
     * @param bands the number of bands, default value is 8
     */
    public PerlinNoise bands(int bands) {
        this.bands = bands;
        return this;
    }

    public NoiseSample sum(Vector3 position) {
        return normalizedSum(position.scale(1.0/domain.x, 1.0/domain.y, 1.0/domain.z), spread, decay, bands);
    }

    @Deprecated
    public NoiseSample sum(Vector3 position, double spread, double decay, int bands) {
        double weight = 1.0;
        NoiseSample sum = new NoiseSample();
        sum.value = 0;
        sum.gradient = Vector3.Zero;

        for (int band = 0; band < bands; ++band) {

            position = position.scale(spread);
            NoiseSample incoming = noise(position);

            sum.value += incoming.value * weight;
            sum.gradient = sum.gradient.plus(incoming.gradient.scale(weight));
            weight *= decay;

        }
        return sum;
    }


    @Deprecated
    public NoiseSample normalizedSum(Vector3 position, Vector3 domain, double spread, double decay, int bands) {
        return normalizedSum(position.scale(1.0/domain.x, 1.0/domain.y, 1.0/domain.z), spread, decay, bands);
    }

    @Deprecated
    public NoiseSample normalizedSum(Vector3 position, double spread, double decay, int bands) {
        double weight = 1.0;
        NoiseSample sum = new NoiseSample();
        sum.value = 0;
        sum.gradient = Vector3.Zero;
        double totalWeight = 0;

        for (int band = 0; band < bands; ++band) {
            totalWeight += weight;
            position = position.scale(spread);
            NoiseSample incoming = noise(position);

        //    System.out.println("iv:" + incoming.value);
            sum.value += incoming.value * weight;
            sum.gradient = sum.gradient.plus(incoming.gradient.scale(weight));
            weight *= decay;


        }
//        System.out.println(totalWeight);
        sum.value /= totalWeight;
        return sum;
    }



}
