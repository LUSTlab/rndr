package net.lustlab.rndr.application;

import net.lustlab.rndr.application.internal.GenericPlatformDriver;
import net.lustlab.rndr.application.internal.MacOSPlatformDriver;
import net.lustlab.rndr.application.internal.PlatformDriver;
import net.lustlab.rndr.application.internal.WindowsPlatformDriver;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.File;

public class Platform {

    static Logger logger = LogManager.getLogger(Platform.class);

    private static PlatformDriver driver = instantiateDriver();
    private static PlatformDriver instantiateDriver() {
        logger.debug("instantiating driver");
        String os = System.getProperty("os.name").toLowerCase();
        if (os.startsWith("windows")) {
            return new WindowsPlatformDriver();
        } else if (os.startsWith("mac")) {
            return new MacOSPlatformDriver();
        } else {
            return new GenericPlatformDriver();
        }
    }

    public static File tempDirectory() {
        return driver.temporaryDirectory();
    }

    public static File cacheDirectory(String programName) {
        return driver.cacheDirectory(programName);
    }

    public static File supportDirectory(String programName) {
        return driver.supportDirectory(programName);
    }


}
