# RNDR

Current version 0.2.10

## Why RNDR?
 * Clean and simple APIs for creative coding
 * Fast OpenGL 3.3 based rendering
 * Easy cross platform development for Windows and OSX (and Linux x64)

## Documentation
We are working on it! Check the [wiki](https://bitbucket.org/LUSTlab/rndr/wiki/) for a start

## Example code
Check the rndr-0.2.x-examples repository