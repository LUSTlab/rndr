package net.lustlab.rndr.draw.stroke;

import net.lustlab.rndr.draw.LineCap;
import net.lustlab.rndr.draw.LineJoin;
import net.lustlab.rndr.math.Vector2;

import java.nio.FloatBuffer;
import java.util.ArrayList;
import java.util.List;

import static net.lustlab.rndr.draw.stroke.Path.Point.*;

public class Path {
    boolean convex;

    public static Path fromLineStrip(List<Vector2> segments ) {
        Path sp = new Path();

        List<Point> path = new ArrayList();
        for (Vector2 v: segments) {
            Point p = new Point();
            p.x = v.x;
            p.y = v.y;
            p.flags = CORNER;
            path.add(p);
        }
        if (path.size() > 0) {
            path.get(0).flags = 0;
            path.get(path.size() - 1).flags = 0;
            sp.contours.add(path);
        }
        return sp;
    }

    public static Path fromLineStrips( List<List<Vector2>> contours ) {
        Path sp = new Path();

        for (List<Vector2> contour: contours) {
            List<Point> path = new ArrayList();
            for (Vector2 v : contour) {
                Point p = new Point();
                p.x = v.x;
                p.y = v.y;
                p.flags = CORNER;

                path.add(p);
            }
            path.get(0).flags = 0;
            path.get(path.size() - 1).flags = 0;
            sp.contours.add(path);
        }

        return sp;
    }

    /**
     * Internal point representation
     */
    public static class Point {
        public double x;
        public double y;
        double dx;
        double dy;
        double length;
        double dmx;
        double dmy;
        public int flags;

        final static int CORNER = 0x01;
        final static int LEFT = 0x02;
        final static int BEVEL = 0x04;
        final static int INNER_BEVEL = 0x08;

        @Override
        public String toString() {
            return "Point{" +
                    "x=" + x +
                    ", y=" + y +
                    ", flags=" + flags +
                    '}';
        }
    }


    public boolean closed;
    public List<List<Point>> contours = new ArrayList<>();
    int nbevel;

    static int curveDivs(double r, double arc, double tol) {
        double da = Math.acos(r / (r + tol)) * 2.0;
        return Math.max(2, (int)Math.ceil(arc / da));
    }


    /**
     *
     * @param points input points
     * @param w
     * @param lineJoin the type of join to use
     * @param miterLimit
     */
    public void calculateJoins(List<Point> points, double w, LineJoin lineJoin, double miterLimit) {

        nbevel = 0;

        double iw = 0.0;
        if (w > 0.0) {
            iw = 1.0 /w;
        }
        int nleft = 0;

        Point p0 = points.get(points.size()-1);
        Point p1 = points.get(0);
        int p1ptr = 0;
        for (int j = 0; j < points.size(); ++j) {
            double dlx0 = p0.dy;
            double dly0 = -p0.dx;
            double dlx1 = p1.dy;
            double dly1 = -p1.dx;

            p1.dmx = (dlx0 + dlx1) * 0.5;
            p1.dmy = (dly0 + dly1) * 0.5;
            double dmr2 = p1.dmx*p1.dmx + p1.dmy*p1.dmy;
            if (dmr2 > 0.000001f) {
                double scale = 1.0 / dmr2;
                if (scale > 600.0) {
                    scale = 600.0;
                }
                p1.dmx *= scale;
                p1.dmy *= scale;
            }
            p1.flags = (p1.flags & CORNER) != 0 ? CORNER : 0;
            double cross = p1.dx * p0.dy - p0.dx * p1.dy;
            if (cross > 0.0) {
                nleft+=1;
                p1.flags |= LEFT;
            }

            // Calculate if we should use bevel or miter for inner join.
            double limit = Math.max(1.01f, Math.min(p0.length, p1.length) * iw);
            if ((dmr2 * limit*limit) < 1.0f) {
                p1.flags |= INNER_BEVEL;
            }

            // Check to see if the corner needs to be beveled.
            if ((p1.flags & CORNER) != 0) {
                if ((dmr2 * miterLimit*miterLimit) < 1.0f || lineJoin == LineJoin.BEVEL || lineJoin == LineJoin.ROUND) {
                    p1.flags |= BEVEL;
                }
            }

            if ((p1.flags & (BEVEL | INNER_BEVEL)) != 0) {
                nbevel++;
            }

            p0 = p1;
            p1ptr++;
            if (p1ptr < points.size()) {
                p1 = points.get(p1ptr);
            }
        }
        convex = nleft == points.size();

    }


    public List<Expansion> expandFill(double w, LineJoin lineJoin, double miterLimit) {

        List<Expansion> result = new ArrayList<>();
        for (List<Point> points: contours) {
            prepare(points);
        }

        for (List<Point> points: contours) {
            calculateJoins(points, w, lineJoin, miterLimit);
        }

        double aa = w;
        // Calculate shape vertices.

        double woff = 0.5 * aa;
        double fringeWidth = 1.0;
        boolean generateFringe = w > 0.0;
        double offset = 0;

        if (generateFringe) {
            // Looping
            for (List<Point> points: contours) {

                int size = 4;
                for (Point point: points) {
                    if ((point.flags & Point.BEVEL) != 0) {
                        size+=12;
                    } else {
                        size+=4;
                    }
                }


//                System.out.println("buffer size: " + size);
                Expansion fill = new Expansion(Expansion.ExpansionType.Fill, FloatBuffer.allocate(size * 50));


                Point p0 = points.get(points.size() - 1);
                Point p1 = points.get(0);
                int p1ptr = 0;

                for (int j = 0; j < points.size(); ++j) {
                    if ((p1.flags & Point.BEVEL) != 0) {
                        if ((p1.flags & Point.LEFT) != 0) {
                            fill.addVertex(p1.x + p1.dmx * woff, p1.y + p1.dmy * woff, 0.5, 1, offset);
                        } else {
                            fill.addVertex(p1.x + p0.dy * woff, p1.y + -p0.dx * woff, 0.5, 1, offset);
                            fill.addVertex(p1.x + p1.dy * woff, p1.y + -p1.dx * woff, 0.5, 1, offset);
                        }
                    } else {
                        fill.addVertex(p1.x + (p1.dmx * woff), p1.y + (p1.dmy * woff), 0.5, 1, offset);
                    }
                    p0 = p1;
                    p1ptr++;
                    if (p1ptr < points.size()) {
                        p1 = points.get(p1ptr);
                    }
                }
                result.add(fill);
            }
        } else {
            //for (int j = 0; j < points.size(); ++j) {
             //   fill.addVertex(points.get(j).x, points.get(j).y, 0.5f,1, offset);
            //}
        }

        // Calculate fringe
        if (generateFringe) {
            for (List<Point> points : contours) {
                int size = 2;
                for (Point point: points) {
                    if ((point.flags & Point.BEVEL) != 0) {
                        size+=12;
                    } else {
                        size+=4;
                    }
                }
                Expansion fringe = new Expansion(Expansion.ExpansionType.Fringe, FloatBuffer.allocate(size * 50));

                double lw = w + woff;
                double rw = w - woff;
                double lu = 0;
                double ru = 1;

                // Create only half a fringe for convex shapes so that
                // the shape can be rendered without stenciling.
                if (convex) {
                    lw = woff;    // This should generate the same vertex as fill inset above.
                    lu = 0.5f;    // Set outline fade at middle.
                }

                // Looping
                Point p0 = points.get(points.size() - 1);
                Point p1 = points.get(0);

                int p1ptr = 0;
                for (int j = 0; j < points.size(); ++j) {
                    if ((p1.flags & (Point.BEVEL | Point.INNER_BEVEL)) != 0) {
                        fringe.bevelJoin(p0, p1, lw, rw, lu, ru, fringeWidth, offset);
                    } else {
                        fringe.addVertex(p1.x + (p1.dmx * lw), p1.y + (p1.dmy * lw), lu, 1, offset);
                        fringe.addVertex(p1.x - (p1.dmx * rw), p1.y - (p1.dmy * rw), ru, 1, offset);
                    }
                    p0 = p1;
                    p1ptr++;
                    if (p1ptr < points.size()) {
                        p1 = points.get(p1ptr);
                    }
                }
                // Loop it
                Vector2 v0 = fringe.vertex(0);
                Vector2 v1 = fringe.vertex(1);

                fringe.addVertex(v0.x, v0.y, lu, 1, offset);
                fringe.addVertex(v1.x, v1.y, ru, 1, offset);
                result.add(fringe);
            }

        }
        return result;

    }

    private void prepare(List<Point> points) {
        Point p0 = points.get(points.size() - 1);
        Point p1 = points.get(0);
        int p1ptr = 0;
        for (int i = 0; i < points.size(); i++) {
            // Calculate segment direction and length
            p0.dx = p1.x - p0.x;
            p0.dy = p1.y - p0.y;
            p0.length = Math.sqrt(p0.dx * p0.dx + p0.dy * p0.dy);
            p0.dx /= p0.length;
            p0.dy /= p0.length;

            p0 = p1;
            p1ptr += 1;
            if (p1ptr < points.size()) {
                p1 = points.get(p1ptr);
            }
        }
    }


    public Expansion expandStroke(double weight, LineCap lineCap, LineJoin lineJoin, double miterLimit) {

        List<Point> points = contours.get(0);

        double tessTol = 0.1;

        int capSteps = curveDivs(weight, Math.PI, tessTol);

        prepare(points);
        calculateJoins(points, weight, lineJoin, miterLimit);

        int cverts = 0;
        if (lineJoin == LineJoin.ROUND) {
            cverts += (points.size() + nbevel * (capSteps + 2) + 1) * 2; // plus one for loop
        } else {
            cverts += (points.size() + nbevel * 5 + 1) * 2; // plus one for loop
        }

        if (!closed) {
            // space for caps
            if (lineCap == LineCap.ROUND) {
                cverts += (capSteps * 2 + 2) * 2;
            } else {
                cverts += (3 + 3) * 2;
            }
        }

        Expansion expansion = new Expansion(Expansion.ExpansionType.Stroke, FloatBuffer.allocate(cverts * 5));

        double offset = 0.0;
        double aa = 1.0;

        Point p0;
        Point p1;
        int start;
        int end;
        int p1ptr;
        if (closed) {
            p0 = points.get(points.size()-1);
            p1 = points.get(0);
            p1ptr = 0;
            start = 0;
            end = points.size();
        } else {
            p0 = points.get(0);
            p1 = points.get(1);
            p1ptr = 1;
            start = 1;
            end = points.size()-1;
        }

        if (!closed) {
            // add cap
            double dx = p1.x - p0.x;
            double dy = p1.y - p0.y;
            double length = Math.sqrt(dx*dx + dy*dy);
            dx /= length;
            dy /= length;
            // Q: is this not p1.dx, p1.dy?

            if (lineCap == LineCap.BUTT) {
                expansion.buttCapStart(p0, dx, dy, weight, -aa * 0.5, aa, offset);
            } else if (lineCap == LineCap.BUTT || lineCap == LineCap.SQUARE) {
                expansion.buttCapStart(p0, dx, dy, weight, weight - aa, aa, offset);
            } else if (lineCap == LineCap.ROUND) {
                expansion.roundCapStart(p0, dx, dy, weight, capSteps, aa, offset);
            }
        }

        for (int j = start; j < end; ++j) {
            offset += p0.length;
            if ((p1.flags & (Point.BEVEL | Point.INNER_BEVEL)) != 0) {
                if (lineJoin == LineJoin.ROUND) {
                    expansion.roundJoin(p0, p1, weight, weight, 0, 1, capSteps, aa, offset);
                } else {
                    expansion.bevelJoin(p0, p1, weight, weight, 0, 1, aa, offset);
                }
            } else {
                expansion.addVertex(p1.x + (p1.dmx * weight), p1.y + (p1.dmy * weight), 0,1, offset);
                expansion.addVertex(p1.x - (p1.dmx * weight), p1.y - (p1.dmy * weight), 1,1, offset);
            }
            p0 = p1;
            p1ptr+=1;
            if (p1ptr < points.size()) {
                p1 = points.get(p1ptr);
            }
        }

        if (closed) {
            // Loop it
            Vector2 v0 = expansion.vertex(0);
            Vector2 v1 = expansion.vertex(1);
            expansion.addVertex(v0.x, v0.y, 0, 1, offset);
            expansion.addVertex(v1.x, v1.y, 1, 1, offset);
        } else {
            // Add cap
            double dx = p1.x - p0.x;
            double dy = p1.y - p0.y;
            double l = Math.sqrt(dx*dx + dy*dy);
            dx/=l;
            dy/=l;

            if (lineCap == LineCap.BUTT) {
                expansion.buttCapEnd(p1, dx, dy, weight, -aa * 0.5f, aa, offset);
            } else if (lineCap == LineCap.BUTT || lineCap == LineCap.SQUARE) {
                expansion.buttCapEnd(p1, dx, dy, weight, weight - aa, aa, offset);
            } else if (lineCap == LineCap.ROUND) {
                expansion.roundCapEnd(p1, dx, dy, weight, capSteps, aa, offset);
            }
        }

        return expansion;
    }

}
