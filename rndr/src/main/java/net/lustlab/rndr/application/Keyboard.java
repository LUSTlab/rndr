package net.lustlab.rndr.application;

import com.jogamp.newt.event.KeyEvent;
import net.lustlab.horizon.Event;

public class Keyboard {

    public static class KeyboardEvent {


        public enum EventType {
            PRESS,
            RELEASE
        }

        public EventType eventType;
        public char key;
        public int keyCode;
        public int modifiers;
        public boolean printable;

        public KeyboardEvent() {

        }
        public KeyboardEvent(KeyboardEvent other) {
            this.eventType = other.eventType;
            this.key = other.key;
            this.keyCode = other.keyCode;
            this.modifiers = other.modifiers;
            this.printable = other.printable;
        }



        public static KeyboardEvent press(char key, int keyCode, int modifiers, boolean printable) {
            KeyboardEvent ke = new KeyboardEvent();
            ke.eventType = EventType.PRESS;
            ke.key = key;
            ke.keyCode = keyCode;
            ke.modifiers = modifiers;
            ke.printable = printable;
            return ke;
        }

        public static KeyboardEvent release(char key, int keyCode, int modifiers, boolean printable) {
            KeyboardEvent ke = new KeyboardEvent();
            ke.eventType = EventType.RELEASE;
            ke.key = key;
            ke.keyCode = keyCode;
            ke.modifiers = modifiers;
            ke.printable = printable;
            return ke;
        }

    }

    public final Event<KeyboardEvent> pressed = new Event<KeyboardEvent>("keyboard-pressed").signature(KeyboardEvent.class);
    public final Event<KeyboardEvent> released = new Event<KeyboardEvent>("keyboard-released").signature(KeyboardEvent.class);

    public static Event.Filter<KeyboardEvent> key(char character) {
        return (e) -> e.key == character;
    }

    public static Event.Filter<KeyboardEvent> modifier(int modifier) {
        return (e -> (e.modifiers & modifier) != 0);
    }

    final public static short UP = KeyEvent.VK_UP;
    final public static short DOWN = KeyEvent.VK_DOWN;
    final public static short LEFT = KeyEvent.VK_LEFT;
    final public static short RIGHT = KeyEvent.VK_RIGHT;

    final public static int SHIFT_MODIFIER = KeyEvent.SHIFT_MASK;
    final public static int ALT_MODIFIER = KeyEvent.ALT_MASK;
    final public static int CTRL_MODIFIER = KeyEvent.CTRL_MASK;
    final public static int META_MODIFIER = KeyEvent.META_MASK;

}
