package net.lustlab.rndr.post.blend;

import net.lustlab.rndr.shaders.FragmentShader;

/**
 * Created by voorbeeld on 3/19/16.
 */
public class Multiply extends BlendFilter {
    public Multiply() {
        super(FragmentShader.fromUrl("cp:shaders/gl3/post/blend/multiply.frag"));
    }
}
