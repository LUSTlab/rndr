package net.lustlab.rndr.post;

import net.lustlab.rndr.shaders.FragmentShader;


public class AbsoluteDifference extends Filter {
    public AbsoluteDifference() {
        super(FragmentShader.fromUrl("cp:shaders/gl3/post/absolute-difference.frag"));
    }
}
