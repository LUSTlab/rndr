#version 330

layout(location=0) out vec4 o_output;


in vec2 v_texCoord0;

uniform sampler2D vertical;
uniform sampler2D diagonal;

uniform vec2 direction0;
uniform vec2 direction1;

const int NUM_SAMPLES = 40;
const float PI = 3.14159265322f;

float saturate(float x) {
    return clamp(x, 0.0, 1.0);
}


vec4 BlurTexture(sampler2D tex, vec2 uv, vec2 direction)
{
    vec4 finalColor = vec4(0.0);
    float blurAmount = 0.0;

    uv += direction * 0.5;

    for (int i = 0; i < NUM_SAMPLES; ++i)
    {
        vec4 color = texture(tex, uv + direction * i);
        color *= color.a;
        blurAmount += color.a;
        finalColor += color;
    }

    return (finalColor / blurAmount);
}


void main() {
    vec2 viewport = textureSize(vertical, 0);
    vec2 invViewDims = 1.0/viewport;

float coc = texture(vertical, v_texCoord0).a;
float coc2 = texture(diagonal, v_texCoord0).a;

// Sample the vertical blur (1st MRT) texture with this new blur direction
vec2 blurDir = coc/40.0 * invViewDims * vec2(cos(-PI/6), sin(-PI/6));
vec4 color = BlurTexture(vertical, v_texCoord0, blurDir) * coc;

// Sample the diagonal blur (2nd MRT) texture with this new blur direction
vec2 blurDir2 = coc2/40.0 * invViewDims * vec2(cos(-5*PI/6), sin(-5*PI/6));
vec4 color2 = BlurTexture(diagonal, v_texCoord0, blurDir2) * coc2;

 o_output = vec4((color.rgb + color2.rgb) * 0.5f, 1.0);

}