package net.lustlab.rndr.math.tests;

import net.lustlab.rndr.math.Matrix44;
import net.lustlab.rndr.math.Transforms;
import net.lustlab.rndr.math.Vector3;
import org.junit.Test;


public class TestTransforms {

    @Test
    public void testLookAt() {
        {
            Matrix44 m = Transforms.lookAt(new Vector3(0, 0, 10), new Vector3(0, 0, 0), new Vector3(0, 1, 0));
            Transforms.normal(m);
        }
        {
            Matrix44 m = Transforms.lookAt(new Vector3(0, 10, 0), new Vector3(0, 0, 0), new Vector3(0, 1, 0));
            Transforms.normal(m);
        }
        {
            Matrix44 m = Transforms.lookAt(new Vector3(10,0, 0), new Vector3(0, 0, 0), new Vector3(0, 1, 0));
            Transforms.normal(m);
        }

    }
}
