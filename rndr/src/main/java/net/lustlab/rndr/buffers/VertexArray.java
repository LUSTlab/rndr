package net.lustlab.rndr.buffers;

import com.jogamp.opengl.GL3;
import net.lustlab.rndr.gl.GLError;
import net.lustlab.rndr.shaders.Shader;


/**
 * Created by voorbeeld on 4/22/14.
 */
public class VertexArray {

    static int defaultVAO = -1;
    public static VertexArray defaultInstance;

    public static VertexArray create(GL3 gl3, VertexBuffer vbo, VertexFormat layout, Shader shader) {


        if (defaultVAO == -1) {
            int vaos[] = new int[1];
            gl3.glGenVertexArrays(1, vaos, 0);
            defaultVAO = vaos[0];
            defaultInstance = new VertexArray(gl3, defaultVAO);
        }

        int vaos[] = new int[1];

        gl3.glGenVertexArrays(1, vaos, 0);
        gl3.glBindVertexArray(vaos[0]);
        vbo.bind();
        VertexBufferDrawer.setupLayoutGL3(gl3, layout, shader);
        gl3.glBindVertexArray(defaultVAO);
        vbo.unbind();

        GLError.check(gl3);
        return new VertexArray(gl3, vaos[0]);
    };

    final int vao;

    GL3 gl3;

    VertexArray(GL3 gl3, int vao) {
        this.vao = vao;
        this.gl3 = gl3;
    }

    public void bind() {
        gl3.glBindVertexArray(vao);
        GLError.debug(gl3);
    }

    public void unbind() {
        gl3.glBindVertexArray(defaultVAO);
        GLError.debug(gl3);
    }

}
