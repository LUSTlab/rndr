package net.lustlab.rndr.post;

import com.jogamp.opengl.GL3;
import com.jogamp.opengl.GLContext;
import net.lustlab.colorlib.ColorRGBa;
import net.lustlab.rndr.buffers.ColorBuffer;
import net.lustlab.rndr.buffers.RenderTarget;
import net.lustlab.rndr.gl.GLError;
import net.lustlab.rndr.math.*;
import net.lustlab.rndr.shaders.FragmentShader;
import net.lustlab.rndr.shaders.Shader;
import net.lustlab.rndr.shaders.ShaderWatcher;
import net.lustlab.rndr.shaders.VertexShader;
import net.lustlab.rndr.buffers.BufferWriter;
import net.lustlab.rndr.buffers.VertexBuffer;
import net.lustlab.rndr.buffers.VertexBufferDrawer;
import net.lustlab.rndr.buffers.VertexFormat;

import java.nio.ByteBuffer;
import java.nio.LongBuffer;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

public class Filter {

    static class FilterPerformance {
        long totalTime;
        long count;
        long last;
    }

    static Map<Filter, FilterPerformance> filterPerformance = new HashMap<>();
    RenderTarget renderTarget;
    Shader shader;
    static VertexBuffer quad;
    VertexFormat layout = new VertexFormat().textureCoordinate(2).position(2);
    GL3 gl3;
    ShaderWatcher shaderWatcher = null;


    Map<String, Object> parameters = new HashMap<>();
    Map<String, String> parameterMap = new HashMap<>();

    /**
     * Adds a mapping from internal uniform names to external uniform names.
     * For example internally tex0, tex1, ... tex15 is used, however these names are not very nice to work with in the shader
     * @param input the input name ("tex0", "tex1" .. )
     * @param output the output name
     */
    public Filter map(String input, String output) {
        parameterMap.put(input, output);
        return this;
    }

    public Filter(FragmentShader fragmentShader) {
        gl3 = GLContext.getCurrentGL().getGL3();
        createQuad();
        this.shader = Shader.createShader(gl3, VertexShader.fromStream(gl3, getClass().getResourceAsStream("/shaders/gl3/filter.vert")),
                fragmentShader);

    }

    public Filter(ShaderWatcher watcher) {
        shaderWatcher = watcher;
        gl3 = GLContext.getCurrentGL().getGL3();
        createQuad();
    }

    private void createQuad() {
        if (quad == null) {
            ByteBuffer bb = ByteBuffer.allocateDirect(layout.size() * 6);
            BufferWriter bw = new BufferWriter(bb);

            bw.write(new Vector2(0,1)).write(new Vector2(0,0))
                    .write(new Vector2(0, 0)).write(new Vector2(0,1))
                    .write(new Vector2(1, 0)).write(new Vector2(1,1));

            bw.write(new Vector2(0,1)).write(new Vector2(0,0))
                    .write(new Vector2(1,1)).write(new Vector2(1,0))
                    .write(new Vector2(1,0)).write(new Vector2(1,1));

            bb.rewind();
            quad = VertexBuffer.createStatic(layout, 6, bb);
        }
    }

    public Filter parameter(String parameter, float value) {
        parameters.put(parameter, value);
        return this;
    }

    public Filter parameter(String parameter, int value) {
        parameters.put(parameter, value);
        return this;
    }

    public Filter parameter(String parameter, Vector2 value) {
        parameters.put(parameter, value);
        return this;
    }
    public Filter parameter(String parameter, Vector3 value) {
        parameters.put(parameter, value);
        return this;
    }

    public Filter parameter(String parameter, Vector4 value) {
        parameters.put(parameter, value);
        return this;
    }

    public Filter parameter(String parameter, ColorRGBa value) {
        parameters.put(parameter, value);
        return this;
    }
    public Filter parameter(String parameter, float[] value) {
        parameters.put(parameter, value);
        return this;
    }
    public Filter parameter(String parameter, Vector4[] value) {
        parameters.put(parameter, value);
        return this;
    }
    public Filter parameter(String parameter, ColorRGBa[] value) {
        parameters.put(parameter, value);
        return this;
    }

    public Filter parameter(String parameter, Matrix44 value) {
        parameters.put(parameter, value);
        return this;
    }

    public Filter parameter(String parameter, Matrix44[] value) {
        parameters.put(parameter, value);
        return this;
    }

    public final Filter apply(ColorBuffer src, ColorBuffer dest) {
        return apply(new ColorBuffer[] {src}, new ColorBuffer[] {dest});
    }

    public final Filter apply(ColorBuffer[] src, ColorBuffer dest) {
        return apply(src, new ColorBuffer[] {dest});
    }

    public final Filter apply(ColorBuffer src, ColorBuffer dest[]) {
        return apply(new ColorBuffer[] {src}, dest);
    }

    public Filter apply(ColorBuffer src[], ColorBuffer[] dest) {

        if (shaderWatcher!=null) {

            Optional<Shader> shaderOptional = shaderWatcher.shader();

            if (shaderOptional.isPresent()) {
                shader = shaderWatcher.shader().get();
            } else {
                shader = null;
            }
        }

        if (shader != null) {
            GLError.debug(gl3);
            if (renderTarget == null) {
                renderTarget = RenderTarget.create(dest[0].width(), dest[0].height());
            }
            long start = System.currentTimeMillis();

            GLError.debug(gl3);
            if (renderTarget == null) {
                renderTarget = RenderTarget.create(dest[0].width(), dest[0].height());
            }

            if (dest[0].width() != renderTarget.width() || dest[0].height() != renderTarget.height()) {
                renderTarget.destroy();
                renderTarget = RenderTarget.create(dest[0].width(), dest[0].height());
            }

            GLError.debug(gl3);
            for (ColorBuffer d : dest) {
                renderTarget.attach(d);
            }
            GLError.debug(gl3);
            {
                int idx = 0;
                if (src != null) {
                    for (ColorBuffer s : src) {
                        gl3.glActiveTexture(GL3.GL_TEXTURE0 + idx);
                        s.bindTexture(gl3);
                        idx++;
                    }
                }
            }
            GLError.debug(gl3);

//        gl3.glClearColor(0,1,0,1);
//        gl3.glClear(GL3.GL_COLOR_BUFFER_BIT);
            gl3.glDisable(GL3.GL_BLEND);
            gl3.glDisable(GL3.GL_DEPTH_TEST);
            gl3.glDisable(GL3.GL_CULL_FACE);

            renderTarget.bindTarget();


            shader.begin();
            GLError.debug(gl3);
            for (Map.Entry<String, Object> e : parameters.entrySet()) {
                if (e.getValue() instanceof Vector2) {
                    shader.setUniform(e.getKey(), (Vector2) e.getValue());
                } else if (e.getValue() instanceof Vector3) {
                    shader.setUniform(e.getKey(), (Vector3) e.getValue());
                } else if (e.getValue() instanceof Vector4) {
                    shader.setUniform(e.getKey(), (Vector4) e.getValue());
                } else if (e.getValue() instanceof ColorRGBa) {
                    shader.setUniform(e.getKey(), (ColorRGBa) e.getValue());
                } else if (e.getValue() instanceof Float) {
                    shader.setUniform1f(e.getKey(), (Float) e.getValue());
                } else if (e.getValue() instanceof Integer) {
                    shader.setUniform1i(e.getKey(), (Integer) e.getValue());
                } else if (e.getValue() instanceof float[]) {
                    shader.setUniform(e.getKey(), (float[]) e.getValue());
                } else if (e.getValue() instanceof Vector4[]) {
                    shader.setUniform(e.getKey(), (Vector4[]) e.getValue());
                } else if (e.getValue() instanceof ColorRGBa[]) {
                    shader.setUniform(e.getKey(), (ColorRGBa[]) e.getValue());
                } else if (e.getValue() instanceof Matrix44) {
                    shader.setUniform(e.getKey(), (Matrix44) e.getValue());
                } else if (e.getValue() instanceof Matrix44[]) {
                    shader.setUniform(e.getKey(), (Matrix44[]) e.getValue());
                }
            }

            if (src != null) {
                for (int i = 0; i < src.length; ++i) {
                    String uniform = "tex" + i;
                    String mapped = parameterMap.get(uniform);
                    uniform = mapped != null ? mapped : uniform;
                    shader.setUniform1i(uniform, i);
                }
            }

            GLError.debug(gl3);
            shader.setUniform("projectionMatrix", Transforms.ortho(0, dest[0].width(), dest[0].height(), 0, -1, 1));
            shader.setUniform("destSize", new Vector2(dest[0].width(), dest[0].height()));

            shader.setUniform("dst_size", new Vector2(dest[0].width(), dest[0].height()));

            if (src != null && src.length > 0) {
                shader.setUniform("inv_size", new Vector2(1.0 / src[0].width(), 1.0 / src[0].height()));
            }


            GLError.debug(gl3);
            quad.bind();
            int queries[] = new int[2];

//        gl3.glGenQueries(2, queries, 0);
//        gl3.glBeginQuery(GL3.GL_TIME_ELAPSED, queries[0]);
            //gl3.glQueryCounter(queries[0],GL3.GL_TIMESTAMP);
            VertexBufferDrawer.draw(gl3, shader, quad, layout, GL3.GL_TRIANGLES, 6);
//        gl3.glFinish();
//        gl3.glFlush();
//        gl3.glEndQuery(GL3.GL_TIME_ELAPSED);
            //gl3.glEndQuery(GL3.GL_TIME_ELAPSED);
            long results[] = new long[2];

            LongBuffer lstart = LongBuffer.allocate(1);

            lstart.rewind();
            int done[] = new int[1];
//        while (done[0] < 1) {
//            gl3.glGetQueryObjectiv(queries[0], GL3.GL_QUERY_RESULT_AVAILABLE, done, 0);
//        }
//        gl3.glGetQueryObjectui64v(queries[0], GL3.GL_QUERY_RESULT, lstart);
//        lstart.rewind();
//        start = lstart.get();


            GLError.debug(gl3);
            quad.unbind();

            gl3.glDeleteQueries(2, queries, 0);
            shader.end();
            GLError.debug(gl3);
            renderTarget.unbindTarget();
            GLError.debug(gl3);
            renderTarget.detachColorBuffers();
            GLError.debug(gl3);
            {
                if (src != null) {
                    for (int i = 0; i < src.length; ++i) {
                        gl3.glActiveTexture(GL3.GL_TEXTURE0 + i);
                        gl3.glBindTexture(GL3.GL_TEXTURE_2D, 0);
                        i++;
                    }
                }
            }

            gl3.glActiveTexture(GL3.GL_TEXTURE0);
            GLError.debug(gl3);

//        System.out.println(start);
            FilterPerformance perf = filterPerformance.computeIfAbsent(this, k -> new FilterPerformance());
            perf.totalTime += (start / 1000000.0);
            perf.count += 1;
            perf.last = (long) (start / 1000000.0);
            filterPerformance.put(this, perf);

        }
        return this;
    }

    public void destroy() {
        renderTarget.destroy();
    }

    public static void printPerformance() {
        System.out.println("--- filter performance: ");
        for (Map.Entry<Filter, FilterPerformance> e: filterPerformance.entrySet()) {
            System.out.println(e.getKey().getClass() + " / " + e.getKey());
            System.out.println(" average: " + e.getValue().totalTime / (e.getValue().count*1.0));
            System.out.println(" total: " + e.getValue().totalTime);
            System.out.println(" last: " + e.getValue().last);
            System.out.println(" count: " + e.getValue().count);
        }
    }

}






