#version 330

in vec2 texCoord0;
in vec2 position;

uniform vec2 destSize;
uniform mat4 projectionMatrix;

out vec2 v_texCoord0;

void main() {

    v_texCoord0 = texCoord0;
    vec2 transformed = position * destSize;
    gl_Position = projectionMatrix * vec4(transformed, 0.0, 1.0);
}