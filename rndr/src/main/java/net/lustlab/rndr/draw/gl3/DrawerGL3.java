package net.lustlab.rndr.draw.gl3;

import com.jogamp.opengl.GL;
import com.jogamp.opengl.GL3;

import net.lustlab.rndr.buffers.BufferTexture;
import net.lustlab.rndr.buffers.VertexBuffer;
import net.lustlab.rndr.draw.*;
import net.lustlab.rndr.buffers.ColorBuffer;
import net.lustlab.rndr.draw.stroke.ExpansionRenderer;
import net.lustlab.rndr.draw.stroke.Expansion;
import net.lustlab.rndr.draw.stroke.Path;
import net.lustlab.rndr.gl.GLError;
import net.lustlab.rndr.math.*;
import net.lustlab.rndr.shape.Circle;
import net.lustlab.rndr.shape.Polygon;
import net.lustlab.rndr.shape.Rectangle;
import net.lustlab.rndr.shape.Shape;

import java.util.*;

public class DrawerGL3 extends Drawer {

    private ImageRenderer imageRenderer;
    private SolidsRenderer solidsRenderer;
    private PolygonRenderer polygonRenderer;
    private CircleRenderer circleRenderer;
    private RectangleRenderer rectangleRenderer;
    private VertexBufferRenderer vertexBufferRenderer;

    private ExpansionRenderer expansionRenderer;

    GL3 gl3;
    public DrawerGL3(GL3 gl3, int width, int height) {
        this.gl3 = gl3;
        gl3.glViewport(0, 0, width, height);

        this.width = width;
        this.height = height;

        solidsRenderer = new SolidsRenderer(gl3);
        polygonRenderer = new PolygonRenderer(gl3);

        //lineRenderer = new LineRenderer();
        imageRenderer = new ImageRenderer();
        circleRenderer = new CircleRenderer();
        rectangleRenderer = new RectangleRenderer();
        vertexBufferRenderer = new VertexBufferRenderer();
        expansionRenderer = new ExpansionRenderer();
    }

    private DrawerGL3(DrawerGL3 copyFrom) {
        width = copyFrom.width;
        height = copyFrom.height;
        gl3 = copyFrom.gl3;
        solidsRenderer = copyFrom.solidsRenderer;
        polygonRenderer = copyFrom.polygonRenderer;
        //lineRenderer = copyFrom.lineRenderer;
        circleRenderer = copyFrom.circleRenderer;
        rectangleRenderer = copyFrom.rectangleRenderer;
        imageRenderer = copyFrom.imageRenderer;
        vertexBufferRenderer = copyFrom.vertexBufferRenderer;
        expansionRenderer = copyFrom.expansionRenderer;

        projectionStack.addAll(copyFrom.projectionStack);
        modelViewStack.addAll(copyFrom.modelViewStack);
        styleStack.addAll(copyFrom.styleStack);
        style = copyFrom.style.copy();
        modelViewMatrix = copyFrom.modelViewMatrix;
        projectionMatrix = copyFrom.projectionMatrix;
    }

    public DrawerGL3 line(Vector2 p0, Vector2 p1) {
        List<Vector2> points = new ArrayList<>();
        points.add(p0);
        points.add(p1);
        lineStrip(points);

        return this;
    }

    @Override
    public DrawerGL3 lineStrip(List<Vector2> points) {

        if (points.size() >= 2) {
            Path sp = Path.fromLineStrip(points);
            Expansion se = sp.expandStroke(
                    drawStyle().strokeWeight,
                    drawStyle().lineCap,
                    drawStyle().lineJoin, 0);
            expansionRenderer.renderStroke(drawContext(), drawStyle(), se);
        }

        return this;
    }

    @Override
    public DrawerGL3 lineLoop(List<Vector2> points) {
        Path sp = Path.fromLineStrip(points);
        sp.closed = true;

        Expansion se = sp.expandStroke(
                drawStyle().strokeWeight,
                drawStyle().lineCap,
                drawStyle().lineJoin, 0);
        expansionRenderer.renderStroke(drawContext(), drawStyle(), se);
        return this;
    }

    @Override
    public DrawerGL3 rectangle(Vector2 position, double width, double height) {
        rectangleRenderer.drawRectangle(drawContext(), drawStyle(), position.xy0(), width, height);
        return this;
    }

    @Override
    public DrawerGL3 rectangles(List<Rectangle> rectangles) {
        rectangleRenderer.drawRectangles(drawContext(), drawStyle(), rectangles);
        return this;
    }

    @Override
    public DrawerGL3 rectangles(List<Vector2> positions, List<Vector2> dimensions) {
        rectangleRenderer.drawRectangles(drawContext(), drawStyle(), positions, dimensions);
        return this;
    }


    @Override
    public DrawerGL3 rectangles(BufferTexture positionsAndDimensions, int count) {
        rectangleRenderer.drawRectangles(drawContext(), drawStyle(), positionsAndDimensions, count);
        return this;
    }


    public DrawerGL3 circle(Vector3 position, double radius) {
        circleRenderer.drawCircle(drawContext(), style, position, radius);
        return this;
    }

    public DrawerGL3 circles(List<Vector2> positions, List<Double> radii) {
        if (positions.size() != radii.size()) {
            throw new IllegalArgumentException("positions has " + positions.size() + " elements, while radii has " + radii.size() + "elements");
        }
        circleRenderer.drawCircles(drawContext(), style, positions, radii);
        return this;
    }

    public DrawerGL3 circles(List<Vector2> positions, double radius) {

        List<Double> radii = new ArrayList<>();
        for (int i = 0; i < positions.size(); ++i) {
            radii.add(radius);
        }

        circleRenderer.drawCircles(drawContext(), style, positions, radii);
        return this;
    }

    public DrawerGL3 circles(BufferTexture positionsAndRadii, int count) {
        circleRenderer.drawCircles(drawContext(), style, positionsAndRadii, 0, 0, count);
        return this;
    }

    public DrawerGL3 circles(BufferTexture positions, double radius, int count) {
        circleRenderer.drawCircles(drawContext(), style, positions, radius, 1, count);
        return this;
    }


    public DrawerGL3 circles(List<Circle> circles) {
        List<Vector2> centers = new ArrayList<>();
        List<Double> radii = new ArrayList<>();

        for (Circle circle:circles) {
            centers.add(circle.center);
            radii.add(circle.radius);
        }

        return circles(centers, radii);
    }

    public DrawerGL3 tesselation(Tessellation tessellation) {
        GLError.debug(gl3);
        TessellationRendererGL3.drawTessellationSmooth(gl3, style.shadeStyle, projectionMatrix, modelViewMatrix,
                tessellation);
        GLError.debug(gl3);
        return this;

    }

    public DrawerGL3 box(double x, double y, double z, double width, double height, double depth) {
        StateTools.setupState(drawStyle());
        solidsRenderer.solid(gl3, projection(), modelView(), style.fill, "box", new Vector3(x, y, z), new Vector3(width, height, depth), style.shadeStyle);
        return this;
    }

    public DrawerGL3 boxes(List<Vector3> positions, List<Vector3> dimensions) {
        StateTools.setupState(drawStyle());

        solidsRenderer.solids(gl3, projection(), modelView(), style.fill, "box", positions, dimensions, style.shadeStyle);
        return this;
    }

    public DrawerGL3 image(ColorBuffer colorBuffer, double sourceX, double sourceY, double sourceWidth, double sourceHeight,
                          double destX, double destY, double destWidth, double destHeight) {

        imageRenderer.drawImage(drawContext(), style, colorBuffer, sourceX, sourceY, sourceWidth, sourceHeight, destX, destY, destWidth, destHeight);
        return this;
    }

    public DrawContext drawContext() {
        return new DrawContext(width, height, projectionMatrix, modelViewMatrix);
    }

    public DrawerGL3 polygon(Polygon polygon) {
        polygonRenderer.drawPolygon(drawContext(), style, polygon);
        return this;
    }

    public DrawerGL3 polygons(List<Polygon> polygons) {
        polygons.forEach(this::polygon);
        return this;
    }

    public DrawerGL3 shape(Shape shape) {
        polygonRenderer.drawShape(drawContext(), style, shape);
        return this;
    }

    public DrawerGL3 shapes(List<Shape> shapes) {
        shapes.forEach(this::shape);
        return this;
    }

    public DrawerGL3 background(double r, double g, double b, double a) {
        gl3.glDepthMask(true);
        gl3.glClearColor((float)r, (float)g, (float)b, (float) a);
        gl3.glClear(GL.GL_COLOR_BUFFER_BIT |GL.GL_DEPTH_BUFFER_BIT | GL.GL_STENCIL_BUFFER_BIT);
        return this;
    }

    public void destroy() {
        imageRenderer.destroy();
        circleRenderer.destroy();
        //lineRenderer.destroy();
        rectangleRenderer.destroy();
        polygonRenderer.destroy();
        vertexBufferRenderer.destroy();
    }

    @Override
    public DrawerGL3 copy() {
        return new DrawerGL3(this);
    }

    @Override
    public Drawer vertexBuffer(VertexBuffer vertexBuffer, Primitive primitive, long vertexCount) {
        vertexBufferRenderer.drawVertexBuffer(drawStyle(), drawContext(), vertexBuffer, primitive, vertexCount);
        return this;
    }

    @Override
    public Drawer vertexBuffers(VertexBuffer vertexBuffer, Primitive primitive, long vertexCount, long instanceCount) {
        vertexBufferRenderer.drawVertexBufferInstances(drawStyle(), drawContext(), vertexBuffer, primitive, vertexCount, instanceCount);
        return this;
    }
}
