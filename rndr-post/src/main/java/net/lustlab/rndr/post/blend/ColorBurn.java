package net.lustlab.rndr.post.blend;

import net.lustlab.rndr.shaders.FragmentShader;

public class ColorBurn extends BlendFilter {
    public ColorBurn() {
        super(FragmentShader.fromUrl("cp:shaders/gl3/post/blend/color-burn.frag"));
    }
}
