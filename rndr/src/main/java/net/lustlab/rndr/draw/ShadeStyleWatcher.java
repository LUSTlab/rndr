package net.lustlab.rndr.draw;

import net.lustlab.rndr.file.FileWatcher;
import org.yaml.snakeyaml.Yaml;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.net.MalformedURLException;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

public class ShadeStyleWatcher {

    static Map<File, ShadeStyleWatcher> watchers = new HashMap<>();
    FileWatcher fileWatcher;
    ShadeStyle shadeStyle;
    private ShadeStyleWatcher(File file) {
        fileWatcher = new FileWatcher(file);
        if (file.exists()) {
            try {
                shadeStyle = ShadeStyle.fromUrl(file.toURI().toURL().toExternalForm());
            } catch (MalformedURLException e) {
                e.printStackTrace();
            }
        }
        fileWatcher.fileModified.listen(this::loadStyle);
    }

    private void loadStyle(FileWatcher.FileModifiedEvent fileModifiedEvent) {


        try (FileReader reader = new FileReader(fileModifiedEvent.file)) {
            shadeStyle = ShadeStyle.fromUrl(fileModifiedEvent.file.toURI().toURL().toExternalForm());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static ShadeStyleWatcher forFile(File file) {
        return watchers.computeIfAbsent(file, k -> new ShadeStyleWatcher(file));
    }

    public Optional<ShadeStyle> shadeStyle() {
        fileWatcher.update();
        return Optional.ofNullable(shadeStyle);
    }
}
