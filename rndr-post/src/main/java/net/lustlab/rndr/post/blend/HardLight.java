package net.lustlab.rndr.post.blend;

import net.lustlab.rndr.shaders.FragmentShader;

public class HardLight extends BlendFilter {
    public HardLight() {
        super(FragmentShader.fromUrl("cp:shaders/gl3/post/blend/hard-light.frag"));
    }
}
