package net.lustlab.rndr.post.linear;

import net.lustlab.rndr.post.Filter;
import net.lustlab.rndr.shaders.FragmentShader;

/**
 * Created by voorbeeld on 8/23/17.
 */
public class Tonemap extends Filter {
    public Tonemap() {
        super(FragmentShader.fromUrl("cp:shaders/gl3/post/linear/tonemap.frag"));
    }
}
