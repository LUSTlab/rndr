package net.lustlab.rndr.color;

import net.lustlab.rndr.math.Matrix44;
import net.lustlab.rndr.math.Vector4;

public class ColorTransforms {
    public static Matrix44 NEUTRAL_GRAYSCALE = Matrix44.fromColumnVectors(new Vector4(0.33, 0.33,0.33,0), new Vector4(0.33,0.33,0.33,0), new Vector4(0.33,0.33,0.33,0), new Vector4(0,0,0,1));

    public static Matrix44 BT709_GRAYSCALE = Matrix44.fromColumnVectors(new Vector4(0.2126, 0.7152,0.0722,0), new Vector4(0.2126, 0.7152,0.0722,0), new Vector4(0.2126, 0.7152,0.0722,0), new Vector4(0,0,0,1));
    public static Matrix44 BT601_GRAYSCALE = Matrix44.fromColumnVectors(new Vector4(0.2999, 0.587, 0.114,0), new Vector4(0.2999, 0.587, 0.114,0), new Vector4(0.2999, 0.587, 0.114,0),  Vector4.UnitW);


    public static Matrix44 RED = Matrix44.fromColumnVectors(new Vector4(1, 0, 0,0), Vector4.Zero, Vector4.Zero, new Vector4(0,0,0,1));
    public static Matrix44 GREEN = Matrix44.fromColumnVectors(Vector4.Zero, new Vector4(0, 1, 0, 0), Vector4.Zero, new Vector4(0,0,0,1));
    public static Matrix44 BLUE = Matrix44.fromColumnVectors(Vector4.Zero, Vector4.Zero, new Vector4(0,0,1,0), new Vector4(0,0,0,1));


    public static Matrix44 NEUTRAL = Matrix44.IDENTITY;

    public static Matrix44 INVERT = new Matrix44(
            -1,0,0,1,
            0,-1,0,1,
            0,0,-1,1,
            0,0,0,1);
}
