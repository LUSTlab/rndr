#version 330

layout(location=0) out vec4 o_vertical;
layout(location=1) out vec4 o_diagonal;

uniform vec2 vertical;
uniform vec2 diagonal;

in vec2 v_texCoord0;

uniform sampler2D image;

const int NUM_SAMPLES = 40;
const float PI = 3.14159265322f;


float saturate(float x) {
    return clamp(x, 0.0, 1.0);
}

void main() {
    vec2 viewport = textureSize(image, 0);

	vec4 curr = texture(image, v_texCoord0);
	vec4 sum = vec4(0.0);

	vec2 s = vec2(sin(3.14159265 / 3), -cos(3.14159265 / 3)) / viewport;
	vec2 p2 = v_texCoord0 + s * 0.5;
	for (int i = 1; i < NUM_SAMPLES; ++i) {
		vec4 tmp = texture(image, p2);
		float alpha = 1.0;//saturate(tmp.a - i);
		alpha *= saturate(curr.a - i);
		sum += tmp * alpha;
		p2 += s;
	}
	if (sum.a < 1)
		o_diagonal = curr;
	else
		o_diagonal = vec4(sum.rgb / sum.a, 1.0) * curr.a;

	s.x = -s.x;
	p2 = v_texCoord0 + s * 0.5;
	for (int i = 1; i < NUM_SAMPLES; ++i) {
		vec4 tmp = texture(image, p2);
		float alpha = 1.0;//saturate(tmp.a - i);
		alpha *= saturate(curr.a - i);
		sum += tmp * alpha;
		p2 += s;
	}

	if (sum.a < 1)
		o_vertical = curr;
	else
		o_vertical = vec4(sum.rgb / sum.a, 1.0) * curr.a;


}