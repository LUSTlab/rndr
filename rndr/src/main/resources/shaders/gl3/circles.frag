#version 330

layout(origin_upper_left) in vec4 gl_FragCoord;
${uniforms! }

in vec2 v_texCoord0;
uniform vec4 fill;
uniform vec4 stroke;

uniform vec2 dimensions;
uniform float strokeWeight;

in float v_radius;
flat in  int v_instance;
out vec4 o_color;


in vec3 v_boundsPosition;

void main(void) {

     // -- shade style support variables
     vec2 screenPosition = gl_FragCoord.xy;
     int instance = v_instance;
     vec3 boundsPosition = v_boundsPosition;


     float radius = v_radius;
     vec4 x_fill = fill;
     vec4 x_stroke = stroke;
     { ${fillTransform! } }
     { ${strokeTransform! } }

     float wd = fwidth(length(v_texCoord0 - vec2(0.5)));
     float d = length(v_texCoord0 - vec2(0.5)) * 2;

     float f = smoothstep(0, wd * 2.5, 1.0 - d);
     float b = (strokeWeight) / radius;
     float f2 = smoothstep(0, wd * 2.5, 1.0 - b - d) * f;

     o_color = vec4(x_fill.rgb*f2 + x_stroke.rgb*(1.0-f2), (x_fill.a*f) * f2 + (x_stroke.a*f) * (1.0-f2) );
}