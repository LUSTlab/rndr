package net.lustlab.rndr.shaders;

import com.jogamp.opengl.GL;
import com.jogamp.opengl.GL3;
import com.jogamp.opengl.GLContext;
import net.lustlab.rndr.RNDRException;
import org.apache.commons.io.IOUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.*;
import java.net.MalformedURLException;
import java.net.URL;

public class FragmentShader {

    /**
     * The OpenGL shader object that identifies the fragment program
     */
    final public int shaderObject;
    final String name;

    @Override
    public String toString() {
        return "FragmentShader{" +
                "shaderObject=" + shaderObject +
                ", name='" + name + '\'' +
                '}';
    }

    static Logger logger = LogManager.getLogger(FragmentShader.class);


    public FragmentShader(int shaderObject, String name) {
        this.shaderObject = shaderObject;
        this.name = name;
    }

    public String name() {
        return name;
    }

    public static FragmentShader fromString(String code, String name) {
        logger.debug("creating fragment shader: {}", name);

        GL gl = GLContext.getCurrentGL();

        if (name == null) {
            name = "<unknown-fragment-shader>";
        }
        if (gl.isGL3()) {
            GL3 gl3 = gl.getGL3();
            int shaderObject = gl3.glCreateShader(GL3.GL_FRAGMENT_SHADER);
            gl3.glShaderSource(shaderObject, 1, new String[]{code}, null,0);

            gl3.glCompileShader(shaderObject);

            int values[] = new int[1];
            gl3.glGetShaderiv(shaderObject, GL3.GL_COMPILE_STATUS, values, 0);
            if (values[0] != GL.GL_TRUE) {
                ShaderInfo.checkShaderInfoLog(gl, shaderObject,code, name);
            }
            return new FragmentShader(shaderObject, name);
        }
        else {
            throw new RNDRException("unsupported GL implementation");
        }
    }


    public static FragmentShader fromUrl(String url)  {
        try {
            return fromUrl(new URL(url));
        } catch (MalformedURLException e) {
            throw new ShaderException("failed to load shader from URL: " + url, e);
        }
    }

    public static FragmentShader fromUrl(URL url)  {
        try {
            return load(url.openStream(), url.toString());
        } catch (MalformedURLException e) {
            throw new ShaderException("failed to load shader from URL: " + url, e);
        } catch (IOException e) {
            throw new ShaderException("failed to load shader form URL: " + url, e);
        }
    }

    public static FragmentShader loadFromFile(GL gl, String filename)  {
        return loadFromFile(gl, new File(filename));
    }

    public static FragmentShader loadFromFile(GL gl, File file)  {
        FileInputStream stream;
        try {
            stream = new FileInputStream(file);
            return load(gl, stream);

        } catch (FileNotFoundException e) {
            throw new ShaderException("Shader file not found: " + file.getName());
        }
    }

    public static FragmentShader load(GL gl, InputStream stream) {
        return load(gl, stream, null);
    }
    public static FragmentShader load(GL gl, InputStream stream, String name)  {

        StringWriter writer = new StringWriter();
        try {
            IOUtils.copy(stream, writer, "utf-8");
            String code = writer.toString();
            return fromString(code, name);
        }
        catch (IOException e) {
            throw new ShaderException("Error reading from shader stream: " + e.getMessage());
        }
        finally {
            try {
                stream.close();
            } catch (IOException e) {
                throw new RNDRException(e);
            }
        }

    }

    public static FragmentShader load(InputStream stream, String name)  {
        return load(GLContext.getCurrentGL(), stream, name);

    }

    public void destroy() {
        GLContext.getCurrentGL().getGL2GL3().glDeleteShader(shaderObject);
    }

}
