package net.lustlab.rndr.draw;

/**
 * Created by voorbeeld on 11/5/17.
 */
public class ChannelMask {

    public final static int RED = 1;
    public final static int GREEN = 2;
    public final static int BLUE = 4;
    public final static int ALPHA = 8;

    final static int NONE = 0;

    final static int ALL = RED | GREEN | BLUE | ALPHA;


}
