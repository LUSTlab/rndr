package net.lustlab.rndr.draw;

import com.jogamp.opengl.GL;
import net.lustlab.rndr.RNDRException;

/**
 * Created by edwin on 13/05/16.
 */
public enum Primitive {

    TRIANGLE_STRIP,
    TRIANGLE_FAN,
    TRIANGLES,
    LINE_SEGMENTS;
    public int toGLPrimitive() {

        switch(this) {

            case LINE_SEGMENTS:
                return GL.GL_LINES;

            case TRIANGLES:
                return GL.GL_TRIANGLES;
            case TRIANGLE_FAN:
                return GL.GL_TRIANGLE_FAN;
            case TRIANGLE_STRIP:
                return GL.GL_TRIANGLE_STRIP;
            default:
                throw new RNDRException("no such primitive");
        }
    }


}
