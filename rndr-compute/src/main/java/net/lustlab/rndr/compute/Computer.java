package net.lustlab.rndr.compute;

import com.jogamp.opencl.*;
import com.jogamp.opencl.gl.CLGLContext;
import com.jogamp.opengl.GLContext;
import net.lustlab.rndr.buffers.*;

import java.lang.management.MemoryUsage;
import java.nio.ByteBuffer;
import java.nio.IntBuffer;

public class Computer {

    final CLDevice device;
    final CLGLContext context;

    private Computer(CLDevice device, CLGLContext context) {
        this.device = device;
        this.context = context;
    }

    public static Computer create() {
        CLDevice device = CLPlatform.getDefault().getMaxFlopsDevice(CLDevice.Type.GPU);
        CLGLContext context = CLGLContext.create(GLContext.getCurrent(), device);
        return new Computer(device, context);
    }

    static CLImageFormat.ChannelOrder fromColorFormat(ColorFormat format) {
        switch (format) {
            case R: return CLImageFormat.ChannelOrder.R;
            case RG: return CLImageFormat.ChannelOrder.RG;
            case RGB: return CLImageFormat.ChannelOrder.RGB;
            case RGBA: return CLImageFormat.ChannelOrder.RGBA;
            default: throw new RuntimeException("not supported");
        }
    }

    static CLImageFormat.ChannelType fromColorType(ColorType type) {
        switch (type) {
            case UINT8: return CLImageFormat.ChannelType.UNSIGNED_INT8;
            case UINT16: return CLImageFormat.ChannelType.UNSIGNED_INT16;
            case FLOAT16: return CLImageFormat.ChannelType.HALF_FLOAT;
            case FLOAT32: return CLImageFormat.ChannelType.FLOAT;
            default: throw new RuntimeException("not supported");
        }
    }

    public ComputeBuffer floatBuffer(int elements) {
        if (elements > 0) {
            return new ComputeBuffer(context.createFloatBuffer(elements, CLMemory.Mem.READ_WRITE));
        }
        else {
            throw new IllegalArgumentException("number of elements must be larger than 0");
        }
    }

    public ComputeBuffer intBuffer(int elements) {
        if (elements > 0) {
            return new ComputeBuffer(context.createIntBuffer(elements, CLMemory.Mem.READ_WRITE));
        } else {
            throw new IllegalArgumentException("number of elements must be larger than 0");
        }
    }

    public ComputeBuffer shortBuffer(int elements) {
        if (elements > 0) {
            return new ComputeBuffer(context.createShortBuffer(elements, CLMemory.Mem.READ_WRITE));
        } else {
            throw new IllegalArgumentException("number of elements must be larger than 0");
        }
    }

    public ComputeBuffer byteBuffer(int elements) {
        if (elements > 0) {
            return new ComputeBuffer(context.createByteBuffer(elements, CLMemory.Mem.READ_WRITE));
        } else {
            throw new IllegalArgumentException("number of elements must be larger than 0");
        }
    }

    public ComputeVertexBuffer vertexBuffer(VertexBuffer vertexBuffer) {
        return new ComputeVertexBuffer(context.createFromGLBuffer(vertexBuffer.vbo, vertexBuffer.vertexCount()*vertexBuffer.format().size()), vertexBuffer);
    }

    public ComputeBufferTexture bufferTexture(BufferTexture bufferTexture) {
        return new ComputeBufferTexture(
                context.createFromGLBuffer(bufferTexture.texture, bufferTexture.elementSize() * bufferTexture.elementCount()),
                bufferTexture
        );
    }

    public ComputeColorBuffer colorBuffer(ColorBuffer colorBuffer) {
        return new ComputeColorBuffer(
        context.createFromGLTexture2d(colorBuffer.target, colorBuffer.texture, 0), colorBuffer);
    }

    public ComputeImage image(int width, int height, ColorFormat format, ColorType type) {
        CLImageFormat clFormat = new CLImageFormat(fromColorFormat(format), fromColorType(type));
        ByteBuffer buffer = ByteBuffer.allocateDirect(format.componentCount() * width * height * type.componentSizeInBytes());
        CLImage2d<ByteBuffer> image = context.createImage2d(buffer, width, height, clFormat);
        context.createImage2d(buffer, width, height, clFormat);
        image.use(buffer);
        return new ComputeImage(image);
    }

    public ComputeImage3d image3d(int width, int height, int depth, ColorFormat format, ColorType type) {
        CLImageFormat clFormat = new CLImageFormat(fromColorFormat(format), fromColorType(type));
        ByteBuffer buffer = ByteBuffer.allocateDirect(format.componentCount() * width * height * depth * type.componentSizeInBytes());
        CLImage3d<ByteBuffer> image = context.createImage3d(buffer, width, height, depth, clFormat);
        image.use(buffer);
        return new ComputeImage3d(image);
    }

    public CommandQueue queue() {
        return new CommandQueue(device.createCommandQueue());
    }
}
