import net.lustlab.rndr.draw.ShadeStyle;
import org.junit.Test;

/**
 * Created by voorbeeld on 10/1/17.
 */
public class TestShadeStyle {

    @Test
    public void testConcat() {
        ShadeStyle a = new ShadeStyle().fill("x_fill = vec3(0.3);");
        ShadeStyle b = new ShadeStyle().fill("x_fill.r * 0.6;");
        ShadeStyle c = a.plus(b);
        System.out.println(c.fillTransform);
    }
}
