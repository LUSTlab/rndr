package net.lustlab.rndr.draw.gl3;

import net.lustlab.colorlib.ColorRGBa;
import net.lustlab.rndr.draw.DrawContext;
import net.lustlab.rndr.draw.DrawStyle;
import net.lustlab.rndr.math.Transforms;
import net.lustlab.rndr.shaders.Shader;

import static net.lustlab.rndr.math.Linear.multiply;

public class ShaderTools {

    public static void setupShader(Shader shader, DrawContext context, DrawStyle style) {


        if (context.modelView != null) {
            if (shader.hasUniform("modelViewMatrix")) {
                shader.setUniform("modelViewMatrix", context.modelView);
            }
        }
        if (context.projection != null && context.modelView != null) {
            if (shader.hasUniform("modelViewProjectionMatrix")) {
                shader.setUniform("modelViewProjectionMatrix", multiply(context.projection, context.modelView));
            }
        }
        if (context.projection != null) {
            if (shader.hasUniform("projectionMatrix")) {
                shader.setUniform("projectionMatrix", context.projection);
            }
        }

        if (context.modelView != null) {
            if (shader.hasUniform("normalMatrix")) {
                shader.setUniform("normalMatrix", Transforms.normal(context.modelView));
            }
        }
        if (style.colorMatrix != null) {
            if (shader.hasUniform("colorMatrix")) {
                shader.setUniform("colorMatrix", style.colorMatrix);
            }
        }

        if (shader.hasUniform("fill")) {
            if (style.fill != null) {
                shader.setUniform("fill", style.fill);
            } else {
                shader.setUniform("fill", new ColorRGBa(0, 0, 0, 0));
            }
        }


        if (shader.hasUniform("tint")) {
            if (style.tint != null) {
                shader.setUniform("tint", style.tint);
            } else {
                shader.setUniform("tint", new ColorRGBa(1, 1, 1, 1));
            }
        }

        if (shader.hasUniform("stroke")) {
            if (style.stroke != null) {
                shader.setUniform("stroke", style.stroke);
            } else {
                if (style.fill != null) {
                    shader.setUniform("stroke", style.fill);
                } else {
                    shader.setUniform("stroke", new ColorRGBa(0, 0, 0, 0));
                }
            }
        }

        if (shader.hasUniform("strokeWeight")) {
            shader.setUniform1f("strokeWeight", (float) style.strokeWeight);
        }

    }
}
