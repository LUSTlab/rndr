package net.lustlab.rndr.math;

import java.util.Arrays;
import java.util.List;

/**
 * Created by edwin on 18/09/16.
 */
public class Bounds {

    public static List<Vector2> bounds(List<Vector2> vectors) {
        double minx = Double.POSITIVE_INFINITY;
        double maxx = Double.NEGATIVE_INFINITY;
        double miny = Double.POSITIVE_INFINITY;
        double maxy = Double.NEGATIVE_INFINITY;

        for (Vector2 v: vectors) {
            minx = Math.min(minx, v.x);
            maxx = Math.max(maxx, v.x);
            miny = Math.min(miny, v.y);
            maxy = Math.max(maxy, v.y);
        }


        return Arrays.asList(new Vector2(minx, miny), new Vector2(maxx, maxy));

    };

}
