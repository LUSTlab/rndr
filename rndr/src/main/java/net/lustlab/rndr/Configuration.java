package net.lustlab.rndr;

import org.yaml.snakeyaml.Yaml;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

/**
 * RNDR Program configuration
 */
public class Configuration implements Serializable {


    public enum DefocusBehaviour {
        NONE,
        SUSPEND,
        THROTTLE
    }

    public DefocusBehaviour defocusBehaviour = DefocusBehaviour.NONE;

	private static final long serialVersionUID = 1L;
	public int width = 640;
	public int height = 480;
	public int left = Integer.MIN_VALUE;
	public int top = Integer.MIN_VALUE;
	public int fps = 60;
	public boolean fullscreen = false;
	public boolean resizable = false;
    public boolean changeMode = false;
    public boolean multithreaded = false;
    public boolean autoSwap = true;
	public boolean sRGB = false;
    public boolean debug = false;

    public String title = "RNDR";
	
	public boolean superSampling = false;
	public int superSamples = 1;


    public boolean hideWindowDecorations = false;
    public boolean backgroundOpaque = true;
	
	public Map<String, String> application = new HashMap<>();
	public Map<String, String> logLevels = new HashMap<>();

    /**
     * configures visibility of the mouse cursor
     */
    public boolean hideMouseCursor = false;
    public boolean useFPSAnimator;

    public static Configuration defaults() {
		Configuration configuration = new Configuration();
		return configuration;
	}

    /**
     * Loads configuration from yaml
     * @param filename
     * @throws RNDRException when loading failed
     * @return
     */
	public static Configuration loadFromYAML(String filename) {
		
		try {
			File file = new File(filename);
			Yaml yaml = new Yaml();
			FileInputStream fis = new FileInputStream(file);
			Configuration configuration = yaml.loadAs(fis, Configuration.class);
			return configuration;
		}
		catch (IOException e) {
            throw new RNDRException(e);
		}
	}

	public final Configuration multithreaded(boolean mt) {
	    this.multithreaded = mt;
	    return this;
    }

    /**
     * Sets the window title
     * @param title the window title
     * @return this
     */
    public final Configuration title(String title) {
        this.title = title;
        return this;
    }

    public final Configuration defocusBehaviour(DefocusBehaviour defocusBehaviour) {
        this.defocusBehaviour = defocusBehaviour;
        return this;
    }

    /**
     * Hides the window decorations
     * @param hide true hides the window decorations
     * @return
     */
    public final Configuration hideWindowDecorations(boolean hide) {
        this.hideWindowDecorations = hide;
        return this;
    }

    /**
     * Hides the mouse cursor
     * @param hide true hides mouse cursor
     * @return this
     */
    public final Configuration hideMouseCursor(boolean hide) {
        this.hideMouseCursor = hide;
        return this;
    }

    /**
     * Sets the window position
     * @param x the top-left position of the window
     * @param y the top-left position of the window
     * @return this
     */
    public final Configuration position(int x, int y) {
        this.left = x;
        this.top = y;
        return this;
    }

    /**
     * Sets the window size to the maximum size possible
     * @return this
     */
    public final Configuration maximumSize() {
        this.width = -1;
        this.height = -1;
        return this;
    }

    /**
     * Sets the window size
     * @param width
     * @param height
     * @return this
     */
    public final Configuration size(int width, int height) {
        this.width = width;
        this.height = height;
        return this;
    }

    /**
     * Configure the Program to run in debug mode
     * @param debug true enables debug
     * @return this
     */
    public final Configuration debug(boolean debug) {
        this.debug = debug;
        return this;
    }

    /**
     * Configure the Progrma to run in fullscreen mode
     * @param fullscreen true enables fullscreen
     * @return this
     */
    public final Configuration fullscreen(boolean fullscreen) {
        this.fullscreen = fullscreen;
        return this;
    }

    public Configuration useFPSAnimator(boolean b) {
        this.useFPSAnimator = b;
        return this;
    }
}
