package net.lustlab.rndr.text;

import com.jogamp.opengl.GL;
import com.jogamp.opengl.GL3;
import com.jogamp.opengl.GLContext;
import net.lustlab.colorlib.ColorRGBa;
import net.lustlab.rndr.RNDRException;

import net.lustlab.rndr.draw.BlendMode;
import net.lustlab.rndr.draw.Drawer;
import net.lustlab.rndr.shape.Rectangle;
import net.lustlab.rndr.draw.ShadeStyle;
import net.lustlab.rndr.font.FontMap;
import net.lustlab.rndr.font.FontMapRenderer;
import net.lustlab.rndr.font.image.FontImageMapRendererGL3;
import net.lustlab.rndr.font.vector.FontVectorMap;
import net.lustlab.rndr.font.vector.FontVectorMapRendererGL3;
import net.lustlab.rndr.math.Matrix44;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Stack;


/**
 * Writer is RNDR's advanced text rendering mechanism.
 */
@SuppressWarnings("UnusedDeclaration")
public class Writer {


    Matrix44 projection;
    Matrix44 modelView;

    ShadeStyle shadeStyle;
    public List<RenderToken> renderTokens;

    BlendMode blendMode = BlendMode.NORMAL;

    public Writer blend(BlendMode blendMode) {
        this.blendMode = blendMode;
        return this;
    }

    public interface Handler {
        public Matrix44 preRender(RenderToken token, Matrix44 modelView);
        public void preEmit(RenderToken token, Cursor cursor);
        public void postEmit(RenderToken token, Cursor cursor);
    }

    final Handler defaultHandler = new Handler() {
        @Override
        public Matrix44 preRender(RenderToken token, Matrix44 modelView) { return modelView; }

        @Override
        public void preEmit(RenderToken token, Cursor cursor) { }

        @Override
        public void postEmit(RenderToken token, Cursor cursor) { }
    };

    Handler handler = defaultHandler;

    public static class RenderToken {
        public double x;
        public double y;
        public double tracking;
        public double width;
        public String token;

        RenderToken(String token, double x, double y, double width, double tracking) {
            this.x = x;
            this.y = y;
            this.width = width;
            this.tracking = tracking;
            this.token = token;
        }

        public List<FontMapRenderer.CharacterRectangle> rectangles;
    }

    /** the active cursor */
    public Cursor cursor = new Cursor(0,0);

    /** the active style */
    public Style style = new Style();

    /** the active box */
    public Box box = new Box(0, 0, Double.MAX_VALUE/2, Double.MAX_VALUE/2);

    public Box clip = new Box(0,0, Double.MAX_VALUE/4, Double.MAX_VALUE/4);

    private Stack<Cursor> cursorStack = new Stack<>();
    private Stack<Box> boxStack = new Stack<>();
    private Stack<Box> clipStack = new Stack<>();

    private Stack<Style> styleStack = new Stack<>();
    private FontMapRenderer renderer;

    private FontMapRenderer imageRenderer;
    private FontMapRenderer vectorRenderer;
    private GL3 gl3;

    private Drawer drawer;

    /**
     * Constructs a drawer that takes the projection and modelview matrices from a DrawerGL3 instance
     * @param drawer drawer instance from which projection and model view matrices are taken
     */
    public Writer(final Drawer drawer) {

        if (drawer != null) {

            this.drawer = drawer;
            gl3 = GLContext.getCurrentGL().getGL3();
            setupRenderers(GLContext.getCurrentGL());
        }
        else {
            throw new RuntimeException("drawer instance = null");
        }
    }

    public Writer() {
        this(GLContext.getCurrentGL());
    }

    public static Writer createWithoutContext() {
        return new Writer((GL) null);
    }


    public Writer handler(Handler handler) {
        this.handler = handler;
        return this;
    }

    public Writer noHandler() {
        this.handler = defaultHandler;
        return this;
    }

    /**
     * Writer constructor
     * @param gl the OpenGL interface to use
     */
    public Writer(GL gl) {
        setupRenderers(gl);
    }

    private void setupRenderers(GL gl) {
        this.imageRenderer = new FontImageMapRendererGL3(gl);
        this.vectorRenderer = new FontVectorMapRendererGL3();
    }

    public Writer projection(Matrix44 projection) {
        if (projection != null) {
            this.projection = projection;
            return this;
        }
        else {
            throw new RuntimeException("projection instance is null");
        }

    }

    public Writer modelView(Matrix44 modelView) {
        if (modelView != null) {
            this.modelView = modelView;return this;

        }
        else {
            throw new RuntimeException("modelView instance is null");
        }

    }


    public ColorRGBa fill() {
        return this.style.fill;
    }

    /**
     * Sets the fill color to be used for the text
     * @param color the fill color
     */
    public Writer fill(ColorRGBa color) {
        this.style.fill = new ColorRGBa(color);
        return this;
    }

    public ColorRGBa stroke() {
        return this.style.stroke;
    }

    public Writer stroke(ColorRGBa color) {
        this.style.stroke = new ColorRGBa(color);
        return this;
    }


    /**
     * Sets the tracking. Tracking is the horizontal space between two consecutive letters.
     * @param tracking the tracking in pixels, default is 0
     */
    public Writer tracking(double tracking) {
        this.style.tracking = tracking;
        return this;
    }

    public double tracking() {
        return this.style.tracking;
    }

    public Writer ellipsis(String ellipsis) {
        style.ellipsis = ellipsis;
        return this;
    }

    public Writer noEllipsis() {
        style.ellipsis = null;
        return this;
    }

    /**
     * Write text at the current cursor position and respects the active box
     * updates the cursor.
     * @param text the text to write
     * @return Writer
     */
    public Writer text(String text) {

            if (drawer != null) {
                projection = drawer.projection();
                modelView = drawer.modelView();
                shadeStyle = drawer.shadeStyle();
            }

            gl3 = GLContext.getCurrentGL().getGL3();
            if (text == null) {
                return this;
            }

            if (font() == null) {
                throw new RNDRException("no font set, use font() to set a font");
            }

            if (style.font instanceof FontVectorMap) {
                renderer = vectorRenderer;
            } else {
                renderer = imageRenderer;
            }

            if (renderer == null) {
                throw new RNDRException("I have no renderer?");
            }

            renderer.shadeStyle(shadeStyle);
            renderer.blend(blendMode);
            renderer.clip(clip.x, clip.y, clip.width, clip.height);

            renderer.fill(style.fill);
            renderer.stroke(style.stroke);

            renderTokens = makeRenderTokens(text, false);

            boolean needFlush = false;

            for (RenderToken renderToken : renderTokens) {
                Matrix44 modelViewTransformed = handler.preRender(renderToken, modelView);
                renderToken.rectangles = renderer.render(gl3, projection, modelViewTransformed, style.font, renderToken.token, renderToken.x, renderToken.y, renderToken.tracking);
                //handler.postRender(renderToken, modelView);
                needFlush = true;
                if (modelViewTransformed != modelView) {
                    renderer.flush();
                    needFlush = false;
                }
            }


            if (needFlush) {
                renderer.flush();
            }

        return this;
    }

    /**
     * Attempts to write text using a list a fonts
     * @param text the text to write
     * @param fontMaps a list of fonts sorted from large to small
     */
    public Writer textFit(String text, FontMap ... fontMaps ) {

        if (drawer != null) {
            projection = drawer.projection();
            modelView = drawer.modelView();
        }

        for (int f = 0; f <  fontMaps.length; ++f) {
            FontMap fontMap = fontMaps[f];

            pushStyle();
            font(fontMap);
            if (style.font instanceof FontVectorMap) {
                renderer = vectorRenderer;
            }
            else {
                renderer = imageRenderer;
            }
            renderer.fill(style.fill);
            renderer.stroke(style.stroke);

            renderTokens = makeRenderTokens(text, f != fontMaps.length - 1);
            if (renderTokens != null) {
                for (RenderToken renderToken : renderTokens) {
                    renderToken.rectangles = renderer.render(gl3, projection, modelView, style.font, renderToken.token, (float) renderToken.x, (float) renderToken.y, renderToken.tracking);
                }

            }

            popStyle();
            if (renderTokens != null) {
                break;
            }
        }
        return this;
    }

    /**
     * Returns the width
     * @param text the text to determine the width for
     * @return the width of the text in pixels
     */
    public double textWidth(String text) {
        if (style.font != null) {

            if (style.font instanceof FontVectorMap) {
                renderer = vectorRenderer;
            } else {
                renderer = imageRenderer;
            }

            return renderer.width(style.font, text) + style.tracking * (text.length());
        } else {
            throw new RuntimeException("no font set");
        }
    }

    /**
     * Pretend renders text, updates the cursor but no visible text is generated
     * @param text the text to pretend render
     */
    public Writer blindText(String text) {
        makeRenderTokens(text, false);
        return this;
    }

    private List<RenderToken> makeRenderTokens(String text, boolean mustFit) {
        if (style.font instanceof FontVectorMap) {
            renderer = vectorRenderer;
        }
        else {
            renderer = imageRenderer;
        }

        boolean fits = true;

        if (text == null) {
            return new ArrayList<>();
        }

        String lines[] = text.split("((?<=\n)|(?=\n))");
        List<String> tokens = new ArrayList<>();
        for (String line: lines) {
            String[] lineTokens = line.split(" ");
            Collections.addAll(tokens, lineTokens);
        }

        double spaceWidth = renderer.width(style.font, " ") + style.tracking;
        double verticalSpace = style.font.fontHeight() + style.font.leading() + style.leading;

        List<RenderToken> renderTokens = new ArrayList<>();
        Cursor tempCursor = new Cursor(cursor);
        for (int i = 0; i < tokens.size(); ++i) {

            String token = tokens.get(i);

            if (token.equals("\n")) {
                tempCursor.x = box.x;
                tempCursor.y += verticalSpace;
            }
            else {
                double width = renderer.width(style.font, token) + style.tracking*(token.length());

                if (tempCursor.x + width < box.x+box.width && tempCursor.y <= box.y + box.height) {
                    RenderToken renderToken = new RenderToken(token, tempCursor.x, tempCursor.y, width, style.tracking);
                    emitToken(tempCursor, renderTokens, renderToken);
                }
                else {

                    if (!(tempCursor.y <= box.y + box.height)) {
                        fits = false;
                    }


                    if (tempCursor.y + verticalSpace <= box.y + box.height) {
                        tempCursor.y += verticalSpace;
                        tempCursor.x = box.x;
                        RenderToken renderToken = new RenderToken(token, tempCursor.x, tempCursor.y, width, style.tracking);
                        emitToken(tempCursor, renderTokens, renderToken);
                    }
                    else {
                        if (!mustFit && style.ellipsis != null && cursor.y <= box.y + box.height) {
                            RenderToken renderToken = new RenderToken(style.ellipsis, tempCursor.x, tempCursor.y, width, style.tracking);
                            emitToken(tempCursor, renderTokens, renderToken);
                            break;
                        }
                        else {
                            fits = false;
                        }

                    }
                }

                tempCursor.x += width;
                if (i != tokens.size()-1) {
                    tempCursor.x += spaceWidth;
                }
            }
        }

        if (fits || (!fits && !mustFit)) {
            cursor = new Cursor(tempCursor);
        }
        else {
            renderTokens = null;
        }
        return renderTokens;
    }

    private void emitToken(Cursor tempCursor, List<RenderToken> renderTokens, RenderToken renderToken) {
        handler.preEmit(renderToken, tempCursor);
        renderTokens.add(renderToken);
        handler.postEmit(renderToken, tempCursor);
    }

    /**
     * Sets the leading. Leading affects the vertical space between two lines of text
     * @param leading the leading in pixels
     */
    public Writer leading(double leading) {
        style.leading = leading;
        return this;
    }


    /**
     * Returns the active leading.
     * @return the active leading in pixels
     */
    public double leading() {
        return style.leading;
    }

    /**
     * Sets the font to use for rendering the text
     * @param font the font to use, default is null
     */
    public Writer font(FontMap font) {
        style.font = font;
        return this;
    }

    /**
     * Returns the active font
     * @return the active font
     */
    public FontMap font() {
        return style.font;
    }

    /**
     * Sets the cursor position
     * @param x the x-coordinate of the cursor
     * @param y the y-coordinate of the cursor
     */
    public Writer cursor(double x, double y) {
        cursor.x = x;
        cursor.y = y;
        return this;
    }

    /**
     * Sets the cursor x-coordinate
     * @param x the x-coordinate of the cursor
     */
    public Writer cursorX(double x) {
        cursor.x = x;
        return this;
    }

    /**
     * Sets the cursor y-coordinate
     * @param y the y-coordinate of the cursor
     */
    public Writer cursorY(double y) {
        cursor.y = y;
        return this;
    }

    /**
     * Pushes the current clipping rectangle on the clip stack
     */
    public Writer pushClip() {
        clipStack.push(new Box(clip));
        return this;
    }

    /**
     * Pops the top of the clip stack onto the current clipping rectangle
     */
    public Writer popClip() {
        clip = clipStack.pop();
        return this;
    }

    /**
     * Sets the clipping the rectangle
     * @param x the x-coordinate of the top-left corner of the clipping rectangle
     * @param y the y-coordinate of the top-left corner of the clipping rectangle
     * @param width the width of the clipping rectangle
     * @param height the height of the clipping rectangle
     */
    public Writer clip(double x, double y, double width, double height) {
        clip = new Box(x, y, width, height);
        return this;
    }


    /**
     * Disables the clipping rectangle
     */
    public Writer noClip() {
        clip = new Box(-1, -1, 8192,8192);
        return this;

    }

    /**
     * Returns the current clipping rectangle
     * @return the current clipping rectangle
     */
    public Box clip() {
        return clip;
    }


    /**
     * Pushes the current cursor position on the cursor stack
     */
    public Writer pushCursor() {
        cursorStack.push(new Cursor(cursor));
        return this;
    }

    public Writer popCursor() {
        cursor = new Cursor(cursorStack.pop());
        return this;
    }

    /**
     * Pushes a copy of the active box on the box stack
     */
    public Writer pushBox() {
        boxStack.push(new Box(box));
        return this;
    }

    /**
     * Pop a box from the stack and set it as the active box
     */
    public Writer popBox() {
        box = boxStack.pop();
        return this;
    }

    public Writer style(Style style) {
        this.style = new Style(style);
        return this;
    }

    /**
     * Pushes a copy of the current style on the style stack
     */
    public Writer pushStyle() {
        styleStack.push(new Style(style));
        return this;
    }

    /**
     * Pops the style from the internal style stack into the active style
     */
    public Writer popStyle() {
        style = styleStack.pop();
        return this;
    }

    /**
     * Increments the cursor y coordinate by the font height + leading of the active style
     */
    public Writer newLine() {
        cursor.x = box.x;
        cursor.y += style.font.fontHeight() + style.font.leading() + style.leading;
        return this;
    }
    /**
     * Increments the cursor y coordinate by the font height + leading of the active style
     */
    public Writer gaplessNewLine() {
        cursor.x = box.x;
        cursor.y += style.font.fontHeight();// + style.font.leading();
        return this;
    }

    /**
     * Increments the cursor position
     * @param x the x increment
     * @param y the y increment
     */
    public Writer move(double x, double y) {
        cursor.x += x;
        cursor.y += y;
        return this;
    }

    /**
     * Returns the current active box
     */
    public Box box() {
        return box;
    }

    /**
     * Sets the active box
     * @param box the box to make active
     */
    public Writer box(Box box) {
        box = new Box(box);
        cursor.x = box.x;
        cursor.y = box.y;
        return this;
    }

    public Writer box(Rectangle area) {
        return box(area.x, area.y, area.width, area.height);
    }

    /**
     * Sets the active box. Moves the cursor to the top left corner.
     * @param x the top left of the box
     * @param y the top right of the box
     * @param width the width of the box
     * @param height the height of the box
     */
    public Writer box(double x, double y, double width, double height) {
        box = new Box(x,y,width,height);
        cursor.x = x;
        cursor.y = y;
        return this;
    }

    /**
     * Sets the active box to a single line. Moves the cursor to the beginning of the line
     * @param x the x-coordinate of the line start
     * @param y the y-coordinate of the line start
     * @param width the width of the box in pixels
     */
    public Writer line(double x, double y, double width) {
        return box(x,y, width, 0);
    }

    /**
     * Sets the active box, without specifying the height. Moves the cursor to the top left corner.
     * @param x the top left of the box
     * @param y the top right of the box
     * @param width the width of the box
     */
    public Writer box(double x, double y, double width) {
        box = new Box(x,y,width, 8192);
        cursor.x = x;
        cursor.y = y;
        return this;
    }

    /**
     * Removes the active box. Leaves the cursor untouched.
     */
    public Writer noBox() {
        box = new Box(0,0,Double.MAX_VALUE/2, Double.MAX_VALUE/2);
        return this;
    }

    /**
     * Moves the active box relative to current box position
     * @param x the x-coordinate increment in pixels
     * @param y the y-coordinate increment in pixels
     */
    public Writer moveBox(int x, int y) {
        box.x += x;
        box.y += y;
        return this;
    }

    /**
     * Changes the box position without changing the box size
     * @param x the new x-coordinate for the box
     * @param y the new y-coordinate for the box
     */
    public Writer boxPosition(int x, int y) {
        box.x = x;
        box.y = y;
        return this;
    }

    /**
     * Changes the box size without changing the box position
     * @param width the new width for the box
     * @param height the new height for the box
     */
    public Writer boxSize(int width, int height) {
        box.width = width;
        box.height = height;
        return this;
    }

}
