package net.lustlab.rndr.shape;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class Figure implements Iterable<Polygon>, Serializable {

	public List<Polygon> polygons = new ArrayList<Polygon>();

    /**
     * Returns the bounds of the figure geometry
     * @return
     */
    public Rectangle bounds() {
        throw new RuntimeException("broken");
//        double minx = Double.POSITIVE_INFINITY;
//        double maxx = Double.NEGATIVE_INFINITY;
//        double maxy = Double.NEGATIVE_INFINITY;
//        double miny = Double.POSITIVE_INFINITY;
//
////        for (CompositionNode node: polygons) {
////            Rectangle localBounds = node.bounds();
////            minx = Math.min(minx, localBounds.x);
////            miny = Math.min(miny, localBounds.y);
////            maxx = Math.max(maxx, localBounds.x + localBounds.width);
////            maxy = Math.max(maxy, localBounds.y + localBounds.height);
////        }
//
//        return new Rectangle(minx, miny, maxx- minx, maxy - miny);
    }

	@Override
	public Iterator<Polygon> iterator() {
		return polygons.iterator();
	}

    /**
     * Adds a polygon to the figure
     * @param polygon
     * @return
     */
	public Figure addPolygon(Polygon polygon) {
		polygons.add(polygon);
		return this;
	}

    /**
     * Retrieves a polygon by name
     * @param name the name of the polygon to find
     * @return A polygon or null if the requested polygon is not found
     */
    public Polygon polygon(String name) {
//        for (Polygon polygon: polygons) {
//            if (name.equals(polygon.id)) {
//                return polygon;
//            }
//        }
//        return null;
        throw new RuntimeException("broken");
    }

    public List<Polygon> polygons(String regex) {
//        List<Polygon> result = new ArrayList<Polygon>();
//        for (Polygon polygon: polygons) {
//            if (polygon.id != null) {
//                if (polygon.id.matches(regex)) {
//                    result.plus(polygon);
//                }
//            }
//        }
//        return result;
        throw new RuntimeException("broken");
    }

	@Override
	public String toString() {
		return "Figure{" +
				"polygons=" + polygons +
				'}';
	}
}
