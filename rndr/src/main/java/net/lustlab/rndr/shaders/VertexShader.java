package net.lustlab.rndr.shaders;

import com.jogamp.opengl.GL;
import com.jogamp.opengl.GL3;
import com.jogamp.opengl.GLContext;
import net.lustlab.rndr.RNDRException;
import org.apache.commons.io.IOUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.*;
import java.net.MalformedURLException;
import java.net.URL;

/**
 *
 */
public class VertexShader {
    /**
     * The OpenGL shader object that identifies the vertex program
     */
    final public int shaderObject;
    final String name;

    @Override
    public String toString() {
        return "VertexShader{" +
                "shaderObject=" + shaderObject +
                ", name='" + name + '\'' +
                '}';
    }

    static Logger logger = LogManager.getLogger(VertexShader.class);
    /**
     *
     * @param shaderObject shader object
     */
    public VertexShader(int shaderObject, String name) {
        this.shaderObject = shaderObject; this.name = name;
    }

    public String name() {
        return name;
    }

    /**
     * Load a vertex shader from string
     * @param code the shader code
     * @return
     */
    public static VertexShader fromString(String code, String name) {

        logger.debug("creating vertex shader: {}", name);
        GL gl = GLContext.getCurrentGL();

        if (name == null) {
            name = "<unknown-vertex-shader>";
        }

        if (gl.isGL3()) {
            GL3 gl3 = gl.getGL3();
            int shaderObject = gl3.glCreateShader(GL3.GL_VERTEX_SHADER);
            gl3.glShaderSource(shaderObject, 1, new String[]{code}, null,0);
            gl3.glCompileShader(shaderObject);
            int values[] = new int[1];
            gl3.glGetShaderiv(shaderObject, GL3.GL_COMPILE_STATUS, values, 0);
            if (values[0] != GL.GL_TRUE) {
                ShaderInfo.checkShaderInfoLog(gl, shaderObject, code, name);
                throw new ShaderException("could not compile vertex shader");
            }

            return new VertexShader(shaderObject, name);

        }
        else {
            throw new RNDRException("unsupported GL implementation");
        }
    }

    /**
     * Load a vertex shader from file
     * @param filename filename of the shader file
     * @return
     */

    public static VertexShader fromFile(String filename)  {
        return fromFile(new File(filename));
    }


    /**
     * Load a vertex shader from url
     * @param url where to find the shader
     * @return
     */

    public static VertexShader fromUrl(String url)  {
        try {
            return fromUrl(new URL(url));
        } catch (MalformedURLException e) {
            throw new ShaderException("failed to load shader from URL", e);
        }
    }

    public static VertexShader fromUrl(URL url)  {
        try {
            return fromStream(url.openStream(), url.toString());
        } catch (MalformedURLException e) {
            throw new ShaderException("failed to load shader from malformed URL: " + url, e);
        } catch (IOException e) {
            throw new ShaderException("failed to load shader from URL: " + url, e);
        }
    }



    /**
     * Load vertex shader from file
     * @param file file instance
     * @return
     */
    public static VertexShader fromFile(File file)  {
        FileInputStream stream;
        try {
            stream = new FileInputStream(file);
            return fromStream(stream);

        } catch (FileNotFoundException e) {
            throw new ShaderException("Shader file not found: " + file.getName());
        }
    }

    public static VertexShader fromStream(GL gl, InputStream stream)  {
        return fromStream(gl, stream, null);
    }

    /**
     * Load vertex shader from stream
     * @param gl the OpenGL context
     * @param stream the input stream
     * @return
     */
    public static VertexShader fromStream(GL gl, InputStream stream, String name)  {
        logger.debug("creating vertex shader from stream: {}", name);
        StringWriter writer = new StringWriter();
        try {
            IOUtils.copy(stream, writer, "utf-8");
            String code = writer.toString();
            return fromString(code, name);
        }
        catch (IOException e) {
            throw new ShaderException("Error reading from shader stream: " + e.getMessage());
        }
        finally {
            try {
                if (stream != null) {
                    stream.close();
                }
            } catch (IOException e) {
                throw new RNDRException(e);
            }
        }
    }

    public static VertexShader fromStream(InputStream stream)  {
        return fromStream(GLContext.getCurrentGL(), stream);
    }
    public static VertexShader fromStream(InputStream stream, String name)  {
        return fromStream(GLContext.getCurrentGL(), stream, name);
    }

    public void destroy() {
        GLContext.getCurrentGL().getGL2GL3().glDeleteShader(shaderObject);
    }

}
