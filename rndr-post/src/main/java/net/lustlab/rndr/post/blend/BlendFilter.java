package net.lustlab.rndr.post.blend;

import net.lustlab.rndr.post.Filter;
import net.lustlab.rndr.shaders.FragmentShader;

public class BlendFilter extends Filter {

    double opacity;

    public BlendFilter opacity(double opacity) {
        this.opacity = opacity;
        parameter("opacity", (float)opacity);
        return this;
    }

    public BlendFilter(FragmentShader fragmentShader) {
        super(fragmentShader);
    }
}
