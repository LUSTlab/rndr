#version 330


in vec3 position;

uniform mat4 modelViewMatrix;
uniform mat4 modelViewProjectionMatrix;
uniform mat4 normalMatrix;

void main() {
    gl_Position = modelViewProjectionMatrix * vec4(position, 1.0);
}