package net.lustlab.rndr.post;

import net.lustlab.rndr.shaders.FragmentShader;

/**
 * Sobel filter
 * output:
 * r - x edge direction, encoded x * gain + bias
 * g - y edge direction, encoded y * gain + bias
 * b = edge magnitude [0..1]
 */
public class Sobel extends Filter {
    public Sobel() {
        super(FragmentShader.fromUrl("cp:shaders/gl3/post/sobel.frag"));
        gain(0.5);
        bias(0.5);
    }

    /**
     * Sets the edge gain which is used to write
     * @param gain the gain, default is 0.5
     * @return
     */
    public Sobel gain(double gain) {
        parameter("gain", (float) gain);
        return this;
    }

    /**
     * Sets the edge bias
     * @param bias
     * @return
     */
    public Sobel bias(double bias) {
        parameter("bias", (float) bias);
        return this;
    }

}
