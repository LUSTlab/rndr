#version 330

in vec3 position;
in vec3 normal;
in vec4 color;

in vec2 texCoord0;

//out vec3 v_normal;
//out vec4 v_color;
//out mat4 v_modelViewMatrix;
//out vec2 v_texCoord0;

out vec2 v_centerDistance;

uniform mat4 modelViewMatrix;
uniform mat4 modelViewProjectionMatrix;
uniform mat4 normalMatrix;

uniform float strokeWeight;
uniform vec2 start;
uniform vec2 end;

void main(void) {

    vec2 direction = end - start;
    vec2 normal = normalize(vec2(direction.y, -direction.x));


    vec2 linePosition = start + (position.x * direction) + (position.y * normal * (strokeWeight*1.0));


    v_centerDistance = vec2((position.x-0.5)*2, position.y);


    gl_Position = modelViewProjectionMatrix * vec4(linePosition, 0, 1.0);
//    v_normal = (normalMatrix * vec4(normal.xyz, 0.0)).xyz;
//    v_color = color;
//    v_modelViewMatrix = modelViewMatrix;
//    v_texCoord0 = texCoord0;
}