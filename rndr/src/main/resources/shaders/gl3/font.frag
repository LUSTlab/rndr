#version 330

layout(origin_upper_left) in vec4 gl_FragCoord;

uniform sampler2D image;
uniform vec4 clipArea;
uniform float viewHeight;

uniform vec4 fill;
in  vec2 v_texCoord0;
out vec4 o_color;

void main() {
	vec4 col = texture(image, v_texCoord0.xy);

	if (gl_FragCoord.x < clipArea.x
	    || gl_FragCoord.x > clipArea.y
	    || (gl_FragCoord.y) < clipArea.z
	    || (gl_FragCoord.y) > clipArea.w) {
	    discard;
	}
	vec2 screenPosition = gl_FragCoord.xy;

    vec4 x_fill = fill;
    { ${fillTransform! } }

	o_color = col * x_fill;
    o_color.rgb *= x_fill.a;
}