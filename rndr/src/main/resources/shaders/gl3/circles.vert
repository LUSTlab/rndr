#version 330

uniform samplerBuffer positions;
uniform mat4 modelViewProjectionMatrix;
uniform int instanceOffset;

${uniforms! }

in vec2 texCoord0;
in vec3 position;


out vec2 v_texCoord0;

uniform mat4 modelViewMatrix;
uniform  mat4 normalMatrix;

uniform float radius;
uniform float radiusMod;

out float v_radius;
flat out int v_instance;

out vec3 v_boundsPosition;
out vec3 v_modelPosition;
out vec3 v_cameraPosition;

void main(void) {

    int instance = gl_InstanceID + instanceOffset;
    v_instance = instance;

    vec4 data = texelFetch(positions, instance);
    v_radius = data.w * (1.0 - radiusMod) + radiusMod * radius;

    float x_radius = v_radius;

    vec3 x_position = data.xyz;
    { ${positionTransform! } }

    v_radius = x_radius;


    gl_Position = modelViewProjectionMatrix * vec4( vec3(position.xy * v_radius, 0) + x_position, 1.0);

    v_boundsPosition = vec3(position.xy, 0);
    v_modelPosition = position * v_radius;
    v_cameraPosition = (modelViewMatrix * vec4( vec3(position.xy * v_radius, 0) + x_position, 1.0)).xyz;

    v_texCoord0 = texCoord0;
}