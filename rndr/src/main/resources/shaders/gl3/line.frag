#version 330

uniform vec4 stroke;
out vec4 o_color;
in vec2 v_centerDistance;


void main(void) {

    float du = (v_centerDistance.x);
    float dv = (v_centerDistance.y);
    float wv = fwidth(dv);
    float wu = fwidth(du);
    float a = smoothstep(0, wv, 1.0-abs(dv));
    float b = smoothstep(0, wu, 1.0-abs(du));

    o_color = stroke;
    o_color.a *= a*b;


}