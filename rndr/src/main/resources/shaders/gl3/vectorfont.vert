#version 150

in vec3 position;
in vec4 color;
out vec4 v_color;

uniform mat4 modelViewProjectionMatrix;

void main(void) {
    v_color = color;

    gl_Position = modelViewProjectionMatrix * vec4(position.xyz, 1.0);
}