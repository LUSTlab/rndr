#version 330

uniform mat4 modelViewMatrix;
uniform mat4 projectionMatrix;

in vec3 position;
in vec2 texCoord0;

out vec2 v_texCoord0;

void main() {
    v_texCoord0 = texCoord0;
    gl_Position = projectionMatrix * modelViewMatrix * vec4(position, 1);
}