package net.lustlab.rndr.application.internal;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.File;

public class MacOSPlatformDriver  implements PlatformDriver{

    public static String randomID;

    public static Logger logger = LogManager.getLogger(MacOSPlatformDriver.class);

    public MacOSPlatformDriver() {
        logger.debug("instantiating MacOS platform driver");
        char[] alphabet = new char[] { 'a','b','d','e','f','g','h','i','j','k','l','m','n','o','p','q','r','s','t','u','v','w','x','y','z'};
        String id = "";
        for (int i = 0; i < 8; ++i) {
            id += alphabet[i];
        }
        randomID = id;
    }

    @Override
    public File temporaryDirectory() {
        String directoryName = System.getProperty("java.io.tmpdir") + "RNDR-"+randomID;
        logger.debug("using temporary directory: {} ", new Object[] { directoryName});
        File file = new File(directoryName);
        if (!file.exists()) {
            file.mkdirs();
        }
        return file;
    }

    @Override
    public File cacheDirectory(String programName) {
        String directoryName = System.getProperty("user.home") + "/Library/Caches/" + programName;
        logger.debug("using cache directory: {} ", new Object[] { directoryName});
        File file = new File(directoryName);
        if (!file.exists()) {
            file.mkdirs();
        }
        return file;
    }

    @Override
    public File supportDirectory(String programName) {
        String directoryName = System.getProperty("user.home") + "/Library/Application Support/" + programName;
        logger.debug("using support directory: {} ", new Object[] { directoryName});
        File file = new File(directoryName);
        if (!file.exists()) {
            file.mkdirs();
        }
        return file;
    }
}
