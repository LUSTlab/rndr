package net.lustlab.rndr.post.blend;

import net.lustlab.rndr.shaders.FragmentShader;

public class Normal extends BlendFilter {
    public Normal() {
        super(FragmentShader.fromUrl("cp:shaders/gl3/post/blend/normal.frag"));
    }
}
