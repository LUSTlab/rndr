package net.lustlab.rndr.shaders;

import freemarker.cache.StringTemplateLoader;
import freemarker.template.Configuration;
import freemarker.template.DefaultObjectWrapper;
import freemarker.template.Template;
import freemarker.template.TemplateException;
import net.lustlab.rndr.RNDRException;

import java.io.*;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;

public class ShaderPreprocessor {
    Map<String, String> registry = new HashMap<>();

    public ShaderPreprocessor registerUrl(String name, String url) {
        try {
            return registerUrl(name, new URL(url));
        } catch (MalformedURLException e) {
            throw new RNDRException("malformed url: " + url);
        }
    }

    public ShaderPreprocessor registerUrl(String name, URL url) {

        try {
            return register(name, url.openStream());
        } catch (IOException e) {
            throw new RNDRException("cannot open url: " + url);
        }

    }

    public ShaderPreprocessor register(String name, String data) {
        registry.put(name, data);
        return this;
    }

    public ShaderPreprocessor register(String name, InputStream stream) {


        BufferedReader br = new BufferedReader(new InputStreamReader(stream));
        StringBuffer sb = new StringBuffer();

        String line = null;
        int lineIdx = 1;
        try {
            while ( (line=br.readLine()) != null) {
                String prefix = "/*" + name + ":" + lineIdx + "*/ ";
                sb.append(prefix + line + "\n");
                lineIdx++;
            }
        } catch (IOException e) {
            throw new RNDRException("failed to read from stream");
        }
        return register(name, sb.toString());
    }


    public String processUrl(Object model, String url) {
        try {
            return processUrl(model, new URL(url));
        } catch (MalformedURLException e) {
            throw new RNDRException("malformed url");
        }
    }

    public String processUrl(Object model, URL url) {
        try {
            return process(model, url.openStream(), url.toString());
        } catch (IOException e) {
            throw new RNDRException("could not open url: " + url);
        }
    }

    public String process(Object model, InputStream stream, String name) {
        BufferedReader br = new BufferedReader(new InputStreamReader(stream));
        StringBuffer sb = new StringBuffer();

        String line = null;
        int lineIdx = 1;
        try {
            while ( (line=br.readLine()) != null) {
                String prefix = "/*" + name + ":" + lineIdx + "*/ ";
                sb.append(prefix + line+"\n");
                lineIdx++;
            }
            return process(model, sb.toString());
        } catch (IOException e) {
            throw new RNDRException("failed to read from stream");
        }
    }
    public String process(Object model, String code) {

        Configuration cfg = new Configuration(Configuration.VERSION_2_3_21);

        StringTemplateLoader stl = new StringTemplateLoader();
        for (Map.Entry<String, String> e: registry.entrySet()) {
            stl.putTemplate(e.getKey(), e.getValue());
        }
        stl.putTemplate("input", code);
        DefaultObjectWrapper dow = new DefaultObjectWrapper(Configuration.VERSION_2_3_21);
        dow.setExposeFields(true);
        cfg.setObjectWrapper(dow);
        cfg.setTemplateLoader(stl);
        cfg.clearTemplateCache();
        StringWriter sw = new StringWriter();
        try {
            Template template = cfg.getTemplate("input");
            template.process(model, sw);

        } catch (IOException e) {
            e.printStackTrace();
        } catch (TemplateException e) {
            throw new ShaderException("error in template", e);
        }


        return sw.toString();

    }

    public Shader fromUrls(Object model, String[] urls) {
        return fromUrls(model, urls[0], urls[1]);
    }

    public Shader fromUrls(Object model, String vsUrl, String fsUrl) {
        return Shader.fromStrings(
                processUrl(model, vsUrl), vsUrl,
                processUrl(model, fsUrl), fsUrl
        );
    }
}
