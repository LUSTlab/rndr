package net.lustlab.rndr.shaders;

import net.lustlab.rndr.file.FileWatcher;

import java.io.File;
import java.net.MalformedURLException;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.Optional;

public class ShaderWatcher implements ShaderProxy {

    URL vertexURL;
    URL fragmentURL;

    FileWatcher vertexWatcher;
    FileWatcher fragmentWatcher;

    Shader shader;
    public static ShaderWatcher forUrls(String vertexURL, String fragmentURL) {
        try {
            return new ShaderWatcher(new URL(vertexURL), new URL(fragmentURL));
        } catch (MalformedURLException e) {
            throw new RuntimeException(e);
        }
    }

    public static ShaderWatcher forFiles(String vertexURL, String fragmentURL) {
        try {
            return new ShaderWatcher(new File(vertexURL).toURI().toURL(), new File(fragmentURL).toURI().toURL());
        } catch (MalformedURLException e) {
            throw new RuntimeException(e);
        }
    }

    public ShaderWatcher(URL vertexURL, URL fragmentURL) {
        this.vertexURL = vertexURL;
        this.fragmentURL = fragmentURL;

        if (vertexURL.getProtocol().equals("file") && fragmentURL.getProtocol().equals("file")) {
            try {
                vertexWatcher = new FileWatcher(new File(vertexURL.toURI()));
                fragmentWatcher = new FileWatcher(new File(fragmentURL.toURI()));
                vertexWatcher.fileModified.listen(this::reloadShader);
                fragmentWatcher.fileModified.listen(this::reloadShader);
            } catch (URISyntaxException e) {
                throw new RuntimeException(e);
            }
        } else {
            throw new RuntimeException("unsupported protocol");
        }
    }

    protected void reloadShader(FileWatcher.FileModifiedEvent event) {
        try {
            Shader candidate = Shader.fromUrls(vertexURL, fragmentURL);
            shader = candidate;
        } catch (ShaderException e) {
            throw e;
        }
    }

    @Override
    public Optional<Shader> shader() {
        fragmentWatcher.update();
        vertexWatcher.update();
        return Optional.ofNullable(shader);
    }
}
