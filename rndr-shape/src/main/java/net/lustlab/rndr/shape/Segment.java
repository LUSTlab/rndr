package net.lustlab.rndr.shape;

import net.lustlab.rndr.math.*;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Represents a single bezier segment
 */
public class Segment implements Serializable {

    public Vector2 start;
    public Vector2 end;
    public Vector2 control[];

    public Segment(Vector2 start, Vector2 end) {
        this.start = start;
        this.end = end;
        this.control = null;
    }
    public Segment(Vector2 start, Vector2 c0, Vector2 c1, Vector2 end) {
        this.start = start;
        this.end = end;
        this.control = new Vector2[] { c0, c1 };
    }
    public Segment(Vector2 start, Vector2 c0, Vector2 end) {
        this.start = start;
        this.end = end;
        this.control = new Vector2[] { c0 };
    }

    public Segment(Segment other) {
        this.start = other.start.copy();
        this.end = other.end.copy();

        if (other.control != null) {
            this.control = new Vector2[other.control.length];
            for (int i = 0; i < other.control.length; ++i) {
                this.control[i] = other.control[i].copy();
            }
        }
    }
//
//    /**
//     * Produces a quadratic bezier representing the segment
//     * @return
//     */
//    public Vector2[] quadraticBezier() {
//
//        if (control == null || control.length == 0) {
//            Vector2 cp0 = start.plus(end.minus(start).scale(1.0 / 3.0));
//            Vector2 cp1 = start.plus(end.minus(start).scale(2.0 / 3.0));
//            return new Vector2[] { start, cp0, cp1, end};
//
//        }
//        else if (control.length == 2) {
//            return new Vector2[] {
//                    start, control[0], control[1], end
//            };
//        }
//        else if (control.length == 1) {
//            Vector2 cp0 = start.plus((control[0].minus(start)).scale(2.0 / 3.0));
//            Vector2 cp1 = end.plus((control[0].minus(end)).scale(2.0 / 3.0));
//            return new Vector2[] { start, cp0, cp1, end};
//        }
//        else {
//            throw new RuntimeException("unsupported amount of control points");
//        }
//    }


    public Segment apply(Matrix44 transform) {
        Vector2 tstart = start.xy0().multiply(transform).xy();
        Vector2 tend = end.xy0().multiply(transform).xy();

        if (isLinear()) {
            return new Segment(tstart, tend);
        } else if (control.length == 1) {
            return new Segment(tstart, control[0].xy0().multiply(transform).xy(), tend);
        } else if (control.length == 2) {
            return new Segment(tstart,
                    control[0].xy0().multiply(transform).xy(),
                    control[1].xy0().multiply(transform).xy(),
                    tend);
        } else {
            throw new RuntimeException("unsupported amount of control points");
        }

    }


    public boolean isLinear() {
        return control == null || control.length == 0;
    }


    public List<Vector2> sampleAdaptive() {
        if (isLinear()) {
            List<Vector2> points = new ArrayList<>();
            points.add(start);
            points.add(end);
            return points;
        }
        else if (control.length == 1) {
            BezierQuadraticRenderer bqr = new BezierQuadraticRenderer();
            return bqr.render(start,  control[0], end);
        }
        else if (control.length == 2) {
            BezierCubicRenderer bcr = new BezierCubicRenderer();
            return bcr.render(start,  control[0], control[1], end);
        }
        else {
            throw new RuntimeException("unsupported number of control points");
        }
    }

    public List<Vector2> sampleEquidistant(int pointCount) {
        List<Vector2> pieces = sampleAdaptive();
        return ContourTools.sampleEquidistant(pieces, pointCount);
    }

    public double length() {
        if (isLinear()) {
            return this.start.minus(this.end).length();
        }
        else {
            if (control.length == 1) {
                BezierQuadraticRenderer bqr = new BezierQuadraticRenderer();
                List<Vector2> pieces = bqr.render(start,  control[0], end);
                double length = 0;
                for (int i = 0; i < pieces.size()-1 ;++i) {
                    length += pieces.get(i).minus(pieces.get(i + 1)).length();
                }
                return length;
            }
            else if (control.length == 2) {
                BezierCubicRenderer bcr = new BezierCubicRenderer();
                List<Vector2> pieces = bcr.render(start,  control[0], control[1], end);
                double length = 0;
                for (int i = 0; i < pieces.size()-1 ;++i) {
                    length += pieces.get(i).minus(pieces.get(i + 1)).length();
                }
                return length;
            }

        }
        return 0;
    }

    public Vector2 direction() {
        return start.minus(end).normalized();
    }

    public Vector2 normal(double t) {
        return direction(t).yx().scale(-1,1);
    }

    public Vector2 direction(double t) {
        if (isLinear()) {
            return direction();
        } else if (control.length == 1) {
            return Bezier.derivative(start, control[0], end, t).normalized();
        } else if (control.length == 2) {
            return Bezier.derivative(start, control[0], control[1], end, t).normalized();
        } else {
            throw new RuntimeException("not implemented");
        }
    }

    public Vector2 velocity(double t) {
        if (isLinear()) {
            return start.minus(end);
        } else if (control.length == 2) {
            return Bezier.derivative(start, control[0], end, t);
        } else if (control.length == 3) {
            return Bezier.derivative(start, control[0], control[1], end, t);
        } else {
            throw new RuntimeException("not implemented");
        }
    }

    public Segment sub(double t0, double t1) {

        // ftp://ftp.fu-berlin.de/tex/CTAN/dviware/dvisvgm/src/Bezier.cpp
        double z0 = t0;
        double z1 = t1;

        if (t0 > t1) {
            z1 = t0;
            z0 = t1;
        }

        if (z0 == 0) {
            return split(z1)[0];
        } else if (z1 == 1) {
            return split(z0)[1];
        } else {
            return split(z0)[1].split(Mapping.map(z0,1, 0, 1, z1))[0];
        }

    }


    /**
     * Splits the segment into two parts
     * @param t the t position at which to split the segment in two
     * @return
     */
    public Segment[] split(double t) {

        // based on the work by Pomax

        t = Mapping.clamp(t, 0, 1);

        if (isLinear()) {
            Vector2 cut = start.plus( (end.minus(start).scale(t)));
            return new Segment[]{ new Segment(start, cut), new Segment(cut, end) };
        } else {
            if (control.length == 2) {

                double z = t;

                double z2 = z*z;
                double z3 = z*z*z;
                double iz = 1-z;
                double iz2 = iz*iz;
                double iz3 = iz*iz*iz;

                Matrix44 lsm = new Matrix44(
                        1,      0,        0,        0,
                        iz,     z,        0,        0,
                        iz2,    2*iz*z,     z2,       0,
                        iz3,    3*iz2*z,  3*iz*z2,  z3);


                Vector4 px = new Vector4(start.x, control[0].x, control[1].x, end.x);
                Vector4 py = new Vector4(start.y, control[0].y, control[1].y, end.y);

                Vector4 plx = px.multiply(lsm);
                Vector4 ply = py.multiply(lsm);

                Vector2 pl0 = new Vector2(plx.x, ply.x);
                Vector2 pl1 = new Vector2(plx.y, ply.y);
                Vector2 pl2 = new Vector2(plx.z, ply.z);
                Vector2 pl3 = new Vector2(plx.w, ply.w);

                Segment left = new Segment(pl0, pl1, pl2, pl3);


                Matrix44 rsm = new Matrix44(
                        iz3,    3*iz2*z,    3*iz*z2,    z3,
                        0,      iz2,        2*iz*z,     z2,
                        0,      0,          iz,         z,
                        0,      0,          0,          1
                );

                Vector4 prx = px.multiply(rsm);
                Vector4 pry = py.multiply(rsm);

                Vector2 pr0 = new Vector2(prx.x, pry.x);
                Vector2 pr1 = new Vector2(prx.y, pry.y);
                Vector2 pr2 = new Vector2(prx.z, pry.z);
                Vector2 pr3 = new Vector2(prx.w, pry.w);

                Segment right = new Segment(pr0, pr1, pr2, pr3);

                return new Segment[] { left, right };


            } else if (control.length == 1) {

                double z = t;
                double iz = 1-z;
                double iz2 = iz*iz;
                double z2 = z*z;

                Matrix44 lsm = new Matrix44(
                        1,      0,      0,      0,
                        iz,     z,      0,      0,
                        iz2,    2*iz*z, z2,     0,
                        0,      0,      0,      0);

                Vector4 px = new Vector4(start.x, control[0].x, end.x, 0);
                Vector4 py = new Vector4(start.y, control[0].y, end.y, 0);

                Vector4 plx = px.multiply(lsm);
                Vector4 ply = py.multiply(lsm);

                Segment left = new Segment(
                        new Vector2(plx.x, ply.x),
                        new Vector2(plx.y, ply.y),
                        new Vector2(plx.z, ply.z));



                Matrix44 rsm = new Matrix44(
                        iz2,    2*iz*z,     z2,     0,
                        0,      iz,         z,      0,
                        0,      0,          1,      0,
                        0,      0,          0,      0);

                Vector4 prx = px.multiply(rsm);
                Vector4 pry = py.multiply(rsm);

                Segment right = new Segment(
                        new Vector2(prx.x, pry.x),
                        new Vector2(prx.y, pry.y),
                        new Vector2(prx.z, pry.z));

                return new Segment[] { left, right };

            } else {
                throw new RuntimeException("not implemented");
            }
        }
    }

    public Vector2 position(double t) {

        if (isLinear()) {
            return new Vector2(start.x * (1.0-t) + end.x * t, start.y * (1.0-t) + end.y * t);
        }
        else if (control.length == 1) {

            t = Math.min(1,Math.max(0, t));

            double it = 1.0 - t;
            double it2 = (1.0-t)*(1.0-t);
            double t2 = t * t;
            double x = (it2 * start.x) + (2*it*t * control[0].x) + (t2 * end.x);
            double y = (it2 * start.y) + (2*it*t * control[0].y) + (t2 * end.y);

            return new Vector2(x,y);
        }
        else if (control.length==2) {
            double it = 1.0-t;
            double it2 = it * it;
            double it3 = it2 * it;
            double t2 = t*t;
            double t3 = t2 * t;

            double x = it3 * start.x + 3*it2 * t * control[0].x + 3*it*t2*control[1].x + t3*end.x;
            double y = it3 * start.y + 3*it2 * t * control[0].y + 3*it*t2*control[1].y + t3*end.y;
            return new Vector2(x,y);
        }
        else {
            // TODO implement generalized beziers here
            return null;
        }
    }


    public final Segment reverse() {
        if (control == null || control.length == 0) {
            return new Segment(end, start);
        }  else if (control.length == 2) {
            return new Segment(end, control[1], control[0], start);
        } else if (control.length == 1) {
            return new Segment(end, control[0], start);
        } else {
            throw new RuntimeException("unsupported amount of control points");
        }
    }

    @Override
    public String toString() {
        return "Segment{" +
                "start=" + start +
                ", end=" + end +
                ", control=" + Arrays.toString(control) +
                '}';
    }

    public final Segment copy() {
        if (control == null || control.length == 0) {
            return new Segment(start, end);
        } else if (control.length == 1) {
            return new Segment(start, control[0], end);
        } else if (control.length == 2) {
            return new Segment(start, control[0], control[1], end);
        } else {
            throw new RuntimeException("unsupported number of control points");
        }

    }
}
