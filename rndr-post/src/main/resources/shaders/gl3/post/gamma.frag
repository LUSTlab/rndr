#version 150

in vec2 v_texCoord0;
uniform sampler2D tex0;
uniform sampler2D tex1;
uniform float gamma;

out vec4 o_color;

float rand(float n){return fract(sin(n) * 43758.5453123);}
float noise(float p){
    float fl = floor(p);
  float fc = fract(p);
    return mix(rand(fl), rand(fl + 1.0), fc);
}
float rand(vec2 n) {
    return fract(sin(dot(n, vec2(12.9898, 4.1414))) * 43758.5453);
}

float noise(vec2 n) {
    const vec2 d = vec2(0.0, 1.0);
  vec2 b = floor(n), f = smoothstep(vec2(0.0), vec2(1.0), fract(n));
    return mix(mix(rand(b), rand(b + d.yx), f.x), mix(rand(b + d.xy), rand(b + d.yy), f.x), f.y);
}
void main() {
    o_color = texture(tex0, v_texCoord0) + texture(tex1, v_texCoord0);
    vec2 p = vec2(gl_FragCoord.x, gl_FragCoord.y);
    o_color.rgb = pow(o_color.rgb, vec3(noise(p)*0.7+0.75));
    o_color.a = 1.0;
}