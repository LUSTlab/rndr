package net.lustlab.rndr.math;

import static net.lustlab.rndr.math.Linear.multiply;


public class Projection {


    public static Vector3 unproject(Vector3 point, Matrix44 projection, Matrix44 view, int width, int height) {
        Matrix44 ipm = multiply(projection, view).inverse();
        Vector3 v = new Vector3( (2 * point.x)  / width -1, (2 * point.y) / height -1,2 *point.z-1  );
        return multiply(ipm, v);
    }

    public static Vector3 project(Vector3 point, Matrix44 projection, Matrix44 view, int width, int height) {
        Vector4 homo = new Vector4(point.x, point.y, point.z, 1);
        Vector4 projectedHomo = homo.multiply(multiply(projection, view));

        // Homogeneous division
        double rhw = 1 / projectedHomo.w;

        return new Vector3((projectedHomo.x * rhw + 1) * width / 2,
                (1 - projectedHomo.y * rhw) * height / 2,
                rhw);
    }
}
