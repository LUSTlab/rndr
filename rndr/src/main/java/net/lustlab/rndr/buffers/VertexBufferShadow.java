package net.lustlab.rndr.buffers;

import java.nio.ByteBuffer;

public class VertexBufferShadow {

    final VertexBuffer vertexBuffer;
    final ByteBuffer data;

    public VertexBufferShadow(VertexBuffer vertexBuffer) {
        this.vertexBuffer = vertexBuffer;
        data = ByteBuffer.allocateDirect((int) (vertexBuffer.format().size() * vertexBuffer.vertexCount()));
    }

    public VertexBufferShadow download() {
        throw new RuntimeException("not implemented");
    }

    public VertexBufferShadow upload() {
        data.rewind();
        vertexBuffer.write(data);
        data.rewind();
        return this;
    }

    public VertexBufferShadow upload(int elementOffset, int elementCount) {
        data.rewind();
        int elementSize = vertexBuffer.format().size();
        data.position(elementSize * elementOffset);
        vertexBuffer.write(data, elementOffset, elementSize*elementCount);
        data.rewind();
        return this;
    }


    public BufferWriter writer() {
        return new BufferWriter(data);
    }

    public ByteBuffer buffer() {
        return data;
    }

}
