package net.lustlab.rndr.video;

import com.jogamp.opengl.GL3;
import net.lustlab.rndr.RNDRException;
import net.lustlab.rndr.buffers.ColorBuffer;
import net.lustlab.rndr.video.profiles.MP4Profile;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.File;
import java.io.IOException;
import java.io.OutputStream;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Video writer
 */
public class VideoWriter {


    final static Logger logger = LogManager.getLogger(VideoWriter.class);

    File ffmpegOutput = new File("ffmpegOutput.txt");

    int frameRate = 25;
    int width = -1;
    int height = -1;

    String filename = "rndr.mp4";

    byte[] frameBufferArray;
    ByteBuffer frameBuffer;
    private Process ffmpeg;
    private OutputStream movieStream;

    private VideoWriterProfile profile = new MP4Profile();

    public VideoWriter profile(VideoWriterProfile profile) {
        this.profile = profile;
        return this;
    }

    public static VideoWriter create() {
        return new VideoWriter();
    }

    public VideoWriter() {

    }

    /**
     * Set the video width, should be set before calling start()
     *
     * @param width the width in pixels
     */
    @Deprecated
    public VideoWriter width(int width) {
        this.width = width;
        return this;
    }

    public int width() {
        return width;
    }

    public int height() {
        return height;
    }

    /**
     * Set the video height, should be set before calling start()
     *
     * @param height the height in pixels
     */
    @Deprecated
    public VideoWriter height(int height) {
        this.height = height;
        return this;
    }


    public VideoWriter size(int width, int height) {
        this.width = width;
        this.height = height;
        return this;
    }


    /**
     * Set the output file, should be set before calling start()
     *
     * @param filename the filename of the output file
     */
    public VideoWriter output(String filename) {
        this.filename = filename;
        return this;
    }


    /**
     * Sets the framerate of the output video
     *
     * @param frameRate the frame rate in frames per second
     * @return this
     */
    public VideoWriter frameRate(int frameRate) {
        this.frameRate = frameRate;
        return this;
    }

    /**
     * Start writing to the video file
     */
    public VideoWriter start() {

        if (filename == null) {
            throw new RNDRException("output not set");
        }

        if (width <= 0) {
            throw new RNDRException("invalid width or width not set");
        }
        if (height <= 0) {
            throw new RNDRException("invalid height or height not set");
        }


        frameBufferArray = new byte[width * height * 4];
        frameBuffer = ByteBuffer.wrap(frameBufferArray);

        ProcessBuilder pb = null;

        String format = "rgba";


        String[] preamble = {
                "-y",
                "-f", "rawvideo",
                "-vcodec", "rawvideo",

                "-s", String.format("%dx%d", width, height),
                "-pix_fmt", format,
                "-r", "" + frameRate,
                "-i", "-",
                "-vf", "vflip",
        };

        String[] codec = profile.arguments();


        List<String> arguments = new ArrayList<>();

        if (System.getProperty("os.name").contains("Windows")) {
            arguments.add("ffmpeg.exe");
        } else {
            arguments.add("/usr/local/bin/ffmpeg");
        }
        arguments.addAll(Arrays.asList(preamble));
        arguments.addAll(Arrays.asList(codec));



        arguments.add(filename);

        pb = new ProcessBuilder().command(arguments.toArray(new String[0]));
        pb.redirectErrorStream(true);
        pb.redirectOutput(ffmpegOutput);

        try {
            ffmpeg = pb.start();
        } catch (IOException e) {
            throw new RNDRException("failed to launch ffmpeg", e);
        }

        movieStream = ffmpeg.getOutputStream();

        return this;
    }

    /**
     * Feed a frame to the video encoder
     *
     * @param frame a ColorBuffer (RGBA, 8bit) holding the image data to be written to the video. The ColorBuffer should have the same resolution as the VideoWriter.
     */
    public VideoWriter frame(ColorBuffer frame) {

        if (!(frame.width() == width) && (frame.height() == height)) {
            throw new RNDRException("frame size mismatch");
        } else {
            frameBuffer.rewind();
            frameBuffer.order(ByteOrder.nativeOrder());
            frame.read(frameBuffer, GL3.GL_UNSIGNED_BYTE);

            try {
                movieStream.write(frameBufferArray);
                movieStream.flush();

            } catch (IOException e) {
                e.printStackTrace();
                throw new RNDRException("failed to write frame", e);
            }
        }

        return this;
    }

    /**
     * Stop writing to the video file. This closes the video, after calling stop() it is no longer possible to provide new frames.
     */
    public VideoWriter stop() {
        try {
            movieStream.close();
            try {
                logger.info("waiting for ffmpeg to finish");
                ffmpeg.waitFor();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        } catch (IOException e) {
            throw new RNDRException("failed to close the movie stream");
        }
        return this;
    }


}
