package net.lustlab.rndr.buffers;

import com.jogamp.opengl.GL;
import com.jogamp.opengl.GL3;
import com.jogamp.opengl.GLContext;
import com.jogamp.opengl.util.texture.TextureData;
import com.jogamp.opengl.util.texture.TextureIO;
import net.lustlab.rndr.RNDRException;
import net.lustlab.rndr.gl.GLError;

import java.net.URL;
import java.nio.Buffer;

public class CubeMap {

    int texture = 0;
    public final ColorFormat format;
    public final ColorType type;
    int size;

    public CubeMap(GL gl, int texture, int size, ColorFormat format, ColorType type) {
        this.texture = texture;
        this.size = size;
        this.format = format;
        this.type = type;
    }

    public static enum Side {
        NEGATIVE_X,
        POSITIVE_X,
        NEGATIVE_Y,
        POSITIVE_Y,
        NEGATIVE_Z,
        POSITIVE_Z,
    }

    public CubeMap filter(MinifyingFilter min, MagnifyingFilter mag) {
        bind();
        GL gl = GLContext.getCurrentGL();
        gl.glTexParameteri(GL.GL_TEXTURE_CUBE_MAP, GL.GL_TEXTURE_MIN_FILTER, min.toGLFilter());
        gl.glTexParameteri(GL.GL_TEXTURE_CUBE_MAP, GL.GL_TEXTURE_MAG_FILTER, mag.toGLFilter());
        return this;
    }


    public ColorBuffer side(Side side) {
        switch (side) {
            case NEGATIVE_X:
                return new ColorBuffer(GLContext.getCurrentGL(), GL.GL_TEXTURE_CUBE_MAP_NEGATIVE_X, texture, size, size, format, type, 1);
            case POSITIVE_X:
                return new ColorBuffer(GLContext.getCurrentGL(), GL.GL_TEXTURE_CUBE_MAP_POSITIVE_X, texture, size, size, format, type, 1);
            case NEGATIVE_Y:
                return new ColorBuffer(GLContext.getCurrentGL(), GL.GL_TEXTURE_CUBE_MAP_NEGATIVE_Y, texture, size, size, format, type, 1);
            case POSITIVE_Y:
                return new ColorBuffer(GLContext.getCurrentGL(), GL.GL_TEXTURE_CUBE_MAP_POSITIVE_Y, texture, size, size, format, type, 1);
            case NEGATIVE_Z:
                return new ColorBuffer(GLContext.getCurrentGL(), GL.GL_TEXTURE_CUBE_MAP_NEGATIVE_Z, texture, size, size, format, type, 1);
            case POSITIVE_Z:
                return new ColorBuffer(GLContext.getCurrentGL(), GL.GL_TEXTURE_CUBE_MAP_POSITIVE_Z, texture, size, size, format, type, 1);
            default:
                throw new RuntimeException("unsupported side" + side);
        }

    }

    public static CubeMap create(int size, ColorFormat format, ColorType type) {
        GL gl = GLContext.getCurrentGL();
        int textures[] = new int[1];

        gl.glGenTextures(1, textures, 0);
        gl.glActiveTexture(GL.GL_TEXTURE0);
        gl.glBindTexture(GL.GL_TEXTURE_CUBE_MAP, textures[0]);

        for (int i = 0; i < 6; ++i) {
            gl.glTexImage2D(GL.GL_TEXTURE_CUBE_MAP_POSITIVE_X+i, 0, ColorBuffer.toGLFormat(format, type), size, size, 0, GL.GL_RGBA, GL.GL_UNSIGNED_BYTE, null);
        }

        return new CubeMap(gl, textures[0], size, format, type);
    }

    public static CubeMap fromUrls(String... urls) {
        URL[] realUrls = new  URL[urls.length];
        try {
            for (int i = 0; i < urls.length; ++i) {
                realUrls[i] = new URL(urls[i]);
            }
        } catch (Exception e) {
            throw new RNDRException(e);
        }
        return fromUrls(realUrls);
    }

    public static CubeMap fromUrls(URL... urls) {
        GL gl = GLContext.getCurrentGL();
        int textures[] = new int[1];

        gl.glGenTextures(1, textures, 0);
        gl.glActiveTexture(GL.GL_TEXTURE0);
        gl.glBindTexture(GL.GL_TEXTURE_CUBE_MAP, textures[0]);
        int size = 0;

        try {
            for (int i = 0, urlsLength = urls.length; i < urlsLength; i++) {
                URL url = urls[i];
                int lastIndex = urls[i].toExternalForm().lastIndexOf(".");

                String extension = url.toExternalForm().substring(lastIndex+1);
                System.out.println(extension);
                TextureData data = TextureIO.newTextureData(gl.getGLProfile(), url.openStream(), true, extension);

                data.setHaveEXTABGR(true);
                Buffer buffer = data.getBuffer();
                if (buffer != null) {
                    gl.glTexImage2D(GL.GL_TEXTURE_CUBE_MAP_POSITIVE_X + i, 0, data.getPixelFormat(), data.getWidth(), data.getHeight(), 0, data.getPixelFormat(), data.getPixelType(), buffer);

                }
                else {
                    Buffer[] mipmapData = data.getMipmapData();


                    for (int i1 = 0; i1 < mipmapData.length; i1++) {
                        Buffer mbuffer = mipmapData[i1];

                        int internalFormat = data.getInternalFormat();
                        if (data.getInternalFormat() == 0) {
                            internalFormat = GL3.GL_RGBA;
                        }
                        gl.glTexImage2D(GL.GL_TEXTURE_CUBE_MAP_POSITIVE_X + i, i1, internalFormat, data.getWidth()>>i1, data.getHeight()>>i1, 0, GL3.GL_BGRA, data.getPixelType(), mbuffer);
                    }


                }

//                if (buffer != null) {
//                    gl.glGenerateMipmap(GL.GL_TEXTURE_CUBE_MAP);
//                }

                GLError.check(gl);

            }
        } catch (Exception e) {
            throw new RuntimeException(e);
        }

        gl.glGenerateMipmap(GL.GL_TEXTURE_CUBE_MAP);
        GLError.check(gl);
        return new CubeMap(gl, textures[0], size, ColorFormat.RGBA, ColorType.UINT8);
    }

    public CubeMap bind() {
        GL gl = GLContext.getCurrentGL();
        gl.glBindTexture(GL.GL_TEXTURE_CUBE_MAP, texture);
        GLError.check(gl);
        return this;
    }
}
