package net.lustlab.rndr.video.profiles;

import net.lustlab.rndr.video.VideoWriterProfile;


public class MP4Profile extends VideoWriterProfile<MP4Profile> {

    public enum WriterMode {
        Normal,
        Lossless,

    }
    WriterMode mode = WriterMode.Normal;
    int constantRateFactor = 23;

    public MP4Profile mode(WriterMode mode) {
        this.mode = mode;
        return this;
    }

    /**
     * Sets the constant rate factor
     * @param constantRateFactor the constant rate factor (default is 23)
     * @return
     */
    public MP4Profile constantRateFactor(int constantRateFactor) {
        this.constantRateFactor = constantRateFactor;
        return this;
    }

    @Override
    public String[] arguments() {
        if (mode == WriterMode.Normal) {

            return new String[] {
                    "-pix_fmt", "yuv420p", // this will produce videos that are playable by quicktime
                    "-an",
                    "-vcodec", "libx264",
                    "-crf", "" + constantRateFactor
            };
        } else if (mode == WriterMode.Lossless) {
            System.out.println("lossless mode");
            return new String[]{
                    "-pix_fmt", "yuv420p", // this will produce videos that are playable by quicktime
                    "-an",
                    "-vcodec", "libx264",
                    "-preset", "ultrafast"
            };
        }
        else {
            throw new RuntimeException("unsupported write mode");
        }
    }


}
