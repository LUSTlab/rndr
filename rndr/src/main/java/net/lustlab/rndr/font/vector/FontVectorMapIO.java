package net.lustlab.rndr.font.vector;

import net.lustlab.rndr.RNDRException;
import net.lustlab.rndr.font.image.FontImageMap;

import java.io.*;

/**
 * Created by voorbeeld on 12/19/13.
 */
public class FontVectorMapIO {
    public static void save(FontVectorMap fontVectorMap, String filename) {
        try {
            File file = new File(filename);
            FileOutputStream fos = new FileOutputStream(file);
            ObjectOutputStream oos = new ObjectOutputStream(fos);
            oos.writeObject(fontVectorMap);

        } catch (IOException e) {
            throw new RNDRException(e);
        }
    }

    public static FontVectorMap load(String filename) {
        try {
            File file = new File(filename);
            FileInputStream fis = new FileInputStream(file);
            ObjectInputStream ois = new ObjectInputStream(fis);
            FontVectorMap fm = (FontVectorMap) ois.readObject();
            return fm;
        } catch (ClassNotFoundException | IOException e) {
            throw new RNDRException(e);
        }
    }}
