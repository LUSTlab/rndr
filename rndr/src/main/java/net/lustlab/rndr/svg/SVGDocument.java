package net.lustlab.rndr.svg;

import net.lustlab.rndr.RNDRException;
import net.lustlab.rndr.shape.Composition;
import net.lustlab.rndr.shape.CompositionNode;
import net.lustlab.rndr.shape.ImageReference;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

public class SVGDocument {


    public String url;
    public static SVGDocument fromUrl(URL url) {
        try {
            return SVGLoader.loadSVG(url.toString(), url.openStream());
        } catch (IOException e) {
            throw new RNDRException(e);
        }
    }

    public static SVGDocument fromUrl(String url) {
        try {
            return fromUrl(new URL(url));
        } catch (MalformedURLException e) {
            throw new RNDRException(e);
        }
    }


    public List<Group> groups = new ArrayList<>();

    /**
     * Generates a Composition from the SVGDocument
     * @return A full Composition object containing Shapes
     */
	public Composition composition() {
		Composition composition = new Composition();
		for (Group group: groups) {
            CompositionNode n = new CompositionNode();
            n.id = group.id;
            composition.root.append(n);
			composeGroup(n, group);
		}
		return composition;
	}

	private void composeGroup(CompositionNode node, Group group) {
		for (Element element:group.elements) {

			if (element instanceof Group) {
                CompositionNode g = new CompositionNode();
				g.transform = element.transform;

                g.id = element.id;
                node.append(g);
                composeGroup(g, (Group) element);
			}
			if (element instanceof Path) {
                CompositionNode s = new CompositionNode();
                s.transform = element.transform;
                s.id = element.id;
                node.append(s);
                s.append(((Path) element).shape());
			}
			if (element instanceof Image) {
                CompositionNode s = new CompositionNode();
                s.transform = element.transform;
                s.id = element.id;
                node.append(s);
                ImageReference ir = new ImageReference();
                ir.width = ((Image)element).width;
                ir.height = ((Image)element).height;
                ir.url = ((Image)element).url;

                s.image(ir);


            }
		}
	}

}
