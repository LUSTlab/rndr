#version 150

in vec2 v_texCoord0;
uniform sampler2D tex0;
uniform sampler2D tex1;
uniform float replacement;
out vec4 o_color;
void main() {
    o_color = texture(tex0, v_texCoord0);
    o_color.a = replacement;

}