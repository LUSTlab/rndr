package net.lustlab.rndr.application.internal;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.File;

public class GenericPlatformDriver implements PlatformDriver{

    public static Logger logger = LogManager.getLogger(GenericPlatformDriver.class);

    public static String randomID;

    public GenericPlatformDriver() {
        logger.debug("instantiating generic platform driver");

        char[] alphabet = new char[] { 'a','b','d','e','f','g','h','i','j','k','l','m','n','o','p','q','r','s','t','u','v','w','x','y','z'};

        String id = "";
        for (int i = 0; i < 8; ++i) {
            id += alphabet[i];
        }

        randomID = id;
        logger.debug("random id is {} ", new Object[] { randomID});
    }

    @Override
    public File temporaryDirectory() {
        String directoryName = System.getProperty("java.io.tmpdir") + "RNDR-"+randomID;
        logger.debug("using temporary directory: " + directoryName);
        File file = new File(directoryName);
        if (!file.exists()) {
            file.mkdirs();
        }

        return file;
    }


    @Override
    public File cacheDirectory(String programName) {
        File f = new File("./cache");
        if (f.exists()) {
            boolean result = f.mkdirs();
            if (!result) {
                throw new RuntimeException("could not create cache directory");
            }
        }
        return f;
    }

    @Override
    public File supportDirectory(String programName) {
        return new File(".");
    }

}
