package net.lustlab.rndr.draw;

import com.jogamp.opengl.GL;
import freemarker.ext.beans.HashAdapter;
import net.lustlab.colorlib.ColorRGBa;
import net.lustlab.rndr.buffers.*;
import net.lustlab.rndr.math.Matrix44;
import net.lustlab.rndr.math.Vector2;
import net.lustlab.rndr.math.Vector3;
import net.lustlab.rndr.math.Vector4;
import org.yaml.snakeyaml.Yaml;

import java.io.IOException;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;

public class ShadeStyle {
    public String fillTransform;
    public String strokeTransform;
    public String tintTransform;
    public String positionTransform;
    public String preamble;

    public Map<String, Object> parameterValues = new HashMap<>();
    public Map<String, String> parameters = new HashMap<>();
    public Map<String, Integer> outputs = new HashMap<>();

    public ShadeStyle() {

    }

    public ShadeStyle(ShadeStyle other) {
        this.fillTransform = other.fillTransform;
        this.strokeTransform = other.strokeTransform;
        this.tintTransform = other.tintTransform;
        this.positionTransform = other.positionTransform;
        this.preamble = other.preamble;
        // shouldn't these be copied?
        this.parameterValues = other.parameterValues;
        this.parameters = other.parameters;
        this.outputs = other.outputs;
    }

    @Override
    public String toString() {
        return "ShadeStyle{" +
                "fillTransform='" + fillTransform + '\'' +
                ", strokeTransform='" + strokeTransform + '\'' +
                ", tintTransform='" + tintTransform + '\'' +
                ", positionTransform='" + positionTransform + '\'' +
                ", preamble='" + preamble + '\'' +
                ", parameters=" + parameterValues +
                ", outputs=" + outputs +
                '}';
    }

    public static ShadeStyle fromUrl(String url) {
        if (url.endsWith(".yaml")) {
            Yaml yaml = new Yaml();
            try {
                ShadeStyle shadeStyle = yaml.loadAs(new URL(url).openStream(), ShadeStyle.class);
                return shadeStyle;
            } catch (IOException e) {
                throw new RuntimeException("error loading shadestyle from " + url, e);
            }
        } else {
            throw new RuntimeException("only yaml is supported");
        }
    }

    public ShadeStyle position(String positionTransform) {
        this.positionTransform = positionTransform;
        return this;
    }

    public ShadeStyle fill(String fillTransform) {
        this.fillTransform = fillTransform;
        return this;
    }

    public ShadeStyle stroke(String strokeTransform) {
        this.strokeTransform = strokeTransform;
        return this;
    }

    public ShadeStyle tint(String tintTransform) {
        this.tintTransform = tintTransform;
        return this;
    }

    public ShadeStyle parameter(String name, Matrix44 value) {
        parameterValues.put(name, value);
        parameters.put(name, "Matrix44");
        return this;
    }


    public ShadeStyle parameter(String name, float value) {
        parameterValues.put(name, value);
        parameters.put(name, "float");
        return this;
    }

    public ShadeStyle parameter(String name, BufferTexture value) {
        parameterValues.put(name, value);
        parameters.put(name, "BufferTexture");
        return this;
    }

    public ShadeStyle parameter(String name, double value) {
        parameterValues.put(name, (float) value);
        parameters.put(name, "float");
        return this;
    }


    public ShadeStyle parameter(String name, Vector2 value) {
        parameterValues.put(name, value);
        parameters.put(name, "Vector2");
        return this;
    }

    public ShadeStyle parameter(String name, Vector3 value) {
        parameterValues.put(name, value);
        parameters.put(name, "Vector3");
        return this;
    }

    public ShadeStyle parameter(String name, Vector4 value) {
        parameterValues.put(name, value);
        parameters.put(name, "Vector4");
        return this;
    }

    public ShadeStyle parameter(String name, ColorRGBa value) {
        parameterValues.put(name, value);
        parameters.put(name, "ColorRGBa");
        return this;
    }

    public ShadeStyle parameter(String name, ColorBuffer value) {
        parameterValues.put(name, value);
        parameters.put(name, "ColorBuffer");
        return this;
    }

    public ShadeStyle output(String name, int slot) {
        outputs.put(name, slot);
        return this;
    }

    public ShadeStyle outputs(Map<String, Integer> outputs) {
        this.outputs.putAll(outputs);
        return this;
    }


    public static class Structure {
        @Override
        public String toString() {
            return "Structure{" +
                    "fillTransform='" + fillTransform + '\'' +
                    ", strokeTransform='" + strokeTransform + '\'' +
                    ", tintTransform='" + tintTransform + '\'' +
                    ", positionTransform='" + positionTransform + '\'' +
                    ", uniforms='" + uniforms + '\'' +
                    ", attributes='" + attributes + '\'' +
                    ", preamble='" + preamble + '\'' +
                    ", outputs='" + outputs + '\'' +
                    ", varyingOut='" + varyingOut + '\'' +
                    ", varyingIn='" + varyingIn + '\'' +
                    ", varyingBridge='" + varyingBridge + '\'' +
                    '}';
        }

        public String fillTransform;
        public String strokeTransform;
        public String tintTransform;
        public String positionTransform;
        public String uniforms;
        public String attributes;
        public String preamble;
        public String outputs;

        public String varyingOut;
        public String varyingIn;
        public String varyingBridge;

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;

            Structure structure = (Structure) o;

            if (fillTransform != null ? !fillTransform.equals(structure.fillTransform) : structure.fillTransform != null)
                return false;
            if (strokeTransform != null ? !strokeTransform.equals(structure.strokeTransform) : structure.strokeTransform != null)
                return false;
            if (tintTransform != null ? !tintTransform.equals(structure.tintTransform) : structure.tintTransform != null)
                return false;
            if (positionTransform != null ? !positionTransform.equals(structure.positionTransform) : structure.positionTransform != null)
                return false;
            if (uniforms != null ? !uniforms.equals(structure.uniforms) : structure.uniforms != null) return false;
            if (attributes != null ? !attributes.equals(structure.attributes) : structure.attributes != null)
                return false;
            if (preamble != null ? !preamble.equals(structure.preamble) : structure.preamble != null) return false;
            if (outputs != null ? !outputs.equals(structure.outputs) : structure.outputs != null) return false;
            if (varyingOut != null ? !varyingOut.equals(structure.varyingOut) : structure.varyingOut != null)
                return false;
            if (varyingIn != null ? !varyingIn.equals(structure.varyingIn) : structure.varyingIn != null) return false;
            return varyingBridge != null ? varyingBridge.equals(structure.varyingBridge) : structure.varyingBridge == null;
        }

        @Override
        public int hashCode() {
            int result = fillTransform != null ? fillTransform.hashCode() : 0;
            result = 31 * result + (strokeTransform != null ? strokeTransform.hashCode() : 0);
            result = 31 * result + (tintTransform != null ? tintTransform.hashCode() : 0);
            result = 31 * result + (positionTransform != null ? positionTransform.hashCode() : 0);
            result = 31 * result + (uniforms != null ? uniforms.hashCode() : 0);
            result = 31 * result + (attributes != null ? attributes.hashCode() : 0);
            result = 31 * result + (preamble != null ? preamble.hashCode() : 0);
            result = 31 * result + (outputs != null ? outputs.hashCode() : 0);
            result = 31 * result + (varyingOut != null ? varyingOut.hashCode() : 0);
            result = 31 * result + (varyingIn != null ? varyingIn.hashCode() : 0);
            result = 31 * result + (varyingBridge != null ? varyingBridge.hashCode() : 0);
            return result;
        }

        public void varyingBridgeFromFormat(VertexFormat format) {
            varyingBridge = "";
            for (VertexElement e: format.items()) {
                String line =  "v_" + e.attribute + " = " + e.attribute + ";\n";
                varyingBridge = varyingBridge + line;
            }
        }

        public void varyingInFromFormat(VertexFormat format) {
            varyingIn = "";
            for (VertexElement e: format.items()) {
                String line = "in";
                String type = glslType(e);
                line = line + " " + type + " " + "v_" + e.attribute + ";\n";
                varyingIn = varyingIn + line;
            }
        }

        public void varyingOutFromFormat(VertexFormat format) {
            varyingOut = "";
            for (VertexElement e: format.items()) {
                String line = "out";
                String type = glslType(e);
                line = line + " " + type + " " + "v_" + e.attribute + ";\n";
                varyingOut = varyingOut + line;
            }
        }


        private String glslType(VertexElement e) {
            String type = "";
            if (e.type == GL.GL_FLOAT) {
                switch (e.count) {
                    case 1: type = "float"; break;
                    case 2: type = "vec2"; break;
                    case 3: type = "vec3"; break;
                    case 4: type = "vec4"; break;
                    default:
                        throw new RuntimeException("unsupported type" + e.count);
                }
            }
            return type;
        }

        public void attributesFromFormat(VertexFormat format) {
            attributes = "";
            for (VertexElement e: format.items()) {
                String line = "in";
                String type = glslType(e);
                line = line + " " + type + " " + e.attribute + ";\n";
                attributes = attributes + line;
            }
        }
    }

    public Structure structural(VertexFormat format) {
        Structure structure = new Structure();
        structure.attributesFromFormat(format);

        String uniforms = "";

        for (Map.Entry<String, String> e : parameters.entrySet()) {
            switch(e.getValue()) {

                case "Matrix44": uniforms+="uniform mat4 p_" + e.getKey() + ";\n"; break;
                case "float": uniforms+="uniform float p_" + e.getKey() +";\n"; break;
                case "Vector2": uniforms+="uniform vec2 p_" + e.getKey() +";\n"; break;
                case "Vector3": uniforms+="uniform vec3 p_" + e.getKey() +";\n"; break;
                case "Vector4": uniforms+="uniform vec4 p_" + e.getKey() +";\n"; break;
                case "ColorRGBa": uniforms+="uniform vec4 p_" + e.getKey() +";\n"; break;
                case "BufferTexture": uniforms+="uniform samplerBuffer p_" + e.getKey() + ";\n"; break;
                case "ColorBuffer": uniforms += "uniform sampler2D p_" + e.getKey() + ";\n";break;
                default:
                    throw new RuntimeException("unsupported type");
            }
        }

        /*
        for (Map.Entry<String, Object> e: parameterValues.entrySet()) {
            if (e.getValue() instanceof Float) {
                uniforms +=  "uniform float p_" + e.getKey() +";\n";
            }
            if (e.getValue() instanceof Vector2) {
                uniforms +=  "uniform vec2 p_" + e.getKey() +";\n";
            }
            if (e.getValue() instanceof Vector3) {
                uniforms +=  "uniform vec3 p_" + e.getKey() +";\n";
            }
            if (e.getValue() instanceof Vector4) {
                uniforms +=  "uniform vec4 p_" + e.getKey() +";\n";
            }
            if (e.getValue() instanceof ColorRGBa) {
                uniforms +=  "uniform vec4 p_" + e.getKey() +";\n";
            }
            if (e.getValue() instanceof ColorBuffer) {
                uniforms += "uniform sampler2D p_" + e.getKey() + ";\n";
            }
            if (e.getValue() instanceof BufferTexture) {
                uniforms += "uniform samplerBuffer p_" + e.getKey() + ";\n";
            }
        }
        */
        structure.outputs = "";
        for (Map.Entry<String, Integer> e: outputs.entrySet()) {
            structure.outputs += String.format("layout(location = %d) out vec4 o_%s;\n", e.getValue(), e.getKey());
        }

        structure.uniforms = uniforms;
        structure.fillTransform = fillTransform;
        structure.strokeTransform = strokeTransform;
        structure.tintTransform = tintTransform;
        structure.positionTransform = positionTransform;
        structure.preamble = preamble;



        return structure;
    }

    private static String isolate(String code) {
        if (code.startsWith("{") && code.endsWith("}")) {
            return code;
        } else {
            return "{" + code + "}";
        }


    }
    private static String concat(String left, String right) {

        if (left == null && right == null) {
            return null;
        } else if (left == null && right != null) {
            return right;
        } else if (left != null && right == null) {
            return left;
        } else {
            return isolate(left) +"\n" + isolate(right);
        }

    }


    final public ShadeStyle plus(final ShadeStyle other) {
        ShadeStyle s = new ShadeStyle();

        s.fillTransform = concat(fillTransform, other.fillTransform);
        s.strokeTransform = concat(strokeTransform, other.strokeTransform);
        s.tintTransform = concat(tintTransform, other.tintTransform);
        s.positionTransform = concat(positionTransform, other.positionTransform);
        s.preamble = (preamble==null?"":preamble) +"\n" + (other.preamble==null?"":other.preamble);

        s.parameters = new HashMap<>();
        s.parameters.putAll(parameters);
        s.parameters.putAll(other.parameters);

        s.parameterValues = new HashMap<>();
        s.parameterValues.putAll(parameterValues);
        s.parameterValues.putAll(other.parameterValues);

        s.outputs = new HashMap<>();
        s.outputs.putAll(outputs);
        s.outputs.putAll(other.outputs);

        return s;
    }

}
