#version 330

in vec3 position;

${uniforms! }

uniform mat4 modelViewMatrix;
uniform mat4 modelViewProjectionMatrix;
uniform mat4 normalMatrix;
uniform vec4 bounds;

out vec3 v_boundsPosition;
out vec3 v_modelPosition;

void main(void) {

    // -- shade style support variables
    int instance = 0;

    v_boundsPosition = vec3((position.xy - bounds.xy) / bounds.zw, 0) * 2 - vec3(1,1,0);


    vec3 x_position = position;
    { ${positionTransform! } }
    v_modelPosition = x_position;

    gl_Position = modelViewProjectionMatrix * vec4(x_position.xyz, 1.0);
}