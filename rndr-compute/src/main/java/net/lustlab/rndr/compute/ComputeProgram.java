package net.lustlab.rndr.compute;

import com.jogamp.opencl.CLException;
import com.jogamp.opencl.CLProgram;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;

public class ComputeProgram {

    CLProgram program;
    Map<String, Kernel> kernels = new HashMap<>();
    public ComputeProgram(CLProgram program) {
        this.program = program;
    }
    public static ComputeProgram fromUrl(Computer computer, URL url) {
        CLProgram program = null;
        try (InputStream stream = url.openStream()){
            program = computer.context.createProgram(stream);
            program.build();


            return new ComputeProgram(program);
        } catch (IOException e) {
            throw new RuntimeException(e);
        } catch (CLException.CLBuildProgramFailureException e) {
            if (program != null) {
                System.out.println(program.getBuildLog());
                System.out.println(program.getBuildLog(computer.device));

            }
            throw new RuntimeException(e);

        }
    }

    public Kernel kernel(String name) {
        return kernels.computeIfAbsent(name, k-> new Kernel(program.createCLKernel(k)) );
    }
}


