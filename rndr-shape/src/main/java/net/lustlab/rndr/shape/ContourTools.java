package net.lustlab.rndr.shape;

import net.lustlab.rndr.math.Vector2;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by voorbeeld on 6/8/14.
 */
public class ContourTools {

    public static List<Vector2> sampleEquidistant(List<Vector2> pieces, int count) {



        List<Vector2> result = new ArrayList<>();

        if (pieces.size() == 0) {
            return result;
        }

        double totalLength = 0;
        for (int i = 0; i < pieces.size()-1 ;++i) {
            totalLength += pieces.get(i).minus(pieces.get(i + 1)).length();
        }

        double spacing = totalLength / (count);

        double runLength = 0.0;
        Vector2 cursor;
        result.add(new Vector2(pieces.get(0).x, pieces.get(0).y));


        for (int i = 0; i < pieces.size()-1 ;++i) {

            Vector2 piece = pieces.get(i).minus(pieces.get(i + 1));
            double pieceLength = piece.length();

            if ((pieceLength+runLength) < spacing) {
                runLength+=pieceLength;
            }
            else {
                double skip = (spacing - runLength);

                if (skip < 0) {
                    throw new RuntimeException("skip < 0");
                }


                int newPieces =  (int) ((pieceLength-skip) / spacing);

                double skipT = skip / pieceLength;
                double spaceT = spacing / pieceLength;

                Vector2 direction = pieces.get(i+1).minus(pieces.get(i));
                Vector2 start = pieces.get(i);

                double t = skipT;

                for (int n = 0; n < 1+newPieces; ++n) {
                    cursor = start.plus(direction.scale(t));
                    t+=spaceT;
                    result.add(new Vector2(cursor));
                }

                runLength = (pieceLength-skip) - (newPieces * spacing);

            }


        }

        //if (result.size() < count) {
            result.add(pieces.get(pieces.size()-1));
        //}

        return result;
    }
}
