package net.lustlab.rndr.draw.stroke;

import com.jogamp.opengl.GL;
import com.jogamp.opengl.GL2GL3;
import com.jogamp.opengl.GLContext;
import net.lustlab.rndr.buffers.BufferWriter;
import net.lustlab.rndr.buffers.VertexBuffer;
import net.lustlab.rndr.buffers.VertexBufferDrawer;
import net.lustlab.rndr.buffers.VertexFormat;
import net.lustlab.rndr.draw.DrawContext;
import net.lustlab.rndr.draw.DrawStyle;
import net.lustlab.rndr.draw.gl3.StateTools;
import net.lustlab.rndr.shaders.ShadeStyleManager;
import net.lustlab.rndr.shaders.Shader;

import java.nio.FloatBuffer;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class ExpansionRenderer {

    VertexBuffer vbo;


    ShadeStyleManager shadeStyleManager = new ShadeStyleManager("cp:shaders/gl3/stroke.vert", "cp:shaders/gl3/stroke.frag");


    Shader fillShader = Shader.fromUrls(
            "cp:shaders/gl3/stroke.vert",
            "cp:shaders/gl3/fill.frag"
    );


    FloatBuffer quad = FloatBuffer.allocate(5*4*6);

    public Command toCommand(VertexBuffer vbo, Expansion expansion, int offset) {
        Command command =new Command(vbo, expansion.type, offset, expansion.vertexCount(),
                (float) expansion.minx, (float)expansion.maxx, (float)expansion.miny, (float)expansion.maxy);

        expansion.fb.position(expansion.bufferPosition);
        vbo.write(expansion.fb, offset, expansion.vertexCount() * 5 );
        return command;
    }

    public List<Command> toCommands(VertexBuffer vbo, List<Expansion> expansions, int offset) {
        int o = offset;
        List<Command> commands = new ArrayList<>();
        for (Expansion expansion: expansions) {
            Command command = toCommand(vbo, expansion, o);
            commands.add(command);
            o += command.vertexCount * 5 * 4;
        }
        return commands;

    }

    public ExpansionRenderer() {
        vbo = VertexBuffer.createDynamic(new VertexFormat().position(2).textureCoordinate(2).attribute("offset", 1, GL.GL_FLOAT), 40000);

    }

    public void renderFillCommands(DrawContext context, DrawStyle style, List<Command> commands) {
        StateTools.setupState(style);
        GL2GL3 gl = GLContext.getCurrentGL().getGL2GL3();

        fillShader.begin();

        fillShader.setUniform("modelViewMatrix", context.modelView);
        fillShader.setUniform("projectionMatrix", context.projection);

        fillShader.setUniform("stroke", style.fill);
        fillShader.setUniform1f("strokeThr", -1.0f);
        fillShader.setUniform1f("strokeMult", 1.0f );


        gl.glEnable(GL.GL_STENCIL_TEST);

        { // pass 1: Draw shapes in stencil only
            for (Command command: commands) {
                if (command.type == Expansion.ExpansionType.Fill) {
                    Command fill = command;
                    gl.glStencilMask(0xff);
                    gl.glStencilFunc(GL.GL_ALWAYS, 0, 0xff);
                    gl.glColorMask(false, false, false, false);

                    gl.glStencilOpSeparate(GL.GL_FRONT, GL.GL_KEEP, GL.GL_KEEP, GL.GL_INCR_WRAP);
                    gl.glStencilOpSeparate(GL.GL_BACK, GL.GL_KEEP, GL.GL_KEEP, GL.GL_DECR_WRAP);
                    gl.glDisable(GL.GL_CULL_FACE);

                    command.vertexBuffer.bind();
                    VertexBufferDrawer.draw(gl, fillShader, command.vertexBuffer, command.vertexBuffer.format(), GL.GL_TRIANGLE_FAN, fill.vertexCount, fill.offset/(4*5));

                    gl.glEnable(GL.GL_CULL_FACE);
                    gl.glColorMask(true, true, true, true);
                }
            }
        }


        { // pass 2: Draw anti-aliased pixels (fringes)
            boolean antiAlias = true;
            if (antiAlias) {

                for (Command command: commands) {
                    if (command.type == Expansion.ExpansionType.Fringe) {
                        fillShader.setUniform1f("strokeThr", 0.0f);
                        gl.glStencilFunc(GL.GL_EQUAL, 0x00, 0xff);
                        gl.glStencilOp(GL.GL_KEEP, GL.GL_KEEP, GL.GL_KEEP);

                        command.vertexBuffer.bind();
                        VertexBufferDrawer.draw(gl, fillShader, command.vertexBuffer, command.vertexBuffer.format(), GL.GL_TRIANGLE_STRIP, command.vertexCount, command.offset / (4*5));
                    }
                }
            }
        }

        { // pass 3: fill in stencilled area in pass 1
            fillShader.setUniform1f("strokeThr", -1.0f);
            Command fill = commands.get(0);

            quad.rewind();
            quad.put(fill.minx).put(fill.miny).put(0.5f).put(1.0f).put(0.0f);
            quad.put(fill.minx).put(fill.maxy).put(0.5f).put(1.0f).put(0.0f);
            quad.put(fill.maxx).put(fill.maxy).put(0.5f).put(1.0f).put(0.0f);
            quad.put(fill.maxx).put(fill.maxy).put(0.5f).put(1.0f).put(0.0f);
            quad.put(fill.maxx).put(fill.miny).put(0.5f).put(1.0f).put(0.0f);
            quad.put(fill.minx).put(fill.miny).put(0.5f).put(1.0f).put(0.0f);
            quad.rewind();
            vbo.bind();
            vbo.write(quad, 0, 6 * 5);

            gl.glStencilFunc(GL.GL_NOTEQUAL, 0x0, 0xff);
            gl.glStencilOp(GL.GL_ZERO, GL.GL_ZERO, GL.GL_ZERO);

            VertexBufferDrawer.draw(gl, fillShader, vbo, vbo.format(), GL.GL_TRIANGLES, 6);
        }

        gl.glDisable(GL.GL_STENCIL_TEST);
        fillShader.end();
        vbo.unbind();
    }

    public void renderFill(DrawContext context, DrawStyle style, List<Expansion> expansions) {
        List<Command> commands = toCommands(vbo, expansions, 0);
        renderFillCommands(context, style, commands);
    }

    public void renderStrokeCommands(DrawContext context, DrawStyle style, List<Command> commands) {
        StateTools.setupState(style);

        Shader strokeShader = shadeStyleManager.shader(style.shadeStyle, vbo.format());
        strokeShader.begin();
        shadeStyleManager.setParameters(strokeShader, style.shadeStyle);

        for (Command command: commands) {
            strokeShader.setUniform("modelViewMatrix", context.modelView);
            strokeShader.setUniform("projectionMatrix", context.projection);

            if (style.stroke != null)
            strokeShader.setUniform("stroke", style.stroke);
            strokeShader.setUniform1f("strokeMult", (float) (style.strokeWeight/2.0 + 0.65 ));


            GL gl = GLContext.getCurrentGL();

            // -- pre
            strokeShader.setUniform1f("strokeThr", (float) (1.0 - 0.5/255.0));

            gl.glEnable(GL.GL_STENCIL_TEST);
            gl.glStencilFunc(GL.GL_EQUAL, 0x0, 0xff);
            gl.glStencilOp(GL.GL_KEEP, GL.GL_KEEP, GL.GL_INCR);

            VertexBufferDrawer.draw(GLContext.getCurrentGL(), strokeShader, vbo, vbo.format(), GL.GL_TRIANGLE_STRIP, command.vertexCount, command.offset / (5*4));

            // -- anti-aliased
            strokeShader.setUniform1f("strokeThr", 0.0f);
            gl.glStencilFunc(GL.GL_EQUAL, 0x0, 0xff);
            gl.glStencilOp(GL.GL_KEEP, GL.GL_KEEP, GL.GL_KEEP);
            VertexBufferDrawer.draw(GLContext.getCurrentGL(), strokeShader, vbo, vbo.format(), GL.GL_TRIANGLE_STRIP, command.vertexCount, command.offset / (5*4));

            // -- reset stencil
            gl.glColorMask(false, false, false, false);
            gl.glStencilFunc(GL.GL_ALWAYS, 0x0, 0xff);
            gl.glStencilOp(GL.GL_ZERO, GL.GL_ZERO, GL.GL_ZERO);
            VertexBufferDrawer.draw(GLContext.getCurrentGL(), strokeShader, vbo, vbo.format(), GL.GL_TRIANGLE_STRIP, command.vertexCount, command.offset / (5*4));
            gl.glDisable(GL.GL_STENCIL_TEST);
            gl.glColorMask(true, true, true, true);


        }
        strokeShader.end();


    }

    public void renderStroke(DrawContext context,
                             DrawStyle style,
                             Expansion expansion) {

        List<Command> commands = toCommands(vbo, Arrays.asList(expansion), 0);
        renderStrokeCommands(context, style, commands);

//        StateTools.setupState(style);
//
//        strokeShader.begin();
//        strokeShader.setUniform("modelViewMatrix", context.modelView);
//        strokeShader.setUniform("projectionMatrix", context.projection);
//
//        strokeShader.setUniform("stroke", style.stroke);
//        strokeShader.setUniform1f("strokeMult", (float)style.strokeWeight/2.0f + 1.0f );
//
//        expansion.fb.rewind();
//        vbo.write(expansion.fb,0,expansion.vertexCount*5);
//
//        GL gl = GLContext.getCurrentGL();
//
//        // pre
////        shader.setUniform1f("strokeThr", (float) (1.0 - 0.5/255.0));
//
//        strokeShader.setUniform1f("strokeThr", -1.0f);
////        gl.glColorMask(false, false, false, false);
//
//        gl.glEnable(GL.GL_STENCIL_TEST);
//        gl.glStencilFunc(GL.GL_EQUAL, 0x0, 0xff);
//        gl.glStencilOp(GL.GL_KEEP, GL.GL_KEEP, GL.GL_INCR);
//        VertexBufferDrawer.draw(GLContext.getCurrentGL(), strokeShader, vbo, vbo.format(), GL.GL_TRIANGLE_STRIP, expansion.vertexCount);
//
//        // anti-aliased
//        strokeShader.setUniform1f("strokeThr", 0.0f);
//        gl.glStencilFunc(GL.GL_EQUAL, 0x0, 0xff);
//        gl.glStencilOp(GL.GL_KEEP, GL.GL_KEEP, GL.GL_KEEP);
//        VertexBufferDrawer.draw(GLContext.getCurrentGL(), strokeShader, vbo, vbo.format(), GL.GL_TRIANGLE_STRIP, expansion.vertexCount);
//
//        // reset stencil
//        gl.glColorMask(false, false, false, false);
//        gl.glStencilFunc(GL.GL_ALWAYS, 0x0, 0xff);
//        gl.glStencilOp(GL.GL_ZERO, GL.GL_ZERO, GL.GL_ZERO);
//        VertexBufferDrawer.draw(GLContext.getCurrentGL(), strokeShader, vbo, vbo.format(), GL.GL_TRIANGLE_STRIP, expansion.vertexCount);
//        gl.glDisable(GL.GL_STENCIL_TEST);
//        gl.glColorMask(true, true, true, true);
//
//        strokeShader.end();
    }
    public void destroy() {
        vbo.destroy();
        shadeStyleManager.destroy();
    }

}
