package net.lustlab.rndr.shape;


import net.lustlab.rndr.math.IntVector2;

public class IntRectangle {

    public int x;
    public int y;
    public int width;
    public int height;

    /**
     * Constructs a rectangle at (0,0) with width and height set to 0.
     */
    public IntRectangle() {
    }

    /**
     * Constructs a triangle at the given position with the given dimensions
     * @param x
     * @param y
     * @param width
     * @param height
     */
    public IntRectangle(int x, int y, int width, int height) {
        this.x = x;
        this.y = y;
        this.width = width;
        this.height = height;
    }

    /** Determines if point is inside rectangle
     * @param point points
     */
    public boolean contains(IntVector2 point) {
        return (x <= point.x
                && y <= point.y
                && x+width >= point.x
                && y+height >= point.y);
    }

    /**
     * Determines if rectangle intersects
     * @param rectangle
     * @return
     */
    public boolean intersects(IntRectangle rectangle) {
        boolean left = x + width < rectangle.x;
        boolean above = y + height < rectangle.y;
        boolean below = rectangle.y + rectangle.height < y;
        boolean right = rectangle.x + rectangle.width < x;

        return !(left || above || below || right);
    }




    @Override
    public String toString() {
        return "Rectangle{" +
                "x=" + x +
                ", y=" + y +
                ", width=" + width +
                ", height=" + height +
                '}';
    }

    /**
     * Returns the position of the center of the rectangle
     * @return
     */
    public IntVector2 center() {
        return new IntVector2(x + width/2, y + height/2);
    }

    /**
     * Return the position of the top-left corner of the rectangle
     * @return
     */
    public IntVector2 topLeft() {
        return new IntVector2(x, y);
    }
}
