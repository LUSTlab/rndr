package net.lustlab.rndr.math;

/**
 * Created by voorbeeld on 12/5/16.
 */
public class IntVector2 {

    @Override
    public String toString() {
        return "IntVector2{" +
                "x=" + x +
                ", y=" + y +
                '}';
    }

    public final int x;
    public final int y;

    public IntVector2(int x, int y) {
        this.x = x;
        this.y = y;
    }

    public IntVector2 plus(IntVector2 other) {
        return new IntVector2(x + other.x, y + other.y);
    }

    public IntVector2 minus(IntVector2 other) {
        return new IntVector2(x - other.x, y - other.y);
    }

    public IntVector2 times(int scale) {
        return new IntVector2(x * scale, y * scale);
    }

    public double length() {
        return Math.sqrt(x*x+y*y);
    }

}
