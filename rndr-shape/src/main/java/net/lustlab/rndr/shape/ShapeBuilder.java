package net.lustlab.rndr.shape;

import net.lustlab.rndr.math.*;

import java.util.ArrayList;
import java.util.List;

import static net.lustlab.rndr.math.Linear.multiply;

/**
 * ShapeBuilder is a tool that assists in producing Shapes
 */
public class ShapeBuilder {

    List<List<Segment>> contours = new ArrayList<>();

    List<Segment> segments = new ArrayList<>();
    Vector2 cursor = Vector2.Zero;

    boolean closed = false;

    public ShapeBuilder() {
        contours.add(segments);
    }

    /**
     * Move cursor
     * @param position
     */
    public ShapeBuilder moveTo(Vector2 position) {
        cursor = position.copy();
        return this;
    }

    public ShapeBuilder moveTo(double x, double y) {
        return moveTo(new Vector2(x,y));
    }

    /**
     * Move cursor in relative fashion
     * @param position
     */
    public ShapeBuilder moveToRelative(Vector2 position) {
        cursor = cursor.plus(position);
        return this;
    }

    public ShapeBuilder moveToRelative(double x, double y) {
        return moveToRelative(new Vector2(x, y));
    }


    public ShapeBuilder lineTo(Vector2 position) {
        segments.add(new Segment(cursor.copy(), position.copy()));
        cursor = position.copy();
        return this;
    }

    public ShapeBuilder lineTo(double x, double y) {
        return lineTo(new Vector2(x, y));
    }


    public ShapeBuilder lineToRelative(Vector2 position) {
        segments.add(new Segment(cursor.copy(), cursor.plus(position)));
        cursor = position.copy();
        return this;

    }

    /**
     * Adds a cubic bezier curve
     * @param c0 first control point
     * @param c1 second control point
     * @param position end point
     */
    public ShapeBuilder curveTo(Vector2 c0, Vector2 c1, Vector2 position) {
        segments.add(new Segment(cursor.copy(), c0.copy(), c1.copy(), position.copy()));
        cursor = position.copy();
        return this;
    }

    /**
     * Adds a quadratic bezier curve
     * @param c0 control point
     * @param position end point
     * @return
     */
    public ShapeBuilder curveTo(Vector2 c0, Vector2 position) {
        segments.add(new Segment(cursor.copy(), c0.copy(), position.copy()));
        cursor = position.copy();
        return this;
    }


    public ShapeBuilder curveToRelative(Vector2 c0, Vector2 c1, Vector2 position) {
        segments.add(new Segment(cursor.copy(), cursor.plus(c0), cursor.plus(c1), cursor.plus(position)));
        cursor = cursor.plus(position);
        return this;
    }

    /**
     * Close contour and start a new contour
     * @return
     */
    public ShapeBuilder close() {
        closed = true;
        segments = new ArrayList<>();
        contours.add(segments);
        return this;
    }

    public ShapeBuilder circle(double x, double y, double radius) {

        double width = radius*2;
        double height = radius*2;
        x-=width/2;
        y-=height/2;

        double kappa = 0.5522848;
        double ox = (width / 2) * kappa, // control point offset horizontal
                oy = (height / 2) * kappa, // control point offset vertical
                xe = x + width,           // x-end
                ye = y + height,           // y-end
                xm = x + width / 2,       // x-middle
                ym = y + height / 2;       // y-middle

        moveTo(x , ym);
        curveTo(new Vector2(x, ym- oy), new Vector2(xm -ox, y), new Vector2(xm, y));
        curveTo(new Vector2(xm+ox, y), new Vector2(xe, ym -oy), new Vector2(xe, ym));
        curveTo(new Vector2(xe, ym + oy), new Vector2( xm + ox, ye), new Vector2( xm, ye));
        curveTo(new Vector2(xm - ox, ye), new Vector2( x, ym + oy), new Vector2( x, ym));

        close();
        return this;
    }

    public ShapeBuilder arcTo(double rx, double ry, double angle, boolean largeArcFlag, boolean sweepFlag, double tx, double ty) {

        if (rx == 0 || ry == 0) {
            lineTo(tx, ty);
        }
        rx = Math.abs(rx);
        ry = Math.abs(ry);
        double angleRad = Math.toRadians(angle%360.0);


        double dx2 = (cursor.x - tx) / 2.0;
        double dy2 = (cursor.y - ty)/ 2.0;

        double cosAngle = Math.cos(angleRad);
        double sinAngle = Math.sin(angleRad);
        double x1 =cosAngle*dx2 + sinAngle*dy2;
        double y1 = -sinAngle*dx2 +cosAngle*dy2;

        double radiiCheck = (x1*x1)/(rx*rx) + (y1*y1)/(ry*ry);
        if (radiiCheck > 1) {
            rx = Math.sqrt(radiiCheck) * rx;
            ry = Math.sqrt(radiiCheck) * ry;
        }


        double rx_sq = rx*rx;
        double ry_sq = ry*ry;

        double y1_sq = y1*y1;
        double x1_sq = x1*x1;
        // Step 2 : Compute (cx1, cy1) - the transformed centre point
        double sign = (largeArcFlag == sweepFlag) ? -1 : 1;
        double sq = ((rx_sq * ry_sq) - (rx_sq * y1_sq) - (ry_sq * x1_sq)) / ((rx_sq * y1_sq) + (ry_sq * x1_sq));
        sq = (sq < 0) ? 0 : sq;
        double coef = (sign * Math.sqrt(sq));
        double cx1 = coef * ((rx * y1) / ry);
        double cy1 = coef * -((ry * x1) / rx);

        // Step 3 : Compute (cx, cy) from (cx1, cy1)
        double sx2 = (cursor.x + tx) / 2.0;
        double sy2 = (cursor.y + ty) / 2.0;
        double cx = sx2 + (cosAngle * cx1 - sinAngle * cy1);
        double cy = sy2 + (sinAngle * cx1 + cosAngle * cy1);

        // Step 4 : Compute the angleStart (angle1) and the angleExtent (dangle)
        double ux = (x1 - cx1) / rx;
        double uy = (y1 - cy1) / ry;
        double vx = (-x1 - cx1) / rx;
        double vy = (-y1 - cy1) / ry;
        double p, n;

        // Compute the angle start
        n = Math.sqrt((ux * ux) + (uy * uy));
        if (n == 0) {
            n = 0.000001;
        }

        p = ux; // (1 * ux) + (0 * uy)
        sign = (uy < 0) ? -1.0 : 1.0;
        double angleStart = Math.toDegrees(sign * Math.acos(p / n));

        // Compute the angle extent
        n = Math.sqrt((ux * ux + uy * uy) * (vx * vx + vy * vy));
        p = ux * vx + uy * vy;


        double ratio = n > 0.0 ? p/n : 0.0;

        sign = (ux * vy - uy * vx < 0) ? -1.0 : 1.0;
        double angleExtent = ratio >= 0.0? Math.toDegrees(sign * Math.acos(p / n)) : 180;


        if (!sweepFlag && angleExtent > 0) {
            angleExtent -= 360.0;
        } else if (sweepFlag && angleExtent < 0) {
            angleExtent += 360.0;
        }
        angleExtent %= 360f;
        angleStart %= 360f;

        //angleExtent = Functions.mod(angleExtent, 360.0);
        //angleStart = Functions.mod(angleStart, 360.0);

        Vector2 coords[] = arcToBeziers(angleStart, angleExtent);


        Matrix44 t = multiply(
                Transforms.translate(new Vector3(cx, cy, 0.0)),
                Transforms.rotateZ(angleRad),
                Transforms.scale(rx, ry, 1.0)
        );

        for (int i = 0; i < coords.length; ++i) {
            coords[i] = coords[i].xy0().multiply(t).xy();
        }

        if (coords.length > 0) {
            coords[coords.length - 1] = new Vector2(tx, ty);
            for (int i = 0; i < coords.length; i += 3) {
                curveTo(coords[i], coords[i + 1], coords[i + 2]);
            }
        } else {
            System.out.println("wot" + angleStart + " " + angleExtent);
        }

        cursor = new Vector2(tx, ty);

        return this;
    }

    private Vector2[] arcToBeziers(double angleStart, double angleExtent) {
        int    numSegments = (int) Math.ceil(Math.abs(angleExtent) / 90.0);

        angleStart = Math.toRadians(angleStart);
        angleExtent = Math.toRadians(angleExtent);
        float  angleIncrement = (float) (angleExtent / numSegments);

        // The length of each control point vector is given by the following formula.
        double  controlLength = 4.0 / 3.0 * Math.sin(angleIncrement / 2.0) / (1.0 + Math.cos(angleIncrement / 2.0));

        Vector2[] coords = new Vector2[numSegments * 3];
        int     pos = 0;

        for (int i=0; i<numSegments; i++)
        {
            double  angle = angleStart + i * angleIncrement;
            // Calculate the control vector at this angle
            double  dx = Math.cos(angle);
            double  dy = Math.sin(angle);
            // First control point
            coords[pos] = new Vector2(dx- controlLength*dy, dy + controlLength * dx);
            pos++;
            // Second control point
            angle += angleIncrement;
            dx = Math.cos(angle);
            dy = Math.sin(angle);
            coords[pos] = new Vector2(dx+controlLength*dy, dy- controlLength*dx);
            pos++;
            // Endpoint of bezier
            coords[pos]= new Vector2(dx, dy);
            pos++;
        }
        return coords;
    }



    /**
     * Build the shape
     * @return
     */
    public Shape build() {
        Shape shape = new Shape();

        for (List<Segment> segmentList: contours) {

            if (segmentList.size() > 0) {
                ShapeContour contour = new ShapeContour();
                contour.segments.addAll(segmentList);
                if (closed) {
                    contour.closed = true;
                }
                shape.contours.add(contour);

            }
        }


        // clean up state
        cursor = null;
        segments = new ArrayList<>();
        contours.clear();

        return shape;
    }

}
