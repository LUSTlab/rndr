package net.lustlab.rndr.math.tests;

import net.lustlab.rndr.math.Matrix33;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

/**
 * Created by voorbeeld on 4/26/14.
 */
public class TestMatrix33 {

    @Test
    public void testInverse() {

        Matrix33 m = new Matrix33(1,1,1);

        Matrix33 mi = m.inverse();

        assertEquals(1.0, mi.m00, 0.001);
        assertEquals(0.0, mi.m01, 0.001);
        assertEquals(0.0, mi.m02, 0.001);

        assertEquals(0.0, mi.m10, 0.001);
        assertEquals(1.0, mi.m11, 0.001);
        assertEquals(0.0, mi.m12, 0.001);

        assertEquals(0.0, mi.m20, 0.001);
        assertEquals(0.0, mi.m21, 0.001);
        assertEquals(1.0, mi.m22, 0.001);

    }

    @Test
    public void testInverse2() {
        Matrix33 a = new Matrix33(2,2,2);
        Matrix33 ai = a.inverse();

        assertEquals(0.5, ai.m00, 0.001);
        assertEquals(0.0, ai.m01, 0.001);
        assertEquals(0.0, ai.m02, 0.001);

        assertEquals(0.0, ai.m10, 0.001);
        assertEquals(0.5, ai.m11, 0.001);
        assertEquals(0.0, ai.m12, 0.001);

        assertEquals(0.0, ai.m20, 0.001);
        assertEquals(0.0, ai.m21, 0.001);
        assertEquals(0.5, ai.m22, 0.001);



    }

    @Test
    public void testInverse3() {
        Matrix33 a = new Matrix33(2,2,2);
        Matrix33 ai = a.inverse();

        ai.multiply(a);

        Matrix33 aia = ai.multiply(a);

        assertEquals(1.0, aia.m00, 0.001);
        assertEquals(0.0, aia.m01, 0.001);
        assertEquals(0.0, aia.m02, 0.001);

        assertEquals(0.0, aia.m10, 0.001);
        assertEquals(1.0, aia.m11, 0.001);
        assertEquals(0.0, aia.m12, 0.001);

        assertEquals(0.0, aia.m20, 0.001);
        assertEquals(0.0, aia.m21, 0.001);
        assertEquals(1.0, aia.m22, 0.001);
    }

    @Test
    public void testInverse4() {
        Matrix33 a = new Matrix33(1,2,3,4,5,6,7,8,1);
        Matrix33 ai = a.inverse();

        ai.multiply(a);

        Matrix33 aia = ai.multiply(a);

        assertEquals(1.0, aia.m00, 0.001);
        assertEquals(0.0, aia.m01, 0.001);
        assertEquals(0.0, aia.m02, 0.001);

        assertEquals(0.0, aia.m10, 0.001);
        assertEquals(1.0, aia.m11, 0.001);
        assertEquals(0.0, aia.m12, 0.001);

        assertEquals(0.0, aia.m20, 0.001);
        assertEquals(0.0, aia.m21, 0.001);
        assertEquals(1.0, aia.m22, 0.001);
    }
}
