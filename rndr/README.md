RNDR
====
Flexible creative coding library for Java 1.8


Basic Usage
===========

```
#!java
class DemoProgram extends Program {

    public void draw() {
        drawer.background(ColorRGBa.DIMGRAY);
        drawer.rectangle(100, 100, 100, 100);
    }

    public static void main(String[] args) {
        Application.run(new Program, new Configuration());
    }
}
```


Documentation
=============

More detailed documentation for RNDR can be found in the RNDR wiki.

Maven Artifact
==============

```
#!xml
<dependency>
    <groupId>net.lustlab.rndr</groupId>
    <artifactId>rndr</artifactId>
    <version>0.2.1-SNAPSHOT</version>
</dependency>
```

Building RNDR
=============

Building RNDR from source is straight forward.

```
mvn compile
mvn install
```


License
=======
RNDR is freely distributed under the BSD 2-clause license