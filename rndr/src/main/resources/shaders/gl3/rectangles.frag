#version 330

layout(origin_upper_left) in vec4 gl_FragCoord;
${uniforms! }

flat in  int v_instance;

in vec2 v_texCoord0;
in vec2 v_dimensions;
uniform vec4 fill;
uniform vec4 stroke;

uniform vec2 dimensions;
uniform float strokeWeight;
//uniform sampler2D image;

in vec3 v_boundsPosition;

out vec4 o_color;
void main(void) {
    int instance = v_instance;
    vec3 boundsPosition = v_boundsPosition;

    vec4 x_fill = fill;
    vec4 x_stroke = stroke;
    { ${fillTransform! } }
    { ${strokeTransform! } }

    vec2 fw = fwidth(v_texCoord0);
    float dv = abs(v_texCoord0.y-0.5)*2.0;
    float du = abs(v_texCoord0.x-0.5)*2.0;

    float wv = fwidth(dv);
    float wu = fwidth(du);

    float bx = (2.0*strokeWeight) / v_dimensions.x;
    float by = (2.0*strokeWeight) / v_dimensions.y;
    float d = smoothstep(0, wu*1.25, 1.0-du) * smoothstep(0, wv*1.25, 1.0-dv);
    float d2 = smoothstep(0, wu*1.25, (1.0-bx-du)) * smoothstep(0, wv*1.25, (1.0-by-dv)) * d;

    o_color = x_fill; //vec4(x_fill.rgb*(d2)+x_stroke.rgb*(1.0-d2),x_fill.a*d2+x_stroke.a*(1.0-d2));
}