#version 330

in vec3 position;
in vec3 normal;
in vec4 color;

in vec2 texCoord0;
in vec2 texCoord1;
in vec2 texCoord2;

out vec2 v_texCoord0;

uniform mat4 modelViewMatrix;
uniform mat4 modelViewProjectionMatrix;
uniform mat4 normalMatrix;
uniform vec4 bounds;

out vec3 v_modelPosition;
out vec3 v_cameraPosition;
out vec3 v_boundsPosition;

out vec3 v_normal;

void main(void) {

    vec3 x_position = position;
    vec3 x_normal = normal;

    gl_Position = modelViewProjectionMatrix * vec4(x_position, 1.0);
    v_cameraPosition = (modelViewMatrix * vec4(x_position, 1.0)).xyz;
    v_texCoord0 = texCoord0;

    v_modelPosition = x_position;
    v_normal = x_normal;
    v_boundsPosition.x = ((position.x - bounds.x) / bounds.z)*2-1;
    v_boundsPosition.y = ((position.y - bounds.y) / bounds.w)*2-1;
    v_boundsPosition.z = 0;
}