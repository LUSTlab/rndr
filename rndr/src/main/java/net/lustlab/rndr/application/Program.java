package net.lustlab.rndr.application;

import com.jogamp.newt.opengl.GLWindow;
import net.lustlab.rndr.Configuration;
import net.lustlab.rndr.draw.DrawThread;
import net.lustlab.rndr.draw.Drawer;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.ArrayList;
import java.util.List;
import java.util.function.BiConsumer;

/**
 * Platform independent Program class
 */
public class Program {

    final Logger logger = LogManager.getLogger(Program.class);
    public Drawer drawer;
    public Configuration configuration;
    public GLWindow window;

    public long start;

    public int width;
    public int height;

    public final Mouse mouse = new Mouse();
    public final Keyboard keyboard = new Keyboard();
    public final Timer timer = new Timer();

    Application application;

    List<ProgramDelegate> delegates = new ArrayList<>();


    public Program() {

    }

    public final long start() {
        return start;
    }

    /**
     * Returns the number of seconds since the program started
     * @return the time since the Program started in seconds
     */
    public final double seconds() {
        return (System.currentTimeMillis() - start) / 1000.0;
    }

    /**
     * Adds a ProgramDelegate to the Program
     * @param delegate the delegate
     * @return this
     */
    public final Program delegate(ProgramDelegate delegate) {
        delegates.add(delegate);
        delegate.setup(this);
        return this;
    }

    final void beforeDraw() {

        timer.update();
        for (ProgramDelegate delegate: delegates) {
            delegate.beforeDraw(this);
        }
    }

    final void afterDraw() {
        for (ProgramDelegate delegate: delegates) {
            delegate.afterDraw(this);
        }
    }

    /**
     * Code that should only be executed once
     */
    public void setup() {

    }

    /**
     * Code that should be executed on every frame
     */
    public void draw() {

    }

    /**
     * Spawns a DrawThread
     * @param toExecute the code to execute
     * @return a new DrawThread
     */
    public DrawThread thread(BiConsumer<DrawThread, Drawer> toExecute) {
        System.out.println(application);
        return application.drawThread(toExecute);
    }

    /**
     * Quits the program
     */
    public final void quit() {
        logger.debug("user requested quit");
        application.quit();
    }

    /**
     * Quits the program with a given error level
     * @param errorLevel the error level [1, 255]
     */
    public final void error(int errorLevel) {
        logger.error("user reported error, exiting with error levell:" + errorLevel);
        application.error(errorLevel);
    }

    /**
     * Returns the Application
     * @return
     */
    public final Application application() {
        return application;
    }

    @Override
    public String toString() {
        return "Program{" +
                "logger=" + logger +
                ", drawer=" + drawer +
                ", configuration=" + configuration +
                ", window=" + window +
                ", start=" + start +
                ", width=" + width +
                ", height=" + height +
                ", mouse=" + mouse +
                ", keyboard=" + keyboard +
                ", application=" + application +
                ", delegates=" + delegates +
                '}';
    }
}
