package net.lustlab.rndr.buffers;

import com.jogamp.opengl.GL;

/**
 * Created by voorbeeld on 7/27/15.
 */
public enum MagnifyingFilter {
    NEAREST,
    LINEAR;

    public int toGLFilter() {
        switch(this) {
            case NEAREST:
                return GL.GL_NEAREST;
            case LINEAR:
                return GL.GL_LINEAR;
            default:
                throw new RuntimeException("unsupported filter");
        }

    }

}
