package net.lustlab.rndr.compute;

import com.jogamp.opencl.CLCommandQueue;

public class CommandQueue {

    CLCommandQueue queue;
    CommandQueue(CLCommandQueue queue) {
        this.queue = queue;
    }


    public CommandQueue acquire(ComputeBufferTexture bufferTexture) {
        queue.putAcquireGLObject(bufferTexture.buffer);
        return this;
    }

    public CommandQueue release(ComputeBufferTexture bufferTexture) {
        queue.putReleaseGLObject(bufferTexture.buffer);
        return this;
    }



    public CommandQueue acquire(ComputeVertexBuffer vertexBuffer) {
        queue.putAcquireGLObject(vertexBuffer.buffer);
        return this;
    }

    public CommandQueue release(ComputeVertexBuffer vertexBuffer) {
        queue.putReleaseGLObject(vertexBuffer.buffer);
        return this;
    }

    public CommandQueue acquire(ComputeColorBuffer sharedImage) {
        queue.putAcquireGLObject(sharedImage.image);
        return this;
    }

    public CommandQueue release(ComputeColorBuffer sharedImage) {
        queue.putAcquireGLObject(sharedImage.image);
        return this;
    }


    public CommandQueue readBlocked(ComputeImage3d image) {
        queue.putReadImage(image.image, true);
        return this;
    }

    public CommandQueue readBlocked(ComputeImage image) {
        queue.putReadImage(image.image, true);
        return this;
    }

    public CommandQueue writeBlocked(ComputeImage3d image) {
        queue.putWriteImage(image.image, true);
        return this;
    }

    public CommandQueue writeBlocked(ComputeImage image) {
        queue.putWriteImage(image.image, true);
        return this;
    }


    public CommandQueue writeBlocked(ComputeBuffer buffer) {
        queue.putWriteBuffer(buffer.buffer, true);
        return this;
    }

    public CommandQueue readBlocked(ComputeBuffer buffer) {
        queue.putReadBuffer(buffer.buffer, true);
        return this;
    }


    public CommandQueue copy(ComputeVertexBuffer source, ComputeBuffer dest) {
        queue.putCopyBuffer(source.buffer, dest.buffer);
        return this;
    }

    public CommandQueue copy(ComputeVertexBuffer source, ComputeVertexBuffer dest) {
        queue.putCopyBuffer(source.buffer, dest.buffer);
        return this;
    }

    public CommandQueue copy(ComputeBuffer source, ComputeVertexBuffer dest) {
        queue.putCopyBuffer(source.buffer, dest.buffer);
        return this;
    }

    public CommandQueue copy(ComputeImage source, ComputeImage dest) {
        queue.putCopyImage(source.image, dest.image);
        return this;
    }

    public CommandQueue copy(ComputeImage source, ComputeColorBuffer dest) {
        queue.putCopyImage(source.image, dest.image);
        return this;
    }

    public CommandQueue copy(ComputeColorBuffer source, ComputeColorBuffer dest) {
        queue.putCopyImage(source.image, dest.image);
        return this;
    }

    public CommandQueue copy(ComputeColorBuffer source, ComputeImage dest) {
        queue.putCopyImage(source.image, dest.image);
        return this;
    }

    public CommandQueue run1D(Kernel kernel, long globalWorkOffset, long globalWorkSize, long localWorksSize) {
        queue.put1DRangeKernel(kernel.kernel, globalWorkOffset, globalWorkSize, localWorksSize);
        return this;
    }

    public CommandQueue run2D(Kernel kernel,  long globalWorkOffsetX,
                              long globalWorkOffsetY,
                              long globalWorkSizeX,
                              long globalWorkSizeY,
                              long localWorkSizeX,
                              long localWorkSizeY) {
        queue.put2DRangeKernel(kernel.kernel, globalWorkOffsetX, globalWorkOffsetY, globalWorkSizeX, globalWorkSizeY, localWorkSizeX, localWorkSizeY);
        return this;
    }

    public CommandQueue run3D(Kernel kernel, long globalWorkOffsetX,
                              long globalWorkOffsetY,
                              long globalWorkOffsetZ,
                              long globalWorkSizeX,
                              long globalWorkSizeY,
                              long globalWorkSizeZ,
                              long localWorkSizeX,
                              long localWorkSizeY,
                              long localWorkSizeZ) {

        queue.put3DRangeKernel(kernel.kernel,globalWorkOffsetX, globalWorkOffsetY, globalWorkOffsetZ, globalWorkSizeX, globalWorkSizeY, globalWorkSizeZ, localWorkSizeX, localWorkSizeY, localWorkSizeZ);
        return this;
    }

    public CommandQueue finish(Kernel kernel) {
        queue.finish();
        return this;
    }


}
