package net.lustlab.rndr.svg;


import net.lustlab.colorlib.ColorRGBa;
import net.lustlab.rndr.shape.ShapeContour;
import net.lustlab.rndr.shape.Segment;
import net.lustlab.rndr.shape.Shape;
import net.lustlab.rndr.math.Vector2;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Path extends Element {


    public static Path fromSVGPath(String svgPath) {
        Path path = new Path();
        String rawCommands[] = svgPath.split("(?=[MmZzLlHhVvCcSsQqTtAa])");
        Pattern numbers = Pattern.compile("[-+]?[0-9]*[.]?[0-9]+(?:[eE][-+]?[0-9]+)?");

        for (String rawCommand: rawCommands) {
            if (rawCommand.length() > 0) {
                Command command = new Command();
                command.op = rawCommand.charAt(0);
                Matcher numberMatcher = numbers.matcher(rawCommand);
                while (numberMatcher.find()) {
                    command.operands.add(new Double(numberMatcher.group()));
                }
                path.commands.add(command);
            }
        }
        return path;
    }


	public List<Command> commands = new ArrayList<Command>();
	public ColorRGBa fill;
	public ColorRGBa stroke;
	public double strokeWidth;

    /**
     * Returns the individual paths in a compound path
     * @return
     */
	List<Path> getCompounds() {
		List<Path> compounds = new ArrayList<Path>();

		List<Integer> compoundIndices = new ArrayList<Integer>();
		int index = 0;
		for (Command c: commands) {
			if (c.op == 'M') {
				compoundIndices.add(index);
			}
			index++;
		}
		for (int i = 0; i < compoundIndices.size(); ++i) {

			int cs = compoundIndices.get(i);
			int ce = i+1 < compoundIndices.size() ? compoundIndices.get(i+1) : commands.size();

			Path path = new Path();
			path.commands.addAll(commands.subList(cs, ce));

			if (compoundIndices.size() > 1) {
				path.commands.add(new Command('Z'));
			}
			compounds.add(path);
		}
		return compounds;
	}

	@Override
	public String toString() {
		return "Path{" +
				"commands=" + commands +
				", fill=" + fill +
				", stroke=" + stroke +
				'}';
	}

    /**
     * Generates a Shape from the SVG path
     * @return A shape
     */
	public Shape shape() {
		Shape shape = new Shape();

		shape.fill = fill!=null?fill.copy():null;
		shape.stroke = stroke!=null?stroke.copy():null;
		shape.strokeWeight = strokeWidth;
		//shape.transform = new Transform(transform);
		//shape.id = id;

		Vector2 cursor = new Vector2();
		Vector2 anchor = cursor.copy();
		Vector2 relativeControl = new Vector2(0,0);
		for (Path compound: getCompounds()) {

			ShapeContour contour = new ShapeContour();

			for (Command command: compound.commands) {

				switch (command.op) {

					case 'M': // Move absolute
						cursor = command.vector(0,1);
						anchor = cursor.copy();
						break;
					case 'm': // Move relative
						cursor = cursor.plus(command.vector(0, 1));
						anchor = cursor.copy();
						break;
					case 'L': // Line absolute
						contour.segments.add(new Segment(new Vector2(cursor), command.vector(0,1)));
						cursor = command.vector(0,1).copy();
						//
						break;
					case 'l': // Line relative
						contour.segments.add(new Segment(new Vector2(cursor), cursor.plus(command.vector(0, 1))));
						cursor = cursor.plus(command.vector(0, 1));
						break;
					case 'h':
						contour.segments.add(new Segment(new Vector2(cursor), cursor.plus(command.vector(0, -1))));
						cursor = cursor.plus(command.vector(0, -1));
						break;
					case 'H': {
						Vector2 target = new Vector2(command.operands.get(0),cursor.y);
						contour.segments.add(new Segment(new Vector2(cursor), target));
						cursor = target.copy();
						break;
					}
					case 'v': {
						Vector2 target = cursor.plus(command.vector(-1, 0));
						contour.segments.add(new Segment(new Vector2(cursor), target));
						cursor = target.copy();
						break;
					}
					case 'V': {
						Vector2 target = new Vector2(cursor.x,command.operands.get(0));
						contour.segments.add(new Segment(new Vector2(cursor), target));
						cursor = target.copy();
						break;
					}
					case 'C': {
						Vector2[] points = command.vectors();
						contour.segments.add(new Segment(new Vector2(cursor), points[0], points[1], points[2]));
						cursor = points[2].copy();
						relativeControl = points[1].minus(points[2]);
						break;
					}
					case 'c': {
						Vector2[] points = command.vectors();
						contour.segments.add(new Segment(new Vector2(cursor), cursor.plus(points[0]), cursor.plus(points[1]), cursor.plus(points[2])));

						relativeControl = cursor.plus(points[1]).minus(cursor.plus(points[2]));
						cursor = cursor.plus(points[2]);
						break;
					}
					case 's': {
						Vector2 reflected = relativeControl.scale(-1);
						Vector2 cp0 = cursor.plus(reflected);
						Vector2 cp1 = cursor.plus(command.vector(0, 1));
						Vector2 target = cursor.plus(command.vector(2, 3));
						contour.segments.add(new Segment(cursor.copy(), cp0, cp1, target));
						cursor = target.copy();
						relativeControl = cp1.minus(target);
						break;
					}

					case 'S': {
						Vector2 reflected = relativeControl.scale(-1);
						Vector2 cp0 = cursor.plus(reflected);
						Vector2 cp1 = command.vector(0, 1);
						Vector2 target = command.vector(2,3);
						contour.segments.add(new Segment(cursor.copy(), cp0, cp1, target));
						cursor = target.copy();
						relativeControl = cp1.minus(target);
						break;
					}
					case 'Z':
					case 'z':
                        if (cursor.minus(anchor).length() >= 0.001) {
                            contour.segments.add(new Segment(cursor.copy(), anchor.copy()));
                        }
						contour.closed = true;
						break;
					default:
						throw new RuntimeException("op not implemented:" + command.op);
				}
			}
			shape.contours.add(contour);
		}

		return shape;

	}
}
