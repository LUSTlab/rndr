package net.lustlab.rndr.font;

import java.io.Serializable;

public class EntityPairIndex implements Serializable {

	public final char left;
	public final char right;

	public EntityPairIndex(char left, char right) {
		this.left = left;
		this.right = right;
	}
	@Override
	public int hashCode() {
		return ((left&0xffff)<<16) + (right&0xffff);
	}	
	@Override
	public boolean equals(Object obj) {
		EntityPairIndex index = (EntityPairIndex) obj;
		return (index.left == left && index.right == right);
	}

}
