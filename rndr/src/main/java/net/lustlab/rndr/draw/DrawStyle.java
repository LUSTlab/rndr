package net.lustlab.rndr.draw;

import net.lustlab.colorlib.ColorRGBa;
import net.lustlab.rndr.math.Matrix44;

public class DrawStyle {
    public ColorRGBa tint = ColorRGBa.WHITE;
    public Matrix44 colorMatrix = Matrix44.IDENTITY;
    public ColorRGBa fill = ColorRGBa.WHITE;
    public ColorRGBa stroke = ColorRGBa.BLACK;
    public int channelMask = ChannelMask.ALL;
    public double strokeWeight = 1;
    public boolean smooth = true;
    public BlendMode blendMode = BlendMode.NORMAL;
    public ShadeStyle shadeStyle = null;
    public DepthTestMode depthTest = DepthTestMode.ALWAYS;
    public boolean depthWrite = true;
    public CullMode cullMode = CullMode.NONE;

    public LineCap lineCap = LineCap.BUTT;
    public LineJoin lineJoin = LineJoin.BEVEL;

    // -- stencilling
    public StencilOperation stencilFailOperation = StencilOperation.KEEP;
    public StencilOperation depthFailOperation = StencilOperation.KEEP;
    public StencilOperation depthPassOperation = StencilOperation.KEEP;
    public int stencilTestMask = 0xff;
    public int stencilTestReference = 0;
    public int stencilWriteMask = 0xff;
    public StencilTest stencilTest = StencilTest.ALWAYS;


    /**
     * Returns a deep copy of this DrawStyle
     * @return
     */
    public DrawStyle copy() {
        DrawStyle copy = new DrawStyle();
        copy.tint = tint.copy();
        copy.colorMatrix = new Matrix44(colorMatrix);
        copy.fill = fill ==null? null:fill.copy();
        copy.stroke = stroke == null? null: stroke.copy();
        copy.strokeWeight = strokeWeight;
        copy.smooth = smooth;
        copy.blendMode = blendMode;
        copy.shadeStyle = shadeStyle;
        copy.depthTest = depthTest;
        copy.depthWrite = depthWrite;
        copy.cullMode = cullMode;

        copy.channelMask = channelMask;

        copy.stencilFailOperation = stencilFailOperation;
        copy.depthFailOperation = depthFailOperation;
        copy.depthPassOperation = depthPassOperation;
        copy.stencilTestMask = stencilTestMask;
        copy.stencilTestReference = stencilTestReference;
        copy.stencilWriteMask = stencilWriteMask;
        copy.stencilTest = stencilTest;

        return copy;
    }
}
