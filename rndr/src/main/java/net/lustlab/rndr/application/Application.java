package net.lustlab.rndr.application;

import net.lustlab.rndr.Configuration;
import net.lustlab.rndr.draw.DrawThread;
import net.lustlab.rndr.draw.Drawer;
import net.lustlab.rndr.urlhandler.ConfigurableStreamHandlerFactory;
import net.lustlab.rndr.urlhandler.Handler;

import java.net.URL;
import java.util.function.BiConsumer;
import java.util.function.Consumer;
import java.util.function.Supplier;

public abstract class Application {

    Program program;
    static boolean once = true;

    public abstract DrawThread drawThread(BiConsumer<DrawThread, Drawer> toExecute);

    /**
     * Runs a Program with a given Configuration
     * @param program the Program to run
     * @param configuration the Configuration to use
     */
    public static void run(Program program, Configuration configuration) {

        run(() -> program, configuration);
    }


    public static void run(Supplier<Program> supplier, Configuration configuration) {

        if (once) {
            ConfigurableStreamHandlerFactory f = new ConfigurableStreamHandlerFactory("cp", new Handler());
            URL.setURLStreamHandlerFactory(f);
            once = false;
        }

        Application application;
        if (!configuration.multithreaded) {
            application = new ApplicationGL3(supplier, configuration);
        }

    }



    public abstract void pause();
    public abstract void resume();

    public abstract void quit();

    public abstract void error(int errorLevel);



    @Override
    public String toString() {
        return "Application{" +
                "program=" + program +
                '}';
    }

}
