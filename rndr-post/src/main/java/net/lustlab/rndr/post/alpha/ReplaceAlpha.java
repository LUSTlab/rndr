package net.lustlab.rndr.post.alpha;

import net.lustlab.rndr.post.Filter;
import net.lustlab.rndr.shaders.FragmentShader;

public class ReplaceAlpha extends Filter {
    public ReplaceAlpha() {
        super(FragmentShader.fromUrl("cp:shaders/gl3/post/alpha/replace.frag"));
        replacement(1);
    }

    public ReplaceAlpha replacement(double r) {
        parameter("replacement", (float)r);
        return this;
    }

}
