#version 330

layout(origin_upper_left) in vec4 gl_FragCoord;

${uniforms! }
// varyings
in vec4 v_color;

// output variables
out vec4 o_color;

void main(void) {

    // -- shade style helpers
    vec2 screenPosition = gl_FragCoord.xy;

    vec4 x_fill = v_color;
    vec4 x_stroke = v_color;

    { ${fillTransform! } }
    { ${strokeTransform! } }
    vec4 x_color = x_fill;

    o_color = x_color;
}