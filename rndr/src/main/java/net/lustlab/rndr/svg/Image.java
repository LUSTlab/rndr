package net.lustlab.rndr.svg;

public class Image extends Element {
    public double width;
    public double height;
    public String url;
}
