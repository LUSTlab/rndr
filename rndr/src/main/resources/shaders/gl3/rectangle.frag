#version 330

${outputs! }
${uniforms! }


in vec2 v_texCoord0;
uniform vec4 fill;
uniform vec4 stroke;

uniform vec2 dimensions;
uniform float strokeWeight;

out vec4 o_color;

in vec3 v_boundsPosition;

void main(void) {


    vec3 boundsPosition = v_boundsPosition;

    vec2 fw = fwidth(v_texCoord0);
    float dv = abs(v_texCoord0.y - 0.5) * 2.0;
    float du = abs(v_texCoord0.x - 0.5) * 2.0;

    float rdv = (v_texCoord0.y - 0.5) * 2.0;
    float rdu = (v_texCoord0.x - 0.5) * 2.0;

    float wv = fwidth(rdv);
    float wu = fwidth(rdu);

    float bx = (2 * strokeWeight) / dimensions.x;
    float by = (2 * strokeWeight) / dimensions.y;

    float smoothv = 1.25;

    vec4 x_fill = fill;
    vec4 x_stroke = stroke;
    { ${fillTransform! } }
    { ${strokeTransform! } }

    float d = smoothstep(0, wu*smoothv, 1.0-du) * smoothstep(0, wv*smoothv, 1.0-dv);
    float d2 = smoothstep(0, wu*smoothv, (1.0-bx-du)) * smoothstep(0, wv*smoothv, (1.0-by-dv)) * d;

    o_color = vec4(x_fill.rgb * (d2) +  x_stroke.rgb * (1.0-d2), d * (x_fill.a*d2+x_stroke.a*(1.0-d2)));
}