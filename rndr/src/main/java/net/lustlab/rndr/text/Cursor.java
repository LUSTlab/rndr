package net.lustlab.rndr.text;

import java.io.Serializable;

/**
 * Created with IntelliJ IDEA.
 * User: voorbeeld
 * Date: 5/20/13
 * Time: 3:24 PM
 * To change this template use File | Settings | File Templates.
 */
public class Cursor implements Serializable {

	public double x = 0;
	public double y = 0;

	public Cursor(double x, double y) {
		this.x = x;
		this.y = y;
	}

	public Cursor(Cursor other) {
		this.x = other.x;
		this.y = other.y;
	}
}
