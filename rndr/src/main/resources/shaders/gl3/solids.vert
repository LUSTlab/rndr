#version 330

// -- transforms --
uniform mat4 modelViewProjectionMatrix;
uniform mat4 modelViewMatrix;
uniform mat4 normalMatrix;
uniform mat4 projectionMatrix;

//uniform vec3 offset;
//uniform vec3 dimensions;

uniform samplerBuffer dataBuffer;
uniform samplerBuffer p_colors2;

${uniforms! }



// -- vertex --
in vec2 texCoord0;
in vec3 normal;
in vec3 position;

// -- output --
out vec3 v_normal;
out vec3 v_cameraPosition;
out vec3 v_modelPosition;
out vec3 v_worldPosition;
out vec3 v_boundsPosition;
out vec3 v_modelNormal;

flat out int v_instance;
void main() {

    int instance = gl_InstanceID;

    vec3 offset = texelFetch(dataBuffer, instance*2).xyz;
    vec3 dimensions = texelFetch(dataBuffer, instance*2+1).xyz;

    v_instance = instance;
    vec3 x_position = position * dimensions;
    vec3 x_normal = normal;

    {${ positionTransform! }}

    v_modelNormal = normal;

    v_boundsPosition = position;
    vec3 transformed = x_position + offset;

    v_modelPosition = (x_position);
    v_worldPosition = v_modelPosition + offset;

    v_cameraPosition = (modelViewMatrix * vec4(transformed, 1)).xyz;
    v_normal = normalize((normalMatrix * vec4(x_normal, 0)).xyz);


    gl_Position = (modelViewProjectionMatrix * vec4(transformed, 1));


}