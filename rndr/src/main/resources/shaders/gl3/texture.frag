#version 330

layout(origin_upper_left) in vec4 gl_FragCoord;

// -- uniforms
uniform sampler2D image;
uniform vec4 tint;
uniform mat4 colorMatrix;


${outputs! }
${uniforms! }
${preamble! }


// -- varyings
in vec2 v_texCoord0;


// -- outputs
out vec4 o_color;

in vec3 v_modelPosition;
in vec3 v_cameraPosition;
in vec3 v_boundsPosition;

void main(void) {

    // -- shade style helpers
    vec2 screenPosition = gl_FragCoord.xy;
    vec3 modelPosition = v_modelPosition;
    vec3 cameraPosition = v_cameraPosition;
    vec3 boundsPosition = v_boundsPosition;

    vec2 x_texCoord0 = v_texCoord0;
    { ${texCoordTransform! } }

    vec4 x_tint = tint;
    { ${tintTransform! } }

    float alpha = texture(image, v_texCoord0).a;
    vec4 color = colorMatrix * vec4(texture(image, v_texCoord0).rgb, 1.0);
    color.rgb /= color.w;
    vec4 x_color = vec4(color.rgb, alpha) * x_tint;

    vec4 x_fill = x_color;
    { ${fillTransform! } }
    //{ ${colorTransform! } }
    o_color = x_fill;



}