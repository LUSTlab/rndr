package net.lustlab.rndr.math;


public class Transforms {


    public static class Look {

        public Vector3 from = new Vector3(0, 0, 1);
        public Vector3 to = Vector3.Zero;
        public Vector3 up = Vector3.UnitY;
        public Look from(Vector3 from) {
            this.from = from;
            return this;
        }
        public Look to(Vector3 to) {
            this.to = to;
            return this;
        }

        public Look withUp(Vector3 up) {
            this.up = up;
            return this;
        }

        public Matrix44 matrix() {
            return lookAt(from, to, up);
        }

    }

    public static Look look() {
        return new Look();
    }


    public static Matrix44 normal(Matrix44 m) {

        Matrix33 mu = new Matrix33(
                m.m00, m.m10, m.m20,
                m.m01, m.m11, m.m21,
                m.m02, m.m12, m.m22 );

        Matrix33 muit = mu.inverse().transpose();

        Matrix44 n = new Matrix44
                (muit.m00, muit.m10, muit.m20,0,
                 muit.m01, muit.m11, muit.m21,0,
                 muit.m02, muit.m12, muit.m22,0,
                 0, 0, 0, 0);
        return n;
    }

    public static Matrix44 ortho(double left, double right, double bottom, double top, double near, double far) {

        double tx = -(right + left) / (right - left);
        double ty = -(top + bottom) / (top - bottom);
        double tz = -(far + near) / (far - near);

        return new Matrix44(
                2.0 / (right - left), 0, 0, tx,
                0, 2.0 / (top - bottom), 0, ty,
                0, 0, -2 / (far - near), tz,
                0, 0, 0, 1);

    }

    public static Matrix44 frustum(double left, double right, double bottom, double top, double near, double far) {

        double a = (right + left) / (right - left);
        double b = (top + bottom) / (top - bottom);
        double c = -(far + near) / (far - near);
        double d = -(2 * far * near) / (far - near);

        return new Matrix44(
                (2 * near) / (right - left), 0, a, 0,
                0, (2 * near) / (top - bottom), b, 0,
                0, 0, c, d,
                0, 0, -1, 0
        );
    }

    public static Matrix44 perspectiveDegrees(double fovy, double aspect, double near, double far, double xoff, double yoff) {

        double fH = Math.tan(fovy / 360 * Math.PI) * near;
        double fW = fH * aspect;

        return frustum(-fW + xoff, fW + xoff, -fH + yoff, fH + yoff, near, far);
    }

    public static Matrix44 perspectiveHorizontalDegrees(double fovy, double aspect, double near, double far, double xoff, double yoff) {

        double fW = Math.tan(fovy / 360 * Math.PI) * near;
        double fH = fW / aspect;

        return frustum(-fW + xoff, fW + xoff, -fH + yoff, fH + yoff, near, far);
    }

    public static Matrix44 perspectiveDegrees(double fovy, double aspect, double near, double far) {

        double fH = Math.tan(fovy / 360 * Math.PI) * near;
        double fW = fH * aspect;

        return frustum(-fW, fW, -fH, fH, near, far);

//        double f = 1.0/Math.tan(fovy / 2);
//        return new Matrix44(
//                f / aspect, 0, 0, 0,
//                0, f, 0, 0,
//                0, 0, (far + near) / (near - far), (2 * far * near) / (near - far),
//                0, 0, -1, 0).transpose();
    }


    public static Matrix44 perspectiveRadians(double fovy, double aspect, double near, double far) {

        double fH = Math.tan(fovy ) * near;
        double fW = fH * aspect;

        return frustum(-fW, fW, -fH, fH, near, far);

//        double f = 1.0/Math.tan(fovy / 2);
//        return new Matrix44(
//                f / aspect, 0, 0, 0,
//                0, f, 0, 0,
//                0, 0, (far + near) / (near - far), (2 * far * near) / (near - far),
//                0, 0, -1, 0).transpose();
    }

    public static Matrix44 translate(Vector3 translation) {
        Matrix44 m = Matrix44.fromColumnVectors(Vector4.UnitX, Vector4.UnitY, Vector4.UnitZ, new Vector4(translation.x, translation.y, translation.z, 1));
        return m;
    }

    // from: http://www.opengl.org/sdk/docs/man2/xhtml/gluLookAt.xml
    public static Matrix44 lookAt(Vector3 eye, Vector3 center, Vector3 up) {

        Matrix44 parts[] = lookAtParts(eye, center, up);

        return parts[0].multiply(parts[1]);

    }

    public static Matrix44[] lookAtParts(Vector3 eye, Vector3 center, Vector3 up) {

        up = up.normalized();

        if ( Math.abs(up.dot(eye.normalized())) > 0.9999) {
            up = new Vector3(up.x, up.z, up.y);
        }

        Vector3 za = center.minus(eye).normalized();
        Vector3 xa = up.cross(za).normalized();
        Vector3 ya = za.cross(xa).normalized();

        Vector3 re = eye.scale(-1);

        Matrix44 t = new Matrix44(
                1,  0,  0,  re.x,
                0,  1,  0,  re.y,
                0,  0,  1,  re.z,
                0,  0,  0,  1);


        Matrix44 m = new Matrix44(
                xa.x, ya.x, -za.x, 0,
                xa.y, ya.y, -za.y, 0 ,
                xa.z, ya.z, -za.z, 0,
                0,0,0, 1).transpose();



        return new Matrix44[] { m, t };

    }


    public static Matrix44 scale(double s) {
        return new Matrix44(s, 0, 0, 0,
                            0, s, 0, 0,
                            0, 0, s, 0,
                            0, 0, 0, 1);
    }

    public static Matrix44 scale(double sx, double sy, double sz) {
        return new Matrix44(sx, 0, 0, 0,
                0, sy, 0, 0,
                0, 0, sz, 0,
                0, 0, 0, 1);
    }

    public static Matrix44 rotate(Quaternion q) {
        return q.toRotationMatrix44();
    }


    // from: http://en.wikipedia.org/wiki/Rotation_matrix
    public static Matrix44 rotate(Vector3 axis, double r) {
        double cr = Math.cos(r);
        double sr = Math.sin(r);

        double ux = axis.x;
        double uy = axis.y;
        double uz = axis.z;

        Vector4 c0 = new Vector4(cr + axis.x * axis.x * (1 - cr),
                axis.y * axis.x * (1 - cr) + axis.z * sr,
                axis.z * axis.x * (1 - cr) - axis.y * sr,0);

        Vector4 c1 = new Vector4(ux * uy * (1 - cr) - uz * sr,
                cr + uy * uy * (1 - cr),
                uz * uy * (1 - cr) + ux * sr,0);

        Vector4 c2 = new Vector4(ux * uz * (1 - cr) + uy * sr,
                uy * uz * (1 - cr) - ux * sr,
                cr + uz * uz * (1 - cr),0);

        return Matrix44.fromColumnVectors(c0, c1, c2, Vector4.UnitW);

    }

    public static Matrix44 rotateX(double r) {
        double cr = Math.cos(r);
        double sr = Math.sin(r);
        return new Matrix44(
                1, 0, 0, 0,
                0, cr, -sr, 0,
                0, sr, cr, 0,
                0, 0, 0, 1
        );
    }

    public static Matrix44 rotateY(double r) {
        double cr = Math.cos(r);
        double sr = Math.sin(r);
        return new Matrix44(
                cr, 0, sr, 0,
                0, 1, 0, 0,
                -sr, 0, cr, 0,
                0, 0, 0, 1
        );
    }


    public static Matrix44 rotateZ(double r) {
        double cr = Math.cos(r);
        double sr = Math.sin(r);
        return new Matrix44(
                cr, -sr, 0, 0,
                sr, cr, 0, 0,
                0, 0, 1, 0,
                0, 0, 0, 1
        );
    }


}
