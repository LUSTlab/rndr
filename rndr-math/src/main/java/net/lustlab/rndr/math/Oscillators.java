package net.lustlab.rndr.math;

/**
 * Created by edwin on 11/11/15.
 */
public class Oscillators {

    public static double saw(double x) {

        double cx = ((x % 2.0) + 2.0) % 2.0;

        double b;

        if (cx < 1) {
            b = cx;
        }
        else {
            b = 1-(cx-1);
        }
        return (b-0.5) * 2.0;

    }

    public static int saw(int x, int length) {

        int ix = ((x % (2*length)) + (2*length)) % (2*length);
        if (ix < length) {
            return ix;
        } else {
            return length - (ix-length) - 1;
        }


    }

}
