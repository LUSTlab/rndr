#version 330

in vec3 position;
in vec3 normal;
in vec4 color;

in vec2 texCoord0;
in vec2 texCoord1;
in vec2 texCoord2;


//out vec3 v_normal;
//out vec4 v_color;
//out mat4 v_modelViewMatrix;
//out vec2 v_texCoord0;

uniform mat4 modelViewMatrix;
uniform  mat4 modelViewProjectionMatrix;
uniform  mat4 normalMatrix;

void main(void) {
    gl_Position = modelViewProjectionMatrix * vec4(position.xyz, 1.0);
//    v_normal = (normalMatrix * vec4(normal.xyz, 0.0)).xyz;
//    v_color = color;
//    v_modelViewMatrix = modelViewMatrix;
//    v_texCoord0 = texCoord0;
}