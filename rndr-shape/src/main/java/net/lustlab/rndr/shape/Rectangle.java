package net.lustlab.rndr.shape;


import net.lustlab.rndr.math.Vector2;

public class Rectangle {

    public double x;
    public double y;
    public double width;
    public double height;

    /**
     * Constructs a rectangle at (0,0) with width and height set to 0.
     */
    public Rectangle() {
    }

    /**
     * Constructs a triangle at the given position with the given dimensions
     * @param x
     * @param y
     * @param width
     * @param height
     */
    public Rectangle(double x, double y, double width, double height) {
        this.x = x;
        this.y = y;
        this.width = width;
        this.height = height;
    }

    /** Determines if point is inside rectangle
     * @param point points
     */
    public boolean contains(Vector2 point) {
        return (x <= point.x
                && y <= point.y
                && x+width >= point.x
                && y+height >= point.y);
    }

    /**
     * Determines if rectangle intersects
     * @param rectangle
     * @return
     */
    public boolean intersects(Rectangle rectangle) {
        boolean left = x + width < rectangle.x;
        boolean above = y + height < rectangle.y;
        boolean below = rectangle.y + rectangle.height < y;
        boolean right = rectangle.x + rectangle.width < x;

        return !(left || above || below || right);
    }


    public static Rectangle bounds(Iterable<Rectangle> rectangles) {
        double minX = Double.POSITIVE_INFINITY;
        double minY = Double.POSITIVE_INFINITY;
        double maxX = Double.NEGATIVE_INFINITY;
        double maxY = Double.NEGATIVE_INFINITY;

        for (Rectangle rectangle: rectangles) {
            minX = Math.min(minX, rectangle.x);
            minX = Math.min(minX, rectangle.x+rectangle.width);
            minY = Math.min(minY, rectangle.y);
            minY = Math.min(minY, rectangle.y+rectangle.height);
            maxX = Math.max(maxX, rectangle.x);
            maxX = Math.max(maxX, rectangle.x+rectangle.width);
            maxY = Math.max(maxY, rectangle.y);
            maxY = Math.max(maxY, rectangle.y+rectangle.height);
        }
        return new Rectangle(minX, minY, maxX - minX, maxY - minY);
    }
    /**
     * Finds rectangle that fits around input rectangles
     * @param rectangles
     * @return
     */
    public static Rectangle bounds(Rectangle ... rectangles) {
        double minX = Double.POSITIVE_INFINITY;
        double minY = Double.POSITIVE_INFINITY;
        double maxX = Double.NEGATIVE_INFINITY;
        double maxY = Double.NEGATIVE_INFINITY;

        for (Rectangle rectangle: rectangles) {
            minX = Math.min(minX, rectangle.x);
            minX = Math.min(minX, rectangle.x+rectangle.width);
            minY = Math.min(minY, rectangle.y);
            minY = Math.min(minY, rectangle.y+rectangle.height);
            maxX = Math.max(maxX, rectangle.x);
            maxX = Math.max(maxX, rectangle.x+rectangle.width);
            maxY = Math.max(maxY, rectangle.y);
            maxY = Math.max(maxY, rectangle.y+rectangle.height);
        }
        return new Rectangle(minX, minY, maxX - minX, maxY - minY);

    }



    @Override
    public String toString() {
        return "Rectangle{" +
                "x=" + x +
                ", y=" + y +
                ", width=" + width +
                ", height=" + height +
                '}';
    }

    /**
     * Returns the position of the center of the rectangle
     * @return
     */
    public Vector2 center() {
        return new Vector2(x + width/2, y + height/2);
    }

    /**
     * Return the position of the top-left corner of the rectangle
     * @return
     */
    public Vector2 topLeft() {
        return new Vector2(x, y);
    }
}
