package net.lustlab.rndr.draw.stroke;

import net.lustlab.rndr.buffers.VertexBuffer;

public class Command {

    final Expansion.ExpansionType type;
    final VertexBuffer vertexBuffer;
    final int offset;
    final int vertexCount;
    final float minx;
    final float maxx;
    final float maxy;
    final float miny;

    public Command(VertexBuffer vertexBuffer, Expansion.ExpansionType type, int offset,int vertexCount,
        float minx, float maxx, float miny, float maxy)

    {
        this.type = type;
        this.vertexBuffer = vertexBuffer;
        this.vertexCount = vertexCount;
        this.offset = offset;
        this.minx = minx;
        this.maxx = maxx;
        this.miny = miny;
        this.maxy = maxy;
    }

}
