package net.lustlab.rndr.font.image;

import com.jogamp.opengl.GLContext;
import com.jogamp.opengl.GLProfile;
import com.jogamp.opengl.util.texture.Texture;
import com.jogamp.opengl.util.texture.awt.AWTTextureIO;
import net.lustlab.packer.*;
import net.lustlab.packer.Rectangle;
import net.lustlab.rndr.RNDRException;
import net.lustlab.rndr.font.EntityPairIndex;
import net.lustlab.rndr.font.FontResolver;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.awt.*;
import java.awt.font.FontRenderContext;
import java.awt.font.GlyphVector;
import java.awt.geom.AffineTransform;
import java.awt.geom.Rectangle2D;
import java.awt.image.BufferedImage;
import java.awt.image.WritableRaster;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.Serializable;
import java.util.*;
import java.util.List;

public class FontImageMapBuilder {

    static private Logger logger = LogManager.getLogger(FontImageMapBuilder.class);

    public static final char[] alphabet = {
            'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z',
            'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z',
            'ë', 'ä', 'ö', 'ü', 'ï', 'ÿ',
            'Ë', 'Ä', 'Ö', 'Ü', 'Ï', 'Ÿ',
            'ñ', 'Ñ',
            'ç', 'Ç',
            'ø', 'Ø',
            'é', 'á', 'ó', 'í', 'ú',
            'É', 'Á', 'Ó', 'Í', 'Ú',
            'è', 'à', 'ò', 'ì', 'ù',
            'È', 'À', 'Ò', 'Ì', 'Ù',
            'â', 'ê', 'î','û','ô',
            'Â', 'Ê', 'Î','Û','Ô',
            'œ','Œ','æ', 'Æ',
            '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', '0',
            '!', '?','¿','¡',
            '…', '.', ',', ' ', ':', ';', '&',
            '#', '№',
            '“','”','‘','’',
            '`',
            '¤','€', '$','£',
            '‒','-', '—', '–','_',
            '·','•','°',
            '@',
            '^',
            '*',
            '«','»',
            '/', '\\',
            '"', '\'',
            '+', '=','÷','~','%',
            '(', ')', '[', ']','{', '}','<', '>',
    };

    public static FontImageMap buildFontMapFromStream(InputStream stream, int size) {
        int format = Font.TRUETYPE_FONT;
        try {
            Font font = Font.createFont(format, stream).deriveFont((float)size);
            return buildFontMap(font, size);
        } catch (FontFormatException | IOException e) {
            throw new RNDRException("error loading font from stream", e);
        }

    }


    public static FontImageMap buildFontMap(Font font, int size) {

        font = font.deriveFont((float)size);

        Map<EntityPairIndex, Float> kerningPairs = new HashMap<>();
        // -- here is were we built the kerning pairs but we dropped the dependency on ipdf
        // -- so no kerning pairs until we find a proper alternative
        final int sanding = 4;

        int packSize = 256;

        BufferedImage offImg;
        Graphics2D graphics;
        FontMetrics metrics;
        Packer packer = new Packer(new AsIsOrderer(), new BinarySplitter(), null);
        PackNode root;
        while (true) {

            offImg = new BufferedImage(packSize,packSize, BufferedImage.TYPE_INT_ARGB);
            graphics = offImg.createGraphics();
            graphics.setFont(font);
            metrics = graphics.getFontMetrics();

            graphics.setRenderingHint(RenderingHints.KEY_TEXT_ANTIALIASING, RenderingHints.VALUE_TEXT_ANTIALIAS_ON);
            graphics.setRenderingHint(RenderingHints.KEY_FRACTIONALMETRICS, RenderingHints.VALUE_FRACTIONALMETRICS_ON);

            root = new PackNode(new Rectangle(0, 0, packSize, packSize));
            if (!attemptPack(root, packer, graphics, font, metrics, sanding)) {
                packSize*=2;
                logger.info("failed, pack size set to : " + packSize);
            }
            else {
                logger.info("pack size set to : " + packSize);
                root = new PackNode(new Rectangle(0, 0, packSize, packSize));
                break;
            }
        }

        graphics.clearRect(0, 0, packSize, packSize);

        Map<Character, PackNode> packMap = new HashMap<>();

        // set the background alpha to 0
        WritableRaster raster = offImg.getRaster();
        for (int j = 0; j <packSize; ++j) {
            for (int i = 0; i < packSize; ++i) {
                raster.setPixel(j, i, new int[] {0,0,0,0});
            }
        }

        List<Entry> entries = getSortedEntries(graphics, font);
        FontRenderContext frc = new FontRenderContext(null, true, true);
        for (Entry entry: entries) {
            PackNode dest = packer.insert(root, new Rectangle(0,0, (int)entry.pixelBounds.getWidth() + 2 * sanding, (int)entry.pixelBounds.getHeight() + 2*sanding));
            dest.setData(entry);

            logger.trace(entry.entity + " " + dest.getArea().width + " " + dest.getArea().height);

            packMap.put(entry.entity, dest);
            entry.width = metrics.stringWidth(""+entry.entity);
            graphics.drawRect((int)dest.getArea().topLeft.x, (int)dest.getArea().topLeft.y, (int)dest.getArea().width, (int)dest.getArea().height);

            if (font.canDisplay(entry.entity)) {

                GlyphVector g = font.createGlyphVector(frc, ""+entry.entity);
                graphics.setRenderingHint(RenderingHints.KEY_TEXT_ANTIALIASING, RenderingHints.VALUE_TEXT_ANTIALIAS_ON);
                graphics.setRenderingHint(RenderingHints.KEY_FRACTIONALMETRICS, RenderingHints.VALUE_FRACTIONALMETRICS_ON);

                double mx = entry.pixelBounds.getMinX();
                double my = entry.pixelBounds.getMinY();
                graphics.drawGlyphVector(g, (int) (dest.getArea().topLeft.x-mx+4), (int) (dest.getArea().topLeft.y-my+4));
            } else {
                logger.warn("font cannot display: " + entry.entity);
            }

        }

        Texture texture = AWTTextureIO.newTexture(GLProfile.getGL2GL3(), offImg, false);
        texture.bind(GLContext.getCurrentGL());
        GLContext.getCurrentGL().glGenerateMipmap(texture.getTarget());

        int fontHeight =  metrics.getMaxAscent() + metrics.getMaxDescent();
        int ascenderLength = metrics.getMaxAscent();
        int descenderLength = metrics.getMaxDescent();

        return new FontImageMap(texture, packMap, kerningPairs, sanding, fontHeight, size, metrics.getLeading(), font.getName())
                .ascenderLength(ascenderLength)
                .descenderLength(descenderLength);
    }

    public static class Entry implements Serializable {
        char entity;
        public Rectangle2D pixelBounds;
        public int width;
    }

    private static boolean attemptPack(PackNode root, Packer packer, Graphics2D graphics, Font font, FontMetrics metrics, int sanding) {
        boolean success = true;

        List<Entry> entries = getSortedEntries(graphics, font);

        for (Entry entry: entries) {
            int head = metrics.getMaxAscent()/4;
            PackNode dest = packer.insert(root, new Rectangle(0,0, (int)entry.pixelBounds.getWidth() + 2 * sanding,(int) entry.pixelBounds.getHeight() + 2*sanding + head));
            if (dest == null) {
                success = false;
                break;
            }
        }
        return success;
    }

    private static List<Entry> getSortedEntries(Graphics2D graphics, Font font) {
        List<Entry> entries = new ArrayList<>();

        AffineTransform at = AffineTransform.getTranslateInstance(0, 0);
        FontRenderContext frc = new FontRenderContext(at, true, true);
        for (char entity: alphabet) {
            FontImageMapBuilder.Entry entry = new FontImageMapBuilder.Entry();
            graphics.setFont(font);

            GlyphVector glyph = font.createGlyphVector(frc, ""+entity);
            Rectangle2D pixelBounds = glyph.getPixelBounds(frc, 0, 0);

            entry.entity = entity;
            entry.pixelBounds = pixelBounds;
            entries.add(entry);
        }

        Collections.sort(entries, new Comparator<Entry>() {
            @Override
            public int compare(FontImageMapBuilder.Entry o1, FontImageMapBuilder.Entry o2) {
                double a1 = o1.pixelBounds.getWidth() * o1.pixelBounds.getHeight();
                double a2 = o2.pixelBounds.getWidth() * o2.pixelBounds.getHeight();

                if (a1 < a2) return 1;
                else if (a1 > a2) return -1;
                else return 0;

            }
        });
        return entries;
    }

}

