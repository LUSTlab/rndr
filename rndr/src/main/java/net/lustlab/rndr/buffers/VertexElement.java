package net.lustlab.rndr.buffers;

public class VertexElement {

	public String attribute;
	public int size;
	public int offset;
	public int type;
	public int count;


	@Override
	public String toString() {
		return "VertexElement{" +
				"attribute='" + attribute + '\'' +
				", size=" + size +
				", offset=" + offset +
				", type=" + type +
				", count=" + count +
				'}';
	}
}
