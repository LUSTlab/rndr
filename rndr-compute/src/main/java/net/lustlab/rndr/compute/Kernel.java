package net.lustlab.rndr.compute;

import com.jogamp.opencl.CLKernel;


public class Kernel {


    Range globalOffset = Range.ZERO_3;
    Range localSize = Range.ZERO_3;
    Range globalSize = Range.ZERO_3;

    final CLKernel kernel;
    public Kernel(CLKernel kernel) {
        this.kernel = kernel;
    }

    public Kernel arguments(Object ... arguments) {

        for (int i = 0; i < arguments.length; ++i) {

            Object a = arguments[i];

            if (a instanceof Integer) {
                kernel.setArg(i, (int)a);
            } else if (a instanceof Float) {
                kernel.setArg(i, (float)a);
            } else if (a instanceof Double) {
                kernel.setArg(i, (double)a);
            } else if (a instanceof ComputeImage) {
                argument(i, (ComputeImage)a);
            } else if (a instanceof ComputeImage3d) {
                argument(i, (ComputeImage3d)a);
            } else if (a instanceof ComputeBuffer) {
                argument(i, (ComputeBuffer)a);
            } else if (a instanceof ComputeColorBuffer) {
                argument(i, (ComputeColorBuffer)a);
            } else if (a instanceof ComputeVertexBuffer) {
                argument(i, (ComputeVertexBuffer)a);
            } else if (a instanceof ComputeBufferTexture) {
                argument(i, (ComputeBufferTexture)a);
            } else {
                throw new IllegalArgumentException("unsupported argument type " + a.getClass() + " for argument " + i );
            }


        }
        return this;
    }

    public Kernel argument(int position, int value) {
        kernel.setArg(position, value);
        return this;
    }

    public Kernel argument(int position, double value) {
        kernel.setArg(position, value);
        return this;
    }

    public Kernel argument(int position, float value) {
        kernel.setArg(position, value);
        return this;
    }

    public Kernel argument(int position, long value) {
        kernel.setArg(position, value);
        return this;
    }

    public Kernel argument(int position, short value) {
        kernel.setArg(position, value);
        return this;
    }

    public Kernel argument(int position, ComputeImage image) {
        kernel.setArg(position, image.image);
        return this;
    }

    public Kernel argument(int position, ComputeImage3d image) {
        kernel.setArg(position, image.image);
        return this;
    }

    public Kernel argument(int position, ComputeColorBuffer image) {
        kernel.setArg(position, image.image);
        return this;
    }


    public Kernel argument(int position, ComputeVertexBuffer vertexBuffer) {
        kernel.setArg(position, vertexBuffer.buffer);
        return this;
    }

    public Kernel argument(int position, ComputeBufferTexture bufferTexture) {
        kernel.setArg(position, bufferTexture.buffer);
        return this;
    }

    public Kernel argument(int position, ComputeBuffer buffer) {
        kernel.setArg(position, buffer.buffer);
        return this;
    }

    public Kernel globalOffset(Range offset) {
        this.globalOffset = offset;
        return this;

    }
    public Kernel globalSize(Range size) {
        this.globalSize = size;
        return this;

    }
    public Kernel localSize(Range size) {
        this.localSize = size;
        return this;
    }

    public Kernel run1D(CommandQueue queue) {
        queue.run1D(this, globalOffset.values[0], globalSize.values[0], localSize.values[0]);
        return this;
    }

    public Kernel run2D(CommandQueue queue) {
        queue.run2D(this, globalOffset.values[0], globalOffset.values[1],
                globalSize.values[0], globalSize.values[1],
                localSize.values[0], localSize.values[1]);
        return this;
    }

    public Kernel run3D(CommandQueue queue) {
        queue.run3D(this, globalOffset.values[0], globalOffset.values[1], globalOffset.values[2],
                globalSize.values[0], globalSize.values[1],  globalSize.values[2],
                localSize.values[0], localSize.values[1], localSize.values[2]);
        return this;
    }


}
