package net.lustlab.rndr.file;

import net.lustlab.horizon.Event;

import java.io.File;

/**
 * Created by voorbeeld on 4/27/17.
 */
public class FileWatcher {

    public static class FileModifiedEvent {
        public File file;

        public FileModifiedEvent(File file) {
            this.file = file;
        }
    }

    public Event<FileModifiedEvent> fileModified = new Event<>("file-modified");

    private long lastModified = 0;
    private File file;

    public FileWatcher(File file) {
        this.file = file;
        if (file.exists()) {
            lastModified = 0;
        } else {
            System.out.println("file not found!" + file);
        }
    }

    public void update() {
        if (file.exists()) {
            long lf = file.lastModified();
            if (lf > lastModified) {
                lastModified = lf;
                fileModified.trigger(new FileModifiedEvent(file));
            }
        }
    }

}
