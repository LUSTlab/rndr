package net.lustlab.rndr.math;

public class Matrix22 {

    final public double m00, m10;
    final public double m01, m11;

    public Matrix22() {
        m00 = m10 = .0;
        m01 = m11 = .0;
    }

    public Matrix22(final double diagonalValue) {
        m00 = m11 = diagonalValue;
        m01 = .0;
        m10 = .0;
    }

    public Matrix22(final Vector2 col0, final Vector2 col1) {
        this.m00 = col0.x; this.m10 = col1.x;
        this.m01 = col0.y; this.m11 = col1.y;
    }

}
