package net.lustlab.rndr.compute;


import com.jogamp.opencl.gl.CLGLImage2d;
import net.lustlab.rndr.buffers.ColorBuffer;

import java.awt.*;

/**
 * Created by edwin on 21/04/17.
 */
public class ComputeColorBuffer {

    final CLGLImage2d image;
    final ColorBuffer colorBuffer;
    ComputeColorBuffer(CLGLImage2d image, ColorBuffer colorBuffer) {
        this.image = image;
        this.colorBuffer = colorBuffer;
    }

    public int width() {
        return image.width;
    }

    public int height() {
        return image.height;
    }

    public ColorBuffer colorBuffer() {
        return colorBuffer;
    }

}
