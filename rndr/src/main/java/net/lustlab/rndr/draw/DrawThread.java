package net.lustlab.rndr.draw;


import net.lustlab.horizon.Event;

public abstract class DrawThread implements Runnable {

    protected boolean running = false;
    boolean stop = false;
    public Event<DrawThread> done = new Event<>("draw-thread-done");

    public boolean done() {
        return !running;
    }

    public void stop() {
        stop = true;
    }
    public boolean stopRequested() {
        return stop;
    }

    public abstract void swap();
}
