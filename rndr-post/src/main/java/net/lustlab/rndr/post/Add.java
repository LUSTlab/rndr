package net.lustlab.rndr.post;

import net.lustlab.rndr.shaders.FragmentShader;


public class Add extends Filter {
    public Add() {
        super(FragmentShader.fromUrl("cp:shaders/gl3/post/add.frag"));
    }
}
