package net.lustlab.rndr.video.profiles;

import net.lustlab.rndr.video.VideoWriterProfile;

public class HAPAlphaProfile extends VideoWriterProfile<HAPAlphaProfile> {
    @Override
    public String[] arguments() {
        return new String[] {
                "-vcodecf", "hap",
                "-format", "hap_alpha",

        };
    }
}
