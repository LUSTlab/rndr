package net.lustlab.rndr.video;

import com.jogamp.common.net.Uri;
import com.jogamp.opengl.GL;
import com.jogamp.opengl.GL3;
import com.jogamp.opengl.GLContext;
import com.jogamp.opengl.util.av.GLMediaPlayer;
import com.jogamp.opengl.util.av.GLMediaPlayerFactory;
import com.jogamp.opengl.util.texture.TextureSequence;
import jogamp.opengl.util.av.GLMediaPlayerImpl;
import jogamp.opengl.util.av.impl.FFMPEGMediaPlayer;
import net.lustlab.horizon.Event;
import net.lustlab.rndr.buffers.*;
import net.lustlab.rndr.draw.Drawer;
import net.lustlab.rndr.draw.ShadeStyle;
import net.lustlab.rndr.draw.gl3.StateTools;
import net.lustlab.rndr.math.Vector2;
import net.lustlab.rndr.math.Vector3;
import net.lustlab.rndr.shaders.ShadeStyleManager;
import net.lustlab.rndr.shaders.Shader;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.File;
import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.ByteBuffer;

public class VideoPlayer {

    static Logger logger = LogManager.getLogger(VideoPlayer.class);
    GLMediaPlayer glmp;
    Uri uri;
    ShadeStyleManager shadeStyleManager;
    VertexBuffer rectangle;
    ByteBuffer rectangleBuffer;
    VertexFormat layout = new VertexFormat().position(3).textureCoordinate(2);

    boolean destroyed = false;

    boolean playRequested = false;
    boolean pauseRequested = false;
    boolean oneFrameCompleted = true;
    boolean needInit = true;
    boolean oneFrameMode = false;
    boolean isNewFrame = false;
    int endThreshold = 1000;

    GL3 gl3;
    int tries = 0;
    long framesProcessed;
    private boolean playEnd;
    private TextureSequence.TextureFrame newestFrame;

    public static class StreamError {
        public String message;

        public StreamError(String message) {
            this.message = message;
        }
    }

    public static class FrameEvent {
        public VideoPlayer videoPlayer;
        public long frame;

        public FrameEvent(VideoPlayer videoPlayer, long frame) {
            this.videoPlayer = videoPlayer;
            this.frame = frame;
        }
    }

    public Event<FrameEvent> newFrame = new Event<>();
    public Event<FrameEvent> ended = new Event<>();
    public Event<StreamError> error = new Event<>();

    public static VideoPlayer forCameraStream() {

        VideoPlayer videoPlayer = new VideoPlayer();
        videoPlayer.source("camera:/0");
        return videoPlayer;
    }

    public static VideoPlayer forCameraStream(int device, int width, int height, String fps) {
        VideoPlayer videoPlayer = new VideoPlayer();
        videoPlayer.source(String.format("camera:/%d?width=%d;height=%d;rate=%s", device, width, height, fps));
        return videoPlayer;
    }

    public static VideoPlayer forCameraStream(int device, String fps) {
        VideoPlayer videoPlayer = new VideoPlayer();
        videoPlayer.source(String.format("camera:/%d?rate=%s", device, fps));
        return videoPlayer;
    }


    public static void setFinalStatic(Field field, int newValue) throws Exception {
        field.setAccessible(true);
        Field modifiersField = Field.class.getDeclaredField("modifiers");
        modifiersField.setAccessible(true);
        modifiersField.setInt(field, field.getModifiers() & ~Modifier.FINAL);
        field.setInt(null, newValue);
    }
    public static void setFinalStatic(Field field, boolean newValue) throws Exception {
        field.setAccessible(true);
        Field modifiersField = Field.class.getDeclaredField("modifiers");
        modifiersField.setAccessible(true);
        modifiersField.setInt(field, field.getModifiers() & ~Modifier.FINAL);
        field.setBoolean(null, newValue);
    }

    public static void setFinalStatic(Field field, float newValue) throws Exception {
        field.setAccessible(true);
        Field modifiersField = Field.class.getDeclaredField("modifiers");
        modifiersField.setAccessible(true);
        modifiersField.setInt(field, field.getModifiers() & ~Modifier.FINAL);
        field.setFloat(null, newValue);
    }

    public void setEndThreshold(int threshold) {
        endThreshold = threshold;
    }

    public void dptsMax(int max) {
        {
            FFMPEGMediaPlayer fm = (FFMPEGMediaPlayer) glmp;

            try {
                setFinalStatic(GLMediaPlayerImpl.class.getDeclaredField("VIDEO_DPTS_MAX"), max);
            } catch (NoSuchFieldException e) {
                e.printStackTrace();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

    }

    public VideoPlayer() {





        gl3 = GLContext.getCurrentGL().getGL3();
        shadeStyleManager = new ShadeStyleManager("cp:shaders/gl3/videoplayer.vert", "cp:shaders/gl3/videoplayer.frag");
        rectangleBuffer = ByteBuffer.allocate(layout.size() * 6);
        rectangle = VertexBuffer.createDynamic(layout, 6);
        glmp = GLMediaPlayerFactory.createDefault();
        System.out.println(glmp.getAudioSink());



        glmp.addEventListener(new GLMediaPlayer.GLMediaEventListener() {
            @Override
            public void attributesChanged(GLMediaPlayer glMediaPlayer, int eventMask, long time) {

                logger.debug("attributesFromFormat changed: " + eventMask);

                if ((eventMask & GLMediaPlayer.GLMediaEventListener.EVENT_CHANGE_INIT) != 0) {
                    logger.debug("init:" + framesProcessed);
                }

                if ((eventMask & GLMediaPlayer.GLMediaEventListener.EVENT_CHANGE_ERR) != 0) {
                    logger.error("VideoPlayer error while playing " + uri.toString());
                    if (glmp.getState() == GLMediaPlayer.State.Uninitialized) {
                        GLMediaPlayer.StreamException e = glmp.getStreamException();
                        logger.error("Likely " + uri.toString() + " could not be found or is not a video file");
                        logger.error("Stream exception: " + e.getMessage());
                        error.trigger(new StreamError(e.getMessage()));
                    } else {
                        GLMediaPlayer.StreamException e = glmp.getStreamException();
                        logger.error("Stream exception: " + e.getMessage());
                        error.trigger(new StreamError(e.getMessage()));
                    }
                }

                if ((eventMask & GLMediaPlayer.GLMediaEventListener.EVENT_CHANGE_EOS) != 0) {
                    logger.debug("end of stream reached");
                    logger.debug("end!");
                    ended.trigger(new FrameEvent(VideoPlayer.this, framesProcessed));
                }

                if ((eventMask & GLMediaPlayer.GLMediaEventListener.EVENT_CHANGE_PLAY) != 0) {
                    logger.debug("play state changed");
                }

            }



            @Override
            public void newFrameAvailable(GLMediaPlayer glMediaPlayer, TextureSequence.TextureFrame textureFrame, long l) {
                isNewFrame = true;
                framesProcessed++;
                newestFrame = textureFrame;


                if (oneFrameMode) {
                    logger.debug("got one frame. stopping");
                    pauseRequested = true;
                    playRequested = false;
                    oneFrameCompleted = true;
                }
                newFrame.trigger(new FrameEvent(VideoPlayer.this, framesProcessed));
            }
        });
    }

    public boolean destroyed() {
        return destroyed;
    }

    public VideoPlayer destroy() {
        if (!destroyed) {
            glmp.destroy(gl3);
            destroyed = true;
        }

        return this;
    }

    public VideoPlayer source(File file) {
        try {
            uri = Uri.valueOf(file);
        } catch (URISyntaxException e) {
            throw new RuntimeException(e);
        }
        return this;
    }

    public VideoPlayer source(URI source) {
        try {
            uri = Uri.valueOf(source);
        } catch (URISyntaxException e) {
            throw new RuntimeException(e);
        }
        return this;
    }

    public VideoPlayer source(URL source) {
        try {
            uri = Uri.valueOf(source);
        } catch (URISyntaxException e) {
            throw new RuntimeException(e);
        }
        return this;
    }

    public VideoPlayer source(String sourceUrl) {
        try {
            try {
                URL url = new URL(sourceUrl);
                if (url.getProtocol().equals("file")) {
                        url = new File( url.getPath()).getAbsoluteFile().toURL();
                }

                uri = Uri.valueOf(url);
            } catch (MalformedURLException e) {
                uri = Uri.cast(sourceUrl);
            }


        } catch (URISyntaxException e) {
            throw new RuntimeException("source url malformed", e);
        }
        return this;
    }

    public VideoPlayer play() {
        playRequested = true;
        oneFrameMode = false;
        return this;
    }


    public VideoPlayer waitForOneFrameAndDraw(Drawer drawer) {

        if (destroyed) {
            logger.error("destroyed! waitForOneFrameAndDraw");
            return this;
        }

        int startPts = glmp.getVideoPTS();

        if (needInit && glmp.getState() == GLMediaPlayer.State.Initialized) {
            try {
                glmp.initGL(GLContext.getCurrentGL());
            } catch (GLMediaPlayer.StreamException e) {
                throw new RuntimeException(e);
            }
            needInit = false;
            startPts = -1;
            logger.debug("initing");
        }

        if (needInit && glmp.getState() == GLMediaPlayer.State.Uninitialized) {
            //System.out.println("need to init..");
            return this;
        }

        playRequested = true;
        oneFrameMode = true;
        oneFrameCompleted = false;

        TextureSequence.TextureFrame tf = null;
        glmp.play();
        int pts = startPts;
        int tries = 0;

        if (startPts >= glmp.getDuration()- 40) {
            //System.err.println("video ended");
            tf = glmp.getNextTexture(gl3);

        } else {

            int sleep = 1;
            while (startPts == pts && tries < 100) {
                tries++;
                if (glmp.getState() == GLMediaPlayer.State.Playing || glmp.getState() == GLMediaPlayer.State.Paused) {
                    if (glmp.isTextureAvailable()) {
                        tf = glmp.getNextTexture(gl3);
                    }
                } else {
                    logger.info("not playing?");
                }

                pts = glmp.getVideoPTS();
                if (framesProcessed < 2) {
                    logger.info("waiting for real frame");
                    sleep  = 40;
                    pts = startPts;

                }

                if (pts == startPts) {
                    try {
                        Thread.sleep(sleep);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                } else {
                    glmp.pause(false);
                }
            }
        }
        if (tf != null) {
            drawImpl(drawer, 0, 0, width(), height(), tf);
        }

        return this;
    }

    public VideoPlayer waitForOneFrame() {
        playRequested = true;
        oneFrameMode = true;
        oneFrameCompleted = false;

        int  tries = 0;
        while (!oneFrameCompleted && !playEnd) {
            try {
                if (glmp.getState() == GLMediaPlayer.State.Playing || glmp.getState() == GLMediaPlayer.State.Paused) {
                    glmp.getNextTexture(gl3);
                }
                update();
                Thread.sleep(10);

            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            tries++;
            if (tries > 1000) {
                logger.info("breaking out of wait loop!");
                break;
            }
        }
//        System.out.println("wait done: " + tries);
        return this;

    }

    public VideoPlayer playOneFrame() {



        if (tries > 100 || oneFrameCompleted) {
            logger.debug("requesting one frame");
            oneFrameMode = true;
            playRequested = true;
            oneFrameCompleted = false;
            tries = 0;
        }
        else {
            //System.out.println("still waiting " + tries);
            tries++;
        }

        return this;
    }

    public boolean newFrame() {
        return isNewFrame;
    }

    public long framesProcessed() {
        return framesProcessed;
    }

    public VideoPlayer start() {
        //glmp.initStream(uri, GLMediaPlayer.STREAM_ID_AUTO, GLMediaPlayer.STREAM_ID_AUTO,GLMediaPlayer.TEXTURE_COUNT_DEFAULT);
        logger.debug("calling initStream");
//        GLMediaPlayer
        glmp.initStream(uri, GLMediaPlayer.STREAM_ID_AUTO, GLMediaPlayer.STREAM_ID_NONE, GLMediaPlayer.TEXTURE_COUNT_DEFAULT);

        System.out.println("hallo");


        return this;
    }

    public VideoPlayer pause() {
        //glmp.pause(true);
        pauseRequested = true;
        return this;
    }

    public VideoPlayer seek(double time) {
        if (!destroyed) {
            logger.debug("calling seek" + time);
            glmp.seek((int) (time * 1000.0));
        } else {
            logger.error("destroyed! (seek)");
        }
        return this;
    }

    public double time() {
        return glmp.getVideoPTS() / 1000.0;
    }

    public VideoPlayer speed(double speed) {
        glmp.setPlaySpeed((float)speed);
        return this;
    }

    public double speed() {
        return glmp.getPlaySpeed();
    }

    public int width() {
        if (glmp.getState() != GLMediaPlayer.State.Uninitialized) {
            return glmp.getWidth();
        }
        else {
            return 0;
        }
    }

    public int height() {
        if (glmp.getState() != GLMediaPlayer.State.Uninitialized) {
            return glmp.getHeight();
        }
        else {
            return 0;
        }
    }


    public double volume() {
        return glmp.getAudioVolume();
    }

    public VideoPlayer volume(double volume) {
        glmp.setAudioVolume((float)volume);
        return this;
    }




    public double duration() {
        return glmp.getDuration()/1000.0;
    }

    public VideoPlayer draw(Drawer drawer) {
        return draw(drawer, 0,0, width(), height());
    }

    public VideoPlayer draw(Drawer drawer, double x, double y) {
        return draw(drawer, x, y, width(), height());
    }

    public VideoPlayer draw(Drawer drawer, double x, double y, double width, double height) {



        update();

        if (glmp.getState() == GLMediaPlayer.State.Paused || glmp.getState() == GLMediaPlayer.State.Playing) {
            TextureSequence.TextureFrame tf;

            if (glmp.getState() == GLMediaPlayer.State.Playing && !oneFrameMode) {
                tf = glmp.getNextTexture(gl3);
            }
            else {
                tf = glmp.getLastTexture();
            }

            //glmp.getNextTexture(gl3);

            //glmp.getLastTexture();

            if (framesProcessed > 0 & newestFrame != null) {
                drawImpl(drawer, x, y, width, height, tf);
            }
        }

        return this;
    }

    private void drawImpl(Drawer drawer, double x, double y, double width, double height, TextureSequence.TextureFrame tf) {
        BufferWriter bw = new BufferWriter(rectangleBuffer);

        StateTools.setupState(drawer.drawStyle());
        rectangleBuffer.rewind();

        double tw = (glmp.getWidth() / (double)tf.getTexture().getWidth());

        bw.write(new Vector3(x, y, 0)).write(new Vector2(0, 0));
        bw.write(new Vector3(width + x, 0, 0)).write(new Vector2(tw, 0));
        bw.write(new Vector3(width + x, height + y, 0)).write(new Vector2(tw, 1));

        bw.write(new Vector3(x, y, 0)).write(new Vector2(0, 0));
        bw.write(new Vector3(x, height + y, 0)).write(new Vector2(0, 1));
        bw.write(new Vector3(width + x, height + y, 0)).write(new Vector2(tw, 1));
        rectangleBuffer.rewind();

        rectangle.write(rectangleBuffer);
        rectangle.bind();

        ShadeStyle shadeStyle = drawer.shadeStyle() == null? new ShadeStyle() : new ShadeStyle(drawer.shadeStyle());
        shadeStyle.preamble = glmp.getTextureLookupFragmentShaderImpl().replace("texture2D(", "texture(");

        if (shadeStyle.preamble.length() < 3) {
            shadeStyle.preamble = "vec4 ffmpegTexture2D(sampler2D c, vec2 tc) { return texture(c, tc); }";
        }

        Shader shader = shadeStyleManager.shader(shadeStyle, rectangle.format());

        shader.begin();
        shadeStyleManager.setParameters(shader, drawer.shadeStyle());
        shader.setUniform("modelViewMatrix", drawer.modelView());
        shader.setUniform("projectionMatrix", drawer.projection());
        shader.setUniform("colorMatrix", drawer.colorMatrix());
        shader.setUniform("tint", drawer.tint());
        shader.setUniform("fill", drawer.fill());
        shader.setUniform("stroke", drawer.stroke());

        shader.setUniform1f("uOffset", (float) tw);
        tf.getTexture().bind(gl3);
        tf.getTexture().setTexParameteri(gl3, GL.GL_TEXTURE_MAG_FILTER, GL.GL_LINEAR);
        VertexBufferDrawer.draw(gl3, shader, rectangle, layout, GL3.GL_TRIANGLES, 6);
        rectangle.unbind();
        shader.end();
    }

    private void update() {

        if (needInit && glmp.getState() == GLMediaPlayer.State.Initialized) {
            try {
                logger.debug("calling initGL");
                glmp.initGL(GLContext.getCurrentGL());

            } catch (GLMediaPlayer.StreamException e) {
                throw new RuntimeException(e);
            }
            needInit = false;
        }

        if (playRequested && glmp.getState() == GLMediaPlayer.State.Paused) {
            logger.debug("playing");
            glmp.play();
            playRequested = false;
        }

        if (pauseRequested && glmp.getState() == GLMediaPlayer.State.Playing) {
            logger.debug("pausing");
            glmp.pause(false);
            pauseRequested = false;
        }



        if (glmp.getDuration() > 0) {

            boolean foundEnd = false;

            if (glmp.getVideoFrames()-5 <= glmp.getPresentedFrameCount() && glmp.getState() == GLMediaPlayer.State.Playing) {
                if (!playEnd) {
                    playEnd = true;
                    ended.trigger(new FrameEvent(this, -1));
                }
                foundEnd = true;
            }

            if (glmp.getVideoPTS() >= glmp.getDuration() - endThreshold) {
                if (!playEnd) {
                    playEnd = true;
                    ended.trigger(new FrameEvent(this, -1));
                }
                foundEnd = true;
            }

            if (!foundEnd) {
                playEnd = false;
            }
        }

        isNewFrame = false;
    }
}
