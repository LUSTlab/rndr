package net.lustlab.rndr.font.image;

import com.jogamp.opengl.GL;
import com.jogamp.opengl.GL3;
import net.lustlab.colorlib.ColorRGBa;
import net.lustlab.rndr.RNDRException;

import net.lustlab.rndr.buffers.*;
import net.lustlab.rndr.draw.BlendMode;
import net.lustlab.rndr.shape.Rectangle;
import net.lustlab.rndr.draw.ShadeStyle;
import net.lustlab.rndr.font.FontMap;
import net.lustlab.rndr.font.FontMapRenderer;
import net.lustlab.rndr.gl.GLError;
import net.lustlab.rndr.math.Matrix44;
import net.lustlab.rndr.shaders.ShadeStyleManager;
import net.lustlab.rndr.shaders.Shader;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;


import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.List;

import static net.lustlab.rndr.math.Linear.vector2;
import static net.lustlab.rndr.math.Linear.vector3;

public class FontImageMapRendererGL3 implements FontMapRenderer {

    static ShadeStyleManager shadeStyleManager = new ShadeStyleManager("cp:shaders/gl3/font.vert", "cp:shaders/gl3/font.frag");

    ShadeStyle shadeStyle;
    final static Logger logger = LogManager.getLogger(FontImageMapRendererGL3.class);
    //static Shader shader = null;
    static VertexBuffer[] vbo;
    static VertexArray[] vao;

    static int bufferCount = 1;

    int bufferIndex;

    final static VertexFormat layout = new VertexFormat().textureCoordinate(2).position(3);
    static ByteBuffer buffer;

    static final int bufferSize = 6 * 3 * layout.size() * 16384;
    static BufferWriter bufferWriter;

    boolean buffered = true;

    public ColorRGBa fill = ColorRGBa.WHITE;

    private Rectangle clipRectangle;

    public int vertexCount = 0;
    private FontImageMap fontImageMap;
    private Matrix44 modelView;
    private Matrix44 projection;
    private BlendMode blendMode = BlendMode.NORMAL;

    public static enum VerticalPositioning {
        Baseline,
        Height
    }

    public VerticalPositioning verticalPositioning = VerticalPositioning.Baseline;

    public GL gl;

    public FontImageMapRendererGL3(GL gl) {
        this.gl = gl;

        if (gl != null) {

            if (vbo == null) {
                buffer = ByteBuffer.allocateDirect(bufferSize);
                bufferWriter = new BufferWriter(buffer);

                vbo = new VertexBuffer[bufferCount];
                vao = new VertexArray[bufferCount];

                for (int i = 0; i < bufferCount; ++i) {
                    vbo[i] = VertexBuffer.createDynamic(layout, 6*3*16384);
                    Shader shader = shadeStyleManager.shader(shadeStyle,  vbo[i].format());

                    vao[i] = VertexArray.create(gl.getGL3(), vbo[i], layout, shader);
                }

            }

        }
        buffer.rewind();
    }

    @Override
    public List<CharacterRectangle> render(GL gl, FontMap fontImageMap, String text, double x, double y) {
        throw new RuntimeException("not implemented");
    }

    @Override
    public List<CharacterRectangle> render(GL gl, FontMap fontMap, String text, double x, double y, double tracking) {
        throw new RuntimeException("not implemented");
    }

    @Override
    public List<CharacterRectangle> render(GL3 gl, Matrix44 projection, Matrix44 modelView, FontMap fontMap, String text, double x, double y) {
        return render(gl, projection, modelView, fontMap, text, x, y, 0);
    }

    @Override
    public List<CharacterRectangle> render(GL3 gl, Matrix44 projection, Matrix44 modelView, FontMap fontMap, String text, double x, double y, double tracking) {

        FontImageMap fontImageMap = (FontImageMap) fontMap;
        Shader shader = shadeStyleManager.shader(shadeStyle, layout);
        GLError.debug(gl);

        this.projection = projection;
        this.modelView = modelView;

        if (!buffered) {
            buffer.rewind();


        }
        List<CharacterRectangle> result = new ArrayList<>();



        if (text != null && fill != null) {
            if (!buffered) {
                setupGL(gl, fontImageMap);
            }


            float xoff = 0;

            for (int i = 0; i < text.length(); ++i) {
                char c = text.charAt(i);

                if (c == '\u00A0') {
                    c = ' ';
                }
                if (i != 0) {
                    char left = text.charAt(i - 1);
                    xoff += fontImageMap.kerning(left, c);
                }
                if (i != 0) {
                    xoff += tracking;
                }

                if (fontImageMap.getMap().get(c) != null) {
                    result.add(new CharacterRectangle(c, (x + (int) xoff), y, fontImageMap.getMap().get(c).getArea().width, fontMap.fontHeight()));
                    renderCharacterQuad(fontImageMap, c, (x + (int) xoff), y);

                    xoff += fontImageMap.characterWidth(c);
                }
            }
            if (!buffered) {
                renderCharacters(gl);

                gl.glBlendFunc(GL.GL_SRC_ALPHA, GL.GL_ONE_MINUS_SRC_ALPHA);
                GLError.debug(gl);

                gl.glEnable(GL.GL_BLEND);
                GLError.debug(gl);

                shader.end();
            }


        }
        return result;
    }

    @Override
    public void blend(BlendMode blendMode) {
        this.blendMode = blendMode;
    }

    private void setupGL(GL3 gl, FontImageMap fontImageMap) {
        GLError.debug(gl);

        if (projection == null) {
            throw new RNDRException("no projection matrix was set");
        }

        if (modelView == null) {
            throw new RNDRException("no model/view matrix was set");
        }
        Shader shader = shadeStyleManager.shader(shadeStyle, layout);

        shader.begin();
        shader.setUniform("modelViewProjectionMatrix", projection.multiply(modelView));

        shader.setUniform("fill", fill);

        double clipLeft = -1;
        double clipRight = 8192 * 4;
        double clipTop = -1;
        double clipBottom = 8192 * 4;
        if (clipRectangle != null) {
            clipLeft = clipRectangle.x;
            clipTop = clipRectangle.y;
            clipRight = clipLeft + clipRectangle.width;
            clipBottom = clipTop + clipRectangle.height;
        }

        shader.setUniform4f("clipArea", (float) clipLeft, (float) clipRight, (float) clipTop, (float) clipBottom);

        int[] viewport = new int[4];
        gl.glGetIntegerv(GL.GL_VIEWPORT, viewport, 0);
        GLError.debug(gl);

        shader.setUniform1f("viewHeight", viewport[3]);
        shader.setUniform1i("image", 0);

        gl.glActiveTexture(GL.GL_TEXTURE0);
        GLError.debug(gl);

        fontImageMap.getTexture().bind(gl);
        gl.glTexParameteri(GL.GL_TEXTURE_2D, GL.GL_TEXTURE_MIN_FILTER, GL.GL_LINEAR);
        GLError.debug(gl);

        gl.glTexParameteri(GL.GL_TEXTURE_2D, GL.GL_TEXTURE_MAG_FILTER, GL.GL_LINEAR);
        GLError.debug(gl);

        gl.glEnable(GL.GL_BLEND);
        GLError.debug(gl);

        switch (blendMode) {
            case NORMAL:
                gl.glBlendFunc(GL.GL_ONE, GL.GL_ONE_MINUS_SRC_ALPHA);
                break;
            case ADD:
                gl.glBlendFunc(GL.GL_ONE, GL.GL_ONE);
                break;
            case ADD_ALPHA:
                gl.glBlendFunc(GL.GL_SRC_ALPHA, GL.GL_ONE);

        }
        GLError.debug(gl);

    }

    @Override
    public void clip(double left, double top, double width, double height) {
        clipRectangle = new Rectangle(left, top, width, height);
    }

    @Override
    public void noClip() {
        clipRectangle = null;

    }

    @Override
    public void fill(ColorRGBa color) {
        fill = new ColorRGBa(color);
    }

    @Override
    public void noFill() {
        fill = null;
    }

    @Override
    public void stroke(ColorRGBa color) {

    }

    @Override
    public void noStroke() {

    }

    @Override
    public void shadeStyle(ShadeStyle shadeStyle) {
        this.shadeStyle = shadeStyle;
    }

    @Override
    public double width(FontMap fontMap, String text) {

        FontImageMap fontImageMap = (FontImageMap) fontMap;

        double width = 0;

        for (int i = 0; i < text.length(); ++i) {
            char c = text.charAt(i);

            if (i != 0) {
                char left = text.charAt(i - 1);
                width += fontImageMap.kerning(left, c);
            }

            width += fontImageMap.characterWidth(c);
        }
        return width;
    }


    public void render(GL3 gl, FontImageMap fontImageMap, char character, int x, int y) {
        fontImageMap.getTexture().enable(gl);
        GLError.debug(gl);

        fontImageMap.getTexture().bind(gl);
        GLError.debug(gl);

        gl.glEnable(GL.GL_BLEND);
        GLError.debug(gl);

        gl.glBlendFunc(GL.GL_ONE, GL.GL_ONE_MINUS_SRC_ALPHA);
        GLError.debug(gl);

        buffer.rewind();
        vertexCount = 0;
        renderCharacterQuad(fontImageMap, character, x, y);
        renderCharacters(gl);

        GLError.debug(gl);

        gl.glBlendFunc(GL.GL_SRC_ALPHA, GL.GL_ONE_MINUS_SRC_ALPHA);
        gl.glEnable(GL.GL_BLEND);
        GLError.debug(gl);

        fontImageMap.getTexture().disable(gl);

    }

    private void renderCharacters(GL3 gl3) {
        GLError.debug(gl);

        buffer.rewind();
        vbo[bufferIndex].write(buffer, 0, vertexCount * layout.size());
        vao[bufferIndex].bind();

        VertexBufferDrawer.draw(gl3, vao[bufferIndex], GL.GL_TRIANGLES, vertexCount);
        vao[bufferIndex].unbind();

        vertexCount = 0;
        buffer.rewind();
        bufferIndex = (bufferIndex + 1) % bufferCount;

    }


    /**
     * @param fontImageMap the font map to use
     * @param character    the character to render
     * @param x            the x position of the character quad
     * @param y            the y position of top-left corner of the character quad
     */
    private void renderCharacterQuad(FontImageMap fontImageMap, char character, double x, double y) {
        GLError.debug(gl);

        this.fontImageMap = fontImageMap;
        BufferWriter bw = bufferWriter;

        double s = fontImageMap.getSanding();

        double packSize = fontImageMap.getTexture().getHeight();

        if (fontImageMap.getMap().get(character) != null) {

            double characterWidth = fontImageMap.getMap().get(character).getArea().width - 2 * s;
            double characterHeight = fontImageMap.getMap().get(character).getArea().height - 2 * s;

            double u0 = (fontImageMap.getMap().get(character).getArea().topLeft.x + s) / packSize;
            double v0 = (fontImageMap.getMap().get(character).getArea().topLeft.y + s) / packSize;
            double u1 = u0 + characterWidth / packSize;
            double v1 = v0 + characterHeight / packSize;

            FontImageMapBuilder.Entry e = (FontImageMapBuilder.Entry) fontImageMap.getMap().get(character).getData();
            double xo = e.pixelBounds.getMinX();
            double yo = e.pixelBounds.getMinY();

            if (verticalPositioning == VerticalPositioning.Height) {
                yo += fontImageMap.fontHeight;
            }

            bw.write(vector2(u0, v0)).write(vector3(x + xo, y + yo, 0));
            bw.write(vector2(u1, v0)).write(vector3(x + characterWidth + xo, y + yo, 0));
            bw.write(vector2(u1, v1)).write(vector3(x + characterWidth + xo, y + characterHeight + yo, 0));

            bw.write(vector2(u0, v0)).write(vector3(x + xo, y + yo, 0));
            bw.write(vector2(u0, v1)).write(vector3(x + xo, y + characterHeight + yo, 0));
            bw.write(vector2(u1, v1)).write(vector3(x + characterWidth + xo, y + characterHeight + yo, 0));

            vertexCount += 6;
        }
    }

    public void flush() {
        GLError.debug(gl);
        Shader shader = shadeStyleManager.shader(shadeStyle, layout);

        if (buffered) {
            if (fontImageMap != null) {

                if (fontImageMap.getTexture() != null) {
                    setupGL(gl.getGL3(), fontImageMap);
                    renderCharacters(gl.getGL3());
                    shader.end();
                    gl.glBindTexture(GL.GL_TEXTURE_2D, 0);
                } else {
                    logger.error("fontImageMap has no texture!" + fontImageMap);
                }
            }
        }
        buffer.rewind();
    }
}