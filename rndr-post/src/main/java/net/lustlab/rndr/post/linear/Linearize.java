package net.lustlab.rndr.post.linear;

import net.lustlab.rndr.post.Filter;
import net.lustlab.rndr.shaders.FragmentShader;

public class Linearize extends Filter{
    public Linearize() {
        super(FragmentShader.fromUrl("cp:shaders/gl3/post/linear/linearize.frag"));
    }
}
