package net.lustlab.rndr.post;

import net.lustlab.rndr.shaders.FragmentShader;


public class Multiply extends Filter {
    public Multiply() {
        super(FragmentShader.fromUrl("cp:shaders/gl3/post/add.frag"));
    }
}
