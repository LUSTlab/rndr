package net.lustlab.rndr.draw.gl3;

import com.jogamp.opengl.GL3;

import net.lustlab.colorlib.ColorRGBa;
import net.lustlab.rndr.draw.*;
import net.lustlab.rndr.draw.stroke.ExpansionRenderer;
import net.lustlab.rndr.math.Vector2;
import net.lustlab.rndr.math.Vector4;
import net.lustlab.rndr.shaders.ShadeStyleManager;
import net.lustlab.rndr.shaders.Shader;
import net.lustlab.rndr.buffers.BufferWriter;
import net.lustlab.rndr.buffers.VertexBuffer;
import net.lustlab.rndr.buffers.VertexBufferDrawer;
import net.lustlab.rndr.buffers.VertexFormat;
import net.lustlab.rndr.shape.Polygon;
import net.lustlab.rndr.shape.Rectangle;
import net.lustlab.rndr.shape.Shape;
import net.lustlab.rndr.shape.Vertex;

import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.List;

import static net.lustlab.rndr.math.Linear.multiply;

public class PolygonRenderer {

    ShadeStyleManager shadeStyleManager;

    //LineRenderer lineRenderer;
    ExpansionRenderer expansionRenderer;

    VertexFormat layout;
    VertexBuffer vbo;

    GL3 gl3;


    @SuppressWarnings("UnusedDeclaration")
    public List<Vector2> weld(List<Vector2> points) {
        List<Vector2> result = new ArrayList<>();

        if (points.size() > 0) {
            result.add(points.get(0));

            if (points.size() > 1) {
                for (int i = 1; i < points.size(); ++i) {
                    Vector2 last = result.get(result.size()-1);
                    Vector2 c = points.get(i);
                    if (last.minus(c).length() > 0.1) {
                        result.add(c);
                    }
                }
            }
        }
        return result;
    }

    public PolygonRenderer(GL3 gl3) {

        expansionRenderer = new ExpansionRenderer();
        layout = new VertexFormat().color(4).position(3);

        this.gl3 = gl3;

        if (shadeStyleManager == null) {
            shadeStyleManager = new ShadeStyleManager(
                    "cp:shaders/gl3/polygon.vert",
                    "cp:shaders/gl3/polygon.frag");
        }


        vbo = VertexBuffer.createDynamic(layout, 10000);

    }

    public void drawShape(DrawContext context, DrawStyle style, Shape shape) {
        drawPolygon(context, style, shape.polygon());
    }

    public void drawPolygon(DrawContext context, DrawStyle style, Polygon polygon) {
        StateTools.setupState(style);
        Tessellation tessellation = Tessellation.tessellate(polygon);
        Rectangle bounds = polygon.bounds();
        BufferWriter bw = vbo.shadow().writer();
        vbo.shadow().buffer().rewind();
        int count = 0;
        Shader shader = shadeStyleManager.shader(style.shadeStyle, vbo.format());
        shader.begin();
        shader.setUniform("modelViewProjectionMatrix", multiply(context.projection, context.modelView));
        shader.setUniform("bounds", new Vector4(bounds.x, bounds.y, bounds.width, bounds.height));


        shadeStyleManager.setParameters(shader, style.shadeStyle);
        if (style.fill != null) {

            shader.setUniform("fill", style.fill);

            polygon.stroke = ColorRGBa.WHITE;

            for (Tessellation.PrimitiveData data : tessellation.data) {
                if (data.command == GL3.GL_TRIANGLES) {
                    for (Vertex vertex : data.vertices) {
                        bw.write(ColorRGBa.WHITE);
                        bw.write(vertex.position);
                        count++;
                    }
                }
            }

            vbo.shadow().buffer().rewind();
            vbo.write(vbo.shadow().buffer(), 0, count * layout.size());
            vbo.bind();
            VertexBufferDrawer.draw(gl3, shader, vbo, layout, GL3.GL_TRIANGLES, count);

        }
        count = 0;


        if (style.smooth) {
            gl3.glEnable(GL3.GL_LINE_SMOOTH);
            gl3.glLineWidth(0.25f);

            for (Tessellation.PrimitiveData data : tessellation.data) {
                if (data.command == GL3.GL_LINE_LOOP) {

                    count = 0;
                    for (Vertex vertex : data.vertices) {
                        bw.write(ColorRGBa.WHITE);
                        bw.write(vertex.position);
                        count++;
                    }


                    vbo.shadow().buffer().rewind();
                    vbo.write(vbo.shadow().buffer(), 0, count * layout.size());

                    VertexBufferDrawer.draw(gl3, shader, vbo, layout, GL3.GL_LINE_LOOP, count);
                }

            }
            gl3.glLineWidth(1.0f);

        }

        shader.end();

        if (style.stroke != null && style.stroke.a > 0.001) {

            for (Tessellation.PrimitiveData data : tessellation.data) {
                if (data.command == GL3.GL_LINE_LOOP || data.command == GL3.GL_LINE_STRIP) {

                    List<Vector2> points = new ArrayList<>();
                    for (Vertex vertex : data.vertices) {
                        points.add(vertex.position.xy());
                        count++;
                    }

//                    points = weld(points);


                    if (data.command == GL3.GL_LINE_LOOP) {


//                        expansionRenderer.
//                        lineRenderer.drawLineLoop(context, style, points);
                    } else {
//                        lineRenderer.drawLineStrip(context, style, points);
                    }
                }
            }
        }
    }

    public void destroy() {
        shadeStyleManager.destroy();
        //lineRenderer.destroy();
        vbo.destroy();
    }



}
