package net.lustlab.rndr.shape;

import net.lustlab.rndr.math.Functions;
import net.lustlab.rndr.math.Mapping;
import net.lustlab.rndr.math.Matrix44;
import net.lustlab.rndr.math.Vector2;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * ShapeContour class.
 */
public class ShapeContour extends Contour implements Serializable {
	public List<Segment> segments = new ArrayList<>();
	public boolean closed = false;


    public ShapeContour(List<Vector2> points) {
        for (int i = 0; i < points.size() - 1; ++i) {
            Segment s = new Segment(points.get(i), points.get(i+1));
            segments.add(s);
        }
    }

    public ShapeContour() {

    }
    public ShapeContour(ShapeContour other) {
        for (Segment segment: other) {
            Segment copySegment = new Segment(segment);
            segments.add(copySegment);
        }
        this.closed = other.closed;
    }

    public boolean closed() {
        return closed;
    }

    @Override
	public Iterator<Segment> iterator() {
		return segments.iterator();
	}

    @Override
    public String toString() {
        return "ShapeContour{" +
                "segments=" + segments +
                ", closed=" + closed +
                '}';
    }

    /**
     * Approximates the total length of the contour. This is potentially slow.
     * @return the total length of the contour
     */
    public double length() {
        double length = 0;
        for (Segment segment:this) {
            length+=segment.length();
        }
        return length;
    }


    public Vector2 position(double t) {
        t = Mapping.clamp(t, 0.0, 1.0);

        double length = segments.size();

        int segment = (int) (t*length);
        double segmentOffset = (t*length) % 1.0;
        return segments.get(Math.min(segments.size()-1,segment)).position(segmentOffset);
    }

    public Vector2 normal(double t) {
        t = Mapping.clamp(t, 0.0, 1.0);

        double length = segments.size();

        int segment = (int) (t*length);
        double segmentOffset = (t*length) % 1.0;
        return segments.get(Math.min(segments.size()-1,segment)).normal(segmentOffset);
    }


    public Vector2 direction(double t) {
        return velocity(t).normalized();
    }

    public Vector2 velocity(double t) {
        t = Mapping.clamp(t, 0.0, 1.0);

        double length = segments.size();

        int segment = (int) (t*length);
        double segmentOffset = (t*length) % 1.0;
        return segments.get(Math.min(segments.size()-1,segment)).velocity(segmentOffset);
    }


    public List<Vector2> adaptivePositions() {
        List<Vector2> adaptivePoints = new ArrayList<>();
        Vector2 last = null;
        for (Segment segment:this.segments) {

            List<Vector2> samples = segment.sampleAdaptive();

            if (samples.size() > 0) {

                Vector2 r = samples.get(0);

                if (last == null || last.minus(r).length()>0.01) {
                    adaptivePoints.add(r);
                }

                for (int i = 1; i < samples.size(); ++i) {
                    adaptivePoints.add(samples.get(i));
                    last = samples.get(i);
                }
            }

        }
        return adaptivePoints;
    }

    /**
     * Samples approximately equidistant points that lie approximately on the contour
     * @param pointCount the number of points to sample on the contour
     * @return a list of Vector2s
     */
    public List<Vector2> equidistantPositions(int pointCount) {
        return ContourTools.sampleEquidistant(adaptivePositions(), pointCount);
    }

    @Override
    public int segmentCount() {
        return segments.size();
    }

    public Contour concatenate(ShapeContour other) {
        ShapeContour r = new ShapeContour();
        r.segments.addAll(segments);
        r.segments.addAll(other.segments);
        return r;
    }

    public Contour sub(double t0, double t1) {

        if (closed && t0 < 0.0 || t1 > 1.0 || t1 < 0.0 || t1 > 1.0) {
            t0 = Functions.mod(t0, 1.0);
            t1 = Functions.mod(t1, 1.0);

            if (t0 > t1)
            return ((ShapeContour)sub(t0, 1.0)).concatenate((ShapeContour)sub(0.0, t1));
        }


        t0 = Mapping.clamp(t0, 0.0, 1.0);
        t1 = Mapping.clamp(t1, 0.0, 1.0);

        double z0 = t0;
        double z1 = t1;

        if (t0 > t1) {
            z0 = t1;
            z1 = t0;
        }

        double length = segments.size();

        int segment0 = (int) (z0*length);
        double segmentOffset0 = segment0 < segments.size() ?  (z0*length) % 1.0 : 1.0;

        int segment1= (int) (z1*length);
        double segmentOffset1 = segment1 < segments.size() ?  (z1*length) % 1.0 : 1.0;

        segment1 = Math.min(segments.size()-1, segment1);
        segment0 = Math.min(segments.size()-1, segment0);

        ShapeContour newContour = new ShapeContour();

        double epsilon = 0.000001;

        for (int s = segment0; s <= segment1; ++s) {
            if (s == segment0 && s == segment1) {
                //if (Math.abs(segmentOffset0-segmentOffset1) > epsilon)
                newContour.segments.add(segments.get(s).sub(segmentOffset0, segmentOffset1));
            } else if (s == segment0) {
                if (segmentOffset0 < 1.0-epsilon)
                newContour.segments.add(segments.get(s).sub(segmentOffset0, 1));
            } else if (s == segment1) {
                if (segmentOffset1 > epsilon)
                newContour.segments.add(segments.get(s).sub(0, segmentOffset1));
            } else {
                newContour.segments.add(segments.get(s));
            }
        }

        return newContour;
    }

    @Override
    public Contour equidistant(int pointCount) {
        ShapeContour result = new ShapeContour();

        List<Vector2> points = equidistantPositions(pointCount);

        for (int i = 0; i < points.size()-1; ++i) {
            result.segments.add(new Segment(points.get(i), points.get(i+1)));
        }


        //for (Vector2 p: sampleEquidistant(pointCount)) {
//            result.vertex(p);
//        }
        if (closed()) {
            result.closed = true;
        }
        return result;
    }

    @Override
    public Contour copy() {
        ShapeContour copy = new ShapeContour();

        for (Segment segment: segments) {
            copy.segments.add(segment.copy());
        }
        return copy;
    }

    @Override
    public Contour apply(Matrix44 transform) {

        ShapeContour contour = new ShapeContour();
        for (Segment segment: contour.segments) {
            contour.segments.add(segment.apply(transform));

        }
        return contour;
    }

    @Override
    public Contour reverse() {

        ShapeContour reversed = new ShapeContour();
        for (int i = segments.size()-1; i >= 0; --i) {
            reversed.segments.add(segments.get(i).reverse());
        }
        return reversed;
    }
}


