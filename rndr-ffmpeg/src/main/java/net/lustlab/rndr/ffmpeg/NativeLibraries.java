package net.lustlab.rndr.ffmpeg;

import org.apache.commons.io.FileUtils;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;

/**
 * Created by lustlab on 4/8/2015.
 */
public class NativeLibraries {

    static boolean inited = false;

    public static void init() {

        if (!inited) {
            inited = true;

            System.out.println(Thread.currentThread());
            String os = System.getProperty("os.name");
            String arch = System.getProperty("os.arch");

            if (os.toLowerCase().contains("windows") && arch.equals("amd64")) {

                String[] libraries = {"avutil-54.dll","postproc-53.dll", "swresample-1.dll", "avcodec-56.dll",  "avformat-56.dll","swscale-3.dll", "avfilter-5.dll", "avdevice-56.dll",  "postproc-53.dll", };
                File tempBase = new File(System.getProperty("java.io.tmpdir"));
                File tempDir = new File(tempBase, "rndr-ffmpeg");

                for (String library : libraries) {
                    try  (InputStream stream = NativeLibraries.class.getResourceAsStream("/ffmpeg/windows-amd64/" + library)) {
                        File target = new File(tempDir, library);
                        if (!target.exists()) {
                            FileUtils.copyInputStreamToFile(stream, target);
                        }
                    } catch (IOException e) {
                        throw new RuntimeException(e);
                    }
                }

                for (String library : libraries) {
//                    System.out.println("loading library" + library);
                    System.load(new File(tempDir, library).getAbsolutePath());
//                    System.out.println("done");
                }
            }
        }
    }

}
