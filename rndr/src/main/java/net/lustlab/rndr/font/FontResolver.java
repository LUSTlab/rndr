package net.lustlab.rndr.font;

import net.lustlab.rndr.RNDRException;

import java.awt.*;
import java.io.File;
import java.io.IOException;

/**
 * Created by voorbeeld on 12/19/13.
 */
public class FontResolver {



    public static Font resolveFileFont(String filename, int size) {
        int format = Font.TRUETYPE_FONT;

        final String[] type1Extensions = new String[] {
                ".pfb", ".pfm", ".afm", ".ofm", ".pfa"
        };
        if (filename.endsWith(".ttf") || filename.endsWith(".otf")) {
            format = Font.TRUETYPE_FONT;
        }
        else {
            for (String extension: type1Extensions) {
                if (filename.endsWith(extension)) {
                    format = Font.TYPE1_FONT;
                }
            }

        }
        File fontFile = new File(filename);
        if (fontFile.exists()) {
            try {
                Font font = Font.createFont(format, fontFile).deriveFont(Font.PLAIN, size);
                return font;
            } catch (FontFormatException | IOException e) {
                throw new RNDRException(e);
            }
        }
        else {
            throw new RuntimeException("font file not found! " + filename);
        }

    }


}
