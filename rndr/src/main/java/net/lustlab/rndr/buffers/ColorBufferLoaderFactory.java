package net.lustlab.rndr.buffers;

import com.jogamp.opengl.GLContext;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * Created by voorbeeld on 12/23/15.
 */
public class ColorBufferLoaderFactory {

    static Logger logger = LogManager.getLogger(ColorBufferLoaderFactory.class);
    static ColorBufferLoaderFactory instance = null;

    ColorBufferLoader loader;
    public static ColorBufferLoaderFactory instance() {
        if (instance == null) {
            logger.debug("creating new ColorBufferLoaderFactory");
            instance = new ColorBufferLoaderFactory();
        }
        return instance;
    }

    public ColorBufferLoader loader() {
        if (loader == null) {
            logger.debug("creating new ColorBufferLoader");
            loader = new ColorBufferLoader(GLContext.getCurrentGL());
            loader.start();
        }
        return loader;
    }

}
