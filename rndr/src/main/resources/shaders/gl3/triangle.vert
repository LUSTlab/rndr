#version 330

in vec3 position;
in vec3 normal;
in vec4 color;

in vec2 texCoord0;


uniform mat4 modelViewMatrix;
uniform mat4 modelViewProjectionMatrix;
uniform mat4 normalMatrix;
uniform vec2 dimensions;
uniform vec3 offset;

uniform vec2 screenSize;

void main(void) {
    vec4 pos = modelViewProjectionMatrix * vec4( position, 1);
    gl_Position = pos;
}