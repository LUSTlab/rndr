package net.lustlab.rndr.svg;

import net.lustlab.rndr.math.Matrix44;

public class Element {

	public Matrix44 transform = Matrix44.IDENTITY;
	public String id;
}
