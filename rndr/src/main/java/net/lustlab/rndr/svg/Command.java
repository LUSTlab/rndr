package net.lustlab.rndr.svg;


import net.lustlab.rndr.math.Vector2;

import java.util.ArrayList;
import java.util.List;

/**
 * Path Command
 */
public class Command {
	public char op;
	public List<Double> operands = new ArrayList<Double>();

	public Command() {

	}

	public Command(char op, Double ... operands) {
		this.op = op;
		for (Double operand: operands) {
			this.operands.add(operand);
		}
	}

    /**
     * Returns the command operands as a Vector2
     * @param i0 the index of the operand that will be placed in the x component of the vector
     * @param i1 the index of the operand that will be placed in the y component of the vector
     * @return a Vector2 instance with x and y set the values of the operands indexed by i0 and i1
     */
	public Vector2 vector(int i0, int i1) {
		double x = i0 == -1? 0: operands.get(i0);
		double y = i1 == -1? 0: operands.get(i1);
		return new Vector2(x, y);
	}

    /**
     * Returns the command operands as a list of Vector2 instances
     */
	public Vector2[] vectors() {
		Vector2[] result = new Vector2[operands.size()/2];
		for (int i = 0; i < result.length; ++i) {
			result[i] = new Vector2(operands.get(i*2), operands.get(i*2+1));
		}
		return result;
	}


	@Override
	public String toString() {
		return "Command{" +
				"op='" + op + '\'' +
				", operands=" + operands +
				'}';
	}
}
