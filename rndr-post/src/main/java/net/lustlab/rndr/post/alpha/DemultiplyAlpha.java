package net.lustlab.rndr.post.alpha;

import net.lustlab.rndr.post.Filter;
import net.lustlab.rndr.shaders.FragmentShader;

public class DemultiplyAlpha extends Filter {
    public DemultiplyAlpha() {
        super(FragmentShader.fromUrl("cp:shaders/gl3/post/alpha/demultiply.frag"));
    }
}
