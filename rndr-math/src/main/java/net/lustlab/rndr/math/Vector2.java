package net.lustlab.rndr.math;

import java.io.Serializable;

public class Vector2 implements Serializable {


    static final long serialVersionUID = 1;

    public static final Vector2 UnitX = new Vector2(1,0);
    public static final Vector2 UnitY = new Vector2(0,1);
    public static final Vector2 Zero = new Vector2(0,0);
    public static final Vector2 Infinity = new Vector2(Double.POSITIVE_INFINITY, Double.POSITIVE_INFINITY);

    @Override
    public String toString() {
        return "Vector2{" +
                "x=" + x +
                ", y=" + y +
                '}';
    }

    public final double x;
    public final double y;

    public Vector2() {
        x = 0;
        y = 0;
    }

    public Vector2(Vector2 that) {
        this.x = that.x;
        this.y = that.y;
    }

    public Vector2(double[] v) {
        this.x = v[0];
        this.y = v[1];
    }

    public Vector2(double x, double y) {
        this.x = x;
        this.y = y;
    }

    public Vector2(double v) {
        this.x = v;
        this.y = v;
    }

    public double squaredLength() {
        return x*x+y*y;
    }

    public Vector2 absolute() {
        return new Vector2(Math.abs(x), Math.abs(y));
    }

    public double length() {
        return Math.sqrt(x*x+y*y);
    }

    public Vector2 multiply(Matrix44 m) {
        return new Vector2(x * m.m00 + y * m.m10 + m.m30,
                x * m.m01 + y * m.m11 + m.m31);
    }

    public double get(int index) {
        switch (index) {
            case 0: return x;
            case 1: return y;
        }
        throw new RuntimeException("invalid element");
    }

    public Vector2 times (double s) {
        return scale(s);
    }

    public Vector2 times (Vector2 v) {
        return new Vector2(x*v.x, y*v.y);
    }

    public Vector2 scale(double s) {
        return new Vector2(x*s, y*s);
    }

    public Vector2 scale(double xs, double ys) {
        return new Vector2(x*xs, y*ys);
    }

    public Vector2 plus(Vector2 other) {
        return new Vector2(x+other.x, y+other.y);
    }

    public Vector2 minus(Vector2 other) {
        return new Vector2(x-other.x, y-other.y);
    }

    public Vector2 mask(Vector2 other) {
        return new Vector2(x*other.x, y*other.y);
    }

    public Vector2 perpendicular() {
        return new Vector2(-y, x);
    }

    public double perpDot(Vector2 other) {
        return x *other.y - y * other.x;
    }

    public double dot(Vector2 other) {
        return x*other.x + y*other.y;
    }

    public Vector2 normalized() {
        double l = length();
        if (l == 0) {
            l=1;
        }
        return new Vector2(x/l,y/l);
    }

    public Vector2 copy() {
        return new Vector2(this);
    }

    public Vector2 multiply(Matrix22 m) {
        return new Vector2(x * m.m00 + y * m.m10,
                x * m.m01 + y * m.m11);

    }

    public Vector2 withX(double x) {
        return new Vector2(x, y);
    }

    public Vector2 withY(double y) {
        return new Vector2(x, y);
    }

    public Vector2 xy() { return new Vector2(x,y); }
    public Vector2 yx() { return new Vector2(y,x); }

    public Vector3 xxx() { return new Vector3(x, x, x); }
    public Vector3 xxy() { return new Vector3(x, x, y); }
    public Vector3 xyx() { return new Vector3(x, y, x); }
    public Vector3 xyy() { return new Vector3(x, y, y); }
    public Vector3 yxx() { return new Vector3(y, x, x); }
    public Vector3 yxy() { return new Vector3(y, x, y); }
    public Vector3 yyx() { return new Vector3(y, y, x); }
    public Vector3 yyy() { return new Vector3(y, y, y); }

    public Vector3 xcy(double c) { return new Vector3(x, c, y); }
    public Vector3 cxy(double c) { return new Vector3(c, x, y); }
    public Vector3 ycx(double c) { return new Vector3(y, c, x); }
    public Vector3 cyx(double c) { return new Vector3(c, y, x); }

    public Vector3 xy0() {
        return new Vector3(x, y, 0);
    }

    public IntVector2 truncate() {
        return new IntVector2((int)x, (int)y);
    }

}
