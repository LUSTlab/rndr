package net.lustlab.rndr.post.blur;

import net.lustlab.colorlib.ColorRGBa;
import net.lustlab.rndr.buffers.ColorBuffer;
import net.lustlab.rndr.post.Filter;
import net.lustlab.rndr.shaders.FragmentShader;

import static net.lustlab.rndr.math.Linear.vector2;

/**
 * Box / uniform blur
 */
public class BoxBlur extends Filter {


    ColorBuffer intermediate;

    public BoxBlur() {
        super(FragmentShader.fromUrl("cp:shaders/gl3/post/blur/box-blur.frag"));
        window(5);
        amplitude(1.0);
    }

    public BoxBlur amplitude(double amplitude) {
        parameter("amplitude", (float) amplitude);
        return this;
    }

    public BoxBlur window(int window) {
        parameter("window", window);
        return this;
    }


    @Override
    public Filter apply(ColorBuffer[] src, ColorBuffer[] dest) {

        if (intermediate != null && (intermediate.width() != dest[0].width() || intermediate.height() != dest[0].height())) {
            intermediate.destroy();
            intermediate = null;
        }

        if (intermediate == null) {
            intermediate = ColorBuffer.create(dest[0].width(), dest[0].height(), dest[0].format(), dest[0].type());
        }

        parameter("blurDirection", vector2(1, 0));
        super.apply(src, new ColorBuffer[] { intermediate });
        parameter("subtract", new ColorRGBa(0, 0, 0, 0));
        parameter("blurDirection", vector2(0, 1));
        super.apply(new ColorBuffer[] { intermediate }, dest);

        return this;
    }
}
