package net.lustlab.rndr.buffers;

import com.jogamp.opengl.GL;
import com.jogamp.opengl.GL2GL3;

/**
 * Created by voorbeeld on 9/3/16.
 */
public enum DepthFormat {
    DEPTH24,
    DEPTH32F,
    DEPTH24_STENCIL8,
    DEPTH32F_STENCIL8;

    public final int toGLFormat() {
        switch(this) {
            case DEPTH24: return GL.GL_DEPTH_COMPONENT24;
            case DEPTH32F: return GL2GL3.GL_DEPTH_COMPONENT32F;
            case DEPTH24_STENCIL8: return GL.GL_DEPTH24_STENCIL8;
            case DEPTH32F_STENCIL8: return GL2GL3.GL_DEPTH32F_STENCIL8;
            default:
                throw new RuntimeException("unsupported depth format: " + this);
        }
    }


}
