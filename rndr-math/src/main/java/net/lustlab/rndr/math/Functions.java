package net.lustlab.rndr.math;

public class Functions {

    public static int mod(int x, int m) {
        return ((x % m) + m) % m;
    }

    public static double mod(double x, double m) {
        return ((x % m) + m) % m;
    }

    public static double saw(double x) {
        double cx = mod(x, 2);
        double b;
        if (cx < 1) {
            b = cx;
        }
        else {
            b = 1-(cx-1);
        }
        return (b-0.5) * 2.0;
    }

    public static int saw(int saw, int pivot) {
        int cx = mod(saw, pivot*2);
        if (cx > pivot) {
            cx = pivot-(cx-pivot);
        }
        return cx;
    }

}
