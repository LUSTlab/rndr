package net.lustlab.rndr.post.blend;

import net.lustlab.rndr.shaders.FragmentShader;

public class Overlay extends BlendFilter {
    public Overlay() {
        super(FragmentShader.fromUrl("cp:shaders/gl3/post/blend/overlay.frag"));
    }
}
