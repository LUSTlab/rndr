package net.lustlab.rndr.font.vector;

import net.lustlab.rndr.RNDRException;
import net.lustlab.rndr.shape.Composition;
import net.lustlab.rndr.shape.Figure;
import net.lustlab.rndr.draw.Tessellation;
import net.lustlab.rndr.font.FontResolver;

import java.awt.*;
import java.awt.font.FontRenderContext;
import java.awt.geom.AffineTransform;
import java.awt.geom.Rectangle2D;
import java.awt.image.BufferedImage;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;

public class FontVectorMapBuilder {

    public static final char[] alphabet = {
            'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z',
            'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z',
            'ë', 'ä', 'ö', 'ü', 'ï', 'ÿ',
            'Ë', 'Ä', 'Ö', 'Ü', 'Ï', 'Ÿ',
            'ñ', 'Ñ',
            'ç', 'Ç',
            'ø', 'Ø',
            'š',
            'é', 'á', 'ó', 'í', 'ú',
            'É', 'Á', 'Ó', 'Í', 'Ú',
            'è', 'à', 'ò', 'ì', 'ù',
            'È', 'À', 'Ò', 'Ì', 'Ù',
            'â', 'ê', 'î','û','ô',
            'Â', 'Ê', 'Î','Û','Ô',
            'œ','Œ','æ', 'Æ',
            '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', '0',
            '!', '?','¿','¡',
            '…', '.', ',', ' ', ':', ';', '&',
            '#', '№',
            '“','”','‘','’',
            '`',
            '¤','€', '$','£',
            '‒','-', '—', '–','_',
            '·','•','°',
            '@',
            '^',
            '*',
            '«','»',
            '/', '\\',
            '"', '\'',
            '+', '=','÷','~','%',
            '(', ')', '[', ']','{', '}','<', '>',
    };


    public static FontVectorMap buildFontMapFromStream(InputStream stream, int size) {
        try {
            Font font = Font.createFont(Font.TRUETYPE_FONT, stream).deriveFont((float)size);
            return buildFontVectorMap(size, font);
        } catch (FontFormatException | IOException e) {
            throw new RNDRException("could not load vector font from stream");
        }
    }

    public static FontVectorMap buildFontMapFromFile(String filename, int size) {
        return buildFontVectorMap(size, FontResolver.resolveFileFont(filename, size));
    }

    @Deprecated
    public static FontVectorMap buildFontMap(String fontName, int size) {
            return buildFontMapFromFile(fontName, size);

    }

    /**
     * Build a FontVectorMap from Font
     * @param size
     * @param font
     * @return
     */
    private static FontVectorMap buildFontVectorMap(int size, Font font) {

        font = font.deriveFont((float)size);
        FontVectorMap fvm = new FontVectorMap(font.getName(), size);
        GlyphExtractor extractor = new GlyphExtractor();


        FontRenderContext frc = new FontRenderContext(AffineTransform.getTranslateInstance(0, 0), false, true);

        {
            BufferedImage offImg  = new BufferedImage(100,100, BufferedImage.TYPE_INT_ARGB);
            Graphics2D graphics = offImg.createGraphics();
            graphics.setFont(font);
            FontMetrics metrics = graphics.getFontMetrics();

            Rectangle2D charBounds = font.getMaxCharBounds(frc);
            fvm.fontHeight = metrics.getMaxAscent() + metrics.getMaxDescent();
            fvm.leading = metrics.getLeading();
            fvm.descenderLength(metrics.getMaxDescent());
            fvm.ascenderLength(metrics.getMaxAscent());
        }

        for (char c: alphabet) {
            Composition composition = extractor.extractGlyph(font, ""+c);
            Figure figure = composition.figure();
            Tessellation tessellation = Tessellation.tessellate(figure);

            Rectangle2D bounds = font.getStringBounds("" + c, frc);

            if (font.canDisplay(c)) {
                FontVectorMap.Entry entry = new FontVectorMap.Entry();
                entry.composition = composition;
                entry.figure = figure;
                entry.tessellation = tessellation;
                entry.width = bounds.getWidth();
                fvm.characters.put(c, entry);
            }
        }
        return fvm;
    }

    public static FontVectorMap buildFontMap(URL url, int size) {
        try {
            Font font = Font.createFont(Font.TRUETYPE_FONT, url.openStream());
            return buildFontVectorMap(size, font);
        }
        catch (FileNotFoundException e) {
            throw new RNDRException("could not load font, because " + url + " was not found");
        } catch (IOException | FontFormatException e) {
            throw new RNDRException("could not load font", e);
        }
    }
}