#version 150

in vec3 position;
in vec2 texCoord0;

uniform mat4 modelViewMatrix;
uniform mat4 modelViewProjectionMatrix;

out vec2 v_texCoord0;

void main(void) {
    gl_Position = modelViewProjectionMatrix * vec4(position.xyz, 1.0);
    v_texCoord0 = texCoord0;
}