package net.lustlab.rndr.font;


/**
 * Generalized font map interface
 */
public interface FontMap<T extends FontMap> {
    /**
     * Returns the height of the font in pixels. This height is calculated by taking the sum of the ascender and descender lengths that are stored in the font metrics.
     * @return the height of the font in pixels
     */

    /**
     * Returns the size at which this font map is generated
     * @return the size in pixels?
     */
    int size();
    String name();

    double ascenderLength();
    T ascenderLength(double length);

    double descenderLength();
    T descenderLength(double length);

    double fontHeight();
    T fontHeight(double height);

    /**
     * Returns the leading in pixels. This leading value taken from the font metrics.
     * @return the leading in pixels
     */

    double leading();
}
