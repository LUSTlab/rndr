package net.lustlab.rndr.buffers;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.Optional;

public class ColorBufferProxy {

    public boolean persistent() {
        return persistent;
    }

    public static ColorBufferProxy fromUrl(String url) {

        try {
            return fromUrl(new URL(url));
        } catch (MalformedURLException e) {
            throw new RuntimeException(e);
        }
    }

    public static ColorBufferProxy fromUrl(URL url) {
        return ColorBufferLoaderFactory.instance().loader().fromUrl(url);
    }

    ColorBufferProxy() {

    }

    /**
     * Makes the ColorBufferProxy persistent. When a ColorBufferProxy is made persistent it will not be unloaded.
     * @param persistent
     * @return
     */
    public ColorBufferProxy persistent(boolean persistent) {
        this.persistent = persistent;
        return this;
    }

    boolean persistent = false;


    public ColorBufferProxy colorBuffer(ColorBuffer colorBuffer) {
        this.colorBuffer = colorBuffer;
        return this;
    }

    public enum State {
        NOT_LOADED,
        QUEUED,
        LOADED,
    }

    ColorBuffer colorBuffer;

    URL url;
    ColorBufferLoader loader;
    long lastTouched = -1;

    public static ColorBufferProxy createLoadable(URL url, ColorBufferLoader loader) {

        ColorBufferProxy cbp = new ColorBufferProxy();
        cbp.url = url;
        cbp.loader(loader);
        return cbp;
    }

    State state = State.NOT_LOADED;
    public State state() {
        return state;
    }

    /**
     * Returns the proxied ColorBuffer
     * @return the proxied ColorBuffer
     */
    public Optional<ColorBuffer> colorBuffer() {
        return Optional.ofNullable(colorBuffer);
    }


    ColorBufferProxy loader(ColorBufferLoader loader) {
        this.loader = loader;
        return this;
    }

    /**
     * Returns the time since the ColorBufferProxy is touched for the last time
     * @return the time since the ColorBufferProxy is touched for the last time in milliseconds.
     */
    public long timeSinceLastTouch() {
        return System.currentTimeMillis() - lastTouched;
    }

    /**
     * Returns when the ColorBufferProxy is touched for the last time
     * @return the time on which the ColorBuffer is touched for the last time
     */
    public long lastTouched() {
        return lastTouched;
    }

    /**
     * Touches the ColorBufferProxy
     * @return this
     */
    public ColorBufferProxy touch() {
        lastTouched = System.currentTimeMillis();
        return this;
    }

    /**
     * Queues the ColorBufferProxy to be loaded by a ColorBufferLoader
     * @return
     */
    public ColorBufferProxy queue() {
        touch();
        if (loader != null) {
            if (state == State.NOT_LOADED) {
                loader.queue(this);
                state =State.QUEUED;
            }
        }
        return this;
    }

    /**
     * Returns the url from which the ColorBufferProxy is or should be loaded
     * @return
     */
    public URL url() {
        return url;
    }

    public ColorBufferProxy state(State state) {
        this.state = state;
        return this;
    }



    public ColorBufferProxy error(String errorMessage) {
        return this;
    }

    @Override
    public String toString() {
        return "ColorBufferProxy{" +
                "persistent=" + persistent +
                ", state=" + state +
                ", url=" + url +
                ", colorBuffer=" + colorBuffer +
                ", lastTouched=" + lastTouched +
                '}';
    }
}
