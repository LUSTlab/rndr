package net.lustlab.rndr.compute;

public class Range {

    final public int[] values;


    final static Range ZERO_1 = new Range(0);
    final static Range ZERO_2 = new Range(0, 0);
    final static Range ZERO_3 = new Range(0, 0, 0);

    public Range(int ... x) {
        values = x;
    }

}
