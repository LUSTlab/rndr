package net.lustlab.rndr.shape;

import net.lustlab.colorlib.ColorRGBa;
import net.lustlab.rndr.math.Matrix44;
import net.lustlab.rndr.math.Vector2;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * Shape
 */
public class Shape  implements Iterable<ShapeContour>, Serializable {

	public List<ShapeContour> contours = new ArrayList<>();
	public ColorRGBa fill;
	public ColorRGBa stroke;
	public double strokeWeight = 1.0;

	public interface Generator {
		String generate(Shape shape);
	}

	Generator generator;


	public void setGenerator(Generator generator) {
		this.generator = generator;
	}

	public Generator getGenerator() {
		return generator;
	}


    public Shape() {
    }

    public Shape fill(ColorRGBa fill) {
        this.fill = fill;
        return this;
    }

    public Shape stroke(ColorRGBa stroke) {
        this.stroke = stroke;
        return this;
    }

	/**
	 * Copy constructor
	 * @param other
     */
    public Shape(Shape other) {
        contours = new ArrayList<>();

        for (ShapeContour contour: other) {
            ShapeContour copyContour = new ShapeContour(contour);
            contours.add(copyContour);
        }

        this.fill = other.fill.copy();
        this.stroke = other.stroke.copy();
		this.strokeWeight = other.strokeWeight;
    }

	/**
	 * Returns a deep copy of the Shape
	 * @return a copy of the Shape
     */
	public Shape copy() {

		Shape copy = new Shape();
		for (ShapeContour contour: contours) {
			ShapeContour copyContour = new ShapeContour(contour);
			copy.contours.add(copyContour);
		}
		copy.fill = fill;
		copy.stroke = stroke;
		copy.strokeWeight = strokeWeight;

		return copy;
	}


	public Shape apply(Matrix44 transform) {

		Shape transformed = new Shape();

		for (ShapeContour contour : this) {
			ShapeContour transformedContour = new ShapeContour();

			for (Segment segment : contour.segments) {
				transformedContour.segments.add(segment.apply(transform));
			}
			transformedContour.closed = contour.closed;

			transformed.contours.add(transformedContour);

		}
		return transformed;
	}

    @Override
	public String toString() {
		return "Shape{" +
				"contours=" + contours +
				", fill=" + fill +
				", stroke=" + stroke +
				'}';
	}

	public boolean contoursClosed() {
		boolean closed = true;

		for (ShapeContour contour: contours) {
			if (!contour.closed) {
				closed = false;
				break;
			}
		}
		return closed;
	}

	@Override
	public Iterator<ShapeContour> iterator() {
		return contours.iterator();
	}

	public Rectangle bounds() {
		// there is a much better way to do this!
		return polygon().bounds();
	}

    public Polygon polygon() {
        return polygon(-1);
    }

    /**
     * Converts the shape to a polygon. This converts bezier contours into polyline contours.
     * @return a polygon
     */
	public Polygon polygon(double distanceTolerance) {
		Polygon polygon = new Polygon();
		polygon.fill = fill != null ? fill.copy() : null;
		polygon.stroke = stroke != null ? stroke.copy() : null;
//		polygon.transform = transform;
//        polygon.id = id;

		for (ShapeContour contour:contours)  {
			PolygonContour polygonContour = new PolygonContour();
			polygonContour.closed = contour.closed;

			int segmentIndex = 0;
			for (Segment segment: contour) {

				if (segment.isLinear()) {
					polygonContour.vertices.add(new Vertex().position(segment.start.copy()));
					if (segmentIndex == contour.segments.size()-1) {
						polygonContour.vertices.add(new Vertex().position(segment.end.copy()));
					}
				}
				else {

                    if (segment.control.length == 1) {
                        BezierQuadraticRenderer bqr = new BezierQuadraticRenderer();
                        if (distanceTolerance!=-1) {
                            bqr.distanceTolerance = distanceTolerance;
                        }

                        List<Vector2> points = bqr.render(segment.start, segment.control[0], segment.end);

                        int o = segmentIndex == contour.segments.size()-1? 0: 1;
                        for (int p = 0; p < points.size() - o; ++p) {
                            polygonContour.vertices.add(new Vertex().position(points.get(p)));
                        }
                    }
                    if (segment.control.length == 2) {
                        BezierCubicRenderer bcr = new BezierCubicRenderer();
                        if (distanceTolerance!=-1) {
                            bcr.distanceTolerance = distanceTolerance;
                        }

                        List<Vector2> points = bcr.render(segment.start, segment.control[0], segment.control[1], segment.end);

                        int o = segmentIndex == contour.segments.size()-1? 0: 1;
                        for (int p = 0; p < points.size() - o; ++p) {
                            polygonContour.vertices.add(new Vertex().position(points.get(p)));
                        }
                    }
				}
				segmentIndex++;
			}

			polygon.contours.add(polygonContour);
		}
		return polygon;
	}
}
