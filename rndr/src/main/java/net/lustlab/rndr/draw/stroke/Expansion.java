package net.lustlab.rndr.draw.stroke;

import net.lustlab.rndr.math.Mapping;
import net.lustlab.rndr.math.Vector2;

import java.nio.FloatBuffer;

import static net.lustlab.rndr.draw.stroke.Path.Point.BEVEL;
import static net.lustlab.rndr.draw.stroke.Path.Point.INNER_BEVEL;
import static net.lustlab.rndr.draw.stroke.Path.Point.LEFT;

public final class Expansion {

    enum ExpansionType {
        Stroke,
        Fill,
        Fringe
    }
    final ExpansionType type;
    final FloatBuffer fb;

    double minx = Double.POSITIVE_INFINITY;
    double maxx = Double.NEGATIVE_INFINITY;
    double miny = Double.POSITIVE_INFINITY;
    double maxy = Double.NEGATIVE_INFINITY;

    final int bufferPosition;
    int vertexCount = 0;

    public int vertexCount() {
        return vertexCount;
    }

    public int bufferPosition() {
        return bufferPosition;
    }

    public Expansion(ExpansionType type, FloatBuffer fb) {
        this.fb = fb;
        this.type = type;
        bufferPosition = fb.position();
    }

    public double[] chooseBevel(boolean bevel, Path.Point p0, Path.Point p1, double w) {

        double x0, y0, x1, y1;

        if (bevel) {
            x0 = p1.x + p0.dy * w;
            y0 = p1.y - p0.dx * w;
            x1 = p1.x + p1.dy * w;
            y1 = p1.y - p1.dx * w;
        } else {
            x0 = p1.x + p1.dmx * w;
            y0 = p1.y + p1.dmy * w;
            x1 = p1.x + p1.dmx * w;
            y1 = p1.y + p1.dmy * w;
        }

        return new double[] { x0, y0, x1, y1 };
    }

    public void bevelJoin(Path.Point p0, Path.Point p1,
                          double lw, double rw, double lu, double ru, double fringe, double offset) {

        final double dlx0 = p0.dy;
        final double dly0 = -p0.dx;
        final double dlx1 = p1.dy;
        final double dly1 = -p1.dx;

        if ((p1.flags & LEFT) != 0) {
            final double r[] = chooseBevel((p1.flags & INNER_BEVEL) != 0, p0, p1, lw);
            final double lx0 = r[0];
            final double ly0 = r[1];
            final double lx1 = r[2];
            final double ly1 = r[3];

            addVertex( lx0, ly0, lu,1, offset);
            addVertex( p1.x - dlx0*rw, p1.y - dly0*rw, ru,1, offset);

            if ((p1.flags & BEVEL)!=0) {
                addVertex( lx0, ly0, lu,1, offset);
                addVertex( p1.x - dlx0*rw, p1.y - dly0*rw, ru,1, offset);

                addVertex( lx1, ly1, lu,1, offset);
                addVertex( p1.x - dlx1*rw, p1.y - dly1*rw, ru,1, offset);
            } else {
                final double rx0 = p1.x - p1.dmx * rw;
                final double ry0 = p1.y - p1.dmy * rw;

                addVertex( p1.x, p1.y, 0.5f,1, offset);
                addVertex( p1.x - dlx0*rw, p1.y - dly0*rw, ru,1, offset);

                addVertex( rx0, ry0, ru,1, offset);
                addVertex( rx0, ry0, ru,1, offset);

                addVertex( p1.x, p1.y, 0.5f,1, offset);
                addVertex( p1.x - dlx1*rw, p1.y - dly1*rw, ru,1, offset);
            }

            addVertex( lx1, ly1, lu,1, offset);
            addVertex( p1.x - dlx1*rw, p1.y - dly1*rw, ru,1, offset);

        } else {
            final double r[] = chooseBevel((p1.flags & INNER_BEVEL) != 0, p0, p1, -rw);

            final double rx0 = r[0];
            final double ry0 = r[1];
            final double rx1 = r[2];
            final double ry1 = r[3];

            addVertex( p1.x + dlx0*lw, p1.y + dly0*lw, lu,1, offset);
            addVertex( rx0, ry0, ru,1, offset);

            if ((p1.flags & BEVEL) != 0) {
                addVertex( p1.x + dlx0*lw, p1.y + dly0*lw, lu,1, offset);
                addVertex( rx0, ry0, ru,1, offset);

                addVertex( p1.x + dlx1*lw, p1.y + dly1*lw, lu,1, offset);
                addVertex( rx1, ry1, ru,1, offset);
            } else {
                final double lx0 = p1.x + p1.dmx * lw;
                final double ly0 = p1.y + p1.dmy * lw;

                addVertex( p1.x + dlx0*lw, p1.y + dly0*lw, lu,1, offset);
                addVertex( p1.x, p1.y, 0.5f,1, offset);

                addVertex( lx0, ly0, lu,1, offset);
                addVertex( lx0, ly0, lu,1, offset);

                addVertex( p1.x + dlx1*lw, p1.y + dly1*lw, lu,1, offset);
                addVertex( p1.x, p1.y, 0.5f,1, offset);
            }

            addVertex( p1.x + dlx1*lw, p1.y + dly1*lw, lu,1, offset);
            addVertex( rx1, ry1, ru,1, offset);
        }

    }


    /**
     *
     * @param p0
     * @param p1
     * @param lw
     * @param rw
     * @param lu
     * @param ru
     * @param ncap
     * @param fringe
     * @param offset
     */
    public void roundJoin(Path.Point p0, Path.Point p1,
                          double lw, double rw, double lu, double ru, int ncap, double fringe, double offset) {


        final double dlx0 = p0.dy;
        final double dly0 = -p0.dx;
        final double dlx1 = p1.dy;
        final double dly1 = -p1.dx;

        if ((p1.flags & LEFT) != 0) {

            final double[] r = chooseBevel((p1.flags & INNER_BEVEL) != 0, p0, p1, lw);
            final double lx0 = r[0];
            final double ly0 = r[1];
            final double lx1 = r[2];
            final double ly1 = r[3];

            double a0 = Math.atan2(-dly0, -dlx0);
            double a1 = Math.atan2(-dly1, -dlx1);
            if (a1 > a0) a0 += Math.PI * 2;

            if (a0 < 0 || a1 < 0) {
                a0+=Math.PI * 2;
                a1+=Math.PI * 2;
            }

            addVertex( lx0, ly0, lu,1,offset);
            addVertex( p1.x - dlx0*rw, p1.y - dly0*rw, ru,1,offset);

            int n = Mapping.clamp((int)Math.ceil(((a0 - a1) / Math.PI) * ncap), 2, ncap);
            for (int i = 0; i < n; i++) {
                double u = i / (n - 1.0);
                double a = a0 + u * (a1 - a0);
                double rx = p1.x + Math.cos(a) * rw;
                double ry = p1.y + Math.sin(a) * rw;
                addVertex( p1.x, p1.y, 0.5f,1,offset);
                addVertex( rx, ry, ru,1,offset);
            }

            addVertex( lx1, ly1, lu,1,offset);
            addVertex( p1.x - dlx1*rw, p1.y - dly1*rw, ru,1,offset);

        } else {
            double r[] = chooseBevel((p1.flags & INNER_BEVEL) != 0, p0, p1, -rw);
            double rx0 = r[0];
            double ry0 = r[1];
            double rx1 = r[2];
            double ry1 = r[3];

            double a0 = Math.atan2(dly0, dlx0);
            double a1 = Math.atan2(dly1, dlx1);
            if (a1 < a0) {
                a1 += Math.PI * 2;
            }
            addVertex( p1.x + dlx0*rw, p1.y + dly0*rw, lu,1,offset);
            addVertex( rx0, ry0, ru,1,offset);


            int n = Mapping.clamp((int)Math.ceil(((a1 - a0) / Math.PI) * ncap), 2, ncap);
            for (int i = 0; i < n; i++) {
                double a = a0 + (double)i / (n-1.0) *(a1-a0);
                addVertex(p1.x + Math.cos(a) * lw, p1.y + Math.sin(a) * lw, lu, 1,offset);
                addVertex( p1.x, p1.y, 0.5, 1,offset);
            }

            addVertex( p1.x + dlx1*rw, p1.y + dly1*rw, lu, 1,offset);
            addVertex( rx1, ry1, ru, 1,offset);
        }


    }



    public void buttCapStart(Path.Point p, double dx, double dy, double w,
                             double d, double aa, double offset) {

        final double px = p.x - dx * d;
        final double py = p.y - dy * d;
        final double dlx = dy;
        final double dly = -dx;

        addVertex(px + dlx*w - dx*aa, py + dly*w - dy*aa, 0,0, offset);
        addVertex(px - dlx*w - dx*aa, py - dly*w - dy*aa, 1,0, offset);
        addVertex(px + dlx*w, py + dly*w, 0,1, offset);
        addVertex(px - dlx*w, py - dly*w, 1,1, offset);
    }

    public void buttCapEnd(Path.Point p, double dx, double dy, double w, double d, double aa, double offset) {
        final double px = p.x - dx*d;
        final double py = p.y - dy*d;
        final double dlx = dy;
        final double dly = -dx;

        addVertex( px + dlx*w, py + dly*w, 0,1, offset);
        addVertex( px - dlx*w, py - dly*w, 1,1, offset);
        addVertex( px + dlx*w + dx*aa, py + dly*w + dy*aa, 0,0, offset);
        addVertex( px - dlx*w + dx*aa, py - dly*w + dy*aa, 1,0, offset);
    }

    public void roundCapStart(Path.Point p,
                              double dx, double dy, double w, int ncap, double aa, double offset) {

        final double px = p.x;
        final double py = p.y;
        final double dlx = dy;
        final double dly = -dx;

        for (int i = 0; i < ncap; i++) {
            final double a = i / (ncap - 1.0) * Math.PI;
            final double ax = Math.cos(a) * w;
            final double ay = Math.sin(a) * w;
            addVertex(px - dlx*ax - dx*ay, py - dly*ax - dy*ay, 0,1, offset);
            addVertex(px, py, 0.5,1, offset);
        }
        addVertex(px + dlx*w, py + dly*w, 0,1, offset);
        addVertex(px - dlx*w, py - dly*w, 1,1, offset);
    }

    public void roundCapEnd(Path.Point p,
                            double dx, double dy, double w, int ncap, double aa, double offset) {


        final double px = p.x;
        final double py = p.y;
        final double dlx = dy;
        final double dly = -dx;

        addVertex( px + dlx*w, py + dly*w, 0,1, offset);
        addVertex( px - dlx*w, py - dly*w, 1,1, offset);
        for (int i = 0; i < ncap; i++) {
            final double a = i/(double)(ncap-1)*Math.PI;
            final double ax = Math.cos(a) * w, ay = Math.sin(a) * w;
            addVertex( px, py, 0.5f,1, offset);
            addVertex( px - dlx*ax + dx*ay, py - dly*ax + dy*ay, 0,1, offset);
        }
    }

    public Vector2 vertex(int idx) {
        return new Vector2(fb.get(idx*5), fb.get(idx*5+1));
    }

    public void addVertex(double x, double y, double u, double v, double offset) {
        minx = Math.min(minx, x);
        maxx = Math.max(maxx, x);
        miny = Math.min(miny, y);
        maxy = Math.max(maxy, y);
//        System.out.println(vertexCount);
        fb.put((float)x); fb.put((float)y); fb.put((float)u); fb.put((float)v); fb.put((float)offset);
        vertexCount++;
    }

}

