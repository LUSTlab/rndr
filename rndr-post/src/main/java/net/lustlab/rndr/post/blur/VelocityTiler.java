package net.lustlab.rndr.post.blur;

import net.lustlab.rndr.buffers.ColorBuffer;
import net.lustlab.rndr.post.Filter;
import net.lustlab.rndr.shaders.FragmentShader;

public class VelocityTiler extends Filter {
    public VelocityTiler() {
        super(FragmentShader.fromUrl("cp:shaders/gl3/post/blur/velocity-tiler.frag"));
    }


    @Override
    public Filter apply(ColorBuffer[] src, ColorBuffer[] dest) {
        return super.apply(src, dest);
    }
}
