package net.lustlab.rndr.shape;

import net.lustlab.colorlib.ColorRGBa;
import net.lustlab.rndr.math.Mapping;
import net.lustlab.rndr.math.Matrix44;
import net.lustlab.rndr.math.Vector2;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

/**
 * Polygon contour
 */
public class PolygonContour extends Contour implements Serializable {
	public List<Vertex> vertices = new ArrayList<>();
	public boolean closed = true;


    public PolygonContour() {

    }

    /**
     * Copy constructor
     * @param other the PolygonContour to copy from
     */
    public PolygonContour(PolygonContour other) {
        for (Vertex vertex: other.vertices) {
            this.vertices.add(new Vertex(vertex));
        }
        this.closed = other.closed;
    }

    public boolean closed() {
        return closed;
    }

    @Override
    public int segmentCount() {
        return vertices.size() + (closed?1:0);
    }

    @Override
    public Contour sub(double t0, double t1) {
//
//        t0 = Mapping.clamp(t0, 0.0, 1.0);
//        t1 = Mapping.clamp(t1, 0.0, 1.0);
//
//        double z0 = t0;
//        double z1 = t1;
//
//        if (t0 > t1) {
//            z0 = t1;
//            z1 = t0;
//        }
//
//        double length = vertices.size()-1;
//
//        int il = vertices.size()-1;
//
//        int segment0 = (int) (z0*length);
//        double segmentOffset0 = segment0 < length ?  (z0*length) % 1.0 : 1.0;
//
//        int segment1= (int) (z1*length);
//        double segmentOffset1 = segment1 < length ?  (z1*length) % 1.0 : 1.0;
//
//        segment1 = Math.min(il, segment1);
//        segment0 = Math.min(il, segment0);
//
//        ShapeContour newContour = new ShapeContour();
//
//
//        List<Vector2> newPoints = new ArrayList<>();
//        for (int s = segment0; s <= segment1; ++s) {
//
//            Vector2 left =
//
//
//
//            if (s == segment0) {
//                //newContour.segments.plus(segments.get(s).sub(segmentOffset0, 1));
//                newPoints.plus( )
//
//
//            } else if (s == segment1) {
//                newContour.segments.plus(segments.get(s).sub(0, segmentOffset1));
//            } else {
//                newContour.segments.plus(segments.get(s));
//            }
//        }
//
//        return newContour;
//
        throw new RuntimeException("not implemented");
    }

    public PolygonContour weld() {
        return weld(0.00001);
    }
    /**
     * Welds vertices in the contour
     * @return a welded version of the contour
     */
    public PolygonContour weld(double threshold) {

        List<Vertex> ok = new ArrayList<>();
        Vertex last = null;
        for (Vertex vertex: vertices) {

            if (last != null) {
                double dx = vertex.position.x - last.position.x;
                double dy = vertex.position.y - last.position.y;

                double d = Math.sqrt(dx*dx+dy*dy);

                if (d >= threshold) {
                    ok.add(vertex);
                    last = vertex;
                }

            } else {
                last = vertex;
            }
        }

        PolygonContour pc = new PolygonContour();
        pc.vertices = ok;
        pc.closed = closed;
        return pc;
    }



    @Override
    public double length() {

        int segments = closed? vertices.size() : vertices.size() -1;

        double length = 0;
        for (int i = 0; i < segments; ++i) {
            int i0 = i;
            int i1 = (i+1) % vertices.size();
            length += vertices.get(i1).position.minus(vertices.get(i0).position).length();
        }
        return length;
    }

    public List<Vector2> equidistantPositions(int pointCount) {


        List<Vector2> positions = new ArrayList<>();

        for (Vertex vertex: vertices) {
            positions.add(new Vector2(vertex.position.x, vertex.position.y));
        }

        if (closed) {
            Vertex vertex = vertices.get(0);
            positions.add(new Vector2(vertex.position.x, vertex.position.y));
        }
        return ContourTools.sampleEquidistant(positions, pointCount);
    }

    @Override
    public List<Vector2> adaptivePositions() {
        List<Vector2> samples = new ArrayList<>();
        for (Vertex v: vertices) {
            samples.add(v.position.xy());
        }
        return samples;
    }

    @Override
    public Vector2 position(double t) {

        t = Mapping.clamp(t, 0.0, 1.0);

        double length = vertices.size();

        int segment = (int) (t*length);
        double segmentOffset = (t*length) % 1.0;


        int leftIndex = Math.min(vertices.size()-1,segment);
        int rightIndex = (Math.min(vertices.size()-1,segment) + 1) % vertices.size();

        Vector2 left = vertices.get(leftIndex).position.xy();
        Vector2 right = vertices.get(rightIndex).position.xy();
        return Mapping.mix(left, right, segmentOffset);
    }

    @Override
    public Vector2 direction(double t) {
        return velocity(t).normalized();

    }


    @Override
    public Vector2 velocity(double t) {

        t = Mapping.clamp(t, 0.0, 1.0);

        double length = vertices.size();

        int segment = (int) (t*length);
        double segmentOffset = (t*length) % 1.0;


        int leftIndex = Math.min(vertices.size()-1,segment);
        int rightIndex = (Math.min(vertices.size()-1,segment) + 1) % vertices.size();

        Vector2 left = vertices.get(leftIndex).position.xy();
        Vector2 right = vertices.get(rightIndex).position.xy();
        return right.minus(left);
    }

    @Override
    public Vector2 normal(double t) {
        return velocity(t).normalized().yx().scale(-1,1);
    }



    /**
     * Adds a point to the contour
     * @param v
     * @return
     */
    public PolygonContour vertex(Vector2 v) {
        vertices.add(new Vertex().position(v).color(ColorRGBa.WHITE));
        return this;
    }

    /**
     * Closes the contour
     * @return
     */
    public PolygonContour close() {
        closed = true;
        return this;
    }

	@Override
	public Iterator<Segment> iterator() {

        List<Segment> segments = new ArrayList<>();


        return segments.iterator();


	}

    @Override
    public Contour equidistant(int pointCount) {
        PolygonContour result = new PolygonContour();
        for (Vector2 p: equidistantPositions(pointCount)) {
            result.vertex(p);
        }
        if (closed()) {
            result.close();
        }
        return result;
    }



    /**
     * Reverses the vertex order of the polygon contour
     * @return
     */
	public PolygonContour reverse() {
		Collections.reverse(vertices);
		return this;
	}

	@Override
	public String toString() {
		return "PolygonContour{" +
				"vertices=" + vertices +
				", closed=" + closed +
				'}';
	}

    @Override
    public Contour copy() {
        throw new RuntimeException("not implemented");
    }

    @Override
    public Contour apply(Matrix44 transform) {
        throw new RuntimeException("not implemented");
    }
}

