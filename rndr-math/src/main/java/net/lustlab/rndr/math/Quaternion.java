
package net.lustlab.rndr.math;

public final class Quaternion  {

    public double x;
    public double y;
    public double z;
    public double w;

    static final long serialVersionUID = 1;

    /**
     * Represents the identity quaternion rotation (0, 0, 0, 1).
     */
    public static final Quaternion IDENTITY = new Quaternion(0, 0, 0, 1);
    public static final Quaternion DIRECTION_Z = fromAxes(Vector3.UnitX,
            Vector3.UnitY, Vector3.UnitZ);
    public static final Quaternion ZERO = new Quaternion(0, 0, 0, 0);

    /**
     *
     * <code>fromRotationMatrix</code> generates a quaternion from a supplied
     * matrix. This matrix is assumed to be a rotational matrix.
     *
     * @param matrix
     *          the matrix that defines the rotation.
     */
    public static Quaternion fromMatrix33(Matrix33 matrix) {
        return fromMatrix33(matrix.m00, matrix.m01, matrix.m02, matrix.m10, matrix.m11,
                matrix.m12, matrix.m20, matrix.m21, matrix.m22);
    }

    public static Quaternion fromMatrix33(double m00, double m01, double m02, double m10, double m11,
                                          double m12, double m20, double m21, double m22) {
        // Use the Graphics Gems code, from
        // ftp://ftp.cis.upenn.edu/pub/graphics/shoemake/quatut.ps.Z
        // *NOT* the "Matrix and Quaternions FAQ", which has errors!

        // the trace is the sum of the diagonal elements; see
        // http://mathworld.wolfram.com/MatrixTrace.html
        double t = m00 + m11 + m22;
        double x,y,z,w;

        // we protect the division by s by ensuring that s>=1
        if (t >= 0) { // |w| >= .5
            double s = Math.sqrt(t + 1); // |s|>=1 ...
            w = 0.5 * s;
            s = 0.5 / s; // so this division isn't bad
            x = (m21 - m12) * s;
            y = (m02 - m20) * s;
            z = (m10 - m01) * s;
        } else if ((m00 > m11) && (m00 > m22)) {
            double s = Math.sqrt(1.0 + m00 - m11 - m22); // |s|>=1
            x = s * 0.5; // |x| >= .5
            s = 0.5 / s;
            y = (m10 + m01) * s;
            z = (m02 + m20) * s;
            w = (m21 - m12) * s;
        } else if (m11 > m22) {
            double s = Math.sqrt(1.0 + m11 - m00 - m22); // |s|>=1
            y = s * 0.5; // |y| >= .5
            s = 0.5 / s;
            x = (m10 + m01) * s;
            z = (m21 + m12) * s;
            w = (m02 - m20) * s;
        } else {
            double s = Math.sqrt(1.0 + m22 - m00 - m11); // |s|>=1
            z = s * 0.5; // |z| >= .5
            s = 0.5 / s;
            x = (m02 + m20) * s;
            y = (m21 + m12) * s;
            w = (m10 - m01) * s;
        }
        return new Quaternion(x, y, z, w);
    }

    /**
     * Constructor instantiates a new <code>Quaternion</code> object initializing
     * all values to zero, except w which is initialized to 1.
     *
     */
    public Quaternion() {
        x = 0; y = 0; z = 0; w = 1;
    }

    /**
     * Constructor instantiates a new <code>Quaternion</code> object from the
     * given list of parameters.
     *
     * @param x
     *          the x value of the quaternion.
     * @param y
     *          the y value of the quaternion.
     * @param z
     *          the z value of the quaternion.
     * @param w
     *          the w value of the quaternion.
     */
    public Quaternion(double x, double y, double z, double w) {
        this.x = x;
        this.y = y;
        this.z = z;
        this.w = w;
    }


    /**
     * @return true if this Quaternion is {0,0,0,1}
     */
//    public boolean isIdentity() {
//        return equalsEpsilon(IDENTITY, Math.FLT_EPSILON);
//    }

    private static class QuaternionTemp {
        double x, y, z, w;
    }

    private Quaternion(QuaternionTemp q) {
        this(q.x, q.y, q.z, q.w);
    }

    /**
     * <code>fromAngles</code> builds a Quaternion from the Euler rotation angles
     * (x,y,z) aka (pitch, yaw, rall)). Note that we are applying in order: (y, z,
     * x) aka (yaw, roll, pitch) but we've ordered them in x, y, and z for
     * convenience.
     *
     * @see <a
     *      href="http://www.euclideanspace.com/maths/geometry/rotations/conversions/eulerToQuaternion/index.htm">http://www.euclideanspace.com/maths/geometry/rotations/conversions/eulerToQuaternion/index.htm</a>
     *
     * @param xAngle
     *          the Euler pitch of rotation (in radians). (aka Attitude, often rot
     *          around x)
     * @param yAngle
     *          the Euler yaw of rotation (in radians). (aka Heading, often rot
     *          around y)
     * @param zAngle
     *          the Euler roll of rotation (in radians). (aka Bank, often rot
     *          around z)
     */
    public static Quaternion fromAngles(double xAngle, double yAngle, double zAngle) {
        double angle;
        double sinY, sinZ, sinX, cosY, cosZ, cosX;
        angle = zAngle * 0.5;
        sinZ = Math.sin(angle);
        cosZ = Math.cos(angle);
        angle = yAngle * 0.5;
        sinY = Math.sin(angle);
        cosY = Math.cos(angle);
        angle = xAngle * 0.5;
        sinX = Math.sin(angle);
        cosX = Math.cos(angle);

        // variables used to reduce multiplication calls.
        double cosYXcosZ = cosY * cosZ;
        double sinYXsinZ = sinY * sinZ;
        double cosYXsinZ = cosY * sinZ;
        double sinYXcosZ = sinY * cosZ;

        QuaternionTemp temp = new QuaternionTemp();
        temp.w = (cosYXcosZ * cosX - sinYXsinZ * sinX);
        temp.x = (cosYXcosZ * sinX + sinYXsinZ * cosX);
        temp.y = (sinYXcosZ * cosX + cosYXsinZ * sinX);
        temp.z = (cosYXsinZ * cosX - sinYXcosZ * sinX);

        return new Quaternion(temp).normalize();
    }

    public Quaternion normalize() {
        double l = Math.sqrt(x*x + y*y + z*z + w*w);
        return new Quaternion(x/l, y/l, z/l, w/l);
    }

    /**
     * <code>toAngles</code> returns this quaternion converted to Euler rotation
     * angles (yaw,roll,pitch).<br/>
     *
     * @see <a
     *      href="http://www.euclideanspace.com/maths/geometry/rotations/conversions/quaternionToEuler/index.htm">http://www.euclideanspace.com/maths/geometry/rotations/conversions/quaternionToEuler/index.htm</a>
     *
     * @param angles
     *          the double[] in which the angles should be stored, or null if you
     *          want a new double[] to be created
     * @return the double[] in which the angles are stored.
     */
    public double[] toAngles(double[] angles) {
        if (angles == null) {
            angles = new double[3];
        } else if (angles.length != 3) {
            throw new IllegalArgumentException(
                    "Angles array must have three elements");
        }

        double sqw = w * w;
        double sqx = x * x;
        double sqy = y * y;
        double sqz = z * z;
        double unit = sqx + sqy + sqz + sqw; // if normalized is one, otherwise
        // is correction factor
        double test = x * y + z * w;
        if (test > 0.499 * unit) { // singularity at north pole
            angles[1] = 2 * Math.atan2(x, w);
            angles[2] = Math.PI/2;
            angles[0] = 0;
        } else if (test < -0.499 * unit) { // singularity at south pole
            angles[1] = -2 * Math.atan2(x, w);
            angles[2] = -Math.PI/2;
            angles[0] = 0;
        } else {
            angles[1] = Math.atan2(2 * y * w - 2 * x * z, sqx - sqy - sqz + sqw); // roll
            // or
            // heading
            angles[2] = Math.asin(2 * test / unit); // pitch or attitude
            angles[0] = Math.atan2(2 * x * w - 2 * y * z, -sqx + sqy - sqz + sqw); // yaw
            // or
            // bank
        }
        return angles;
    }

    /**
     * <code>toRotationMatrix</code> converts this quaternion to a rotational
     * matrix. Note: the result is created from a normalized version of this quat.
     *
     * @return the rotation matrix representation of this quaternion.
     */
    public Matrix33 toRotationMatrix() {

        double norm = norm();
        // we explicitly test norm against one here, saving a division
        // at the cost of a test and branch. Is it worth it?
        double s = (norm == 1.0) ? 2.0 : (norm > 0.0) ? 2.0 / norm : 0;

        // compute xs/ys/zs first to save 6 multiplications, since xs/ys/zs
        // will be used 2-4 times each.
        double xs = x * s;
        double ys = y * s;
        double zs = z * s;
        double xx = x * xs;
        double xy = x * ys;
        double xz = x * zs;
        double xw = w * xs;
        double yy = y * ys;
        double yz = y * zs;
        double yw = w * ys;
        double zz = z * zs;
        double zw = w * zs;

        // using s=2/norm (instead of 1/norm) saves 9 multiplications by 2 here
        return new Matrix33(1 - (yy + zz), (xy - zw), (xz + yw), (xy + zw),
                1 - (xx + zz), (yz - xw), (xz - yw), (yz + xw), 1 - (xx + yy));
    }

    /**
     * <code>toRotationMatrix</code> converts this quaternion to a rotational
     * matrix. The result is stored in result. 4th row and 4th column values are
     * untouched. Note: the result is created from a normalized version of this
     * quat.
     *
     */
    public Matrix44 toRotationMatrix44() {
        double norm = norm();
        // we explicitly test norm against one here, saving a division
        // at the cost of a test and branch. Is it worth it?
        double s = (norm == 1f) ? 2f : (norm > 0f) ? 2f / norm : 0;

        // compute xs/ys/zs first to save 6 multiplications, since xs/ys/zs
        // will be used 2-4 times each.
        double xs = x * s;
        double ys = y * s;
        double zs = z * s;
        double xx = x * xs;
        double xy = x * ys;
        double xz = x * zs;
        double xw = w * xs;
        double yy = y * ys;
        double yz = y * zs;
        double yw = w * ys;
        double zz = z * zs;
        double zw = w * zs;

        // using s=2/norm (instead of 1/norm) saves 9 multiplications by 2 here
        return new Matrix44( //
                1 - (yy + zz), (xy - zw), (xz + yw), 0, //
                (xy + zw), 1 - (xx + zz), (yz - xw), 0, //
                (xz - yw), (yz + xw), 1 - (xx + yy), 0, //
                0, 0, 0, 1);
    }

    /**
     * <code>getRotationColumn</code> returns one of three columns specified by
     * the parameter. This column is returned as a <code>Vector3f</code> object.
     * The value is retrieved as if this quaternion was first normalized.
     *
     * @param i
     *          the column to retrieve. Must be between 0 and 2.
     * @return the column specified by the index.
     */
    public Vector3 getRotationColumn(int i) {
        double norm = norm();
        if (norm != 1.0) {
            norm = 1.0/Math.sqrt(norm);
        }

        double xx = x * x * norm;
        double xy = x * y * norm;
        double xz = x * z * norm;
        double xw = x * w * norm;
        double yy = y * y * norm;
        double yz = y * z * norm;
        double yw = y * w * norm;
        double zz = z * z * norm;
        double zw = z * w * norm;
        double rx, ry, rz;

        switch (i) {
            case 0:
                rx = 1 - 2 * (yy + zz);
                ry = 2 * (xy + zw);
                rz = 2 * (xz - yw);
                break;
            case 1:
                rx = 2 * (xy - zw);
                ry = 1 - 2 * (xx + zz);
                rz = 2 * (yz + xw);
                break;
            case 2:
                rx = 2 * (xz + yw);
                ry = 2 * (yz - xw);
                rz = 1 - 2 * (xx + yy);
                break;
            default:
                //logger.warning("Invalid column index.");
                throw new IllegalArgumentException("Invalid column index. " + i);
        }

        return new Vector3(rx, ry, rz);
    }

    /**
     * <code>fromAngleAxis</code> sets this quaternion to the values specified by
     * an angle and an axis of rotation. This method creates an object, so use
     * fromAngleNormalAxis if your axis is already normalized.
     *
     * @param angle
     *          the angle to rotate (in radians).
     * @param axis
     *          the axis of rotation.
     * @return this quaternion
     */
    public static Quaternion fromAngleAxis(double angle, Vector3 axis) {
        Vector3 normAxis = axis.normalized();
        return fromAngleNormalAxis(angle, normAxis);
    }

    /**
     * <code>fromAngleNormalAxis</code> sets this quaternion to the values
     * specified by an angle and a normalized axis of rotation.
     *
     * @param angle
     *          the angle to rotate (in radians).
     * @param axis
     *          the axis of rotation (already normalized).
     */
    public static Quaternion fromAngleNormalAxis(double angle, Vector3 axis) {
        if (axis.x == 0 && axis.y == 0 && axis.z == 0) {
            return IDENTITY;
        }

        double halfAngle = 0.5 * angle;
        double sin = Math.sin(halfAngle);
        return new Quaternion(sin * axis.x, sin * axis.y, sin * axis.z,
                Math.cos(halfAngle));
    }

    /**
     * <code>slerp</code> sets this quaternion's value as an interpolation between
     * two other quaternions.
     *
     * @param q1
     *          the first quaternion.
     * @param q2
     *          the second quaternion.
     * @param t
     *          the amount to interpolate between the two quaternions.
     */
    public static Quaternion slerp(Quaternion q1, Quaternion q2, double t) {
        return q1.slerp(q2, t);
    }


    public Quaternion negate() {
        return new Quaternion(-x, -y, -z, -w);
    }

    public double dot(Quaternion other) {
        return x*other.x + y * other.y + z * other.z + w * other.w;
    }

    /**
     * Sets the values of this quaternion to the slerp from itself to q2 by
     * changeAmnt
     *
     * @param q2
     *          Final interpolation value
     * @param changeAmnt
     *          The amount diffrence
     */
    public Quaternion slerp(Quaternion q2, double changeAmnt) {
        if (this.x == q2.x && this.y == q2.y && this.z == q2.z && this.w == q2.w) {
            return this;
        }

        double result = (this.x * q2.x) + (this.y * q2.y) + (this.z * q2.z)
                + (this.w * q2.w);
        if (result < 0.0f) {
            // Negate the second quaternion and the result of the dot product
            q2 = q2.negate();
            result = -result;
        }

        // Set the first and second scale for the interpolation
        double scale0 = 1 - changeAmnt;
        double scale1 = changeAmnt;

        // Check if the angle between the 2 quaternions was big enough to
        // warrant such calculations
        if ((1 - result) > 0.1f) {
            // Get the angle between the 2 quaternions, and then store the sin()
            // of that angle
            double theta = Math.acos(result);
            double invSinTheta = 1f / Math.sin(theta);

            // Calculate the scale for q1 and q2, according to the angle and
            // it's sine value
            scale0 = Math.sin((1 - changeAmnt) * theta) * invSinTheta;
            scale1 = Math.sin((changeAmnt * theta)) * invSinTheta;
        }

        // Calculate the x, y, z and w values for the quaternion by using a
        // special
        // form of linear interpolation for quaternions.
        return new Quaternion(
                (scale0 * this.x) + (scale1 * q2.x),
                (scale0 * this.y) + (scale1 * q2.y),
                (scale0 * this.z) + (scale1 * q2.z),
                (scale0 * this.w) + (scale1 * q2.w));
    }

    /**
     * Sets the values of this quaternion to the nlerp from itself to q2 by blend.
     *
     * @param q2
     * @param blend
     */
    public Quaternion nlerp(Quaternion q2, double blend) {
        double dot = dot(q2);
        double blendI = 1.0f - blend;
        QuaternionTemp q = new QuaternionTemp();
        if (dot < 0.0f) {
            q.x = blendI * x - blend * q2.x;
            q.y = blendI * y - blend * q2.y;
            q.z = blendI * z - blend * q2.z;
            q.w = blendI * w - blend * q2.w;
        } else {
            q.x = blendI * x + blend * q2.x;
            q.y = blendI * y + blend * q2.y;
            q.z = blendI * z + blend * q2.z;
            q.w = blendI * w + blend * q2.w;
        }
        return new Quaternion(q).normalize();
    }

    /**
     * <code>multiply</code> multiplies this quaternion by a parameter quaternion. The
     * result is returned as a new quaternion. It should be noted that quaternion
     * multiplication is not commutative so q * p != p * q.
     *
     * @param q
     *          the quaternion to multiply this quaternion by.
     * @return the new quaternion.
     */

    public Quaternion multiply(Quaternion q) {
        QuaternionTemp res = new QuaternionTemp();
        res.x = x * q.w + y * q.z - z * q.y + w * q.x;
        res.y = -x * q.z + y * q.w + z * q.x + w * q.y;
        res.z = x * q.y - y * q.x + z * q.w + w * q.z;
        res.w = -x * q.x - y * q.y - z * q.z + w * q.w;
        return new Quaternion(res);
    }

    /**
     * <code>apply</code> multiplies this quaternion by a parameter matrix
     * internally.
     *
     * @param matrix
     *          the matrix to apply to this quaternion.
     */
    public Quaternion apply(Matrix33 matrix) {
        return multiply(Quaternion.fromMatrix33(matrix));
    }

    /**
     *
     * <code>fromAxes</code> creates a <code>Quaternion</code> that represents the
     * coordinate system defined by three axes. These axes are assumed to be
     * orthogonal and no error checking is applied. Thus, the user must insure
     * that the three axes being provided indeed represents a proper right handed
     * coordinate system.
     *
     * @param axis
     *          the array containing the three vectors representing the coordinate
     *          system.
     */
    public static Quaternion fromAxes(Vector3[] axis) {
        return fromAxes(axis[0], axis[1], axis[2]);
    }

    /**
     *
     * <code>fromAxes</code> creates a <code>Quaternion</code> that represents the
     * coordinate system defined by three axes. These axes are assumed to be
     * orthogonal and no error checking is applied. Thus, the user must insure
     * that the three axes being provided indeed represents a proper right handed
     * coordinate system.
     *
     * @param xAxis
     *          vector representing the x-axis of the coordinate system.
     * @param yAxis
     *          vector representing the y-axis of the coordinate system.
     * @param zAxis
     *          vector representing the z-axis of the coordinate system.
     */
    public static Quaternion fromAxes(Vector3 xAxis, Vector3 yAxis,
                                      Vector3 zAxis) {
        return Quaternion.fromMatrix33(new Matrix33(xAxis.x, yAxis.x, zAxis.x, xAxis.y,
                yAxis.y, zAxis.y, xAxis.z, yAxis.z, zAxis.z));
    }

    /**
     *
     * <code>toAxes</code> takes in an array of three vectors. Each vector
     * corresponds to an axis of the coordinate system defined by the quaternion
     * rotation.
     *
     * @param axis
     *          the array of vectors to be filled.
     */
    public void toAxes(Vector3 axis[]) {
        Matrix33 tempMat = toRotationMatrix();
        axis[0] = tempMat.column(0);
        axis[1] = tempMat.column(1);
        axis[2] = tempMat.column(2);
    }

    /**
     * <code>multiply</code> multiplies this quaternion by a parameter vector. The
     * result is returned as a new vector.
     *
     * @param v
     *          the vector to multiply this quaternion by.
     * @return the new vector.
     */
    public Vector3 multiply(Vector3 v) {
        double tempX = w * w * v.x + 2 * y * w * v.z - 2 * z * w * v.y + x * x * v.x
                + 2 * y * x * v.y + 2 * z * x * v.z - z * z * v.x - y * y * v.x;
        double tempY = 2 * x * y * v.x + y * y * v.y + 2 * z * y * v.z + 2 * w * z
                * v.x - z * z * v.y + w * w * v.y - 2 * x * w * v.z - x * x * v.y;
        double tempZ = 2 * x * z * v.x + 2 * y * z * v.y + z * z * v.z - 2 * w * y
                * v.x - y * y * v.z + 2 * w * x * v.y - x * x * v.z + w * w * v.z;
        return new Vector3(tempX, tempY, tempZ);
    }

    /**
     * <code>norm</code> returns the norm of this quaternion. This is the dot
     * product of this quaternion with itself.
     *
     * @return the norm of the quaternion.
     */
    public double norm() {
        return dot(this);
    }

    /**
     * <code>inverse</code> returns the inverse of this quaternion as a new
     * quaternion. If this quaternion does not have an inverse (if its normal is 0
     * or less), then null is returned.
     *
     * @return the inverse of this quaternion or null if the inverse does not
     *         exist.
     */

    public Quaternion inverse() {
        double norm = norm();
        if (norm > 0.0) {
            double invNorm = 1.0 / norm;
            return new Quaternion(-x * invNorm, -y * invNorm, -z * invNorm, w
                    * invNorm);
        }
        // return an invalid result to flag the error
        return null;
    }


    protected Quaternion build(double x, double y, double z, double w) {
        return new Quaternion(x, y, z, w);
    }


    protected Quaternion build(double[] v) {
        return new Quaternion(v[0], v[1], v[2], v[3]);
    }


    protected Quaternion build(double s) {
        return new Quaternion(s, s, s, s);
    }


    @Override
    public String toString() {
        return "Quaternion{" +
                "x=" + x +
                ", y=" + y +
                ", z=" + z +
                ", w=" + w +
                '}';
    }

    public Vector3 times(Vector3 v) {
        return multiply(v);
    }

}