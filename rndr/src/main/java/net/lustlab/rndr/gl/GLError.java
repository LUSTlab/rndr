package net.lustlab.rndr.gl;

import com.jogamp.opengl.GL;
import net.lustlab.rndr.RNDRException;


/**
 * Created by voorbeeld on 1/19/14.
 */
public class GLError {

    public static boolean debug = false;

    public static void debug(GL gl) {
        if (debug) {
            check(gl);
        }

    }

    public static void check(GL gl) {

        int error = gl.glGetError();
        String message = "<untranslated: " + error +">";
        if (error != GL.GL_NO_ERROR) {

            switch(error) {
                case GL.GL_INVALID_OPERATION:
                    message = "GL_INVALID_OPERATION";
                    break;
                case GL.GL_INVALID_VALUE:
                    message = "GL_INVALID_VALUE";
                    break;
                case GL.GL_INVALID_ENUM:
                    message = "GL_INVALID_ENUM";
                    break;
                case GL.GL_INVALID_FRAMEBUFFER_OPERATION:
                    message = "GL_INVALUD_FRAMEBUFFER_OPERATION";
                    break;
            }

            throw new RNDRException("GL ERROR:" + message);
        }


    }

    public static void clear(GL gl) {
        gl.glGetError();
    }
}
