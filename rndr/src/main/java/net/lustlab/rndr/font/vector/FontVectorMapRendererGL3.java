package net.lustlab.rndr.font.vector;

import com.jogamp.opengl.GL;
import com.jogamp.opengl.GL3;

import net.lustlab.colorlib.ColorRGBa;
import net.lustlab.rndr.draw.BlendMode;
import net.lustlab.rndr.shape.Rectangle;
import net.lustlab.rndr.draw.ShadeStyle;
import net.lustlab.rndr.font.FontMap;
import net.lustlab.rndr.font.FontMapRenderer;
import net.lustlab.rndr.math.Matrix44;
import net.lustlab.rndr.math.Transforms;
import net.lustlab.rndr.math.Vector3;
import net.lustlab.rndr.shaders.FragmentShader;
import net.lustlab.rndr.shaders.Shader;
import net.lustlab.rndr.shaders.VertexShader;
import net.lustlab.rndr.buffers.VertexArray;

import java.util.ArrayList;
import java.util.List;

public class FontVectorMapRendererGL3 implements FontMapRenderer {

    ShadeStyle shadeStyle;
    public ColorRGBa fill = ColorRGBa.WHITE;
    public ColorRGBa stroke = null;
    private Rectangle clipRectangle = null;

    private static Shader shader;


    @Override
    public void shadeStyle(ShadeStyle shadeStyle) {
        this.shadeStyle = shadeStyle;
    }

    @Override
    public double width(FontMap fontMap, String text) {

        FontVectorMap fontVectorMap = (FontVectorMap) fontMap;

        double width = 0;
        for (char c: text.toCharArray()) {
            FontVectorMap.Entry entity = fontVectorMap.characters.get(c);
            if (entity != null) {
                width += entity.width;
            }
        }

        return width;
    }

    @Override
    public List<CharacterRectangle> render(GL gl, FontMap fontMap, String text, double x, double y) {
        return null;
    }

    @Override
    public List<CharacterRectangle> render(GL gl, FontMap fontMap, String text, double x, double y, double tracking) {

        return null;
    }

    @Override
    public List<CharacterRectangle> render(GL3 gl, Matrix44 projection, Matrix44 modelView, FontMap fontMap, String text, double x, double y) {
        return render(gl, projection, modelView, fontMap, text, x, y, 0);
    }

    @Override
    public List<CharacterRectangle> render(GL3 gl, Matrix44 projection, Matrix44 modelView, FontMap fontMap, String text, double x, double y, double tracking) {

        gl.glEnable(GL.GL_BLEND);
        gl.glBlendFunc(GL.GL_SRC_ALPHA, GL.GL_ONE_MINUS_SRC_ALPHA);
        gl.glEnable(GL.GL_LINE_SMOOTH);

        if (shader == null) {
            shader = Shader.createShader(gl,
                    VertexShader.fromStream(gl, getClass().getResourceAsStream("/shaders/gl3/vectorfont.vert")),
                    FragmentShader.load(gl, getClass().getResourceAsStream("/shaders/gl3/vectorfont.frag")));
        }

        FontVectorMap map = (FontVectorMap) fontMap;
        if (map.vao == null) {
            map.bakeVBO(gl, shader);
        }
        map.vao.bind();
        shader.begin();

        if (clipRectangle != null) {
            shader.setUniform4f("clipArea", (float) clipRectangle.x, (float) (clipRectangle.x + clipRectangle.width), (float) clipRectangle.y, (float) (clipRectangle.y + clipRectangle.height));
        }
        else {
            shader.setUniform4f("clipArea", -Float.MIN_VALUE, Float.MAX_VALUE, Float.MIN_VALUE, Float.MAX_VALUE);
        }

        int[] viewport = new int[4];
        gl.glGetIntegerv(GL.GL_VIEWPORT, viewport,0);
        shader.setUniform1f("viewHeight", viewport[3]);
        shader.setUniform("color", fill);

        List<CharacterRectangle> result = new ArrayList<>();
        double cursorX = x;
        for (char c: text.toCharArray()) {
            if (c == '\u00A0') c = ' ';
            FontVectorMap.Entry entry = map.characters.get(c);
            if (entry != null) {
                Matrix44 transform = modelView.multiply(Transforms.translate(new Vector3(cursorX, y, 0)));

                Matrix44 modelViewProjection = projection.multiply(transform);
                shader.setUniform("modelViewProjectionMatrix", modelViewProjection);

                for (FontVectorMap.VBORange range: entry.ranges) {

                    if (range.command != GL3.GL_LINE_LOOP) {
                        gl.glDrawArrays((int) range.command, (int) range.offset, (int) range.vertices);
                    }
                    if (range.command == GL3.GL_LINE_LOOP) {
                        gl.glDrawArrays(GL3.GL_TRIANGLES, (int) range.offset, (int) range.vertices);
                    }
                }


                CharacterRectangle characterRectangle = new CharacterRectangle(c, cursorX, y, entry.width, map.fontHeight());
                result.add(characterRectangle);

                cursorX += entry.width + tracking;
            }
        }
        shader.end();

        //map.vao.unbind();
        VertexArray.defaultInstance.bind();


        return result;
    }

    @Override
    public void blend(BlendMode blendMode) {

    }

    @Override
    public void clip(double left, double top, double width, double height) {


        if (left == 0 && top == 0 && (float)width == Float.POSITIVE_INFINITY && (float)height == Float.POSITIVE_INFINITY) {
            clipRectangle = null;
        }
        else {
            clipRectangle = new Rectangle(left, top, width, height);
        }
    }

    @Override
    public void noClip() {
        clipRectangle = null;
    }

    @Override
    public void fill(ColorRGBa color) {
        this.fill = new ColorRGBa(color);
    }

    @Override
    public void noFill() {
        fill = null;
    }

    @Override
    public void stroke(ColorRGBa color) {
        if (color != null) {
            stroke = new ColorRGBa(color);
        }
        else {
            stroke = null;
        }
    }

    @Override
    public void noStroke() {
        stroke = null;
    }

    @Override
    public void flush() {

    }
}
