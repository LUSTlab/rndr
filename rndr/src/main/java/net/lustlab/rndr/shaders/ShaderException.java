package net.lustlab.rndr.shaders;

public class ShaderException extends RuntimeException{
	private static final long serialVersionUID = 1L;

	public ShaderException(String message) {
		super(message);
	}

    public ShaderException(String message, Exception e) {
        super(message, e);
    }
}
