package net.lustlab.rndr.shaders;

import java.util.Optional;

public interface ShaderProxy {

    public Optional<Shader> shader();

}
