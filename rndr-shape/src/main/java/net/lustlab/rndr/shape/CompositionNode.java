package net.lustlab.rndr.shape;

import net.lustlab.rndr.math.Matrix44;
import net.lustlab.rndr.math.Vector2;

import java.awt.*;
import java.util.*;
import java.util.List;
import java.util.stream.Stream;


public class CompositionNode {
	public Matrix44 localTransform = Matrix44.IDENTITY;
    public Matrix44 userTransform = Matrix44.IDENTITY;

    public Matrix44 transform = Matrix44.IDENTITY;

    CompositionNode parent;
	public String id;

	String text;

    /**
     * Calculates the total bounds of the node and children
     * @return
     */
    public Rectangle bounds() {
        if (shape != null) {


            return shape.bounds();



        }

        if (image != null) {
            Vector2 topLeft = image.bounds().topLeft();
            Vector2 bottomRight = image.bounds().topLeft().plus(new Vector2(image.bounds().width, image.bounds().height));

            topLeft = topLeft.xy0().multiply(transform).xy();
            bottomRight = bottomRight.xy0().multiply(transform).xy();


            double width = bottomRight.x - topLeft.x;
            double height = bottomRight.y - topLeft.y;

            System.out.println("image bounds" + width + " " + height + " " + topLeft.x + " " + topLeft.y);
            return new Rectangle(topLeft.x, topLeft.y, width, height);



            //return image.bounds();
        }

        {
            List<Rectangle> bounds = new ArrayList<>();
            for (CompositionNode c: children()) {
                Rectangle b = c.bounds();
                if (b != null) {
                    bounds.add(b);
                }
            }
            if (bounds.size() > 0) {
                return Rectangle.bounds(bounds);
            } else {
                return null;
            }
        }
    }

    public CompositionNode parent() {
        return parent;
    }

    CompositionNode parent(CompositionNode parent) {
        this.parent = parent;
        return this;
    }

    @Override
    public String toString() {
        return "CompositionNode{" +
                "localTransform=" + localTransform +
                ", userTransform=" + userTransform +
                ", transform=" + transform +
                ", id='" + id + '\'' +
                ", children=" + children +
                ", shape=" + shape +
                '}';
    }

    public List<CompositionNode> children() {

        if (children == null) {
            return Collections.emptyList();
        } else {
            return children;
        }
    }

    List<CompositionNode> children;
    Shape shape;
    ImageReference image;



    public CompositionNode append(Shape shape) {
        this.shape = shape;
        return this;
    }

    public CompositionNode shape(Shape shape) {
        this.shape = shape;
        return this;
    }

    public CompositionNode image(ImageReference image) {
        this.image = image;
        return this;
    }

    public CompositionNode append(CompositionNode child) {
        child.parent(this);
        if (children == null) {
            children = new ArrayList<>();
        }
        children.add(child);
        return this;
    }


    public ImageReference image() {
        return image;
    }

    public Shape shape() {
        return shape;
    }


    public CompositionNode text(String text) {
        this.text = text;
        return this;
    }

    public Optional<String> text() {
        return Optional.ofNullable(text);
    }

    public CompositionNode copy() {
        CompositionNode copy = new CompositionNode();
        copy.localTransform = localTransform;
        copy.userTransform = userTransform;
        copy.transform = transform;
        copy.shape = (shape == null)? null: shape.copy();
        copy.id = id;
        for (CompositionNode child: children()) {
            copy.append(child.copy());
        }
        return copy;
    }

    public Stream<CompositionNode> stream() {

        List<CompositionNode> result = new ArrayList<>();


        Stack<CompositionNode> stack = new Stack<>();

        stack.push(this);

        while (!stack.isEmpty()) {

            CompositionNode node = stack.pop();
            result.add(node);
            node.children().forEach(stack::add);
        }

        return result.stream();
    }

}
