#version 330

layout(location=0) out vec4 o_output;

uniform sampler2D image;
uniform sampler2D position;
in vec2 v_texCoord0;

uniform float minCoc;
uniform float maxCoc;
uniform float focus;
uniform float blurScale;
uniform float depthScale;

void main() {

    vec3 color = texture(image, v_texCoord0).rgb;
    float depth = 1.0 - texture(position, v_texCoord0).z;

    float size =  blurScale * abs(1.0 - (focus/(depth*depthScale)));
    size = clamp(size, minCoc, maxCoc);
    o_output = vec4(color, size);
}