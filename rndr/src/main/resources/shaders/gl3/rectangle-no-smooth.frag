#version 330

${outputs! }
${uniforms! }

in vec3 v_boundsPosition;


uniform vec4 fill;
uniform vec4 stroke;

uniform vec2 dimensions;
uniform float strokeWeight;

out vec4 o_color;
void main(void) {

    vec3 boundsPosition = v_boundsPosition;

    vec4 x_fill = fill;
    vec4 x_stroke = stroke;
    { ${fillTransform! } }
    { ${strokeTransform! } }


    o_color = vec4(x_fill);
}