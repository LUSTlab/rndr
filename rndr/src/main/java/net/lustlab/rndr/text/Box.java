package net.lustlab.rndr.text;

import java.io.Serializable;

/**
 * Created with IntelliJ IDEA.
 * User: voorbeeld
 * Date: 12/8/13
 * Time: 5:59 PM
 * To change this template use File | Settings | File Templates.
 */
public class Box implements Serializable {

	public double x;
	public double y;
	public double width;
	public double height;

	public Box(double x, double y, double width, double height) {
		this.x = x;
		this.y = y;
		this.width = width;
		this.height = height;
	}


	public Box(Box box) {
		this.x = box.x;
		this.y = box.y;
		this.width = box.width;
		this.height = box.height;
	}
}
