package net.lustlab.rndr.shape;

/**
 * Created by edwin on 29/05/17.
 */
public class ImageReference {
    public double width;
    public double height;
    public String url;

    public Rectangle bounds() {
        return new Rectangle(0.0, 0.0, width, height);
    }

}
