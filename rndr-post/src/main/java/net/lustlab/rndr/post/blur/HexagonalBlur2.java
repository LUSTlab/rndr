package net.lustlab.rndr.post.blur;

import net.lustlab.rndr.buffers.*;
import net.lustlab.rndr.math.Vector2;
import net.lustlab.rndr.post.Filter;
import net.lustlab.rndr.shaders.FragmentShader;

/**
 * Created by edwin on 24/04/17.
 */
public class HexagonalBlur2 extends Filter {

    public static class Prepass extends Filter {
        public Prepass() {
            super(FragmentShader.fromUrl("cp:shaders/gl3/post/blur/hexagonal-blur-2-coc.frag"));
            map("tex0", "image");
            map("tex1", "position");
        }
    }

    public static class Pass1 extends Filter {

        public Pass1() {
            super(FragmentShader.fromUrl("cp:shaders/gl3/post/blur/hexagonal-blur-2-pass-1.frag"));
            map("tex0", "image");
        }
    }

    public static class Pass2 extends Filter {
        public Pass2() {
            super(FragmentShader.fromUrl("cp:shaders/gl3/post/blur/hexagonal-blur-2-pass-2.frag"));
            map("tex0", "vertical");
            map("tex1", "diagonal");
        }

    }
    Pass1 pass1 = new Pass1();
    Pass2 pass2 = new Pass2();
    Prepass prepass = new Prepass();

    ColorBuffer vertical;
    ColorBuffer diagonal;
    ColorBuffer pre;

    public double minCoc = 0.0;
    public double maxCoc = 40.0;

    public HexagonalBlur2() {
        super(FragmentShader.fromUrl("cp:shaders/gl3/post/blur/hexagonal-blur-pass-1.frag"));
    }

    @Override
    public Filter apply(ColorBuffer[] src, ColorBuffer[] dest) {

        if (vertical != null && (vertical.width() != dest[0].width() || vertical.height() != dest[0].height())) {
            vertical.destroy();
            vertical = null;
            diagonal.destroy();
            diagonal = null;
            pre.destroy();
            pre = null;
        }

        if (vertical == null) {
            vertical = ColorBuffer.create(dest[0].width(), dest[0].height(), ColorFormat.RGBA, ColorType.FLOAT32);
            diagonal = ColorBuffer.create(dest[0].width(), dest[0].height(), ColorFormat.RGBA, ColorType.FLOAT32);
            pre = ColorBuffer.create(dest[0].width(), dest[0].height(), ColorFormat.RGBA, ColorType.FLOAT32);
        }

        src[0].filter(MinifyingFilter.LINEAR, MagnifyingFilter.LINEAR); // image
        src[1].filter(MinifyingFilter.NEAREST, MagnifyingFilter.NEAREST); // positions
        vertical.filter(MinifyingFilter.LINEAR, MagnifyingFilter.LINEAR);
        diagonal.filter(MinifyingFilter.LINEAR, MagnifyingFilter.LINEAR);
        pre.filter(MinifyingFilter.LINEAR, MagnifyingFilter.LINEAR);


        prepass.parameter("minCoc", (float)minCoc);
        prepass.parameter("maxCoc", (float)maxCoc);
        prepass.apply(src, pre);

        pass1.parameter("vertical", new Vector2(Math.cos(Math.PI/2), Math.sin(Math.PI/2)));
        pass1.parameter("diagonal", new Vector2(Math.cos(-Math.PI/6), Math.sin(-Math.PI/6)));
        pass1.apply(pre, new ColorBuffer[] { vertical, diagonal});
        pass2.parameter("direction0", new Vector2(Math.cos(-Math.PI/6), Math.sin(-Math.PI/6)));
        pass2.parameter("direction1", new Vector2(Math.cos(-5*Math.PI/6), Math.sin(-5*Math.PI/6)));
        pass2.apply(new ColorBuffer[] { vertical, diagonal}, dest);
        return this;
    }
}
