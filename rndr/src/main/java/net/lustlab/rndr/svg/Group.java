package net.lustlab.rndr.svg;

import java.util.ArrayList;
import java.util.List;

/**
 * Group Element
 */
public class Group extends Element{

	public String name;
	public List<Element> elements = new ArrayList<Element>();
}