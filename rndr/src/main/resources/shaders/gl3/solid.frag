#version 330
layout(origin_upper_left) in vec4 gl_FragCoord;

// -- inputs --
in vec3 v_normal;
in vec3 v_cameraPosition;
in vec3 v_modelPosition;
in vec3 v_worldPosition;
in vec3 v_boundsPosition;
in vec3 v_modelNormal;


// -- color --
uniform vec4 fill;
uniform vec3 cameraPosition;

// -- parameters --
${uniforms! }

// -- output --
out vec4 o_color;
${outputs! }

${preamble! }

void main() {

    int instance = 0;
    vec3 worldPosition = v_worldPosition;
    vec3 cameraPosition = v_cameraPosition;
    vec3 modelPosition = v_modelPosition;
    vec2 screenPosition = gl_FragCoord.xy;
    vec3 boundsPosition = v_boundsPosition;
    vec3 normal = normalize(v_normal);
    vec3 modelNormal = normalize(v_modelNormal);

    vec4 x_fill = fill;
    x_fill.a = 1.0;
    {
        { ${fillTransform! } }
    }

    vec4 x_color = x_fill;
    {
        { ${colorTransform! } }
    }
    o_color = x_color;
}