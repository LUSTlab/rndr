package net.lustlab.rndr.text;

import net.lustlab.colorlib.ColorRGBa;
import net.lustlab.rndr.font.FontMap;

import java.io.Serializable;

public class Style implements Serializable {

	public FontMap font;
	public ColorRGBa fill = ColorRGBa.WHITE;
    public ColorRGBa stroke = null;
	public double leading = 0;
	public double tracking = 0;
    public String ellipsis = "...";

    public Align align;

    public enum Align {
        Left,
        Right,
        Center
    }


	public Style(Style style) {
		this.font = style.font;
        this.fill = style.fill != null? new ColorRGBa(style.fill) : null;
        this.stroke = style.stroke != null? new ColorRGBa(style.stroke) : null;
		this.leading = style.leading;
		this.tracking = style.tracking;
        this.ellipsis = new String(style.ellipsis);
        this.align = style.align;
	}

	public Style() {

	}
}
