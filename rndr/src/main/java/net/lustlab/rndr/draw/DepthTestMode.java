package net.lustlab.rndr.draw;

import com.jogamp.opengl.GL;

/**
 * Created by voorbeeld on 3/8/16.
 */
public enum DepthTestMode {
    NEVER,
    LESS,
    EQUAL,
    LESS_OR_EQUAL,
    GREATER,
    NOT_EQUAL,
    GREATER_OR_EQUAL,
    ALWAYS;

    public int toGLDepthFunc() {
        switch(this) {
            case NEVER:
                return GL.GL_NEVER;
            case LESS:
                return GL.GL_LESS;
            case EQUAL:
                return GL.GL_EQUAL;
            case LESS_OR_EQUAL:
                return GL.GL_LEQUAL;
            case GREATER:
                return GL.GL_GREATER;
            case NOT_EQUAL:
                return GL.GL_NOTEQUAL;
            case GREATER_OR_EQUAL:
                return GL.GL_GEQUAL;
            case ALWAYS:
                return GL.GL_ALWAYS;
            default:
                throw new RuntimeException("unsupported depth test mode");
        }
    }


}
