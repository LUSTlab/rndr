package net.lustlab.rndr.draw.gl3;

import com.jogamp.opengl.GLContext;
import net.lustlab.rndr.buffers.VertexBuffer;
import net.lustlab.rndr.buffers.VertexBufferDrawer;
import net.lustlab.rndr.draw.DrawContext;
import net.lustlab.rndr.draw.DrawStyle;
import net.lustlab.rndr.draw.Primitive;
import net.lustlab.rndr.shaders.ShadeStyleManager;
import net.lustlab.rndr.shaders.Shader;

public class VertexBufferRenderer {

    ShadeStyleManager shaderManager;

    public VertexBufferRenderer() {
        shaderManager = new ShadeStyleManager("cp:shaders/gl3/vertexbuffer.vert", "cp:shaders/gl3/vertexbuffer.frag");
    }

    public void drawVertexBuffer(DrawStyle style, DrawContext context, VertexBuffer vertexBuffer, Primitive primitive, long vertexCount) {
        Shader shader = shaderManager.shader(style.shadeStyle, vertexBuffer.format());
        StateTools.setupState(style);
        shader.begin();
        shaderManager.setParameters(shader, style.shadeStyle);
        ShaderTools.setupShader(shader, context, style);
        vertexBuffer.bind();
        VertexBufferDrawer.draw(GLContext.getCurrentGL(), shader, vertexBuffer, vertexBuffer.format(), primitive.toGLPrimitive(), (int)vertexCount);
        vertexBuffer.unbind();
        shader.end();
    }

    public void drawVertexBufferInstances(DrawStyle style, DrawContext context, VertexBuffer vertexBuffer, Primitive primitive, long vertexCount, long instanceCount) {
        Shader shader = shaderManager.shader(style.shadeStyle, vertexBuffer.format());
        StateTools.setupState(style);
        shader.begin();
        shaderManager.setParameters(shader, style.shadeStyle);
        ShaderTools.setupShader(shader, context, style);
        vertexBuffer.bind();
        VertexBufferDrawer.drawInstances(GLContext.getCurrentGL(), shader, vertexBuffer, vertexBuffer.format(), primitive.toGLPrimitive(), (int)vertexCount, (int)instanceCount);
        vertexBuffer.unbind();
        shader.end();
    }

    public void destroy() {
        shaderManager.destroy();
    }


}
