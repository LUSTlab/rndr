package net.lustlab.rndr.post;

import net.lustlab.rndr.shaders.FragmentShader;

/**
 * Precise but slow Gaussian blur
 */
public class GaussianBlur extends Filter {

    public GaussianBlur() {
        super(FragmentShader.fromUrl("cp:shaders/gl3/post/gaussian-blur.frag"));
    }

    public GaussianBlur window(int window) {
        parameter("window", window);
        return this;
    }

    public GaussianBlur sigma(double sigma) {
        parameter("sigma", (float) sigma);
        return this;
    }

}
