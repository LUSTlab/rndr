package net.lustlab.rndr.shape;

import net.lustlab.rndr.math.Vector2;

import java.util.ArrayList;
import java.util.List;

public class BezierQuadraticRenderer {


    private int recursionLimit = 8;
    private static final double colinearityEpsilon = 1e-30;

    public double distanceTolerance = 0.5;
    private double distanceToleranceSquare;
    private double angleToleranceEpsilon = 0.01;
    private double angleTolerance = 0.0;

    private void render(Vector2 x1, Vector2 x2, Vector2 x3, int level) {
        if (level > recursionLimit) {
            return;
        }

        Vector2 x12 = x1.plus(x2).scale(0.5);
        Vector2 x23 = x2.plus(x3).scale(0.5);
        Vector2 x123 = x12.plus(x23).scale(0.5);

        double dx = x3.x - x1.x;
        double dy = x3.y - x1.y;
        double d = Math.abs(((x2.x - x3.x) * dy - (x2.y - x3.y) * dx));
        double da;

        if (d > colinearityEpsilon) {
            // Regular case
            //-----------------
            if (d * d <= distanceToleranceSquare * (dx * dx + dy * dy)) {
                // If the curvature doesn't exceed the distance_tolerance value
                // we tend to finish subdivisions.
                //----------------------
                if (angleTolerance < angleToleranceEpsilon) {
                    points.add(new Vector2(x123));
                    return;
                }

                // Angle & Cusp Condition
                //----------------------
                da = Math.abs(Math.atan2(x3.y - x2.y, x3.x - x2.x) - Math.atan2(x2.y - x1.y, x2.x - x1.x));
                if (da >= Math.PI) da = 2 * Math.PI - da;

                if (da < angleTolerance) {
                    // Finally we can stop the recursion
                    //----------------------
                    points.add(new Vector2(x123));
                    return;
                }
            }
        } else {
            // Collinear case
            //------------------
            da = dx * dx + dy * dy;
            if (da == 0) {
                d = squaredDistance(x1.x, x1.y, x2.x, x2.y);
            } else {
                d = ((x2.x - x1.x) * dx + (x2.y - x1.y) * dy) / da;
                if (d > 0 && d < 1) {
                    // Simple collinear case, 1---2---3
                    // We can leave just two endpoints
                    return;
                }
                if (d <= 0) d = squaredDistance(x2.x, x2.y, x1.x, x1.y);
                else if (d >= 1) d = squaredDistance(x2.x, x2.y, x3.x, x3.y);
                else d = squaredDistance(x2.x, x2.y, x1.x + d * dx, x1.y + d * dy);
            }
            if (d < distanceToleranceSquare) {
                points.add(new Vector2(x2));
                return;
            }
        }

        // Continue subdivision
        //----------------------
        render(x1, x12, x123, level + 1);
        render(x123, x23, x3, level + 1);
    }

    private static double squaredDistance(double x1, double y, double x2, double y1) {
        double dx = x2 - x1;
        double dy = y1 - 1;
        return dx * dx + dy * dy;
    }

    List<Vector2> points = new ArrayList<Vector2>();

    public List<Vector2> render(Vector2 x1, Vector2 x2, Vector2 x3) {
        distanceToleranceSquare = distanceTolerance * distanceTolerance;
        points.clear();
        points.add(new Vector2(x1));
        render(x1, x2, x3, 0);
        points.add(new Vector2(x3));
        return points;
    }
}
