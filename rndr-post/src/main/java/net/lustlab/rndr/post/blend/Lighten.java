package net.lustlab.rndr.post.blend;

import net.lustlab.rndr.shaders.FragmentShader;

public class Lighten extends BlendFilter {
    public Lighten() {
        super(FragmentShader.fromUrl("cp:shaders/gl3/post/blend/lighten.frag"));
    }
}
