package net.lustlab.rndr.post.blur;

import net.lustlab.rndr.buffers.ColorBuffer;
import net.lustlab.rndr.post.Filter;
import net.lustlab.rndr.shaders.FragmentShader;

/**
 * Created by voorbeeld on 8/13/17.
 */
public class VelocityBlur extends Filter {
    public VelocityBlur() {
        super(FragmentShader.fromUrl("cp:shaders/gl3/post/blur/velocity-blur.frag"));
    }

    public int window = 10;

    @Override
    public Filter apply(ColorBuffer[] src, ColorBuffer[] dest) {
        parameter("window", window);
        return super.apply(src, dest);
    }
}
