package net.lustlab.rndr.post;

import net.lustlab.rndr.shaders.FragmentShader;


public class Subtract extends Filter {
    public Subtract() {
        super(FragmentShader.fromUrl("cp:shaders/gl3/post/subtract.frag"));
    }
}
