package net.lustlab.rndr.post.blur;


import net.lustlab.rndr.buffers.ColorBuffer;
import net.lustlab.rndr.post.Filter;
import net.lustlab.rndr.shaders.FragmentShader;



public class PoissonBlur extends Filter {
    ColorBuffer intermediate;

    public PoissonBlur() {
        super(FragmentShader.fromUrl("cp:shaders/gl3/post/blur/poisson-blur.frag"));
    }

    public int passes = 2;
    public double radius = 21.0;
    public double amplitude = 1.1;



    @Override
    public Filter apply(ColorBuffer[] src, ColorBuffer[] dest) {

        if (intermediate != null && (intermediate.width() != dest[0].width() || intermediate.height() != dest[0].height())) {
            intermediate.destroy();
            intermediate = null;
        }

        if (intermediate == null) {
            intermediate = ColorBuffer.create(dest[0].width(), dest[0].height(), dest[0].format(), dest[0].type());
        }


        parameter("amplitude", (float)amplitude);
        for (int pass = 0; pass < passes; ++pass) {
            parameter("offset", (float)(pass/10.0));
            parameter("amplitude", (float)amplitude);
            parameter("radius", (float)radius / (pass*2+1));
            super.apply(src, new ColorBuffer[]{intermediate});
            parameter("offset", (float)(pass/10.0));
            parameter("radius", (float)radius / (pass*2+1+1));
            super.apply(new ColorBuffer[]{intermediate}, dest);
        }

        return this;
    }

}
