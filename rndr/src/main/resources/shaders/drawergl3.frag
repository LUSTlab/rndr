#version 330

uniform vec4 fill;
uniform vec4 stroke;

//in vec2 v_texCoord0;
//in vec4 v_color;
//in vec3 v_normal;
//in mat4 v_modelViewMatrix;

out vec4 o_color;
void main(void) {
    o_color = fill;
}