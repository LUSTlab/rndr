#version 330

${attributes! }
${uniforms! }
out vec2 v_texCoord0;

uniform mat4 modelViewMatrix;
uniform mat4 modelViewProjectionMatrix;
uniform mat4 normalMatrix;
uniform vec2 dimensions;
uniform vec3 offset;

uniform vec2 screenSize;

out vec3 v_boundsPosition;

void main(void) {

    v_boundsPosition = (vec3(position.xy, 0) - vec3(0.5, 0.5, 0))*2.0;

    vec4 pos = modelViewProjectionMatrix * vec4( vec3(position.xy * dimensions, 0) + offset, 1.0);

    gl_Position = pos;// + vec4(s * d,0,0);
    v_texCoord0 = texCoord0;
}