import net.lustlab.rndr.shape.Rectangle;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class TestRectangle {

    @Test
    public void intersection() {
        Rectangle a = new Rectangle(40,40,40,40);
        assertFalse(a.intersects(new Rectangle(10,10,10,10)));
        assertFalse(a.intersects(new Rectangle(100,10,10,10)));
        assertFalse(a.intersects(new Rectangle(10,100,100,10)));
        assertFalse(a.intersects(new Rectangle(-100,10,100,100)));
        assertTrue(a.intersects(a));
        assertTrue(a.intersects(new Rectangle(50, 50, 20, 20)));
    }

    @Test
    public void bounds() {
        Rectangle a = new Rectangle(40,40,40,40);
        Rectangle b = new Rectangle(40,40,50,50);

        Rectangle c = Rectangle.bounds(a, b);
        assertEquals(40, c.x, 0.0001);
        assertEquals(40, c.y, 0.0001);
        assertEquals(50, c.width, 0.0001);
        assertEquals(50, c.height, 0.0001);
    }

}
