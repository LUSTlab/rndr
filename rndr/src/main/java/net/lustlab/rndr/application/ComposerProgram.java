package net.lustlab.rndr.application;


import net.lustlab.colorlib.ColorRGBa;
import net.lustlab.rndr.buffers.RenderTarget;

public class ComposerProgram  extends Program {


    RenderTarget backgroundTarget = null;
    public void backgroundTarget(RenderTarget target) {
        backgroundTarget = target;

    }

    @Override
    public void draw() {
        if (backgroundTarget != null) {
            drawer.image(backgroundTarget.colorBuffer(0), 0, 0);
        }
        drawer.fill(ColorRGBa.BLUE);
        drawer.circle((Math.cos(seconds())*0.5+0.5) * width, (Math.sin(seconds())*0.5+0.5) * height, 50.0);

    }
}
