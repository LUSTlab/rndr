package net.lustlab.rndr.compute;

import com.jogamp.opencl.gl.CLGLBuffer;
import net.lustlab.rndr.buffers.BufferTexture;

public class ComputeBufferTexture {

    final BufferTexture bufferTexture;
    final CLGLBuffer buffer;

    ComputeBufferTexture(CLGLBuffer buffer, BufferTexture bufferTexture) {
        this.buffer = buffer;
        this.bufferTexture = bufferTexture;
    }

    public BufferTexture bufferTexture() {
        return bufferTexture;
    }


}
