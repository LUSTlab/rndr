#version 330

uniform mat4 modelViewMatrix;
uniform mat4 modelViewProjectionMatrix;
uniform mat4 normalMatrix;

uniform vec3 offset;

uniform samplerBuffer positions;
uniform int instanceOffset;

flat out int v_instance;


in vec3 position;
in vec3 normal;
in vec4 color;

in vec2 texCoord0;
in vec2 texCoord1;
in vec2 texCoord2;

out vec2 v_texCoord0;
out vec2 v_dimensions;
out vec3 v_boundsPosition;


void main(void) {

    v_boundsPosition = (vec3(position.xy, 0) - vec3(0.5, 0.5, 0))*2.0;

    int instance = gl_InstanceID + instanceOffset;
    v_instance = instance;


    vec4 data = texelFetch(positions, instance);

    vec3 offset = vec3(data.xy, 0);
    vec2 dimensions = data.zw;
    v_dimensions = dimensions;

    gl_Position = modelViewProjectionMatrix * vec4( vec3(position.xy * dimensions, 0) + offset, 1.0);

    v_texCoord0 = texCoord0;
}