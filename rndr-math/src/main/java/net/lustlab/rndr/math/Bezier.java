package net.lustlab.rndr.math;

/**
* Created by voorbeeld on 12/19/13.
*/
public abstract class Bezier {


    /* quadratic bezier */
    public static double bezier(double x0, double c0, double x1, double t) {
        double it = 1.0 - t;
        double it2 = it * it;
        double t2 = t * t;
        return it2 * x0 + 2 * it * t * c0 + t2 * x1;
    }

    public static double derivative(double x0, double c0, double x1, double t) {

        double it = 1.0-t;
        return 2 * it * (c0-x0) + 2 * t * (x1-c0);

    }

    public static Vector2 derivative(Vector2 x0, Vector2 c0, Vector2 x1, double t) {
        double it = 1.0-t;
        return new Vector2(2 * it - (c0.x-x0.x) + 2 * t * (x1.x-c0.x),2 * it - (c0.x-x0.x) + 2 * t * (x1.x-c0.x));
    }

    public static Vector2 derivative(Vector2 p0, Vector2 p1, Vector2 p2, Vector2 p3, double t) {
        double it = 1.0 - t;
        return p1.minus(p0).scale(3 * it * it).
                plus(p2.minus(p1).scale(6*it*t)).
                plus(p3.minus(p2).scale(3*t*t));
    }

    public static Vector2 normal(Vector2 x0, Vector2 c0, Vector2 x1, double t) {
        Vector2 d = derivative(x0, c0, x1, t);
        return new Vector2(-d.y, d.x).normalized();
    }


    public static Vector2 bezier(Vector2 x0, Vector2 c0, Vector2 x1, double t) {
        double it = 1.0 - t;
        double it2 = it * it;
        double t2 = t * t;

        Vector2 result = new Vector2(
            it2 * x0.x + 2 * it * t * c0.x + t2 * x1.x,
            it2 * x0.y + 2 * it * t * c0.y + t2 * x1.y
        );
        return result;
    }

    public static Vector3 bezier(Vector3 x0, Vector3 c0, Vector3 x1, double t) {
        double it = 1.0 - t;
        double it2 = it * it;
        double t2 = t * t;

        Vector3 result = new Vector3(
        it2 * x0.x + 2 * it * t * c0.x + t2 * x1.x,
         it2 * x0.y + 2 * it * t * c0.y + t2 * x1.y,
         it2 * x0.z + 2 * it * t * c0.z + t2 * x1.z);
        return result;
    }

    /* cubic bezier */
    public static double bezier(double x0, double c0, double c1, double x1, double t) {
        double it = 1.0-t;
        double it2 = it * it;
        double it3 = it2 * it;
        double t2 = t*t;
        double t3 = t2 * t;

        return it3 * x0 + 3*it2 * t * c0 + 3*it*t2*c1 + t3*x1;
    }

    public static Vector2 bezier(Vector2 x0, Vector2 c0, Vector2 c1, Vector2 x1, double t) {
        double it = 1.0-t;
        double it2 = it * it;
        double it3 = it2 * it;
        double t2 = t*t;
        double t3 = t2 * t;

        Vector2 result = new Vector2(
        it3 * x0.x + 3*it2 * t * c0.x + 3*it*t2*c1.x + t3*x1.x,
        it3 * x0.y + 3*it2 * t * c0.y + 3*it*t2*c1.y + t3*x1.y);
        return result;
    }

    /**
     * Samples a single point on a 3d Bezier curve
     * @param x0 start point of the curve
     * @param c0 first control point
     * @param c1 second control point
     * @param x1 end point of the curve
     * @param t [0, 1]
     * @return a sample on the curve
     */
    public static Vector3 bezier(Vector3 x0, Vector3 c0, Vector3 c1, Vector3 x1, double t) {
        double it = 1.0-t;
        double it2 = it * it;
        double it3 = it2 * it;
        double t2 = t*t;
        double t3 = t2 * t;

        Vector3 result = new Vector3(
        it3 * x0.x + 3*it2 * t * c0.x + 3*it*t2*c1.x + t3*x1.x,
        it3 * x0.y + 3*it2 * t * c0.y + 3*it*t2*c1.y + t3*x1.y,
        it3 * x0.z + 3*it2 * t * c0.z + 3*it*t2*c1.z + t3*x1.z);
        return result;
    }


}
