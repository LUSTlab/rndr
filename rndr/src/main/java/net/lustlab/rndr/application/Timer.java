package net.lustlab.rndr.application;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;
import java.util.function.Consumer;

public class Timer {

    public static class TimerJob {
        Consumer<TimerJob> function;
        TimeUnit timeUnit;
        long next;
        long interval;
        long count;
        public void cancel() {
            count = 0;
        }
    }

    List<TimerJob> jobs = new ArrayList<>();

    public Timer after(long units, TimeUnit timeUnit, Consumer<TimerJob> function) {
        TimerJob timerJob = new TimerJob();
        timerJob.function = function;
        timerJob.next = System.currentTimeMillis() + TimeUnit.MILLISECONDS.convert(units, timeUnit);
        timerJob.timeUnit = timeUnit;
        timerJob.interval = 0;
        timerJob.count = 1;
        jobs.add(timerJob);
        return this;
    }

    public Timer repeat(long interval, TimeUnit intervalTimeUnit, Consumer<TimerJob> function) {
        TimerJob timerJob = new TimerJob();
        timerJob.function = function;
        timerJob.next = System.currentTimeMillis() + TimeUnit.MILLISECONDS.convert(interval, intervalTimeUnit);
        timerJob.timeUnit = intervalTimeUnit;
        timerJob.interval = TimeUnit.MILLISECONDS.convert(interval, intervalTimeUnit);
        timerJob.count = -1;
        jobs.add(timerJob);
        return this;
    }


    public void update() {

        List<TimerJob> toRemove = new ArrayList<>();

        long t = System.currentTimeMillis();
        for (TimerJob timerJob: jobs) {
            if (t > timerJob.next) {
                timerJob.function.accept(timerJob);
                timerJob.next += timerJob.interval;
                if (timerJob.count > 0) {
                    timerJob.count--;
                }

            }
            if (timerJob.count == 0) {
                toRemove.add(timerJob);
            }
        }

        jobs.removeAll(toRemove);
    }

}
