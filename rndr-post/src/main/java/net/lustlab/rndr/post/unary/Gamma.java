package net.lustlab.rndr.post.unary;

import net.lustlab.rndr.post.Filter;
import net.lustlab.rndr.shaders.FragmentShader;


public class Gamma extends Filter {
    public Gamma() {
        super(FragmentShader.fromUrl("cp:shaders/gl3/post/gamma.frag"));

        parameter("gamma", 1.0f);
    }

    public Gamma gamma(double gamma) {
        parameter("gamma", (float) gamma);
        return this;
    }

}
