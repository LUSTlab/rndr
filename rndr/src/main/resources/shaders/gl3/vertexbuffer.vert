#version 330

${attributes! }
${uniforms! }

uniform mat4 modelViewMatrix;
uniform mat4 modelViewProjectionMatrix;
uniform mat4 normalMatrix;

${varyingOut! }

<#if attributes?contains("vec3 normal;")>
    out vec3 v_modelNormal;
</#if>

flat out int v_instance;

out vec3 v_originalPosition;
out vec3 v_originalNormal;

out vec3 v_modelPosition;
out vec3 v_viewPosition;
out mat4 v_normalMatrix;

void main(void) {
    int instance = gl_InstanceID;
    v_instance = instance;

    <#if attributes?contains("vec3 normal;")>
    vec3 x_normal = normal;
    v_originalNormal = normal;
    </#if>

    vec3 x_position = position;
    v_originalPosition = position;
    { ${positionTransform! } }

    ${varyingBridge! }
    <#if attributes?contains("vec3 normal;")>
        v_normal = (normalMatrix * vec4(x_normal, 0.0)).xyz;
        v_modelNormal = x_normal;
    </#if>
    v_normalMatrix = normalMatrix;



    v_modelPosition = x_position;
    v_viewPosition = (modelViewMatrix * vec4(x_position, 1.0)).xyz;

    gl_Position = modelViewProjectionMatrix * vec4(x_position.xyz, 1.0);
}