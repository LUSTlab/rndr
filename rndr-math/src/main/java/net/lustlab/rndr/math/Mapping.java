package net.lustlab.rndr.math;

/**
 * Mapping operations
 */
public class Mapping {


    public static double mix(double left, double right, double factor) {
        return left * (1.0-factor) + right * factor;
    }

    public static Vector2 mix(Vector2 left, Vector2 right, double factor) {
        return left.scale(1.0-factor).plus(right.scale(factor));
    }

    public static Vector3 mix(Vector3 left, Vector3 right, double factor) {
        return left.scale(1.0-factor).plus(right.scale(factor));
    }

    public static Vector4 mix(Vector4 left, Vector4 right, double factor) {
        return left.scale(1.0-factor).plus(right.scale(factor));
    }

    public static double step(double edge, double x) {
        return x < edge? 0 : 1.0;
    }


    /**
     * Linearly maps a value, which is given in the before domain to a value in the after domain
     * @param beforeLeft the left value of the before domain
     * @param beforeRight the right value of the before domain
     * @param afterLeft the left value of the after domain
     * @param afterRight the right value of the after domain
     * @param value the value to map from the before domain to the after domain
     * @return a value in the after domain
     */
    public static double map(double beforeLeft, double beforeRight, double afterLeft, double afterRight, double value) {
        double n = (value - beforeLeft) / (beforeRight-beforeLeft);
        return afterLeft + n * (afterRight - afterLeft);
    }

    public static double linearstep(double edge0, double edge1, double x) {
        x = saturate((x - edge0)/(edge1 - edge0));
        return x;
    }


    /**
     * Smoothstep
     * @param edge0
     * @param edge1
     * @param x
     * @return a mapped value in the interval [0, 1]
     */
    public static double smoothstep(double edge0, double edge1, double x) {
        // Scale, bias and saturate x to 0..1 range
        x = saturate((x - edge0)/(edge1 - edge0));
        // Evaluate polynomial
        return x*x*(3 - 2*x);
    }


    /**
     * Smootherstep
     * @param edge0
     * @param edge1
     * @param x
     * @return a mapped value in the interval [0, 1]
     */
    public static double smootherstep(double edge0, double edge1, double x) {
        // Scale, bias and saturate x to 0..1 range
        x = saturate((x - edge0)/(edge1 - edge0));
        // Evaluate polynomial
        return x*x*x*(x*(x*6 - 15) + 10);
    }

    public static double clamp(double x, double min, double max) {
        return Math.min(max, Math.max(min, x));
    }
    public static int clamp(int x, int min, int max) {
        return Math.min(max, Math.max(min, x));
    }


    /**
     * Clips x between 0 and 1
     * @param x the input value
     * @return 0 if x <= 0, <0, 1> if 0 < x < 1, 1 if x >= 1
     */
    public static double saturate(double x) {
        return Math.max(0,Math.min(1, x));
    }
}
