package net.lustlab.rndr.font.vector;

import net.lustlab.rndr.font.FontDescriptor;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;

public class FontVectorMapManager {
    static Logger logger = LogManager.getLogger(FontVectorMapManager.class);
    private static Map<FontDescriptor, FontVectorMap> fontMaps = new HashMap<>();


    public static FontVectorMap fromUrl(String url, int size) {
        try {
            return fromUrl(new URL(url), size);
        } catch (MalformedURLException e) {
            throw new RuntimeException("could not load FontVectorMap from malformed url: " + url);
        }
    }


    public static FontVectorMap fromUrl(URL url, int size) {
        FontDescriptor descriptor = new FontDescriptor(url.toString(), size);
        FontVectorMap fontVectorMap = fontMaps.get(descriptor);
        if (fontVectorMap == null) {
            logger.debug("building new font map {} {}", url, size);
            fontVectorMap = FontVectorMapBuilder.buildFontMap(url, descriptor.size);
        }
        fontMaps.put(descriptor, fontVectorMap);
        return fontVectorMap;
    }
}