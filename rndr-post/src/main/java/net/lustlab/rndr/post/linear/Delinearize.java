package net.lustlab.rndr.post.linear;

import net.lustlab.rndr.post.Filter;
import net.lustlab.rndr.shaders.FragmentShader;

public class Delinearize extends Filter{
    public Delinearize() {
        super(FragmentShader.fromUrl("cp:shaders/gl3/post/linear/delinearize.frag"));
    }
}
