package net.lustlab.rndr.shape;

import net.lustlab.rndr.math.Matrix44;
import net.lustlab.rndr.math.Vector2;

import java.util.Iterator;
import java.util.List;

/**
 *
 */

public abstract class Contour implements Iterable<Segment> {

    /**
     * Calculates the length of the contour by summing the segment lengths;
     * @return the unitless length of the contour
     */
    public abstract double length();

    /**
     * Evaluate the position at t [0, 1]
     * @param t
     * @return the position on the contour at t
     */
    public abstract Vector2 position(double t);

    /**
     * Evaluate the normal at t~[0,1]
     * @param t
     * @return the normal on the contour at t
     */
    public abstract Vector2 normal(double t);

    /**
     * Evaluate the curve velocity at t~[0, 1]
     * @param t
     * @return the velocity of the contour at t
     */
    public abstract Vector2 velocity(double t);

    /**
     * Evaluate the curve direction or tangent at t~[0, 1]
     * @param t
     * @return the direction of the contour at t
     */
    public abstract Vector2 direction(double t);

    public abstract Iterator<Segment> iterator();

    /**
     * Determines if the contour is closed
     * @return true iff the contour is closed
     */
    public abstract boolean closed();

    /**
     * Returns a resampled copy of the Contour. All segments of the resampled copy are line segments.
     * @param pointCount
     * @return
     */
    public abstract Contour equidistant(int pointCount);

    /**
     * Returns the number of segments in the Contour
     * @return
     */
    public abstract int segmentCount();

    /**
     * Returns a sub Contour of this Contour
     * @param t0 the start of the sub-contour ~[0, 1]
     * @param t1 the end of the sub-contour ~[0,1]
     * @return a sub contour
     */
    public abstract Contour sub(double t0, double t1);

    /**
     * Returns a deep copy of this Contour
     * @return a deep copy
     */
    public abstract Contour copy();

    /**
     * Returns a transformed copy of this Contour
     * @param transform the transform to apply to the Contour
     * @return a tranformed copy
     */
    public abstract Contour apply(Matrix44 transform);

    /**
     * Returns a reversed copy of the Contour
     * @return a reversed copy of this Contour
     */
    public abstract Contour reverse();

    /**
     * Returns a list of equidistant points on the Contour
     * @param pointCount the number of equidistant points
     * @return
     */
    public abstract List<Vector2> equidistantPositions(int pointCount);

    /**
     * Returns a list of points on the Contour that preserve curvature
     * @return
     */
    public abstract List<Vector2> adaptivePositions();

}
