package net.lustlab.rndr.compute;

import com.jogamp.opencl.gl.CLGLBuffer;
import net.lustlab.rndr.buffers.VertexBuffer;

public class ComputeVertexBuffer {
    final CLGLBuffer buffer;
    final VertexBuffer vertexBuffer;
    public ComputeVertexBuffer(CLGLBuffer buffer, VertexBuffer vertexBuffer) {
        this.buffer = buffer;
        this.vertexBuffer = vertexBuffer;
    }
    public VertexBuffer vertexBuffer() {
        return this.vertexBuffer;
    }

}
