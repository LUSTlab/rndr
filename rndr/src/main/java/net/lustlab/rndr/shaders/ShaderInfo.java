package net.lustlab.rndr.shaders;

import com.jogamp.opengl.GL;
import com.jogamp.opengl.GL3;
import org.apache.logging.log4j.LogManager;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.IntBuffer;
import java.util.logging.Logger;

public final class ShaderInfo {

    public static void checkShaderInfoLog(GL gl, int object, String code, String sourceFile) {

        if (gl.isGL3()) {
            GL3 gl3 = gl.getGL3();
            IntBuffer iVal = IntBuffer.allocate(1);
            gl3.glGetShaderiv(object, GL3.GL_INFO_LOG_LENGTH, iVal);

            iVal.rewind();
            int logLength = iVal.get();
            if (logLength == 0) {
                return;
            }
            else {
                ByteBuffer infoLog = ByteBuffer.allocate(logLength);
                iVal.flip();
                gl3.glGetShaderInfoLog(object, logLength, iVal, infoLog);
                byte[] infoBytes = new byte[logLength];
                infoLog.get(infoBytes);
                LogManager.getLogger(ShaderInfo.class.getName()).warn("GLSL Validation >> " + new String(infoBytes));

                try {
                    File temp = File.createTempFile("rndr", ".fs");
                    FileWriter fw = new FileWriter(temp);

                    fw.write(code);
                    fw.close();
                    System.err.println("at unknown.shader(" + temp.getAbsolutePath() + ":1)");

                } catch (IOException e) {
                    e.printStackTrace();
                }
                throw new ShaderException("Shader error: " + sourceFile);
            }
        }

    }
    public static void checkProgramInfoLog(GL gl, int object, String sourceFile) {

         if (gl.isGL3()) {
            GL3 gl3 = gl.getGL3();
            IntBuffer iVal = IntBuffer.allocate(1);
            gl3.glGetProgramiv(object, GL3.GL_INFO_LOG_LENGTH, iVal);

            iVal.rewind();
            int logLength = iVal.get();
            if (logLength <= 1) {
                return;
            }
            else {
                ByteBuffer infoLog = ByteBuffer.allocate(logLength);
                iVal.flip();
                gl3.glGetProgramInfoLog(object, logLength, iVal, infoLog);
                byte[] infoBytes = new byte[logLength];
                infoLog.get(infoBytes);
                LogManager.getLogger(ShaderInfo.class.getName()).warn("link problems in " + sourceFile + ":\n" + new String(infoBytes));
            }
        }

    }

}
