package net.lustlab.rndr.draw.gl3;

import com.jogamp.opengl.GL;
import com.jogamp.opengl.GL3;
import com.jogamp.opengl.GLContext;
import net.lustlab.rndr.draw.*;
import net.lustlab.rndr.gl.GLError;

import java.nio.channels.Channel;

public class StateTools {

    public static void setupState(DrawStyle drawStyle) {

        GL3 gl = GLContext.getCurrentGL().getGL3();
        GLError.debug(gl);

        gl.glColorMask(
                (drawStyle.channelMask & ChannelMask.RED) != 0,
                (drawStyle.channelMask & ChannelMask.GREEN) != 0,
                (drawStyle.channelMask & ChannelMask.BLUE) != 0,
                (drawStyle.channelMask & ChannelMask.ALPHA) != 0);


        gl.glDepthMask(drawStyle.depthWrite);
        GLError.debug(gl);

        gl.glEnable(GL.GL_DEPTH_TEST);
        GLError.debug(gl);

        gl.glDepthFunc(drawStyle.depthTest.toGLDepthFunc());
        GLError.debug(gl);

        if (drawStyle.depthWrite) {
            gl.glDepthMask(true);
        } else {
            gl.glDepthMask(false);
        }

        if (drawStyle.cullMode != CullMode.NONE) {
            gl.glEnable(GL.GL_CULL_FACE);
            gl.glCullFace(drawStyle.cullMode.toGLCullFace());
        } else {
            gl.glDisable(GL.GL_CULL_FACE);
        }

        if (drawStyle.blendMode != BlendMode.NONE) {



            switch (drawStyle.blendMode) {
                case NONE:
                    gl.glDisable(GL.GL_BLEND);
                    break;
                case ADD:
                    gl.glEnable(GL.GL_BLEND);
                    gl.glBlendFunci(0, GL.GL_ONE, GL.GL_ONE);
                    break;
                case ADD_ALPHA:
                    gl.glEnable(GL.GL_BLEND);
                    gl.glBlendFunci(0, GL.GL_SRC_ALPHA, GL.GL_ONE);
                    break;
                case OVER:
                    gl.glEnable(GL.GL_BLEND);
                    //gl.glBlendFunc(GL.GL_ONE, GL.GL_ONE_MINUS_SRC_ALPHA);
                    gl.glBlendFuncSeparatei(0, GL.GL_ONE, GL.GL_ONE_MINUS_SRC_ALPHA, GL.GL_ONE, GL.GL_ONE_MINUS_SRC_ALPHA);


                case NORMAL:
                    gl.glEnable(GL.GL_BLEND);
                    //gl.glBlendFunc(GL.GL_SRC_ALPHA, GL.GL_ONE_MINUS_SRC_ALPHA);
                    //gl.glBlendEquationSeparate(GL.GL_FUNC_ADD, GL3.GL_MAX);
                    gl.glBlendFuncSeparatei(0, GL.GL_SRC_ALPHA, GL.GL_ONE_MINUS_SRC_ALPHA, GL.GL_ONE, GL.GL_ONE_MINUS_SRC_ALPHA);
                    break;

                case MULTIPLY:
                    gl.glEnable(GL.GL_BLEND);
                    gl.glBlendFunci(0, GL.GL_DST_COLOR, GL.GL_ZERO);
                    break;
            }
        } else {
            gl.glDisable(GL.GL_BLEND);
        }

        if (drawStyle.stencilTest == StencilTest.ALWAYS
                && drawStyle.depthPassOperation == StencilOperation.KEEP
                && drawStyle.depthFailOperation == StencilOperation.KEEP
                && drawStyle.stencilFailOperation == StencilOperation.KEEP) {
            gl.glDisable(GL.GL_STENCIL_TEST);
        } else {
            gl.glEnable(GL.GL_STENCIL_TEST);
            gl.glStencilFunc(glStencilTest(drawStyle.stencilTest), drawStyle.stencilTestReference, drawStyle.stencilTestMask);
            gl.glStencilOp(glStencilOp(drawStyle.stencilFailOperation), glStencilOp(drawStyle.depthFailOperation), glStencilOp(drawStyle.depthPassOperation));
            gl.glStencilMask(drawStyle.stencilWriteMask);
        }

    }

    static int glStencilTest(StencilTest test) {
        switch(test) {
            case NEVER:
                return GL.GL_NEVER;
            case ALWAYS:
                return GL.GL_ALWAYS;
            case LESS:
                return GL.GL_LESS;
            case LESS_OR_EQUAL:
                return GL.GL_LEQUAL;
            case GREATER:
                return GL.GL_GREATER;
            case GREATER_OR_EQUAL:
                return GL.GL_GEQUAL;
            case EQUAL:
                return GL.GL_EQUAL;
            case NOT_EQUAL:
                return GL.GL_NOTEQUAL;
            default:
                throw new RuntimeException("unsupported test");
        }
    }

    static int glStencilOp(StencilOperation op) {
        switch (op) {
            case KEEP:
                return GL.GL_KEEP;
            case DECREASE:
                return GL.GL_DECR;
            case DECREASE_WRAP:
                return GL.GL_DECR_WRAP;
            case INCREASE:
                return GL.GL_INCR;
            case INCREASE_WRAP:
                return GL.GL_INCR_WRAP;
            case ZERO:
                return GL.GL_ZERO;
            case INVERT:
                return GL.GL_INVERT;
            case REPLACE:
                return GL.GL_REPLACE;
            default:
                throw new RuntimeException("unsupported op");
        }
    }
}
