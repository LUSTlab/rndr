package net.lustlab.rndr.application.internal;

import java.io.File;

public interface PlatformDriver {
    File temporaryDirectory();
    File cacheDirectory(String programName);
    File supportDirectory(String programName);
}
