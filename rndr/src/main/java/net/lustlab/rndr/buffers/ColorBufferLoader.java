package net.lustlab.rndr.buffers;

import com.jogamp.opengl.*;
import com.jogamp.opengl.util.texture.Texture;
import com.jogamp.opengl.util.texture.TextureIO;
import net.lustlab.rndr.RNDRException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.*;

import static net.lustlab.rndr.buffers.ColorBufferProxy.State.LOADED;
import static net.lustlab.rndr.buffers.ColorBufferProxy.State.NOT_LOADED;

/**
 * Asynchronous image loader.
 */
public class ColorBufferLoader extends Thread {

    boolean fixIntel = false;
    int cycle = 0;
    int cycleInterval = 20;
    long sleep = -1;

    public static interface URLLoader {
        public Texture load(URL url) throws IOException;
    }


    URLLoader defaultURLLoader = url -> TextureIO.newTexture(url, true, null);

    public ColorBufferLoader fixIntel(boolean fixIntel) {
        this.fixIntel = fixIntel;
        return this;
    }

    public ColorBufferLoader sleepDuration(long sleep) {
        this.sleep = sleep;
        return this;
    }

    private static Logger logger = LogManager.getLogger(ColorBufferLoader.class.getName());
    private final Map<URL, ColorBufferProxy> entries = new HashMap<>();
    private final ArrayList<URL> queue = new ArrayList<>();

    GLContext context;
    public final Set<ColorBufferProxy> active = new HashSet<>();

    GL gl;
    private int timeBeforeUnload = 1500;
    private int timeToLoad = 1500;

    public static class Statistics {
        public long activeColorBuffers;
        public long totalImagesLoaded;
        public long totalLoadingTime;
        public long totalImagesUnloaded;
        public long totalBytesDecoded;
        public long totalBytesRead;
        public long errors;
        public double averageLoadingTime;
        public double averageReadSpeed;

    }

    Statistics statistics = new Statistics();

    Map<String,URLLoader> loaders = new HashMap<>();

    public ColorBufferLoader loader(String protocol, URLLoader loader) {
        loaders.put(protocol, loader);
        return this;
    }

    public Statistics statistics() {
        statistics.activeColorBuffers = active.size();
        statistics.averageLoadingTime = statistics.totalLoadingTime / (1.0 * statistics.totalImagesLoaded);
        statistics.averageReadSpeed = statistics.totalBytesRead  / (statistics.totalLoadingTime/1000.0);
        return statistics;
    }

    public ColorBufferLoader timeBeforeUnload(int timeInMs) {
        timeBeforeUnload = timeInMs;
        return this;
    }

    public ColorBufferLoader timeToLoad(int timeInMs) {
        timeToLoad = timeInMs;
        return this;
    }


    public ColorBufferLoader(GL gl) {
        logger.debug("creating new ColorBufferLoader");
        GLDrawable drawable = gl.getContext().getGLDrawable();
        GLOffscreenAutoDrawable offscreen = GLDrawableFactory.getFactory(drawable.getGLProfile())
                .createOffscreenAutoDrawable(null, drawable.getChosenGLCapabilities(), null, 640, 480);
        offscreen.setSharedContext(gl.getContext());
        offscreen.display();
        this.gl = gl;
        this.context = offscreen.getContext();
    }

//    public ColorBufferLoader(GLContext context) {
//        this.context = context;
//    }


    public ColorBufferProxy fromUrl(String url) {
        try {
            return fromUrl(new URL(url));
        } catch (MalformedURLException e) {
            throw new RNDRException(e);
        }
    }

    /**
     * Returns an image instance
     * @param url filename of the image
     * @return an image, possible not loaded yet
     */
    public ColorBufferProxy fromUrl(URL url) {
        synchronized(entries) {
            ColorBufferProxy entry = entries.get(url);
            if (entry == null) {
                entry = ColorBufferProxy.createLoadable(url, this);
                entries.put(url, entry);
            }
            return entry;
        }
    }


//    /**
//     * Queue the image to be loaded
//     * @param ColorBufferProxy the image to queue
//     */
    public void queue(ColorBufferProxy colorBufferProxy) {
        synchronized (queue) {
            logger.trace("queued: " + colorBufferProxy + " " + queue.size());
            fromUrl(colorBufferProxy.url()).state(ColorBufferProxy.State.QUEUED);
            queue.add(colorBufferProxy.url());
        }
    }

    @Override
    public void run() {

        setName("ColorBufferLoaderThread");
        logger.debug("started ColorBufferLoader thread");
        context.makeCurrent();
        while (true) {

            if (sleep >= 0) {
                try {
                    sleep(sleep);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }

            sortQueue();

            synchronized (active) {
                List<ColorBufferProxy> toRemove = new ArrayList<>();
                for (ColorBufferProxy entry : active) {
                    long delta = entry.timeSinceLastTouch();
                    if (delta > timeBeforeUnload && !entry.persistent()) {
                        logger.trace("unloading texture for: " + entry + " " + active.size());

                        entry.colorBuffer().ifPresent(cb -> {
                            cb.destroy(context.getGL());
                            statistics.totalImagesUnloaded++;
                            gl.glFlush();
                            gl.glFinish();
                        });
                        entry.colorBuffer(null);
                        //entry.loadTime = -1;
                        entry.state(NOT_LOADED);
                        toRemove.add(entry);
                    }
                }

                for (ColorBufferProxy entry : toRemove) {
                    active.remove(entry);
                }
            }

            // TURN THE INTEL HD GRAPHICS WHEEL
            try {
                if (fixIntel) {
                    if (cycle % cycleInterval == 0) {
                        gl.glClear(GL.GL_COLOR_BUFFER_BIT);
                    }
                }
            } catch (NullPointerException e) {
                break;
            }

            cycle++;
            URL index;
            synchronized (queue) {
                index = queue.size() > 0 ? queue.get(0) : null;
            }
            if (index != null) {
                synchronized (queue) {
                    queue.remove(0);
                }

                ColorBufferProxy entry = fromUrl(index);

                long timeSinceTouch = entry.timeSinceLastTouch();

                if (timeSinceTouch < timeToLoad || entry.persistent()) {
                    loadImage(entry);
                } else {
                    synchronized (entries) {
                        entry.state(NOT_LOADED);
                        entry.colorBuffer(null);
                    }
                }

            }
        }
    }

    private void loadImage(ColorBufferProxy entry) {
        try {
            TextureIO.setTexRectEnabled(false);
            Texture texture;

            long loadStart = System.currentTimeMillis();

            URLLoader loader = loaders.get(entry.url().getProtocol());

            if (loader == null) {
                loader = defaultURLLoader;
            }
            texture = loader.load(entry.url()); //TextureIO.newTexture(entry.url, true, null);

            if (gl.isGL3()) {
                texture.bind(gl);
                gl.getGL3().glGenerateMipmap(GL.GL_TEXTURE_2D);
                gl.glTexParameteri(GL.GL_TEXTURE_2D, GL.GL_TEXTURE_MIN_FILTER, GL.GL_LINEAR_MIPMAP_LINEAR);
                gl.glBindTexture(GL.GL_TEXTURE_2D, 0);
            }

            context.getGL().glFinish();
            long loadEnd = System.currentTimeMillis();

            synchronized (entries) {
                entry.colorBuffer(ColorBuffer.wrapTexture(texture));
                entry.state(LOADED);
                logger.trace("loaded image: " + entry);
            }

            if (loadEnd-loadStart > 200) {
                logger.warn("loading image {} took {}ms, image resolution {}x{}", entry.url(), loadEnd-loadStart,entry.colorBuffer().get().width(), entry.colorBuffer().get().height() );
            }

            statistics.totalLoadingTime += (loadEnd-loadStart);
            statistics.totalImagesLoaded++;

//            if (entry.url().getProtocol().equals("file")) {
//                try {
//                    File f = new File(entry.url().toURI());
//                    statistics.totalBytesRead += f.length();
//                } catch (URISyntaxException e) {
//                    e.printStackTrace();
//                }
//            }

            statistics.totalBytesDecoded += entry.colorBuffer().get().width() * entry.colorBuffer().get().height() * 4;

            entry.touch();
            synchronized (active) {
                active.add(entry);
            }

        } catch (GLException e) {
            logger.error("OpenGLException while loading: " + entry, e);
            entry.error("OpenGLException: " + e);
        } catch (IOException e) {
            logger.error("IO exception while loading: " + entry, e);
            entry.error("IOException: " + e);
        } catch (Exception e) {
            logger.error("exception while loading: " + entry, e);
            entry.error("Exception: " + e);
        }

    }

    private void sortQueue() {
        synchronized (queue) {
            final Map<URL, Long> times = new HashMap<>();
            for (URL queued: queue) {
                times.put(queued, entries.get(queued).lastTouched());
            }

            Collections.sort(queue, (o1, o2) -> {
                long t1 = times.get(o1);
                long t2 = times.get(o2);

                if (t1 < t2) {
                    return 1;
                } else if (t1 > t2) {
                    return -1;
                } else {
                    return 0;
                }

            });
        }
    }
}
