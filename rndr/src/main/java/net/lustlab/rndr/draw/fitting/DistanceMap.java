package net.lustlab.rndr.draw.fitting;

import net.lustlab.colorlib.ColorRGBa;
import net.lustlab.rndr.draw.BlendMode;
import net.lustlab.rndr.draw.Drawer;
import net.lustlab.rndr.draw.gl3.DrawerGL3;
import net.lustlab.rndr.draw.ShadeStyle;
import net.lustlab.rndr.buffers.ColorBuffer;
import net.lustlab.rndr.buffers.ColorFormat;
import net.lustlab.rndr.buffers.ColorType;
import net.lustlab.rndr.buffers.RenderTarget;
import net.lustlab.rndr.math.Mapping;
import net.lustlab.rndr.math.Vector2;
import net.lustlab.rndr.post.Filter;
import net.lustlab.rndr.shaders.FragmentShader;

import java.nio.FloatBuffer;

public class DistanceMap {

    boolean dirty;
    public ColorBuffer buffers[] = new ColorBuffer[2];
    FloatBuffer shadow;

    final int width;
    final int height;
    final Drawer drawer;

    ShadeStyle lineDistance;
    ShadeStyle pointDistance;

    Filter combiner;

    public DistanceMap(Drawer drawer, int width, int height) {
        for (int i = 0; i < 2; ++i) {
            buffers[i] = ColorBuffer.create(width, height, ColorFormat.RGBA, ColorType.FLOAT32);

            RenderTarget rt = RenderTarget.wrap(buffers[i]);

            rt.clearColor(ColorRGBa.RED.shade(255));


        }
        dirty = true;
        shadow = FloatBuffer.allocate(width*height*4);
        //shadow.order(ByteOrder.nativeOrder());
        this.width = width;
        this.height = height;
        this.drawer = drawer;


        pointDistance = new ShadeStyle().fill("x_fill = vec4(length(boundsPosition.xy),boundsPosition.x, boundsPosition.y,1);");
        lineDistance = new ShadeStyle().stroke("x_stroke = vec4( length(v_centerDistance.y*p_normal), v_centerDistance.y * p_normal.x,v_centerDistance.y * p_normal.y,1);");

        combiner = new Filter(FragmentShader.fromUrl("cp:shaders/gl3/distancemap-combiner.frag"));


    }

    /**
     * Peforms a distance transform on a point
     * @param point the coordinate of the point to draw in the distance map
     */
    public void point(Vector2 point) {

        RenderTarget rt = RenderTarget.wrap(buffers[0]);

        rt.bindTarget();
        rt.clearColor(ColorRGBa.RED.shade(255));

        drawer.pushStyle();

        //drawer.blend(BlendMode.NORMAL, BlendFunction.MIN);
        drawer.smooth(false);


        drawer.shadeStyle(pointDistance);



        drawer.rectangle(point.x-30.0, point.y-30.0, 60, 60);

        drawer.strokeWeight(60);

        drawer.popStyle();

        rt.unbindTarget();

        combiner.apply(buffers, new ColorBuffer[]{buffers[1]});
        drawer.blend(BlendMode.NORMAL);

        dirty = true;
    }


    /**
     * Performs a distance transform on a line segment
     * @param start the coordinate of the start of the line segment
     * @param end the coordinate of the end of the line segment
     */
    public void line(Vector2 start, Vector2 end) {

        RenderTarget rt = RenderTarget.wrap(buffers[0]);

        rt.bindTarget();
        rt.clearColor(ColorRGBa.RED.shade(255));

        drawer.pushStyle();

        drawer.smooth(false);

        drawer.strokeWeight(60);
        drawer.shadeStyle(lineDistance);

        Vector2 direction = end.minus(start);
        Vector2 normal = direction.normalized().yx().scale(-1, 1).scale(1);

        drawer.shadeStyle().parameter("normal", normal);


        drawer.line(start, end);



        drawer.blend(BlendMode.NORMAL);
        drawer.popStyle();

        rt.unbindTarget();

        combiner.apply(buffers, new ColorBuffer[] { buffers[1]});
        drawer.blend(BlendMode.NORMAL);

        dirty = true;


    }

    /**
     * Clears the distance map
     */
    public void clear() {
        dirty = true;
        RenderTarget rt = RenderTarget.wrap(buffers[1]);

        rt.bindTarget();
        rt.clearColor(ColorRGBa.RED.shade(255).opacify(0));
        rt.unbindTarget();
    }

    public Vector2 distance(Vector2 point) {

        if (dirty) {
            buffers[1].read(shadow);
            shadow.rewind();
            dirty = false;
        }

        int x = (int) point.x;
        int y = (int) point.y;
        double fx = point.x - x;
        double fy = point.y - y;
        double ifx = 1.0-fx;
        double ify = 1.0-fy;

        int x0 =Mapping.clamp(x, 0, width-1);
        int x1 = Mapping.clamp(x + 1, 0, width-1);
        int y0 =height-1 - Mapping.clamp(y, 0, height-1);
        int y1 =height-1 - Mapping.clamp(y+1, 0, height-1);


        if (x0 < 0 || y0 < 0 || (x0>width-1) || (y0>height-1)) {
            throw new RuntimeException("krak" + x0 + " " + y0);
        }

        double d00x = shadow.get((y0 * width*4 + x0*4 + 1));
        double d10x = shadow.get((y0 * width*4  + x1*4 + 1));
        double d11x = shadow.get((y1 * width*4  + x1*4 + 1));
        double d01x = shadow.get((y1 * width*4 + x0*4 + 1));

        double d00y = shadow.get((y0 * width*4  + x0*4 + 2));
        double d10y = shadow.get((y0 * width*4  + x1*4 + 2));
        double d11y = shadow.get((y1 * width*4  + x1*4 + 2));
        double d01y = shadow.get((y1 * width*4  + x0*4 + 2));

        double fdx =
                d00x * ifx * ify +
                d01x * ifx * fy +
                d11x * fx * fy +
                d10x * fx * ify;
        double fdy =
                d00y * ifx * ify +
                d01y * ifx * fy +
                d11y * fx * fy +
                d10y * fx * ify;

        return new Vector2(fdx, fdy);
//        return new Vector2(d00x, d00y);
    }


}
