package net.lustlab.rndr.math;

import java.io.Serializable;

public class Vector4 implements Serializable {

    static final long serialVersionUID = 1;

    public double x;
	public double y;
	public double z;
	public double w;

	public static final Vector4 Zero = new Vector4(0,0,0,0);
	public static final Vector4 UnitX = new Vector4(1,0,0,0);
	public static final Vector4 UnitY = new Vector4(0,1,0,0);
	public static final Vector4 UnitZ = new Vector4(0,0,1,0);
	public static final Vector4 UnitW = new Vector4(0,0,0,1);

	public Vector4() {
		x = 0;
		y = 0;
		z = 0;
		w = 0;

	}

	public Vector4(double[] v) {
		this.x = v[0];
		this.y = v[1];
		this.z = v[2];
		this.w = v[3];

	}

	public Vector4(double x, double y, double z, double w) {
		this.x = x;
		this.y = y;
		this.z = z;
		this.w = w;
	}

	public Vector4(double v) {
		this.x = v;
		this.y = v;
		this.z = v;
		this.w = v;
	}

	public double squaredLength() {
		return x*x+y*y+z*z+w*w;
	}

	public double length() {
		return Math.sqrt(x*x+y*y+z*z+w*w);
	}

	public double get(int index) {
		switch (index) {
			case 0: return x;
			case 1: return y;
			case 2: return z;
			case 3: return w;
		}
		throw new RuntimeException("invalid element");
	}

	public Vector4 times(double s) {
		return scale(s);
	}

	public Vector4 times(Vector4 v) {
		return new Vector4(x*v.x, y*v.y, z*v.z, w*v.w);
	}

	public Vector4 scale(double s) {
		return new Vector4(x*s, y*s, z*s, w*s);
	}

	public Vector4 scale(double xs, double ys, double zs, double ws) {
		return new Vector4(x*xs, y*ys, z*zs, w*ws);
	}

	public Vector4 plus(Vector4 other) {
		return new Vector4(x+other.x, y+other.y, z+other.z, w+other.w);
	}

	public Vector4 minus(Vector4 other) {
		return new Vector4(x-other.x, y-other.y, z-other.z, w-other.w);
	}

	public Vector4 mask(Vector4 other) {
		return new Vector4(x*other.x, y*other.y, z*other.z, w * other.z);
	}

    public Vector4 multiply(Matrix44 m) {
        return new Vector4(x * m.m00 + y * m.m10 + z * m.m20 + w * m.m30,
                x * m.m01 + y * m.m11 + z * m.m21 + w * m.m31,
                x * m.m02 + y * m.m12 + z * m.m22 + w * m.m32,
                x * m.m03 + y * m.m13 + z * m.m23 + w * m.m33);
    }

    public double dot(Vector4 other) {
		return x*other.x + y*other.y + z*other.z + w*other.w;
	}

	public Vector4 normalized() {
		double l = length();
		if (l == 0) {
			l=1;
		}
		return new Vector4(x/l,y/l,z/l,w/l);
	}

    public Vector2 xx() { return new Vector2(x, x); }
    public Vector2 xy() { return new Vector2(x, y); }
    public Vector2 xz() { return new Vector2(x, z); }
    public Vector2 xw() { return new Vector2(x, w); }
    public Vector2 yx() { return new Vector2(y, x); }
    public Vector2 yy() { return new Vector2(y, y); }
    public Vector2 yz() { return new Vector2(y, z); }
    public Vector2 yw() { return new Vector2(y, w); }
    public Vector2 zx() { return new Vector2(z, x); }
    public Vector2 zy() { return new Vector2(z, y); }
    public Vector2 zz() { return new Vector2(z, z); }
    public Vector2 zw() { return new Vector2(z, w); }
    public Vector2 wx() { return new Vector2(w, x); }
    public Vector2 wy() { return new Vector2(w, y); }
    public Vector2 wz() { return new Vector2(w, z); }
    public Vector2 ww() { return new Vector2(w, w); }

    public Vector3 xxx() { return new Vector3(x, x, x); }
    public Vector3 xxy() { return new Vector3(x, x, y); }
    public Vector3 xxz() { return new Vector3(x, x, z); }
    public Vector3 xyx() { return new Vector3(x, y, x); }
    public Vector3 xyy() { return new Vector3(x, y, y); }
    public Vector3 xyz() { return new Vector3(x, y, z); }
    public Vector3 xzx() { return new Vector3(x, z, x); }
    public Vector3 xzy() { return new Vector3(x, z, y); }
    public Vector3 xzz() { return new Vector3(x, z, z); }

    public Vector3 yxx() { return new Vector3(y, x, x); }
    public Vector3 yxy() { return new Vector3(y, x, y); }
    public Vector3 yxz() { return new Vector3(y, x, z); }
    public Vector3 yyx() { return new Vector3(y, y, x); }
    public Vector3 yyy() { return new Vector3(y, y, y); }
    public Vector3 yyz() { return new Vector3(y, y, z); }
    public Vector3 yzx() { return new Vector3(y, z, x); }
    public Vector3 yzy() { return new Vector3(y, z, y); }
    public Vector3 yzz() { return new Vector3(y, z, z); }

    public Vector3 zxx() { return new Vector3(z, x, x); }
    public Vector3 zxy() { return new Vector3(z, x, y); }
    public Vector3 zxz() { return new Vector3(z, x, z); }
    public Vector3 zyx() { return new Vector3(z, y, x); }
    public Vector3 zyy() { return new Vector3(z, y, y); }
    public Vector3 zyz() { return new Vector3(z, y, z); }
    public Vector3 zzx() { return new Vector3(z, z, x); }
    public Vector3 zzy() { return new Vector3(z, z, y); }
    public Vector3 zzz() { return new Vector3(z, z, z); }

    @Override
    public String toString() {
        return "Vector4{" +
                "x=" + x +
                ", y=" + y +
                ", z=" + z +
                ", w=" + w +
                '}';
    }
}
