package net.lustlab.rndr.buffers;

import com.jogamp.opengl.GL;

/**
 * Created by voorbeeld on 7/30/15.
 */
public enum VertexElementType {
    FLOAT32;

    public int toGLType() {
        switch(this) {
            case FLOAT32:
                return GL.GL_FLOAT;


            default:
                throw new RuntimeException("unsupported format");
        }
    }
}
