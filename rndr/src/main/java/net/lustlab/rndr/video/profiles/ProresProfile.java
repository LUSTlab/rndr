package net.lustlab.rndr.video.profiles;

import net.lustlab.rndr.video.VideoWriterProfile;

public class ProresProfile extends VideoWriterProfile<ProresProfile> {
    @Override
    public String[] arguments() {
        return new String[] {
                "-c:v", "prores_ks",
                "-profile:v", "3",
                "-vendor", "ap10",
                "-pix_fmt", "yuv422p10le"
        };
    }
}
