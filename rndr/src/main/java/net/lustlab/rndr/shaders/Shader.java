package net.lustlab.rndr.shaders;

import com.jogamp.opengl.*;
import net.lustlab.colorlib.ColorRGBa;
import net.lustlab.rndr.RNDRException;
import net.lustlab.rndr.gl.GLError;
import net.lustlab.rndr.math.*;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.net.MalformedURLException;
import java.net.URL;
import java.nio.ByteBuffer;
import java.nio.FloatBuffer;
import java.nio.IntBuffer;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

public class Shader {

    private final FragmentShader fragmentShader;
    private final VertexShader vertexShader;
    Map<String, Integer> uniforms = new HashMap<>();
    Map<String, Integer> attributes = new HashMap<>();
    private static Logger logger = LogManager.getLogger(Shader.class);

    boolean destroyed;

    @Override
    public String toString() {
        return "Shader{" +
                "fragmentShader=" + fragmentShader +
                ", vertexShader=" + vertexShader +
                ", program=" + program +
                ", running=" + running +
                ", name='" + name + '\'' +
                ", destroyed=" + destroyed +
                '}';
    }

    public final int program;
    public final GL gl;
    private boolean running;
    String name;

    private GL2GL3 gl23;
    Set<String> issuedUniformWarnings = new HashSet<>();

    Shader(GL gl, int program, String name, VertexShader vertexShader, FragmentShader fragmentShader) {
        this.program = program;
        this.gl = gl;
        this.gl23 = gl.getGL2GL3();
        this.name = name;
        this.vertexShader = vertexShader;
        this.fragmentShader = fragmentShader;
    }

    public Shader name(String name) {
        this.name = name;
        return this;
    }

    public static Shader fromUrls(String vertexShaderURL, String fragmentShaderURL) {
        try {
            return fromUrls(new URL(vertexShaderURL), new URL(fragmentShaderURL));
        } catch (MalformedURLException e) {
            throw new ShaderException("failed to construct Shader from URLs", e);
        }
    }

    public static Shader fromUrls(URL vertexShaderURL, URL fragmentShaderURL) {
        return createShader(VertexShader.fromUrl(vertexShaderURL), FragmentShader.fromUrl(fragmentShaderURL));
    }

    public static Shader fromStrings(String vertexShaderCode, String fragmentShaderCode) {
        return fromStrings(vertexShaderCode, "unknown-vertex-shader", fragmentShaderCode, "unknown-fragment-shader");
    }

    public static Shader fromStrings(String vertexShaderCode, String vertexShaderName, String fragmentShaderCode, String fragmentShaderName) {
        VertexShader vertexShader = VertexShader.fromString(vertexShaderCode, vertexShaderName);
        FragmentShader fragmentShader = FragmentShader.fromString(fragmentShaderCode, fragmentShaderName);

        return createShader(vertexShader, fragmentShader);

    }

    public static Shader createShader(GL gl, FragmentShader fragment) {
        return createShader(gl, null, fragment);
    }

    public static Shader createShader(GL gl, VertexShader vertex) {
        return createShader(gl, vertex, null);
    }

    public static Shader createShader(VertexShader vertex, FragmentShader fragment) {
        return createShader(GLContext.getCurrentGL(), vertex, fragment);
    }

    public static Shader createShader(GL gl, VertexShader vertex, FragmentShader fragment) {

        logger.debug("creating shader {} {}", vertex, fragment);
        String name = "";
        if (vertex != null) {
            name += vertex.name();
        }
        if (fragment != null) {

            if (name.length() > 0) {
                name += " / ";
            }
            name += fragment.name();
        }


        if (gl.isGL3()) {

            GL3 gl3 = gl.getGL3();
            GLError.check(gl3);

            int program = gl3.glCreateProgram();

            int count[]  = new int[1];
            int names[] = new int[100];
            gl3.glGetAttachedShaders(program, 100, count, 0, names, 0);
            if (!gl3.glIsProgram(program)) {
                throw new ShaderException("that is no program");
            }

            if (vertex != null) {
                if (!gl3.glIsShader(vertex.shaderObject)) {
                    throw new ShaderException("that is no vertex shader object ");
                }
                gl3.glAttachShader(program, vertex.shaderObject);

                int status = gl3.glGetError();
                if (status != GL.GL_NO_ERROR) {

                    switch (status) {
                        case GL.GL_INVALID_VALUE:
                            throw new ShaderException("Failed to attach vertex shader. GL_INVALID_VALUE. " + vertex.shaderObject);
                        case GL.GL_INVALID_OPERATION:
                            throw new ShaderException("Failed to attach vertex shader. GL_INVALID_OPERATION. "  + vertex.shaderObject);

                    }
                }
            }
            if (fragment != null) {
                gl3.glAttachShader(program, fragment.shaderObject);
                int status = gl3.glGetError();
                if (status != GL.GL_NO_ERROR) {
                    throw new ShaderException("Failed to attach fragment shader.");
                }

            }

            gl3.glGetAttachedShaders(program, 100, count, 0, names, 0);


            gl3.glLinkProgram(program);
            int[] linkStatus = new int[1];
            gl3.glGetProgramiv(program, GL3.GL_LINK_STATUS, linkStatus, 0);

            if (linkStatus[0] != GL.GL_TRUE) {
                ShaderInfo.checkProgramInfoLog(gl, program, name);
            }
            GLError.check(gl3);

            return new Shader(gl, program, name, vertex, fragment);

        }
        else {
            throw new RNDRException("unsupported GL implementation");
        }
    }


    public Shader setUniform1f(String uniform, float u0) {
        ensureRunning();
        int uniformIndex = uniformIndex(uniform);
        if (uniformIndex != -1) {
            gl23.glUniform1f(uniformIndex(uniform), u0);
        }
        GLError.debug(gl);
        return this;
    }


    public Shader setUniform1d(String uniform, double u0) {
        ensureRunning();
        int uniformIndex = uniformIndex(uniform);
        if (uniformIndex != -1) {
            gl23.glUniform1f(uniformIndex(uniform), (float)u0);
        }
        return this;
    }
    /***
     *
     * @param uniform the uniform to set
     * @param u0 the first element of the uniform vector
     * @param u1 the second element of the uniform vector
     */
    public Shader setUniform2f(String uniform, float u0, float u1) {
        ensureRunning();
        if (uniformIndex(uniform) != -1) {
            gl23.glUniform2f(uniformIndex(uniform), u0, u1);
        }
        else {
            warnInvalidUniform(uniform);
        }
        GLError.debug(gl);
        return this;
    }

    public Shader setUniform(String uniform, ColorRGBa[] colors) {
        ensureRunning();
        if (uniformIndex(uniform) != -1) {
            FloatBuffer buffer = FloatBuffer.allocate(4*4*colors.length);
            for (ColorRGBa v: colors) {
                buffer.put(v.toFloatArray());
            }
            buffer.rewind();
            gl23.glUniform4fv(uniformIndex(uniform), colors.length, buffer);
        } else {
            warnInvalidUniform(uniform);
        }
        GLError.debug(gl);
        return this;
    }


    public Shader setUniform(String uniform, Vector4[] vector4s) {
        ensureRunning();
        if (uniformIndex(uniform) != -1) {
            FloatBuffer buffer = FloatBuffer.allocate(4*4*vector4s.length);
            for (Vector4 v: vector4s) {
                buffer.put((float)v.x).put((float)v.y).put((float)v.z).put((float)v.w);
            }
            buffer.rewind();
            gl23.glUniform4fv(uniformIndex(uniform), vector4s.length, buffer);
        } else {
            warnInvalidUniform(uniform);
        }
        GLError.debug(gl);
        return this;
    }



    public Shader setUniform(String uniform, Vector3[] vector3s) {
        ensureRunning();
        if (uniformIndex(uniform) != -1) {
            FloatBuffer buffer = FloatBuffer.allocate(3*4*vector3s.length);
            for (Vector3 v: vector3s) {
                buffer.put((float)v.x).put((float) v.y).put((float) v.z);
            }
            buffer.rewind();
            gl23.glUniform3fv(uniformIndex(uniform), vector3s.length, buffer);
        }  else {
            warnInvalidUniform(uniform);
        }
        GLError.debug(gl);
        return this;
    }


    public Shader setUniform(String uniform, Vector2[] vector2s) {
        ensureRunning();
        if (uniformIndex(uniform) != -1) {
            FloatBuffer buffer = FloatBuffer.allocate(2*4*vector2s.length);
            for (Vector2 v: vector2s) {
                buffer.put((float)v.x).put((float)v.y);
            }
            buffer.rewind();
            gl23.glUniform2fv(uniformIndex(uniform), vector2s.length, buffer);
        }  else {
            warnInvalidUniform(uniform);
        }
        GLError.debug(gl);
        return this;
    }

    public Shader setUniform(String uniform, float[] floats) {
        ensureRunning();
        if (uniformIndex(uniform) != -1) {
            FloatBuffer buffer = FloatBuffer.allocate(4*floats.length);
            for (float f: floats) {
                buffer.put(f);
            }
            buffer.rewind();
            gl23.glUniform1fv(uniformIndex(uniform), floats.length, buffer);
        }  else {
            warnInvalidUniform(uniform);
        }
        GLError.debug(gl);
        return this;
    }
    public Shader setUniform(String uniform, int[] ints) {
        ensureRunning();
        if (uniformIndex(uniform) != -1) {
            IntBuffer buffer = IntBuffer.allocate(4 * ints.length);
            for (int i: ints) {
                buffer.put(i);
            }
            buffer.rewind();
            gl23.glUniform1iv(uniformIndex(uniform), ints.length, buffer);
        }  else {
            warnInvalidUniform(uniform);
        }
        GLError.debug(gl);
        return this;
    }


    /***
     *
     * @param uniform the uniform to set
     * @param u0 the first element of the uniform vector
     * @param u1 the second element of the uniform vector
     * @param u2 the third element of the uniform vector
     */
    public Shader setUniform3f(String uniform, float u0, float u1, float u2) {
        ensureRunning();
        if (uniformIndex(uniform)!=-1) {
            gl23.glUniform3f(uniformIndex(uniform), u0, u1, u2);
        } else {
            warnInvalidUniform(uniform);
        }
        GLError.debug(gl);
        return this;
    }

    public Shader setUniform1i(String uniform, int u0) {
        ensureRunning();
        if (uniformIndex(uniform)!=-1) {
            gl23.glUniform1i(uniformIndex(uniform), u0);
        }  else {
            warnInvalidUniform(uniform);
        }
        GLError.debug(gl);
        return this;
    }


    public boolean hasUniform(String uniform) {
        return uniformIndex(uniform) >= 0;
    }

    public int uniformIndex(String uniform) {

        Integer loc = uniforms.get(uniform);

        if (loc == null) {

            int location = gl23.glGetUniformLocation(program, uniform);
            if (location == -1) {
//            throw new RuntimeException("invalid uniform: " + uniform);

            }
            uniforms.put(uniform, location);
            loc = location;
        }
        GLError.debug(gl);
        return loc;

    }

    public int attributeIndex(String attribute) {
        Integer loc = attributes.get(attribute);

        if (loc == null) {
            int location = gl23.glGetAttribLocation(program, attribute);
            if (location == -1) {

                //            throw new RuntimeException("invalid attibute: " + attribute);
            }
            attributes.put(attribute, location);
            loc = location;
        }
        GLError.debug(gl);
        return loc;
    }


    public Shader setUniform(String uniform, Matrix44 matrix) {
        ensureRunning();
        FloatBuffer fb = FloatBuffer.allocate(16);
        fb.rewind();
        matrix.toFloatBuffer(fb);
        fb.rewind();

        if (uniformIndex(uniform) != -1) {
            gl23.glUniformMatrix4fv(uniformIndex(uniform),1, false, fb);
        }
        else {
            warnInvalidUniform(uniform);
        }
        GLError.debug(gl);
        return this;

    }

    public Shader setUniform(String uniform, Matrix33 matrix) {
        ensureRunning();
        FloatBuffer fb = FloatBuffer.allocate(9);
        fb.rewind();
        matrix.toFloatBuffer(fb);
        fb.rewind();

        if (uniformIndex(uniform) != -1) {
            gl23.glUniformMatrix3fv(uniformIndex(uniform),1, false, fb);
        }
        else {
            warnInvalidUniform(uniform);
        }
        GLError.debug(gl);
        return this;

    }


    private void warnInvalidUniform(String uniform) {
        if (!issuedUniformWarnings.contains(uniform)) {
            logger.warn("problem with uniform:" + uniform + " in shader " + name);
            issuedUniformWarnings.add(uniform);
        }
    }

    /***
     *
     * @param uniform the uniform to set
     * @param u0 the first element of the uniform vector
     * @param u1 the second element of the uniform vector
     * @param u2 the third element of the uniform vector
     * @param u3 the fourth elemenet of the uniform vector
     */

    public Shader setUniform4f(String uniform, float u0, float u1, float u2, float u3) {
        ensureRunning();
        int uniformIndex = uniformIndex(uniform);
        if (uniformIndex != -1) {
            gl23.glUniform4f(uniformIndex, u0, u1, u2, u3);
        }  else {
            warnInvalidUniform(uniform);
        }
        GLError.debug(gl);
        return this;
    }

    public Shader setUniform(String uniform, Vector2 v) {
        return setUniform2f(uniform, (float)v.x, (float)v.y);
    }

    public Shader setUniform(String uniform, Vector3 v) {
        ensureRunning();
        return setUniform3f(uniform, (float)v.x, (float)v.y, (float)v.z);
    }

    public Shader setUniform(String uniform, Vector4 v) {
        ensureRunning();
        return setUniform4f(uniform, (float)v.x, (float)v.y, (float)v.z, (float)v.w);
    }

    public Shader setUniform(String uniform, ColorRGBa c) {
        ensureRunning();
        return setUniform4f(uniform, (float)c.r, (float)c.g, (float)c.b, (float)c.a);
    }


    /**
     * Start using the shader. Binds the shader.
     */
    public Shader begin() {


        if (!destroyed) {
            GLError.debug(gl);

            if (running) {
                throw new ShaderException("Shader is already in use");
            }

            if (gl.isGL2()) {
                gl.getGL2().glUseProgramObjectARB(program);
            } else if (gl.isGL3()) {
                gl.getGL3().glUseProgram(program);
            }
            GLError.debug(gl);
            running = true;
            return this;
        } else {
            throw new ShaderException("shader is destroyed");
        }
    }

    /**
     * Stop using the shader. Unbinds the shader.
     */
    public Shader end(){
        if (!running) {
            throw new ShaderException("Shader is not in use");
        }

        if (gl.isGL2()) {
            gl.getGL2().glUseProgramObjectARB(0);
        }
        else if (gl.isGL3()) {
            gl.getGL3().glUseProgram(0);

        }
        GLError.debug(gl);
        running = false;
        return this;
    }

    public Shader setUniform(String uniform, boolean value) {
        ensureRunning();
        int uniformIndex = uniformIndex(uniform);
        if (uniformIndex != -1) {
            gl23.glUniform1i(uniformIndex, value? 1: 0);
        }
        GLError.debug(gl);
        return this;
    }

    public Shader setUniform(String uniform, Matrix44[] matrices) {
        ensureRunning();
        FloatBuffer fb = FloatBuffer.allocate(16 * matrices.length);
        fb.rewind();
        for (Matrix44 matrix: matrices) {
            matrix.toFloatBuffer(fb);
        }
        fb.rewind();

        int uniformIndex = uniformIndex(uniform);
        if (uniformIndex != -1) {
            gl23.glUniformMatrix4fv(uniformIndex(uniform), matrices.length, false, fb);
        }
        GLError.debug(gl);
        return this;

    }

    public Map<String, Class>  uniforms() {

        Map<String, Class> result = new HashMap<>();

        int[] count = new int[1];
        gl23.glGetProgramiv(program, GL2GL3.GL_ACTIVE_UNIFORMS, count, 0);

        System.out.println("count: " + count[0]);
        for (int i = 0; i < count[0]; ++i) {
            IntBuffer length = IntBuffer.allocate(1);
            IntBuffer size = IntBuffer.allocate(1);
            IntBuffer type = IntBuffer.allocate(1);
            ByteBuffer text = ByteBuffer.allocate(255);

            gl23.glGetActiveUniform(program, i,255, length, size, type, text);
            byte[] bs = new byte[255];
            text.rewind();
            text.get(bs);
            System.out.println(new String(bs) + " " + type.get(0));
        }


        return result;
    }

    void ensureRunning() {
        if (!running) {
            throw new ShaderException("shader not in use");
        }
    }

    public VertexShader vertexShader() {
        return vertexShader;
    }

    public FragmentShader fragmentShader() {
        return fragmentShader;
    }

    public void destroy() {
        if (!running) {
            logger.debug("destroying shader");
            destroyed = true;
            if (vertexShader != null) {
                gl23.glDetachShader(program, vertexShader.shaderObject);
                vertexShader.destroy();
            }
            if (fragmentShader != null) {
                gl23.glDetachShader(program, fragmentShader.shaderObject);
                fragmentShader.destroy();
            }
            gl23.glDeleteProgram(program);

            issuedUniformWarnings.clear();
        } else {
            throw new RuntimeException("shader is in use, it cannot be destroyed");
        }
    }
}
