package net.lustlab.rndr.post.blur;

import net.lustlab.rndr.buffers.*;
import net.lustlab.rndr.math.Vector2;
import net.lustlab.rndr.post.Filter;
import net.lustlab.rndr.shaders.FragmentShader;


public class PositionToCoc extends Filter {

    public PositionToCoc() {
        super(FragmentShader.fromUrl("cp:shaders/gl3/post/blur/position-to-coc.frag"));
        map("tex0", "image");
        map("tex1", "position");


    }

    public double focus = 10.0;
    public double minCoc = 1.0;
    public double maxCoc = 40.0;
    public double blurScale = 32.0;
    public double depthScale = 1.0;

    @Override
    public Filter apply(ColorBuffer[] src, ColorBuffer[] dest) {

        parameter("blurScale", (float)blurScale);

        parameter("focus", (float)focus);
        parameter("minCoc", (float)minCoc);
        parameter("maxCoc", (float)maxCoc);
        parameter("depthScale", (float)depthScale);
        super.apply(src, dest);
        return this;
    }
}
