#version 330

uniform sampler2D tex0;
uniform sampler2D tex1;
in vec2 v_texCoord0;
out vec4 o_color;
void main() {

    vec4 newDistance = texture(tex0, v_texCoord0);
    vec4 oldDistance = texture(tex1, v_texCoord0);


    vec4 result = oldDistance;
    if (newDistance.r < oldDistance.r) {

        result = newDistance;

    }

    o_color = result;



}