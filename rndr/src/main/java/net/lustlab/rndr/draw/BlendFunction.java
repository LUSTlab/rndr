package net.lustlab.rndr.draw;

public enum BlendFunction {
    ADD,
    SUBSTRACT,
    REVERSE_SUBTRACT,
    MIN,
    MAX
}
