package net.lustlab.rndr.application;

/**
 * ProgramDelegate
 */
public interface ProgramDelegate {

    /**
     * Called when the ProgramDelegate is attached to the Program using Program.delegate(ProgramDelegate)
     * @param program
     */
    void setup(Program program);

    /**
     * Called before Program.draw() is called
     * @param program
     */
    void beforeDraw(Program program);

    /**
     * Called after Program.draw() is called
     * @param program
     */
    void afterDraw(Program program);

    void resize(Program program, int width, int height);
}
