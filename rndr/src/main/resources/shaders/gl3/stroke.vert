#version 330

uniform mat4 modelViewMatrix;
uniform mat4 projectionMatrix;

in vec2 position;
in vec2 texCoord0;
in float offset;
out vec2 ftcoord;
out vec2 fpos;
out float v_offset;
void main() {
    v_offset = offset;
    ftcoord = texCoord0;
    fpos = position;
    gl_Position = projectionMatrix * modelViewMatrix * vec4(position,0,1);
}