package net.lustlab.rndr.buffers;

import net.lustlab.colorlib.ColorRGBa;
import net.lustlab.colorlib.ColorSRGB;
import net.lustlab.rndr.RNDRException;

//import sun.misc.Cleaner;
//import sun.nio.ch.DirectBuffer;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;

public class ColorBufferShadow {

    ByteBuffer buffer;
    final ColorBuffer colorBuffer;

    final int elementSize;
    final int width;
    final int height;

    public ByteBuffer buffer() {
        return buffer;
    }

    public int width() {
        return colorBuffer().width();
    }

    public int height() {
        return colorBuffer().height();
    }

    public ColorBufferShadow(ColorBuffer cb) {
        width = cb.width;
        height = cb.height;
        colorBuffer = cb;
        elementSize = cb.type.componentSizeInBytes() * cb.format.componentCount();

        if (cb.format == ColorFormat.sRGBA && cb.type == ColorType.UINT8) {
            buffer = ByteBuffer.allocateDirect(cb.width * cb.height * elementSize);
            buffer.order(ByteOrder.nativeOrder());
        } else if (cb.format == ColorFormat.RGBA && cb.type == ColorType.UINT8) {
            buffer = ByteBuffer.allocateDirect(cb.width * cb.height * elementSize);
            buffer.order(ByteOrder.nativeOrder());
        } else if (cb.format == ColorFormat.RGB && cb.type == ColorType.UINT8) {
            buffer = ByteBuffer.allocateDirect(cb.width * cb.height * elementSize);
            buffer.order(ByteOrder.nativeOrder());
        } else if (cb.format == ColorFormat.RGB && cb.type == ColorType.FLOAT32) {
            buffer = ByteBuffer.allocateDirect(cb.width * cb.height * elementSize);
            buffer.order(ByteOrder.nativeOrder());
        } else if (cb.format == ColorFormat.RGBA && cb.type == ColorType.FLOAT32) {
            buffer = ByteBuffer.allocateDirect(cb.width * cb.height * elementSize);
            buffer.order(ByteOrder.nativeOrder());
        } else {
            throw new RNDRException(this.getClass().getSimpleName() + " only supports (RGBA,RGB)/UINT8 color buffers " + cb.type + " " + cb.format);
        }

    }

    public ColorBuffer colorBuffer() {
        return colorBuffer;
    }

    public BufferWriter writer() {
        return new BufferWriter(buffer);
    }

    public ColorBufferShadow upload() {
        buffer.rewind();
        colorBuffer.write(buffer, colorBuffer.type);
        return this;
    }


    /**
     * Downloads the contents of the backing ColorBuffer into the shadow
     * @return
     */
    public ColorBufferShadow download() {
        buffer.rewind();
        colorBuffer.read(buffer, colorBuffer.type);
        return this;
    }

    /**
     * reads from shadow
     *
     * @param x
     * @param y
     * @return
     */

    public int readInt(int x, int y) {
        if (x < 0)
            x = 0;
        if (y < 0)
            y = 0;
        if (x >= colorBuffer.width) {
            x = colorBuffer.width - 1;
        }
        if (y >= colorBuffer.height) {
            y = colorBuffer.height - 1;
        }
        //final int offset = colorBuffer.upsideDown ? (y * colorBuffer.width + x) * elementSize : ((colorBuffer.height-1-y) * colorBuffer.width + x) * elementSize;
        final int offset = colorBuffer.width * y + x;
        return buffer.getInt(offset);
    }

    public ColorRGBa read(int x, int y) {
        if (x < 0)
            x = 0;
        if (y < 0)
            y = 0;
        if (x >= colorBuffer.width) {
            x = colorBuffer.width - 1;
        }
        if (y >= colorBuffer.height) {
            y = colorBuffer.height - 1;
        }

        if (colorBuffer.type == ColorType.UINT8) {
            final int offset = colorBuffer.upsideDown ? (y * colorBuffer.width + x) * elementSize : ((colorBuffer.height - 1 - y) * colorBuffer.width + x) * elementSize;

            int r = buffer.get(offset) & 0xff;
            int g = buffer.get(offset + 1) & 0xff;
            int b = buffer.get(offset + 2) & 0xff;
            int a = 255;

            if (colorBuffer.format == ColorFormat.RGBA) {
                a = buffer.get(offset + 3) & 0xff;
            }
            return new ColorRGBa(
                    (r) / 255.0,
                    ((g) & 0xff) / 255.0,
                    ((b) & 0xff) / 255.0,
                    ((a) & 0xff) / 255.0);
        } else if (colorBuffer.type == ColorType.FLOAT32) {
            final int offset = colorBuffer.upsideDown ? (y * colorBuffer.width + x) * elementSize : ((colorBuffer.height - 1 - y) * colorBuffer.width + x) * elementSize;

            float r = buffer.getFloat(offset);
            float g = buffer.getFloat(offset + 4);
            float b = buffer.getFloat(offset + 8);
            float a = 1.0f;

            if (colorBuffer.format == ColorFormat.RGBA) {
                a = buffer.getFloat(offset + 12);
            }
            return new ColorRGBa(r,g,b,a);
        } else {
            throw new RuntimeException("unsupported type");
        }
    }

    public ColorSRGB readSRGB(int x, int y) {
        if (x < 0)
            x = 0;
        if (y < 0)
            y = 0;
        if (x >= colorBuffer.width) {
            x = colorBuffer.width - 1;
        }
        if (y >= colorBuffer.height) {
            y = colorBuffer.height - 1;
        }
        final int offset = colorBuffer.upsideDown ? (y * colorBuffer.width + x) * elementSize : ((colorBuffer.height - 1 - y) * colorBuffer.width + x) * elementSize;

        int r = buffer.get(offset) & 0xff;
        int g = buffer.get(offset + 1) & 0xff;
        int b = buffer.get(offset + 2) & 0xff;

        return new ColorSRGB(
                (r) / 255.0,
                ((g) & 0xff) / 255.0,
                ((b) & 0xff) / 255.0);
    }

    public ColorBufferShadow write(int x, int y, ColorRGBa color) {

        if (x < 0 || y < 0 || x > width - 1 || y > height - 1) {
            throw new RuntimeException("write out of bounds:" + x + ", " + y + "[" + width + "x" + height + "]");
        }



        if (colorBuffer.type == ColorType.UINT8) {
            final int offset = colorBuffer.upsideDown ? (y * colorBuffer.width + x) * colorBuffer.format().componentCount() : ((colorBuffer.height - 1 - y) * colorBuffer.width + x) * colorBuffer.format().componentCount();

            int r = (int) (color.r * 255);
            int g = (int) (color.g * 255);
            int b = (int) (color.b * 255);
            int a = (int) (color.a * 255);

            int c = r + (g << 8) + (b << 16) + (a << 25);
            if (colorBuffer.format == ColorFormat.RGBA) {
                buffer.putInt(offset, c);
            } else if (colorBuffer.format == ColorFormat.RGB) {
                buffer.put(offset, (byte) (r & 0xff));
                buffer.put(offset + 1, (byte) (g & 0xff));
                buffer.put(offset + 2, (byte) (b & 0xff));
            }
        } else if (colorBuffer.type == ColorType.FLOAT32) {
            final int offset = colorBuffer.upsideDown
                    ? (y * colorBuffer.width + x) * 16
                    : ((colorBuffer.height - 1 - y) * colorBuffer.width + x) * 16;

            float r = (float) (color.r);
            float g = (float) (color.g);
            float b = (float) (color.b);
            float a = (float) (color.a);

            buffer.putFloat(offset, r);
            buffer.putFloat(offset + 4, g);
            buffer.putFloat(offset + 8, b);
            buffer.putFloat(offset + 12, a);
        }
        return this;
    }

    public ColorBufferShadow destroy() {

        buffer = null;
        return this;
    }
}
