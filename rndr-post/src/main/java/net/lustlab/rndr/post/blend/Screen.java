package net.lustlab.rndr.post.blend;

import net.lustlab.rndr.shaders.FragmentShader;

public class Screen extends BlendFilter {
    public Screen() {
        super(FragmentShader.fromUrl("cp:shaders/gl3/post/blend/screen.frag"));
    }
}
