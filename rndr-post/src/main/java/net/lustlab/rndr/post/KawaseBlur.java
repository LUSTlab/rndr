package net.lustlab.rndr.post;

import com.jogamp.opengl.GL;
import net.lustlab.rndr.buffers.ColorBuffer;
import net.lustlab.rndr.buffers.MagnifyingFilter;
import net.lustlab.rndr.buffers.MinifyingFilter;
import net.lustlab.rndr.shaders.FragmentShader;

import static net.lustlab.rndr.math.Linear.vector2;

/**
 * Approximate but fast Gaussian blur
 */
public class KawaseBlur extends Filter {

    ColorBuffer[] intermediate;

    int passes = 5;

    double spread = 1.0;
    public KawaseBlur() {
        super(FragmentShader.fromUrl("cp:shaders/gl3/post/kawase-blur.frag"));
        window(5);
        sigma(3);
        spread(1.0);
    }

    public KawaseBlur window(int window) {
//        parameter("window", window);
        return this;
    }
    public KawaseBlur spread(double spread) {
        this.spread = spread;
        parameter("spread", (float) spread);
        return this;
    }

    public KawaseBlur sigma(double sigma) {
//        parameter("sigma", (float) sigma);
        return this;
    }

    @Override
    public Filter apply(ColorBuffer[] src, ColorBuffer[] dest) {

        if (intermediate != null && (intermediate[0].width() != dest[0].width() || intermediate[0].height() != dest[0].height())) {
            intermediate[0].destroy();
            intermediate[1].destroy();
            intermediate = null;
        }

        if (intermediate == null) {
            intermediate = new ColorBuffer[2];
            intermediate[0] = ColorBuffer.create(dest[0].width(), dest[0].height(), dest[0].format(), dest[0].type());
            intermediate[1] = ColorBuffer.create(dest[0].width(), dest[0].height(), dest[0].format(), dest[0].type());
        }

        dest[0].filter(MinifyingFilter.LINEAR, MagnifyingFilter.LINEAR);
        intermediate[0].filter(MinifyingFilter.LINEAR, MagnifyingFilter.LINEAR);
        intermediate[1].filter(MinifyingFilter.LINEAR, MagnifyingFilter.LINEAR);


        ColorBuffer targets[] = new ColorBuffer[2];


        targets[0] = intermediate[0];
        targets[1] = intermediate[1];


        for (int i = 0; i < passes; ++i) {

            parameter("iteration", i);

            int t0 = i % 2;
            int t1 = (i+1) % 2;
            if (i ==0) {
                super.apply(src, new ColorBuffer[]{targets[t1]});
            }
            else if (i != passes -1) {
                super.apply(new ColorBuffer[]{targets[t0]}, new ColorBuffer[]{targets[t1]});
            }
            else if (i == passes - 1) {
                super.apply(new ColorBuffer[]{targets[t0]}, new ColorBuffer[]{dest[0]});
            }

        }


        return this;
    }

    public KawaseBlur passes(int i) {
        this.passes = i;
        return this;
    }
}
