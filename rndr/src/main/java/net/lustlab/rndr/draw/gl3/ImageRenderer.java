package net.lustlab.rndr.draw.gl3;

import com.jogamp.opengl.GL;
import com.jogamp.opengl.GL3;
import com.jogamp.opengl.GLContext;
import net.lustlab.colorlib.ColorRGBa;
import net.lustlab.rndr.buffers.*;
import net.lustlab.rndr.draw.DrawContext;
import net.lustlab.rndr.draw.DrawStyle;
import net.lustlab.rndr.math.Vector2;
import net.lustlab.rndr.math.Vector3;
import net.lustlab.rndr.math.Vector4;
import net.lustlab.rndr.shaders.ShadeStyleManager;
import net.lustlab.rndr.shaders.Shader;

import java.nio.ByteBuffer;

public class ImageRenderer {

    ShadeStyleManager shaderManager;

    ByteBuffer buffer;
    VertexBuffer vertices;

    public ImageRenderer() {
        VertexFormat vertexFormat = new VertexFormat();
        vertexFormat.textureCoordinate(2);
        vertexFormat.position(3);

        buffer = ByteBuffer.allocate(vertexFormat.size() * 6);
        vertices = VertexBuffer.createDynamic(vertexFormat, 6);

        shaderManager = new ShadeStyleManager("cp:shaders/gl3/texture.vert", "cp:shaders/gl3/texture.frag");

    }

    public void drawImage(
            DrawContext context,
            DrawStyle drawStyle,
            ColorBuffer colorBuffer,
            double sourceX, double sourceY, double sourceWidth, double sourceHeight,
            double destX, double destY, double destWidth, double destHeight
    ) {


        BufferWriter bw = new BufferWriter(buffer);
        GL gl = GLContext.getCurrentGL();
        double u0 = sourceX / (colorBuffer.width()-0);
        double u1 = (sourceX+sourceWidth) / (colorBuffer.width()-0);
        double v0 = sourceY / (colorBuffer.height()-0);
        double v1 = (sourceY + sourceHeight) / (colorBuffer.height()-0);

        if (!colorBuffer.upsideDown()){
            v0 = 1.0 - v0;
            v1 = 1.0 - v1;
        }

        bw.write(new Vector2(u0, v0)).write(new Vector3(destX, destY, 0));
        bw.write(new Vector2(u1, v0)).write(new Vector3(destX + destWidth, destY, 0));
        bw.write(new Vector2(u1, v1)).write(new Vector3(destX + destWidth, destY + destHeight, 0));

        bw.write(new Vector2(u0, v0)).write(new Vector3(destX, destY, 0));
        bw.write(new Vector2(u0, v1)).write(new Vector3(destX , destY + destHeight,  0));
        bw.write(new Vector2(u1, v1)).write(new Vector3(destX + destWidth, destY + destHeight, 0));

        buffer.rewind();

        vertices.write(buffer, 0, buffer.capacity());

        Shader shader = shaderManager.shader(drawStyle.shadeStyle, vertices.format());

        gl.glActiveTexture(GL.GL_TEXTURE0);
        colorBuffer.bindTexture();
        shader.begin();
        shader.setUniform("bounds", new Vector4(destX, destY, destWidth, destHeight));
        shader.setUniform1i("image", 0);

        ShaderTools.setupShader(shader, context, drawStyle);
        shaderManager.setParameters(shader, drawStyle.shadeStyle);
        StateTools.setupState(drawStyle);

        vertices.bind();
        VertexBufferDrawer.draw(gl, shader, vertices, vertices.format(), GL3.GL_TRIANGLES, 6);
        vertices.unbind();

        shader.end();
        gl.glBindTexture(GL3.GL_TEXTURE_2D, 0);
    }

    public void texturedQuad(
            DrawContext context,
            DrawStyle style,
            ColorBuffer colorBuffer,
            Vector3 posTL,
            Vector2 texTL,
            Vector3 posTR,
            Vector2 texTR,
            Vector3 posBR,
            Vector2 texBR,
            Vector3 posBL,
            Vector2 texBL
    ) {

        GL gl = GLContext.getCurrentGL();

        BufferWriter bw = new BufferWriter(buffer);
        buffer.rewind();
        bw.write(texTL).write(ColorRGBa.WHITE).write(posTL);
        bw.write(texTR).write(ColorRGBa.WHITE).write(posTR);
        bw.write(texBR).write(ColorRGBa.WHITE).write(posBR);

        bw.write(texTL).write(ColorRGBa.WHITE).write(posTL);
        bw.write(texBL).write(ColorRGBa.WHITE).write(posBL);
        bw.write(texBR).write(ColorRGBa.WHITE).write(posBR);

        buffer.rewind();
        vertices.write(buffer, 0, buffer.capacity());

        gl.glActiveTexture(GL.GL_TEXTURE0);
        colorBuffer.bindTexture(gl);

        Shader shader = shaderManager.shader(style.shadeStyle, vertices.format());

        shader.begin();
        ShaderTools.setupShader(shader, context, style);
        shaderManager.setParameters(shader, style.shadeStyle);

        vertices.bind();
        VertexBufferDrawer.draw(gl, shader, vertices, vertices.format(), GL3.GL_TRIANGLES, 6);
        vertices.unbind();

        shader.end();
        gl.glBindTexture(GL.GL_TEXTURE_2D, 0);
    }

    public void destroy() {
        shaderManager.destroy();
        vertices.destroy();
    }
}
