package net.lustlab.rndr.draw;

import com.jogamp.opengl.GL;
import com.jogamp.opengl.GL2GL3;
import com.jogamp.opengl.glu.GLU;
import com.jogamp.opengl.glu.GLUtessellator;
import com.jogamp.opengl.glu.GLUtessellatorCallbackAdapter;
import net.lustlab.colorlib.ColorRGBa;
import net.lustlab.rndr.math.Vector3;
import net.lustlab.rndr.shaders.Shader;
import net.lustlab.rndr.buffers.BufferWriter;
import net.lustlab.rndr.buffers.VertexArray;
import net.lustlab.rndr.buffers.VertexBuffer;
import net.lustlab.rndr.buffers.VertexFormat;
import net.lustlab.rndr.shape.*;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.Serializable;
import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class Tessellation implements Serializable {

    static Logger logger = LogManager.getLogger(Tessellation.class);
    public HashMap<Polygon, Integer> polygons;
    public List<PrimitiveData> data = new ArrayList<>();

    public static class PrimitiveData implements Serializable {

        public static VertexFormat layout = new VertexFormat().color(4).position(3);

        public enum ColorSource {
            Fill,
            Stroke
        }

        public int command;
        public ColorSource colorSource;
        public Polygon polygon;
        public List<Vertex> vertices = new ArrayList<>();
        public VertexBuffer vbo;
        public VertexArray vao;

        public void createVBO(GL gl, Shader shader) {

            if (vbo == null) {
                ByteBuffer buffer = ByteBuffer.allocateDirect(layout.size() * vertices.size());

                BufferWriter bw = new BufferWriter(buffer);

                for (Vertex vertex : vertices) {
                    bw.write(ColorRGBa.WHITE);
                    bw.write(vertex.position);
                }
                buffer.rewind();

                vbo = VertexBuffer.createStatic(layout, vertices.size(), buffer);
                vao = VertexArray.create(gl.getGL3(), vbo, layout, shader);
            }
            else {
                throw new RuntimeException("already has vbo");
            }
        }

    }

    public void outputEdge(PolygonContour contour, Polygon polygon) {

        PrimitiveData pd = new PrimitiveData();
        pd.polygon = polygon;
        pd.colorSource = PrimitiveData.ColorSource.Stroke;

        pd.command = GL2GL3.GL_LINE_STRIP;

        for (Vertex vertex: contour.vertices) {
            pd.vertices.add(vertex);
        }
        data.add(pd);

    }
    public static Tessellation tessellate(Figure figure) {

        GLU glu = new GLU();
        GLUtessellator tessellator = GLU.gluNewTess();

        Tessellation tessellation = new Tessellation();
        TesselCallBack callback = new TesselCallBack(glu, tessellation);
        GLU.gluTessCallback(tessellator, GLU.GLU_TESS_VERTEX, callback);
        GLU.gluTessCallback(tessellator, GLU.GLU_TESS_BEGIN_DATA, callback);
        GLU.gluTessCallback(tessellator, GLU.GLU_TESS_END, callback);
        GLU.gluTessCallback(tessellator, GLU.GLU_TESS_ERROR, callback);
        GLU.gluTessCallback(tessellator, GLU.GLU_TESS_COMBINE, callback);
        GLU.gluTessCallback(tessellator, GLU.GLU_TESS_EDGE_FLAG_DATA, callback);

        for (Polygon polygon: figure) {
            processPolygon(tessellator, tessellation, callback, polygon);
        }

        GLU.gluDeleteTess(tessellator);


        return tessellation;
    }

    private static void processPolygon(GLUtessellator tessellator, Tessellation tessellation, TesselCallBack callback, Polygon polygon) {
        if (polygon.contoursClosed()) {
            GLU.gluTessProperty(tessellator, GLU.GLU_TESS_BOUNDARY_ONLY, GLU.GLU_FALSE);
            callback.mode = TesselCallBack.TessellatorMode.Fill;
            tessellatePolygon(tessellator, polygon);

            GLU.gluTessProperty(tessellator, GLU.GLU_TESS_BOUNDARY_ONLY, GLU.GLU_TRUE);
            callback.mode = TesselCallBack.TessellatorMode.Fill;
            tessellatePolygon(tessellator, polygon);

            GLU.gluTessProperty(tessellator, GLU.GLU_TESS_BOUNDARY_ONLY, GLU.GLU_TRUE);
            callback.mode = TesselCallBack.TessellatorMode.Stroke;
            tessellatePolygon(tessellator, polygon);

//            for (PolygonContour contour: polygon) {
//                tessellation.outputEdge(contour, polygon);
//            }
        }
        else {
            //logger.warn("generating stroke data");
//            System.out.println("generating stroke data");
           // if (polygon.stroke != null) {
                for (Contour contour: polygon) {
                    tessellation.outputEdge((PolygonContour)contour, polygon);
                }
           // }
        }
    }

    public static Tessellation tessellate(Polygon polygon) {

        GLU glu = new GLU();
        GLUtessellator tessellator = GLU.gluNewTess();

        Tessellation tessellation = new Tessellation();
        TesselCallBack callback = new TesselCallBack(glu, tessellation);
        GLU.gluTessCallback(tessellator, GLU.GLU_TESS_VERTEX, callback);
        GLU.gluTessCallback(tessellator, GLU.GLU_TESS_BEGIN_DATA, callback);
        GLU.gluTessCallback(tessellator, GLU.GLU_TESS_END, callback);
        GLU.gluTessCallback(tessellator, GLU.GLU_TESS_ERROR, callback);
        GLU.gluTessCallback(tessellator, GLU.GLU_TESS_COMBINE, callback);
        GLU.gluTessCallback(tessellator, GLU.GLU_TESS_EDGE_FLAG_DATA, callback);

        processPolygon(tessellator, tessellation, callback, polygon);

        GLU.gluDeleteTess(tessellator);

        return tessellation;
    }

    private static void tessellatePolygon(GLUtessellator tessellator, Polygon polygon) {
        GLU.gluTessBeginPolygon(tessellator, polygon);
        for (Contour polygonContour : polygon) {
            GLU.gluTessBeginContour(tessellator);
            for (Vertex vertex: ((PolygonContour)polygonContour).vertices) {

                Vector3 t = vertex.position;//  vertex.position.multiply(polygon.transform.matrix);
                GLU.gluTessVertex(tessellator, t.array(), 0, t.array());
            }
            GLU.gluTessEndContour(tessellator);
        }
        GLU.gluTessEndPolygon(tessellator);
    }

    static class TesselCallBack
            extends GLUtessellatorCallbackAdapter
    {
        private GLU glu;
        Tessellation shape;
        boolean edge;

        enum TessellatorMode {
            Fill,
            Stroke
        }

        TessellatorMode mode = TessellatorMode.Fill;

        public TesselCallBack(GLU glu, Tessellation shape) {

            this.glu = glu;
            this.shape = shape;
        }

        public void beginData(int type, Object polygonData) {
            edge = false;

            Polygon polygon = (Polygon) polygonData;
            PrimitiveData pd = new PrimitiveData();
            pd.command = type;
            pd.polygon = polygon;
            if (mode == TessellatorMode.Fill) {
                pd.colorSource = PrimitiveData.ColorSource.Fill;
            }
            else if (mode == TessellatorMode.Stroke) {
                pd.colorSource = PrimitiveData.ColorSource.Stroke;
            }
            shape.data.add(pd);
        }


        public void end() {
        }

        public void vertex(Object vertexData) {
            if (vertexData != null) {
                double[] pointer;
                pointer = (double[]) vertexData;
                Vertex vertex = new Vertex().position(pointer[0], pointer[1], pointer[2]).edge(edge);
                shape.data.get(shape.data.size()-1).vertices.add(vertex);
            }

        }

        @Override
        public void edgeFlagData(boolean b, Object o) {
            edge = b;
        }

        public void combine(double[] coords, Object[] data, //
                            float[] weight, Object[] outData)	{
            double[] vertex = new double[6];

            vertex[0] = coords[0];
            vertex[1] = coords[1];
            vertex[2] = coords[2];
            outData[0] = vertex;
        }

        public void combineData(double[] coords, Object[] data, //
                                float[] weight, Object[] outData, Object polygonData)
        {
        }

        public void error(int errnum) {
            String estring;
            estring = glu.gluErrorString(errnum);
            logger.error("tessellation error " + estring);
            System.exit(0);
        }
    }
}
