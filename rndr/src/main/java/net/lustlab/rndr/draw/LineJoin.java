package net.lustlab.rndr.draw;

public enum LineJoin {
    MITER,
    BEVEL,
    ROUND
}
