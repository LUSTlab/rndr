#version 150

in vec2 v_texCoord0;
uniform sampler2D tex0;
uniform vec2 blurDirection;

uniform int window;
uniform float sigma;
uniform float amplitude;
uniform vec4 subtract;
out vec4 o_color;
void main() {

    vec2 s = textureSize(tex0, 0).xy;
    s = vec2(1.0/s.x, 1.0/s.y);

    int w = window;

    vec4 sum = vec4(0.0);
    float weight = 0;
    for (int x = -w; x<= w; ++x) {
        float lw = exp(-(x*x) / (2 * sigma * sigma));
        vec2 tc = v_texCoord0 + x * blurDirection * s;

        // -- u wrap around for cylinder projection
        sum += max(texture(tex0, tc)-subtract,vec4(0)) * lw;
        weight+=lw;
    }

    o_color = (sum / weight) * amplitude;
    o_color.a = 1.0;

}