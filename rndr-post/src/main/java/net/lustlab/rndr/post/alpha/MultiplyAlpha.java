package net.lustlab.rndr.post.alpha;

import net.lustlab.rndr.post.Filter;
import net.lustlab.rndr.shaders.FragmentShader;

public class MultiplyAlpha extends Filter {
    public MultiplyAlpha() {
        super(FragmentShader.fromUrl("cp:shaders/gl3/post/alpha/multiply.frag"));
    }
}
