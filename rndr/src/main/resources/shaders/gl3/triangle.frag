#version 330

uniform vec4 fill;
uniform vec4 stroke;

uniform vec2 dimensions;
uniform float strokeWeight;

out vec4 o_color;
void main(void) {
    o_color = vec4(fill);
}