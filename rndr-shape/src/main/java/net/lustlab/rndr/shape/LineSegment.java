package net.lustlab.rndr.shape;

import net.lustlab.rndr.math.Vector2;


public class LineSegment {

    public final Vector2 start;
    public final Vector2 end;

    public LineSegment(Vector2 start, Vector2 end) {
        this.start = start;
        this.end = end;
    }

    // positive is on the left
    public double side(Vector2 query) {
        Vector2 a = start;
        Vector2 b = end;
        Vector2 c = query;
        double d = ((b.x - a.x)*(c.y - a.y) - (b.y - a.y)*(c.x - a.x));
        if (d == 0.0) {
            return 0.0;
        } else if (d > 0.0) {
            return -1.0;
        } else {
            return 1.0;
        }
    }

    public Vector2 direction() {
        return end.minus(start);
    }

    public Vector2 normal() {
        return direction().normalized().perpendicular();
    }


    public Vector2 nearest(Vector2 query) {
        double l2 = end.minus(start).length();
        l2*=l2;
        if (l2 == 0) return start;

        double t = ((query.x - start.x) * (end.x - start.x) + (query.y - start.y) * (end.y - start.y)) / l2;
        t = Math.max(0, Math.min(1, t));
        return  new Vector2( start.x + t * (end.x - start.x),
                start.y + t * (end.y - start.y));
    }

    public double distance(Vector2 query) {
        double l2 = end.minus(start).length();
        l2*=l2;
        if (l2 == 0) return  (query.minus(start)).length();

        double t = ((query.x - start.x) * (end.x - start.x) + (query.y - start.y) * (end.y - start.y)) / l2;
        t = Math.max(0, Math.min(1, t));
        return query.minus( new Vector2( start.x + t * (end.x - start.x),
                            start.y + t * (end.y - start.y) ) ).length();
    }

}
