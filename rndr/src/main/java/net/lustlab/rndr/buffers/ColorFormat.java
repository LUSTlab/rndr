package net.lustlab.rndr.buffers;

import com.jogamp.opengl.GL;
import com.jogamp.opengl.GL3;
import net.lustlab.rndr.RNDRException;

public enum ColorFormat {
    R,
    RG,
    RGB,
    RGBA,
    sRGB,
    sRGBA;

    /**
     * Returns the number of color components in the format
     * @return the number of color components in the format.
     */
    public final int componentCount() {
        switch(this) {
            case R:
                return 1;
            case RG:return 2;
            case RGB:return 3;
            case RGBA: return 4;

            case sRGB: return 3;
            case sRGBA: return 4;
            default:
                throw new RNDRException("unsupported format");
        }

    }

    public final int toGLFormat() {
        switch(this) {

            case R:
                return GL3.GL_RED;
            case RG:
                return GL3.GL_RG;
            case RGB:
                return GL3.GL_RGB;
            case RGBA:
                return GL3.GL_RGBA;
            case sRGB:
                return GL3.GL_RGB;
            case sRGBA:
                return GL3.GL_RGBA;
            default:
                throw new RNDRException("unsupported format");
        }
    }

    public static ColorFormat fromGLFormat(int format) {

        switch (format) {
            case GL.GL_RGBA8:
                return RGBA;
            case GL.GL_RGB8:
                return RGB;
            case GL.GL_RG8:
                return RGB;
            case GL.GL_R8:
                return R;
            case GL3.GL_R16:
                return R;

            case GL.GL_RGBA16F:
                return RGBA;
            case GL.GL_RGB16F:
                return RGB;
            case GL.GL_RG16F:
                return RGB;
            case GL.GL_R16F:
                return R;
            case GL.GL_RGBA32F:
                return RGBA;
            case GL.GL_RGB32F:
                return RGB;
            case GL.GL_RG32F:
                return RG;
            case GL.GL_R32F:
                return R;

            case GL.GL_RGBA:
                return RGBA;

            case GL.GL_RGB:
                return RGB;
            case GL.GL_SRGB:
                return sRGB;
            case GL3.GL_SRGB8:
                return sRGB;

            case GL.GL_SRGB_ALPHA:
                return sRGBA;
            case GL.GL_SRGB8_ALPHA8:
                return sRGBA;

            default:
                throw new RNDRException("unknown format: " + format);
        }
    }


}
