package net.lustlab.rndr.svg;


import net.lustlab.colorlib.ColorRGBa;
import net.lustlab.rndr.math.Matrix44;
import net.lustlab.rndr.math.Vector2;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.parser.Parser;

import java.io.*;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * SVGLoader class
 */
public class SVGLoader {

    static Logger logger = LogManager.getLogger(SVGLoader.class.getName());

    /**
     * Loads the svg from the specified filename
     *
     * @param filename
     * @return an SVGDocument instance
     * @throws IOException
     */
    public static SVGDocument loadSVG(String urlString, String filename) throws IOException {
        File f = new File(filename);
        FileInputStream fis = new FileInputStream(f);
        return loadSVG(urlString, fis);
    }

    public static SVGDocument loadSVG(String urlString, URL url) throws IOException {
        return loadSVG(urlString, url.openStream());
    }

    public static void handlePolygon(Group group, SVGDocument doc, Element e) {
        String[] tokens = e.attr("points").split("[ ,\n]+");

        List<String> nonEmptyTokens = new ArrayList<>();
        for (String token:tokens) {
            String clean = token.trim();
            if (clean.length() > 0) {
                nonEmptyTokens.add(token);
            }
        }
        tokens = nonEmptyTokens.toArray(new String[nonEmptyTokens.size()]);

        List<Vector2> points = new ArrayList<Vector2>();

        for (int i = 0; i < tokens.length / 2; ++i) {
            points.add(new Vector2(new Double(tokens[i * 2].trim()), new Double(tokens[i * 2 + 1].trim())));
        }
        Path path = new Path();
        path.id = e.id();
        parseDrawAttributes(e, path);
        path.commands.add(new Command('M', points.get(0).x, points.get(0).y));
        for (int i = 1; i < points.size(); ++i) {
            path.commands.add(new Command('L', points.get(i).x, points.get(i).y));
        }
        path.commands.add(new Command('z'));
        group.elements.add(path);
    }

    public static void handleRectangle(Group group, SVGDocument doc, Element e) {

        if (e.attr("id").equals("background")) {
            return;
        }

        Double x = e.attr("x").length() == 0 ? 0.0 : new Double(e.attr("x"));
        Double y = e.attr("y").length() == 0 ? 0.0 : new Double(e.attr("y"));
        Double width = new Double(e.attr("width"));
        Double height = new Double(e.attr("height"));

        Path path = new Path();
        path.id = e.id();
        parseDrawAttributes(e, path);
        parseTransform(e, path);
        path.commands.add(new Command('M', x, y));
        path.commands.add(new Command('h', width));
        path.commands.add(new Command('v', height));
        path.commands.add(new Command('h', -width));
        path.commands.add(new Command('z'));

        group.elements.add(path);

    }


    public static void handleLine(Group group, SVGDocument doc, Element e) {
        Double x1 = new Double(e.attr("x1"));
        Double x2 = new Double(e.attr("x2"));
        Double y1 = new Double(e.attr("y1"));
        Double y2 = new Double(e.attr("y2"));


        Path path = new Path();
        path.id = e.id();

        parseDrawAttributes(e, path);

        Command c0 = new Command();
        c0.op = 'M';
        c0.operands.add(x1);
        c0.operands.add(y1);

        Command c1 = new Command();
        c1.op = 'L';
        c1.operands.add(x2);
        c1.operands.add(y2);

        path.commands.add(c0);
        path.commands.add(c1);

        group.elements.add(path);
    }

    public static void handlePath(Group group, SVGDocument doc, Element e) {

        Path path = Path.fromSVGPath(e.attr("d"));
        path.id = e.id();
        parseDrawAttributes(e, path);

        group.elements.add(path);
    }

    private static void parseTransform(Element e, net.lustlab.rndr.svg.Element target) {

        Pattern p = Pattern.compile("(matrix|translate|scale|rotate|skewX|skewY)\\(.+\\)");
        Matcher m = p.matcher(e.attr("transform"));


        while (m.find()) {
            String token = m.group();
            if (token.startsWith("matrix")) {
                List<Double> operands = getTransformOperands(token);
                Matrix44 mat = new Matrix44(
                        operands.get(0), operands.get(2), 0, operands.get(4),
                        operands.get(1), operands.get(3), 0, operands.get(5),
                        0, 0, 1, 0,
                        0, 0, 0, 1);
                target.transform = mat;

            }

        }

    }

    private static List<Double> getTransformOperands(String token) {
        Pattern number = Pattern.compile("-?[0-9\\.eE\\-]+");
        Matcher nm = number.matcher(token);
        List<Double> operands = new ArrayList<Double>();
        while (nm.find()) {
            Double n = new Double(nm.group());
            operands.add(n);
        }
        return operands;
    }

    private static void parseDrawAttributes(Element e, Path path) {
        if (e.attr("fill").length() == 0) {
            path.fill = new ColorRGBa(0, 0, 0, 1);
        } else {
            path.fill = parseColor(e.attr("fill"));
        }

        if (e.attr("stroke").length() == 0) {
            path.stroke = null;
        } else {
            path.stroke = parseColor(e.attr("stroke"));
        }

        if (e.attr("stroke-width").length() != 0) {
            path.strokeWidth = Double.parseDouble(e.attr("stroke-width"));
        } else {
            path.strokeWidth = 1.0;
        }
    }

    public static void handleGroup(Group parent, SVGDocument doc, Element e) {

        Group group = new Group();

        group.id = e.id();

        if (parent == null) {
            doc.groups.add(group);
        } else {
            parent.elements.add(group);
        }

        for (Element c : e.children()) {
            if (c.tagName().equals("g")) {
                handleGroup(group, doc, c);
            } else if (c.tagName().equals("path")) {
                handlePath(group, doc, c);
            } else if (c.tagName().equals("line")) {
                handleLine(group, doc, c);
            } else if (c.tagName().equals("polygon")) {
                handlePolygon(group, doc, c);
            } else if (c.tagName().equals("rect")) {
                handleRectangle(group, doc, c);
            } else if (c.tagName().equals("ellipse")) {
                handleEllipse(group, doc, c);
            } else if (c.tagName().equals("circle")) {
                handleCircle(group, doc, c);
            } else if (c.tagName().equals("polyline")) {
                handlePolyline(group, doc, c);
            } else if (c.tagName().equals("image")) {
                handleImage(group, doc, c);
            }
        }
    }

    public static ColorRGBa parseColor(String scolor) {

        if (scolor.equals("none")) {
            return null;
        } else if (scolor.startsWith("#")) {
            Long v = Long.decode(scolor);
            int vi = v.intValue();
            int r = (vi >> 16) & 0xff;
            int g = (vi >> 8) & 0xff;
            int b = (vi) & 0xff;
            ColorRGBa color = new ColorRGBa(r / 255.0, g / 255.0, b / 255.0, 1);
            return color;
        } else {
            throw new RuntimeException("could not parse color: " + scolor);
        }

    }

    public static SVGDocument loadSVG(String url, InputStream stream) throws IOException {


        SVGDocument svg = new SVGDocument();
        svg.url = url;
        Document doc = Jsoup.parse(stream, "UTF-8", "", Parser.xmlParser());
        Element root = doc.select("svg").first();


        for (Element e : root.children()) {
            if (e.tagName().equals("g")) {
                handleGroup(null, svg, e);
            }
            if (e.tagName().equals("path")) {
                Group g = new Group();
                svg.groups.add(g);

                handlePath(g, svg, e);
            }
            if (e.tagName().equals("line")) {
                Group g = new Group();
                svg.groups.add(g);
                handleLine(g, svg, e);
            }
            if (e.tagName().equals("polygon")) {
                Group g = new Group();
                svg.groups.add(g);
                handlePolygon(g, svg, e);
            }
            if (e.tagName().equals("rect")) {
                Group g = new Group();
                svg.groups.add(g);
                handleRectangle(g, svg, e);
            }
            if (e.tagName().equals("ellipse")) {
                Group g = new Group();
                svg.groups.add(g);
                handleEllipse(g, svg, e);
            }
            if (e.tagName().equals("polyline")) {
                Group g = new Group();
                svg.groups.add(g);
                handlePolyline(g, svg, e);
            }

            if (e.tagName().equals("image")) {
                Group g = new Group();
                svg.groups.add(g);
                handleImage(g, svg, e);
            }
        }
        return svg;
    }

    private static void handleImage(Group group, SVGDocument svg, Element e) {
        Image image = new Image();
        image.id = e.id();

        parseTransform(e, image);


        image.width = Double.parseDouble(e.attr("width"));
        image.height = Double.parseDouble(e.attr("height"));

        String base = svg.url;
        int last = base.lastIndexOf('/');
        if (last != -1) {
            base = base.substring(0, last + 1);
        } else {
            base = "file:";
        }

        image.url = base + "/" + e.attr("xlink:href");

        group.elements.add(image);

    }

    private static void handlePolyline(Group group, SVGDocument svg, Element e) {
        Path path = new Path();
        path.id = e.id();
        parseDrawAttributes(e, path);
        parseTransform(e, path);

        String tokens[] = e.attr("points").split("[ ,\n]+");

        List<String> nonEmptyTokens = new ArrayList<>();
        for (String token:tokens) {
            String clean = token.trim();
            if (clean.length() > 0) {
                nonEmptyTokens.add(token);
            }
        }
        tokens = nonEmptyTokens.toArray(new String[nonEmptyTokens.size()]);



        path.commands.add(new Command('M', new Double(tokens[0]), new Double(tokens[1])));
        for (int i = 1; i < tokens.length / 2; ++i) {
            path.commands.add(new Command('L', new Double(tokens[i * 2]), new Double(tokens[i * 2 + 1])));
        }

        // TODO figure out when to close this (add Z op)

        group.elements.add(path);

    }

    private static void handleEllipse(Group group, SVGDocument svg, Element e) {

        logger.debug("handling ellipse " + group + " " + e);
        Double x = e.attr("cx").length() == 0 ? 0.0 : new Double(e.attr("cx"));
        Double y = e.attr("cy").length() == 0 ? 0.0 : new Double(e.attr("cy"));
        Double width = new Double(e.attr("rx")) * 2;
        Double height = new Double(e.attr("ry")) * 2;
        x -= width / 2;
        y -= height / 2;

        double kappa = 0.5522848;
        double ox = (width / 2) * kappa, // control point offset horizontal
                oy = (height / 2) * kappa, // control point offset vertical
                xe = x + width,           // x-end
                ye = y + height,           // y-end
                xm = x + width / 2,       // x-middle
                ym = y + height / 2;       // y-middle

        Path path = new Path();
        path.id = e.id();
        parseDrawAttributes(e, path);
        parseTransform(e, path);
        path.commands.add(new Command('M', x, ym));
        path.commands.add(new Command('C', x, ym - oy, xm - ox, y, xm, y));
        path.commands.add(new Command('C', xm + ox, y, xe, ym - oy, xe, ym));
        path.commands.add(new Command('C', xe, ym + oy, xm + ox, ye, xm, ye));
        path.commands.add(new Command('C', xm - ox, ye, x, ym + oy, x, ym));
        path.commands.add(new Command('z'));
        group.elements.add(path);
    }

    private static void handleCircle(Group group, SVGDocument svg, Element e) {
        logger.trace("handling circle " + group + " " + e);
        Double x = e.attr("cx").length() == 0 ? 0.0 : new Double(e.attr("cx"));
        Double y = e.attr("cy").length() == 0 ? 0.0 : new Double(e.attr("cy"));
        Double width = new Double(e.attr("r")) * 2;
        Double height = new Double(e.attr("r")) * 2;
        x -= width / 2;
        y -= height / 2;

        double kappa = 0.5522848;
        double ox = (width / 2) * kappa, // control point offset horizontal
                oy = (height / 2) * kappa, // control point offset vertical
                xe = x + width,           // x-end
                ye = y + height,           // y-end
                xm = x + width / 2,       // x-middle
                ym = y + height / 2;       // y-middle

        Path path = new Path();
        path.id = e.id();
        parseDrawAttributes(e, path);
        parseTransform(e, path);
        path.commands.add(new Command('M', x, ym));
        path.commands.add(new Command('C', x, ym - oy, xm - ox, y, xm, y));
        path.commands.add(new Command('C', xm + ox, y, xe, ym - oy, xe, ym));
        path.commands.add(new Command('C', xe, ym + oy, xm + ox, ye, xm, ye));
        path.commands.add(new Command('C', xm - ox, ye, x, ym + oy, x, ym));
        path.commands.add(new Command('z'));
        group.elements.add(path);
    }

}
