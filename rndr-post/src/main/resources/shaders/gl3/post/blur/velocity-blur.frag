#version 150

in vec2 v_texCoord0;
uniform sampler2D tex0;
uniform sampler2D tex1;

uniform int window;
uniform float sigma;
uniform float amplitude;
uniform vec4 subtract;
out vec4 o_color;

float rand(vec2 co){
  return fract(sin(dot(co.xy ,vec2(12.9898,78.233))) * 43758.5453);
}
void main() {

    vec2 s = textureSize(tex0, 0).xy;
    s = vec2(1.0/s.x, 1.0/s.y);


    vec2 blurDirection = texture(tex1, v_texCoord0).xy;

    float blurMagnitude = length(blurDirection);

    int w = window;

    vec4 sum = vec4(0,0,0,0);
    float weight = 0;
    for (int x = -w; x<= w; ++x) {
        float lw = 1.0;

        vec2 r = 2.0 * vec2(rand(v_texCoord0+vec2(w,0.3)),rand(v_texCoord0+vec2(0.0,w))) - vec2(1,1);

        sum+= texture(tex0, v_texCoord0 + x * blurDirection * s);
        weight+=lw;
    }

    o_color = (sum / weight);// * amplitude;
    o_color.a = 1.0;

}