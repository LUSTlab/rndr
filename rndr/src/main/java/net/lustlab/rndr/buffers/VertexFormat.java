package net.lustlab.rndr.buffers;

import com.jogamp.opengl.GL;
import net.lustlab.colorlib.ColorRGBa;
import net.lustlab.rndr.math.Vector2;
import net.lustlab.rndr.math.Vector3;
import net.lustlab.rndr.math.Vector4;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;


/**
 * VertexBuffer Layout describes how data is organized in the VertexBuffer
 */
public class VertexFormat {



    List<VertexElement> items = new ArrayList<>();
    int vertexSize = 0;

    public List<VertexElement> items() {
        return items;
    }

    /**
     * Returns the size of the vertex
     * @return the size of the vertex in bytes
     */
    public int size() {
        return vertexSize;
    }

    /**
     * Appends a position component to the layout
     * @param dimensions
     */
    public VertexFormat position(int dimensions) {
        return attribute("position", dimensions, GL.GL_FLOAT);
    }

    /**
     * Appends a normal component to the layout
     * @param dimensions the number of dimensions of the normal vector
     */
    public VertexFormat normal(int dimensions) {
        return attribute("normal", dimensions, GL.GL_FLOAT);

    }

    /**
     * Appends a color attribute to the layout
     * @param dimensions
     */
    public VertexFormat color(int dimensions) {
        return attribute("color", dimensions, GL.GL_FLOAT);
    }

    public VertexFormat textureCoordinate(int index, int dimensions) {
        return attribute("texCoord"+index, dimensions, GL.GL_FLOAT);
    }

    /**
     * Adds a primary texture coordinate attribute to the layout
     * @param dimensions the dimension the texture coordinates
     */
    public VertexFormat textureCoordinate(int dimensions) {
        return textureCoordinate(0, dimensions);
    }

    static int size(int type) {
        switch(type) {
            case GL.GL_FLOAT:
                return 4;
            default:
                throw new RuntimeException("unsupported element type: " + type);
        }
    }


    public VertexFormat attribute(String name, int dimensions, VertexElementType type) {
        return attribute(name, dimensions, type.toGLType());
    }


    /**
     * Adds a custom attribute to the layout
     * @param name the name of the attribute
     * @param dimensions the dimensionality of the attribute
     * @param type the type of the attribute. GL_FLOAT
     * @return
     */


    public VertexFormat attribute(String name, int dimensions, int type) {
        VertexElement item = new VertexElement();
        item.attribute = name;
        item.size = dimensions * size(type);

        int offset = 0;
        for (VertexElement i: items) {
            offset += i.size;
        }

        item.offset = offset;
        item.type = type;
        item.count = dimensions;
        items.add(item);
        vertexSize += item.size;
        return this;
    }

    @Override
    public String toString() {
        return "VertexFormat{" +
                "items=" + items +
                ", vertexSize=" + vertexSize +
                '}';
    }
}
