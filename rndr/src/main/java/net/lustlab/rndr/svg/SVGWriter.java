package net.lustlab.rndr.svg;

import net.lustlab.rndr.math.Mapping;
import net.lustlab.rndr.math.Matrix44;
import net.lustlab.rndr.shape.*;

import java.io.*;

/**
 * Proof-of-concept quality SVG writer
 */
public class SVGWriter {

    /**
     * SVGWriter constructor
     * @param file the file to write the SVG data to
     * @param composition the composition to write SVG
     */
    public SVGWriter(File file, Composition composition) {
        try {
            BufferedWriter bw = new BufferedWriter(new FileWriter(file));
            PrintWriter pw = new PrintWriter(bw);

            pw.println("<?xml version=\"1.0\" encoding=\"utf-8\"?>");
            pw.println("<!DOCTYPE svg PUBLIC \"-//W3C//DTD SVG 1.1 Tiny//EN\" \"http://www.w3.org/Graphics/SVG/1.1/DTD/svg11-tiny.dtd\">");
            pw.println("<svg version=\"1.1\" baseProfile=\"tiny\" id=\"Layer_1\" xmlns=\"http://www.w3.org/2000/svg\" xmlns:xlink=\"http://www.w3.org/1999/xlink\"  x=\"0px\" y=\"0px\"\n" +
                    "\t width=\"2676px\" height=\"2048px\">");

            write(composition.root, pw);

            pw.println("</svg>");

            pw.close();
            bw.close();

        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    void write(CompositionNode node, PrintWriter pw) {

        boolean content = node.shape() != null || node.text().isPresent();

        if (node.shape() != null) {
            String path = shapeToPath(node.shape());

            node.shape().contours.set(0, (ShapeContour) node.shape().contours.get(0).sub(0.4, 0.6));

            int r = 0;
            int g = 0;
            int b = 0;

            if (node.shape().stroke != null) {
                r = (int) Mapping.clamp(node.shape().stroke.r * 255, 0, 255);
                g = (int) Mapping.clamp(node.shape().stroke.g * 255, 0, 255);
                b = (int) Mapping.clamp(node.shape().stroke.b * 255, 0, 255);
            }

            String strokeString = String.format("#%02X%02x%02x", r, g, b);

            int fr = 0;
            int fg = 0;
            int fb = 0;
            if (node.shape().fill != null) {
                fr = (int) Mapping.clamp(node.shape().fill.r * 255, 0, 255);
                fg = (int) Mapping.clamp(node.shape().fill.g * 255, 0, 255);
                fb = (int) Mapping.clamp(node.shape().fill.b * 255, 0, 255);
            }

            String fillString = String.format("#%02X%02x%02x", fr, fg, fb);
            if (node.shape().fill == null) {
                fillString = "none";
            }

            if (node.shape().stroke == null) {
                strokeString = "none";
            }

            String idString = node.id != null? "id=\"" + node.id + "\"" : "";

            String strokeWeight = ""+node.shape().strokeWeight;

            Rectangle bounds = node.shape().bounds();

            double opacity = 1.0;
            if (node.shape().fill != null) {
                opacity = node.shape().fill.a;
            }

            String pathElement = String.format("<path %s opacity=\"%f\" fill=\"" + fillString + "\" stroke=\"" + strokeString + "\" stroke-width=\"" + strokeWeight + "\"" +
                    "" +
                    "" +
                    " d=\"%s\"> " +

                    ((node.shape().getGenerator()!= null)? node.shape().getGenerator().generate(node.shape())
                    : "")

                    +

                    "</path> ", idString, opacity, path, Math.random()*0.2 + (bounds.center().y/ 2776.0));
            pw.println(pathElement);

        }

        if (node.text().isPresent()) {
            pw.println("<text font-size=\"8\">");
            // TODO: Escape the text here!
            pw.println(node.text().get());
            pw.println("</text>");
        }

        if (!content){
            if (node.localTransform == Matrix44.IDENTITY) {
                pw.println(String.format("<g>"));
            } else {
                pw.println(String.format("<g transform=\"%s\">", matrixToTransform(node.localTransform)));
            }
            for (CompositionNode child: node.children()) {
                write(child, pw);
            }
            pw.println("</g>");
        }
    }

    String matrixToTransform(Matrix44 matrix) {
        String transform = "matrix(";
        transform += matrix.m00 +"," + matrix.m01 + "," + matrix.m10 +",";
        transform += matrix.m11 +"," + matrix.m30 + "," + matrix.m31 +")";
        return transform;
    }

    String shapeToPath(Shape shape) {
        String path = "";
        for (Contour contour: shape) {
            ShapeContour sc = (ShapeContour) contour;

            boolean first = true;

            for (Segment segment: sc) {

                if (first) {
                    path += "M" + segment.start.x + "," + segment.start.y;
                }

                if (segment.control == null || segment.control.length == 0) {
                    path += "L" + segment.end.x + "," + segment.end.y;
                } else if (segment.control.length == 1) {
                    path += "C" + segment.control[0].x + "," + segment.control[0].y + "," + segment.end.x +"," + segment.end.y;
                } else if (segment.control.length == 2) {
                    path += "C" + segment.control[0].x + "," + segment.control[0].y + "," + segment.control[1].x + "," + segment.control[1].y + "," + segment.end.x +"," + segment.end.y;
                }
                first = false;
            }
            if (contour.closed()) {
                path +="Z ";
            }

        }
        return path;
    }

}
