package net.lustlab.rndr.draw.gl3;

import com.jogamp.opengl.GL3;
import com.jogamp.opengl.GLContext;
import net.lustlab.rndr.buffers.*;
import net.lustlab.rndr.draw.DrawContext;
import net.lustlab.rndr.draw.DrawStyle;
import net.lustlab.rndr.shape.Rectangle;
import net.lustlab.rndr.gl.GLError;
import net.lustlab.rndr.math.Vector2;
import net.lustlab.rndr.math.Vector3;
import net.lustlab.rndr.shaders.ShadeStyleManager;
import net.lustlab.rndr.shaders.Shader;

import java.nio.ByteBuffer;
import java.util.List;

public class RectangleRenderer {

    VertexBuffer rectangle;

    int rectangleCount = 1_000_000;
    ShadeStyleManager rectangleShaderManager;
    ShadeStyleManager rectangleShaderManagerNoSmooth;
    ShadeStyleManager rectanglesShaderManager;

    BufferTexture rectanglePositions;

    public RectangleRenderer() {

        VertexFormat format = new VertexFormat().textureCoordinate(2).position(3);

        rectanglePositions = BufferTexture.createStream(ColorFormat.RGBA, ColorType.FLOAT32, rectangleCount);
        rectangleShaderManager = new ShadeStyleManager("cp:shaders/gl3/rectangle.vert", "cp:shaders/gl3/rectangle.frag");
        rectanglesShaderManager = new ShadeStyleManager("cp:shaders/gl3/rectangles.vert", "cp:shaders/gl3/rectangles.frag");
        rectangleShaderManagerNoSmooth = new ShadeStyleManager("cp:shaders/gl3/rectangle-no-smooth.vert", "cp:shaders/gl3/rectangle-no-smooth.frag");

        {
            ByteBuffer buffer = ByteBuffer.allocateDirect(format.size() * 6);
            BufferWriter bw = new BufferWriter(buffer);

            Vector2 uv00 = new Vector2(0, 0);
            Vector2 uv01 = new Vector2(0, 1);
            Vector2 uv11 = new Vector2(1, 1);
            Vector2 uv10 = new Vector2(1, 0);
            Vector3 p00 = new Vector3(0, 0, 0);
            Vector3 p01 = new Vector3(0, 1, 0);
            Vector3 p10 = new Vector3(1, 0, 0);
            Vector3 p11 = new Vector3(1, 1, 0);

            bw.write(uv00).write(p00).write(uv01).write(p01).write(uv11).write(p11);
            bw.write(uv11).write(p11).write(uv10).write(p10).write(uv00).write(p00);

            buffer.rewind();

            rectangle = VertexBuffer.createStatic(format, 6, buffer);
        }


    }

    // -- rectangle --

    public void drawRectangle(DrawContext context, DrawStyle style, Vector3 position, double width, double height) {
        GL3 gl3 = GLContext.getCurrentGL().getGL3();
        StateTools.setupState(style);

        Shader rectangleShader;
        if (style.smooth) {
            rectangleShader = rectangleShaderManager.shader(style.shadeStyle, rectangle.format());
        } else {
            rectangleShader = rectangleShaderManagerNoSmooth.shader(style.shadeStyle, rectangle.format());
        }
        rectangleShader.begin();
        ShaderTools.setupShader(rectangleShader, context, style);

        rectangleShader.setUniform("offset", position);
        rectangleShader.setUniform2f("dimensions", (float) width, (float) height);

        rectangleShaderManager.setParameters(rectangleShader, style.shadeStyle);

        rectangle.bind();
        VertexBufferDrawer.draw(gl3, rectangleShader, rectangle, rectangle.format(), GL3.GL_TRIANGLES, 6);
        rectangleShader.end();
        rectangle.unbind();
        GLError.debug(gl3);

    }


    public void drawRectangles(DrawContext context, DrawStyle style, BufferTexture positionsAndDimensions, int count) {

        GL3 gl3 = GLContext.getCurrentGL().getGL3();
        StateTools.setupState(style);

        Shader rectanglesShader = rectanglesShaderManager.shader(style.shadeStyle, rectangle.format());

        rectanglesShader.begin();
        ShaderTools.setupShader(rectanglesShader, context, style);
        rectanglesShaderManager.setParameters(rectanglesShader, style.shadeStyle);
        rectanglesShader.setUniform1i("positions", 0);
        rectangle.bind();

        positionsAndDimensions.bindTexture(0);

        int instancesPerBatch = 320_000;

        int batches = (int) Math.ceil((double)count / instancesPerBatch);
        int processed = 0;
        long start = System.currentTimeMillis();
        for (int batch = 0; batch <  batches; ++batch) {
            rectanglesShader.setUniform1i("instanceOffset", processed);
            VertexBufferDrawer.drawInstances(gl3, rectanglesShader, rectangle, rectangle.format(), GL3.GL_TRIANGLES, 6, Math.min(instancesPerBatch, count - processed));
            processed += instancesPerBatch;
        }
        long end = System.currentTimeMillis();
        GLContext.getCurrentGL().glFlush();


        rectanglesShader.end();
        rectangle.unbind();

        positionsAndDimensions.unbindTexture();
        GLError.debug(gl3);
    }

    public void drawRectangles(DrawContext context, DrawStyle style, List<Vector2> positions, List<Vector2> sizes) {

        {
            long start = System.currentTimeMillis();
            int idx = 0;
            BufferWriter bw = rectanglePositions.shadow().writer();
            rectanglePositions.shadow().buffer().rewind();
            for (Vector2 position : positions) {
                Vector2 size = sizes.get(idx);
                bw.write(position).write(size);
                idx++;
            }
            rectanglePositions.shadow().buffer().rewind();
            rectanglePositions.write(rectanglePositions.shadow().buffer(), positions.size());
            long end = System.currentTimeMillis();
            //System.out.println("upload:" + (end-start));
        }

        drawRectangles(context, style, rectanglePositions, positions.size());

    }

    public void drawRectangles(DrawContext context, DrawStyle style, List<Rectangle> rectangles) {

        {
            long start = System.currentTimeMillis();
            BufferWriter bw = rectanglePositions.shadow().writer();
            rectanglePositions.shadow().buffer().rewind();
            for (Rectangle rectangle: rectangles) {
                Vector2 size = new Vector2(rectangle.width, rectangle.height);
                Vector2 position = new Vector2(rectangle.x, rectangle.y);
                bw.write(position).write(size);
            }
            rectanglePositions.shadow().buffer().rewind();
            rectanglePositions.write(rectanglePositions.shadow().buffer(), rectangles.size());
            long end = System.currentTimeMillis();
//            System.out.println("upload:" + (end-start));
        }


        drawRectangles(context, style, rectanglePositions, rectangles.size());

    }

    public void destroy() {
        rectangleShaderManager.destroy();
        rectangleShaderManagerNoSmooth.destroy();
        rectanglesShaderManager.destroy();
        rectanglePositions.destroy();
        rectangle.destroy();
    }


}
