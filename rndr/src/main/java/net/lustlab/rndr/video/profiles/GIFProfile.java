package net.lustlab.rndr.video.profiles;

import net.lustlab.rndr.video.VideoWriterProfile;

/**
 * Created by edwin on 08/11/16.
 */
public class GIFProfile extends VideoWriterProfile<GIFProfile> {

    boolean loop = true;


    public GIFProfile loop(boolean loop) {
        this.loop = loop;
        return this;
    }

    public boolean loop() {
        return loop;
    }

    @Override
    public String[] arguments() {
        return new String[] {
                "-ignore_loop","0"
        };
    }
}
