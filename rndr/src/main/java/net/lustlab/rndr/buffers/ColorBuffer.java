package net.lustlab.rndr.buffers;

import com.jogamp.common.nio.ByteBufferInputStream;
import com.jogamp.opengl.*;
import com.jogamp.opengl.util.texture.Texture;
import com.jogamp.opengl.util.texture.TextureData;
import com.jogamp.opengl.util.texture.TextureIO;
import net.lustlab.rndr.RNDRException;
import net.lustlab.rndr.gl.GLError;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.FloatBuffer;

public class ColorBuffer {

    static Logger logger = LogManager.getLogger(ColorBuffer.class);

    public final int texture;
    final int width;
    final int height;
    final ColorFormat format;
    final ColorType type;
    final int levels;

    public final int target;
    private final GL gl;
    boolean destroyed = false;
    boolean upsideDown;

    private ColorBufferShadow shadow = null;


    @Override
    public String toString() {
        return "ColorBuffer{" +
                "height=" + height +
                ", width=" + width +
                ", format=" + format +
                ", type=" + type +
                ", destroyed=" + destroyed +
                ", upsideDown=" + upsideDown +
                '}';
    }

    /**
     * Returns an indication of how the ColorBuffer is oriented.
     * @return true iff the ColorBuffer is stored upside-down
     */
    public boolean upsideDown() {
        return upsideDown;
    }

    /**
     * Sets an indiciation of how the ColorBuffer is oriented.
     * @param upsideDown
     * @return this
     */
    public ColorBuffer upsideDown(boolean upsideDown) {
        this.upsideDown = upsideDown;
        return this;
    }

    /**
     * Generates a mipmap for the ColorBuffer
     * @return this
     */
    public ColorBuffer generateMipmap() {
        bindTexture();
        gl.glGenerateMipmap(target);
        return this;
    }

    /**
     * Returns the width of the ColorBuffer
     * @return the width of the ColorBuffer in pixel units
     */
    public final int width() {
        return width;
    }

    /**
     * Returns the height of the ColorBuffer
     * @return the height of the ColorBuffer in pixel units
     */
    public final int height() {
        return height;
    }

    public static class ConversionEntry {
        final ColorFormat format;
        final ColorType type;
        final int glFormat;
        public ConversionEntry(ColorFormat format, ColorType type, int glFormat) {
            this.format = format;
            this.type = type;
            this.glFormat = glFormat;
        }
    }

    public static ColorBuffer fromBuffer(ByteBuffer buffer, String extension) {
        ByteBufferInputStream bbs = new ByteBufferInputStream(buffer);
        return fromStream(bbs, extension);
    }

    /**
     * Load a ColorBuffer from a URL
     * @param url URL encoded as a string
     * @return loaded ColorBuffer
     */
    public static ColorBuffer fromUrl(String url) {
        try {
            return fromUrl(new URL(url));
        } catch (MalformedURLException e) {
            throw new RNDRException("malformed url:" + url);
        }
    }

    /**
     * Loads a ColorBuffer from file
     * @param file the file to load the ColorBuffer from
     * @return loaded ColorBuffer
     */
    public static ColorBuffer fromFile(File file) {
        try {
            return fromUrl(file.toURI().toURL());
        } catch (MalformedURLException e) {
            throw new RNDRException(e);
        }
    }

    /**
     * Loads a ColorBuffer from a URL
     * @param url the url l=to load the ColorBuffer from
     * @return loaded ColorBuffer
     */
    public static ColorBuffer fromUrl(URL url) {
        logger.debug("loading ColorBuffer from url {}" + url);
        int lastIndex = url.toExternalForm().lastIndexOf(".");
        String extension = url.toExternalForm().substring(lastIndex + 1);

        try( InputStream is = url.openStream()) {
            return fromStream(is, extension);
        } catch (IOException e) {
            throw new RNDRException("could not load ColorBuffer from url: " + url, e);
        } catch (RNDRException e) {
            throw new RNDRException("could not load ColorBuffer from stream: " + url, e);
        }
    }

    public static ColorBuffer fromStream(InputStream stream, String extension) {


        int textures[] = new int[1];
        GL gl = GLContext.getCurrentGL();
        gl.glGenTextures(1, textures, 0);
        gl.glActiveTexture(GL.GL_TEXTURE0);
        gl.glBindTexture(GL.GL_TEXTURE_2D, textures[0]);

        try {
            TextureData data = TextureIO.newTextureData(gl.getGLProfile(), stream, false, extension);

            final int[] align = new int[1];
            gl.glGetIntegerv(GL.GL_UNPACK_ALIGNMENT, align, 0); // save alignment
            gl.glPixelStorei(GL.GL_UNPACK_ALIGNMENT, data.getAlignment());

            int levels = 1;


            if (!data.isDataCompressed()) {
                if (data.getBuffer() != null) {
                    gl.glTexImage2D(GL.GL_TEXTURE_2D, 0, data.getPixelFormat(), data.getWidth(), data.getHeight(), 0, data.getPixelFormat(), data.getPixelType(), data.getBuffer());
                    gl.glGenerateMipmap(GL.GL_TEXTURE_2D);
                } else {
                    levels = data.getMipmapData().length;
                    for (int i = 0; i < levels; ++i) {
                        gl.glTexImage2D(GL.GL_TEXTURE_2D, 0, data.getPixelFormat(), data.getWidth() >> i, data.getHeight() >> i, 0, data.getPixelFormat(), data.getPixelType(), data.getMipmapData()[i]);
                    }
                }
            } else {
                throw new RuntimeException("compressed data is not supported");
            }
            gl.glFlush();
            gl.glFinish();
            gl.glPixelStorei(GL.GL_UNPACK_ALIGNMENT, align[0]); // restore alignment

            logger.debug("creating ColorBuffer {}x{}", data.getWidth(), data.getHeight());
            return new ColorBuffer(gl,
                    GL.GL_TEXTURE_2D,
                    textures[0],
                    data.getWidth(), data.getHeight(),
                    ColorFormat.fromGLFormat(data.getPixelFormat()),
                    ColorType.fromGLType(data.getPixelType()), levels).upsideDown(data.getMustFlipVertically());
        } catch (IOException e) {
            throw new RNDRException("could not load ColorBuffer from stream: " + stream);
        }


    }
    public static int toGLFormat(ColorFormat format, ColorType type) {
        ConversionEntry entries[] = new ConversionEntry[] {
                new ConversionEntry(ColorFormat.R, ColorType.UINT8, GL3.GL_R8),
                new ConversionEntry(ColorFormat.R, ColorType.UINT16, GL3.GL_R16),
                new ConversionEntry(ColorFormat.R, ColorType.FLOAT16, GL3.GL_R16F),
                new ConversionEntry(ColorFormat.R, ColorType.FLOAT32, GL3.GL_R32F),
                new ConversionEntry(ColorFormat.RG, ColorType.UINT8, GL3.GL_RG8),
                new ConversionEntry(ColorFormat.RG, ColorType.FLOAT16, GL3.GL_RG16F),
                new ConversionEntry(ColorFormat.RG, ColorType.FLOAT32, GL3.GL_RG32F),
                new ConversionEntry(ColorFormat.RGB, ColorType.UINT8, GL3.GL_RGB8),
                new ConversionEntry(ColorFormat.RGB, ColorType.FLOAT16, GL3.GL_RGB16F),
                new ConversionEntry(ColorFormat.RGB, ColorType.FLOAT32, GL3.GL_RGB32F),
                new ConversionEntry(ColorFormat.RGBA, ColorType.UINT8, GL3.GL_RGBA8),
                new ConversionEntry(ColorFormat.RGBA, ColorType.FLOAT16, GL3.GL_RGBA16F),
                new ConversionEntry(ColorFormat.RGBA, ColorType.FLOAT32, GL3.GL_RGBA32F),
                new ConversionEntry(ColorFormat.R, ColorType.UINT8, GL3.GL_R8),
                new ConversionEntry(ColorFormat.R, ColorType.FLOAT16, GL3.GL_R16F),
                new ConversionEntry(ColorFormat.R, ColorType.FLOAT32, GL3.GL_R32F),
                new ConversionEntry(ColorFormat.sRGB, ColorType.UINT8, GL3.GL_SRGB8),
                new ConversionEntry(ColorFormat.sRGBA, ColorType.UINT8, GL3.GL_SRGB8_ALPHA8)

        } ;
        for (ConversionEntry entry: entries) {
            if (entry.format == format && entry.type == type ){
                return entry.glFormat;
            }

        }
        throw new RNDRException("no conversion entry for " + format + "/" + type);
    }

    /**
     * Wrap around a JOGL texture object
     * @param texture JOGL texture
     */
    public static ColorBuffer wrapTexture(Texture texture) {
        return new ColorBuffer(GLContext.getCurrentGL(),texture.getTarget(), texture.getTextureObject(), texture.getWidth(), texture.getHeight(), ColorFormat.RGBA, ColorType.UINT8, 1).upsideDown(texture.getMustFlipVertically());
    }


    /**
     * Creates a color buffer with RGBA color format using 8 bit per channels
     * @param width the width of the color buffer in pixels
     * @param height the height of the color buffer in pixels
     * @return a new ColorBuffer
     */
    public static ColorBuffer create(int width, int height) {
        return create(GLContext.getCurrentGL(), width, height);
    }


    /**
     *
     * @param width the width of the color buffer in pixels
     * @param height the height of the color buffer in pixels
     * @param format the color format
     * @param type the color data type
     * @return a new ColorBuffer
     */
    public static ColorBuffer create(int width, int height, ColorFormat format, ColorType type) {
        return create(GLContext.getCurrentGL(), width, height, format, type);
    }


    static ColorBuffer create(GL gl, int width, int height) {
        return create(gl, width, height, ColorFormat.RGBA, ColorType.UINT8);
    }


    static ColorBuffer create(GL gl, int width, int height, ColorFormat format, ColorType type) {
        int glFormat = toGLFormat(format, type);

        GLError.check(gl);
        if (width <= 0 || height <= 0) {
            throw new RNDRException("cannot create ColorBuffer with dimensions: " + width + "x" + height);
        }
        int textures[] = new int[1];
        gl.glGenTextures(1, textures, 0);
        int texture = textures[0];
        gl.glActiveTexture(GL.GL_TEXTURE0);
        gl.glBindTexture(GL.GL_TEXTURE_2D, texture);

                                            // internal format          // format    // type
        gl.glTexImage2D(GL.GL_TEXTURE_2D, 0, glFormat, width, height, 0, format.toGLFormat(), type.toGLType(), null);
        GLError.check(gl);
        gl.glTexParameteri(GL.GL_TEXTURE_2D, GL.GL_TEXTURE_MIN_FILTER, GL.GL_LINEAR);
        gl.glTexParameteri(GL.GL_TEXTURE_2D, GL.GL_TEXTURE_MAG_FILTER, GL.GL_LINEAR);
        gl.glTexParameteri(GL.GL_TEXTURE_2D, GL.GL_TEXTURE_WRAP_S, GL.GL_CLAMP_TO_EDGE);
        gl.glTexParameteri(GL.GL_TEXTURE_2D, GL.GL_TEXTURE_WRAP_T, GL.GL_CLAMP_TO_EDGE);

        GLError.check(gl);

        return new ColorBuffer(gl, GL.GL_TEXTURE_2D, texture, width, height, format, type, 1);
    }

    /**
     * Read the ColorBuffer contents
     * @param buffer the buffer to copy the ColorBuffer contents to
     * @param type
     * @return this
     */
    @Deprecated
    public ColorBuffer read(ByteBuffer buffer, int type) {
        int pformat = guessFormat();
        gl.glActiveTexture(GL3.GL_TEXTURE0);
        int packAlignment  = glGetInteger(gl, GL.GL_PACK_ALIGNMENT);
        gl.glPixelStorei(GL.GL_PACK_ALIGNMENT, 1);

        bindTexture(gl);
        buffer.order(ByteOrder.nativeOrder());
        gl.getGL3().glGetTexImage(target, 0, pformat, type, buffer);
        buffer.rewind();
        gl.glPixelStorei(GL.GL_PACK_ALIGNMENT, packAlignment);

        return this;
    }
    public ColorBuffer read(ByteBuffer buffer, ColorType type) {
        int pformat = guessFormat();
        gl.glActiveTexture(GL3.GL_TEXTURE0);
        int packAlignment  = glGetInteger(gl, GL.GL_PACK_ALIGNMENT);
        gl.glPixelStorei(GL.GL_PACK_ALIGNMENT, 1);

        bindTexture(gl);
        buffer.order(ByteOrder.nativeOrder());
        gl.getGL3().glGetTexImage(target, 0, pformat, type.toGLType(), buffer);
        buffer.rewind();
        gl.glPixelStorei(GL.GL_PACK_ALIGNMENT, packAlignment);

        return this;
    }


    /**
     * Read the ColorBuffer contents as floating point data
     * @param buffer
     */
    public ColorBuffer read(FloatBuffer buffer) {
        int pformat = guessFormat();
        gl.glActiveTexture(GL3.GL_TEXTURE0);
        bindTexture(gl);
        gl.getGL3().glGetTexImage(target, 0, pformat, GL.GL_FLOAT, buffer);
        buffer.rewind();
        return this;
    }

    @Deprecated
    public ColorBuffer write(ByteBuffer bb, int type) {
        int pformat = guessFormat();
        bindTexture(gl);
        gl.glTexSubImage2D(target, 0, 0, 0, width, height, pformat, type, bb);
        return this;
    }

    public ColorBuffer write(ByteBuffer bb, ColorType type) {
        int pformat = guessFormat();
        bindTexture(gl);

        gl.glTexSubImage2D(target, 0, 0, 0, width, height, pformat, type.toGLType(), bb);
        return this;
    }


    private int guessFormat() {
        int pformat;
        switch (format) {
            case RGBA:
                pformat = GL3.GL_RGBA;
                break;
            case RGB:
                pformat = GL3.GL_RGB;
                break;
            case RG:
                pformat = GL3.GL_RG;
                break;
            case R:
                pformat = GL3.GL_RED;
                break;
            case sRGB:
                pformat = GL3.GL_RGB;
                break;
            case sRGBA:
                pformat = GL3.GL_RGBA;
                break;

            default:
                throw new RNDRException("unsupported format");
        }
        return pformat;
    }

    public ColorBuffer filter(MinifyingFilter min, MagnifyingFilter mag) {
        bindTexture();
        gl.glTexParameteri(target, GL.GL_TEXTURE_MIN_FILTER, min.toGLFilter());
        gl.glTexParameteri(target, GL.GL_TEXTURE_MAG_FILTER, mag.toGLFilter());
        return this;
    }


    public ColorBuffer wrap(WrapMode u, WrapMode v) {
        bindTexture();
        gl.glTexParameteri(target, GL.GL_TEXTURE_WRAP_S, u.toGLWrapMode());
        gl.glTexParameteri(target, GL.GL_TEXTURE_WRAP_T, v.toGLWrapMode());
        return this;
    }

    public ColorBuffer(GL gl, int target, int texture, int width, int height, ColorFormat format, ColorType type, int levels) {
        this.target = target;
        this.gl = gl;
        this.texture = texture;
        this.width = width;
        this.height = height;
        this.format = format;
        this.type = type;
        this.levels = levels;

    }


    public void bindTexture(GL gl) {
        if (!destroyed) {
            gl.glBindTexture(target, texture);
        }
        else {
            throw new RNDRException("colorbuffer cannot be used, because the texture is destroyed");
        }
    }

    public void bindTexture() {
        if (!destroyed) {
            gl.glBindTexture(target, texture);
        }
        else {
            throw new RNDRException("colorbuffer cannot be used, because the texture is destroyed");
        }
    }

    public void bindTexture(int textureUnit) {
        if (!destroyed) {
            gl.glActiveTexture(GL.GL_TEXTURE0 + textureUnit);
            gl.glBindTexture(target, texture);
        }
        else {
            throw new RNDRException("colorbuffer cannot be used, because the texture is destroyed");
        }
    }

    private static int glGetInteger(GL gl, int pname) {
        int[] tmp = new int[1];
        gl.glGetIntegerv(pname, tmp, 0);
        return tmp[0];
    }

    private static int glGetTexLevelParameteri(GL2GL3 gl, int target, int level, int pname) {
        int[] tmp = new int[1];
        gl.glGetTexLevelParameteriv(target, 0, pname, tmp, 0);
        return tmp[0];
    }

    /**
     * Save the ColorBuffer contents to a file
     * @param file the file to save to
     * @return this
     */

    public ColorBuffer save(File file) {
        return save(file.getAbsolutePath());
    }

    public ColorBuffer save(String filename)  {
        logger.debug("saving ColorBuffer to {}", filename);
        GLError.debug(gl);
        Texture tex = new Texture(texture, target, width, height, width, height, false);

        if (tex.getTarget() != GL.GL_TEXTURE_2D) {
            throw new GLException("Only GL_TEXTURE_2D textures are supported");
        }
        GL _gl = GLContext.getCurrentGL();
        GLError.debug(gl);

        if (!_gl.isGL2GL3()) {
            throw new GLException("Implementation only supports GL2GL3 (Use GLReadBufferUtil and the TextureData variant), have: " + _gl);
        }
        GL2GL3 gl = _gl.getGL2GL3();

        tex.bind(gl);
        GLError.debug(gl);

        int internalFormat = glGetTexLevelParameteri(gl, GL.GL_TEXTURE_2D, 0, GL2.GL_TEXTURE_INTERNAL_FORMAT);
        int width  = glGetTexLevelParameteri(gl, GL.GL_TEXTURE_2D, 0, GL2.GL_TEXTURE_WIDTH);
        GLError.debug(gl);

        int height = glGetTexLevelParameteri(gl, GL.GL_TEXTURE_2D, 0, GL2.GL_TEXTURE_HEIGHT);
        GLError.debug(gl);

        int border = 0;//glGetTexLevelParameteri(gl, GL.GL_TEXTURE_2D, 0, GL2.GL_TEXTURE_BORDER);
        GLError.debug(gl);

        TextureData data = null;
        if (internalFormat == GL.GL_COMPRESSED_RGB_S3TC_DXT1_EXT ||
                internalFormat == GL.GL_COMPRESSED_RGBA_S3TC_DXT1_EXT ||
                internalFormat == GL.GL_COMPRESSED_RGBA_S3TC_DXT3_EXT ||
                internalFormat == GL.GL_COMPRESSED_RGBA_S3TC_DXT5_EXT) {
            // Fetch using glGetCompressedTexImage
            int size   = glGetTexLevelParameteri(gl, GL.GL_TEXTURE_2D, 0, GL2.GL_TEXTURE_COMPRESSED_IMAGE_SIZE);
            ByteBuffer res = ByteBuffer.allocate(size);
            gl.glGetCompressedTexImage(GL.GL_TEXTURE_2D, 0, res);
            data = new TextureData(gl.getGLProfile(), internalFormat, width, height, border, internalFormat, GL.GL_UNSIGNED_BYTE,
                    false, true, true, res, null);
        } else {
            int bytesPerPixel;
            int fetchedFormat;
            int fetchedType = GL.GL_UNSIGNED_BYTE;
            switch (internalFormat) {
                case GL.GL_RGB:
                case GL2.GL_BGR:
                case GL.GL_RGB8:
                    bytesPerPixel = 3;
                    fetchedFormat = GL.GL_RGB;
                    break;
                case GL.GL_RGBA:
                case GL.GL_BGRA:
                case GL2.GL_ABGR_EXT:
                case GL.GL_RGBA8:
                    bytesPerPixel = 4;
                    fetchedFormat = GL.GL_RGBA;
                    break;
                case GL.GL_RGBA32F:
                    bytesPerPixel = 16;
                    fetchedFormat = GL.GL_RGBA;
                    fetchedType = GL.GL_UNSIGNED_INT;
                    break;
                default:
                    throw new RuntimeException("Unsupported texture internal format 0x" + Integer.toHexString(internalFormat));
            }

            // Fetch using glGetTexImage
            int packAlignment  = glGetInteger(gl, GL.GL_PACK_ALIGNMENT);
            GLError.debug(gl);

            int packRowLength  = glGetInteger(gl, GL2.GL_PACK_ROW_LENGTH);
            GLError.debug(gl);

            int packSkipRows   = glGetInteger(gl, GL2.GL_PACK_SKIP_ROWS);
            GLError.debug(gl);

            int packSkipPixels = glGetInteger(gl, GL2.GL_PACK_SKIP_PIXELS);
            GLError.debug(gl);

            int packSwapBytes  = glGetInteger(gl, GL2.GL_PACK_SWAP_BYTES);
            GLError.debug(gl);

            gl.glPixelStorei(GL.GL_PACK_ALIGNMENT, 1);
            GLError.debug(gl);
            gl.glPixelStorei(GL2.GL_PACK_ROW_LENGTH, 0);
            GLError.debug(gl);

            gl.glPixelStorei(GL2.GL_PACK_SKIP_ROWS, 0);
            GLError.debug(gl);

            gl.glPixelStorei(GL2.GL_PACK_SKIP_PIXELS, 0);
            GLError.debug(gl);

            gl.glPixelStorei(GL2.GL_PACK_SWAP_BYTES, 0);
            GLError.debug(gl);

            ByteBuffer res = ByteBuffer.allocate((width + (2 * border)) *
                    (height + (2 * border)) *
                    bytesPerPixel);
            gl.glGetTexImage(GL.GL_TEXTURE_2D, 0, fetchedFormat, fetchedType, res);
            GLError.debug(gl);

            gl.glPixelStorei(GL.GL_PACK_ALIGNMENT, packAlignment);
            GLError.debug(gl);

            gl.glPixelStorei(GL2.GL_PACK_ROW_LENGTH, packRowLength);
            GLError.debug(gl);

            gl.glPixelStorei(GL2.GL_PACK_SKIP_ROWS, packSkipRows);
            GLError.debug(gl);

            gl.glPixelStorei(GL2.GL_PACK_SKIP_PIXELS, packSkipPixels);
            GLError.debug(gl);

            gl.glPixelStorei(GL2.GL_PACK_SWAP_BYTES, packSwapBytes);
            GLError.debug(gl);

            data = new TextureData(gl.getGLProfile(), internalFormat, width, height, border, fetchedFormat, fetchedType,
                    false, false, false, res, null);
            GLError.debug(gl);

            GLError.clear(gl);
        }
        try {
            TextureIO.write(data, new File(filename));
            GLError.clear(gl);

        } catch (IOException e) {
            throw new RuntimeException(e);
        }
        return this;
    }

    public ColorBuffer destroy() {
        return destroy(this.gl);
    }

    public ColorBuffer destroy(GL gl) {
        if (!destroyed) {
            logger.debug("destroying ColorBuffer");
            GLError.debug(gl);
            gl.glDeleteTextures(1, new int[]{texture}, 0);
            GLError.debug(gl);
            destroyed = true;
        } else {
            logger.debug("attempting to destroy already destroyed ColorBuffer");
        }
        return this;
    }

    /**
     * Returns the number of mipmap levels
     * @return number of mipmap levels
     */
    public final int levels() {
        return levels;
    }

    /**
     * Returns the color format of the ColorBuffer
     * @return color format
     */
    public final ColorFormat format() {
        return format;
    }

    /**
     * Returns the color type of the ColorBuffer
     * @return color type
     */
    public final ColorType type() {
        return type;
    }

    /**
     * Returns a shadow for the ColorBuffer. Creates a new shadow once, returns existing shadow after.
     * @return
     */
    public final ColorBufferShadow shadow() {
        if (shadow == null) {
            shadow = new ColorBufferShadow(this);
        }
        return shadow;
    }
}
