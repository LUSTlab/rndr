package net.lustlab.rndr.buffers;


import net.lustlab.colorlib.ColorRGBa;
import net.lustlab.rndr.math.Matrix44;
import net.lustlab.rndr.math.Vector2;
import net.lustlab.rndr.math.Vector3;
import net.lustlab.rndr.math.Vector4;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;

/**
 * BufferWriter is a helper class that makes it easier to write RNDR types into ByteBuffers
 */
public class BufferWriter {
    
    private final ByteBuffer buffer;

    public BufferWriter rewind() {
        buffer.rewind();
        return this;
    }

    public long position() {
        return buffer.position();
    }

    public BufferWriter seek(long position) {
        buffer.position((int)position);
        return this;
    }

    public BufferWriter(ByteBuffer buffer) {
        buffer.order(ByteOrder.nativeOrder());
        this.buffer = buffer;
    }

    public BufferWriter write(int i) {
        if (buffer != null) {
            buffer.putInt(i);
        }
        return this;
    }

    public BufferWriter write(Vector3 vector3) {
        if (buffer != null) {
            buffer.putFloat((float) vector3.x);
            buffer.putFloat((float) vector3.y);
            buffer.putFloat((float) vector3.z);
        }
        return this;
    }

    /**
     * Writes a single float in the BufferWriter
     * @param x the float to write in the BufferWriter
     */
    public BufferWriter write(float x) {
        if (buffer != null) {
            buffer.putFloat(x);
        }
        return this;
    }
    public BufferWriter write(float[] xs) {
        if (buffer != null) {
            for (float x: xs) {
                buffer.putFloat(x);
            }
        }
        return this;
    }
    public BufferWriter write(double[] xs) {
        if (buffer != null) {
            for (double x: xs) {
                buffer.putFloat((float)x);
            }
        }
        return this;
    }

    public BufferWriter write(double x, double y) {
        if (buffer != null) {
            buffer.putFloat((float)x);
            buffer.putFloat((float)y);
        }
        return this;
    }

    public BufferWriter write(double x, double y, double z) {
        if (buffer != null) {
            buffer.putFloat((float)x);
            buffer.putFloat((float)y);
            buffer.putFloat((float)z);
        }
        return this;
    }

    public BufferWriter write(Matrix44 m) {
        buffer.putFloat((float)m.m00);
        buffer.putFloat((float)m.m01);
        buffer.putFloat((float)m.m02);
        buffer.putFloat((float)m.m03);
        buffer.putFloat((float)m.m10);
        buffer.putFloat((float)m.m11);
        buffer.putFloat((float)m.m12);
        buffer.putFloat((float)m.m13);
        buffer.putFloat((float)m.m20);
        buffer.putFloat((float)m.m21);
        buffer.putFloat((float)m.m22);
        buffer.putFloat((float)m.m23);
        buffer.putFloat((float)m.m30);
        buffer.putFloat((float)m.m31);
        buffer.putFloat((float)m.m32);
        buffer.putFloat((float)m.m33);
        return this;
    }

    public BufferWriter write(double x, double y, double z, double w) {
        if (buffer != null) {
            buffer.putFloat((float)x);
            buffer.putFloat((float)y);
            buffer.putFloat((float)z);
            buffer.putFloat((float)w);
        }
        return this;
    }

    /**
     * Writes a {@code ColorRGBa} into the ByteBuffer
     * @param color the color to write
     */
    public BufferWriter write(ColorRGBa color) {
        if (buffer != null) {
            buffer.putFloat((float) color.r);
            buffer.putFloat((float) color.g);
            buffer.putFloat((float) color.b);
            buffer.putFloat((float) color.a);
        }
        return this;
    }
    public BufferWriter write(Vector4 vector4) {
        if (buffer != null) {
            buffer.putFloat((float) vector4.x);
            buffer.putFloat((float) vector4.y);
            buffer.putFloat((float) vector4.z);
            buffer.putFloat((float) vector4.w);
        }
        return this;

    }

    /**
     * Write a @code{Vector2} into the BufferWriter (casted to floats)
     * @param vector2 the vector2 to write into the BufferWriter
     */
    public BufferWriter write(Vector2 vector2) {
        if (buffer != null) {
            buffer.putFloat((float) vector2.x);
            buffer.putFloat((float) vector2.y);
        }
        return this;
    }
    
}
