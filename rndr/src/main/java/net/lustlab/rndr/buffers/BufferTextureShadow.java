package net.lustlab.rndr.buffers;

import java.nio.ByteBuffer;

public class BufferTextureShadow {

    final BufferTexture bufferTexture;
    ByteBuffer buffer;

    public BufferTextureShadow(BufferTexture bufferTexture) {
        this.bufferTexture = bufferTexture;
        this.buffer = ByteBuffer.allocateDirect(bufferTexture.elementSize() * bufferTexture.elementCount);
    }

    public BufferWriter writer() {
        return new BufferWriter(buffer);
    }

    public BufferTextureShadow upload() {
        buffer.rewind();
        bufferTexture.write(buffer);
        return this;
    }

    public BufferTextureShadow upload(int elementCount) {
        buffer.rewind();
        bufferTexture.write(buffer, elementCount);
        return this;
    }

    public ByteBuffer buffer() {
        return buffer;
    }
}