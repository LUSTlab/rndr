package net.lustlab.rndr.shape;

import net.lustlab.colorlib.ColorRGBa;
import net.lustlab.rndr.math.Vector2;
import net.lustlab.rndr.math.Vector3;

import java.io.Serializable;


public class Vertex implements Serializable {
	public ColorRGBa color;
	public Vector3 position;
	public Vector2 texture;
    public boolean edge;
    public boolean touchesEdge;

    public Vertex() {

    }

    public Vertex(Vertex other) {
        if (other.color != null) {
            this.color = other.color.copy();
        }
        if (other.position != null) {
            this.position = other.position.copy();
        }
        if (this.texture != null) {
            this.texture = other.texture.copy();
        }
        this.edge = other.edge;
        this.touchesEdge = other.touchesEdge;
    }


    Vertex color(ColorRGBa color) {
		this.color = color;
		return this;
	}

	public Vertex position(Vector3 position) {
		this.position = position;
		return this;
	}

	public Vertex position(Vector2 position) {
		this.position = new Vector3(position.x, position.y, 0);
		return this;
	}

	public Vertex position(double x, double y, double z) {
		this.position = new Vector3(x,y,z);
		return this;
	}
    public Vertex edge(boolean edge) {
        this.edge = edge;
        return this;
    }


	Vertex texture(Vector2 texture ) {
		this.texture = texture;
		return this;
	}

	@Override
	public String toString() {
		return "Vertex{" +
				"color=" + color +
				", position=" + position +
				", texture=" + texture +
				", edge=" + edge +
				'}';
	}
}
