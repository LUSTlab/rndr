#version 330

layout(location=0) out vec4 o_output;

uniform sampler2D image;
uniform sampler2D position;
in vec2 v_texCoord0;

//const float focal_distance =1000.0f;
//const float focal_length = 10000.0f;
//const float f_stop = 1.0;
//float cocFromZ(float z)
//{	float p = focal_distance; // focal distance
//	float f = focal_length; // focal length
//	float F = f_stop; // F-stop
//	return ((z * f) / (z - f) - (p * f) / (p - f)) * (p - f) / (p * F) ;
//}


void main() {

     vec3 color = texture(image, v_texCoord0).rgb;
     float depth = -texture(position, v_texCoord0).z;
     //float coc = 0.1 + min(0.90, depth/2000.0); ////0.01 + abs(cocFromZ(depth)/200.0);

    float coc =  8.0*abs(1.0 - (10.0/depth));
//     coc += length(v_texCoord0-vec2(0.5)*2)*0.125;

      coc = clamp(coc ,10.0, 40.0);

     o_output = vec4(color*coc, coc);
}