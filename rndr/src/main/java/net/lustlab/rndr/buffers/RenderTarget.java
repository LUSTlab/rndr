package net.lustlab.rndr.buffers;

import com.jogamp.opengl.GL;
import com.jogamp.opengl.GL2GL3;
import com.jogamp.opengl.GL3;
import com.jogamp.opengl.GLContext;
import net.lustlab.colorlib.ColorRGBa;
import net.lustlab.rndr.RNDRException;

import net.lustlab.rndr.gl.GLError;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.nio.IntBuffer;
import java.util.*;

/**
 * Rendertarget class
 */
public class RenderTarget {

    static Logger logger = LogManager.getLogger(RenderTarget.class);
    private final int frameBuffer;

    private List<ColorBuffer> colorBuffers = new ArrayList<>();
    private DepthBuffer depthBuffer;

    private GL gl;
    private boolean boundTarget;
    private int[] viewport = new int[4];

    private int width;
    private int height;
    private boolean validated = false;
    private int previousRenderTarget;
    private Map<String, ColorBuffer> namedColorBuffers = new HashMap<>();

    static Map<GLContext, Stack<RenderTarget>> active = new HashMap<>();//new Stack<>();

    /**
     * Returns the width of the RenderTarget
     * @return width in pixel units
     */
    public int width() {
        return width;
    }

    /**
     * Returns the height of the RenderTarget
     * @return the height in pixel units
     */
    public int height() {
        return height;
    }


    boolean sRGB = false;
    public RenderTarget sRGB(boolean sRGB) {
        this.sRGB = sRGB;
        return this;
    }

    public boolean sRGB() {
        return sRGB;
    }
    /**
     * Returns the currently bound RenderTarget
     * @return RenderTarget
     */
    public static RenderTarget current() {
        Stack<RenderTarget> stack = active.computeIfAbsent(GLContext.getCurrent(), k-> new Stack<>());

        if (stack.size() > 0) {
            return stack.peek();
        } else {
            return null;
        }
    }

    /**
     * Creates a RenderTarget instance
     * @param gl the OpenGL interface to use for creating the render target
     * @param width the width of the render target
     * @param height the height of the render target
     * @return
     */
    static RenderTarget create(GL gl, int width, int height) {
        IntBuffer framebuffers = IntBuffer.allocate(1);
        gl.glGenFramebuffers(1, framebuffers);
        int frameBuffer = framebuffers.get(0);
        GLError.check(gl);
        return new RenderTarget(gl, frameBuffer, width, height);
    }

    /**
     * Creates a RenderTarget by wrapping around an existing ColorBuffer
     * @param colorBuffer the ColorBuffer to wrap
     * @return a new RenderTarget
     */

    public static RenderTarget wrap(ColorBuffer colorBuffer) {
        return create(colorBuffer.width, colorBuffer.height).attach(colorBuffer);
    }
    /**
     * Creates a RenderTarget instance
     * @param width the width of the render target
     * @param height the height of the render target
     * @return
     */
    public static RenderTarget create(int width, int height) {
        return create(GLContext.getCurrentGL(), width, height);
    }

    private RenderTarget(GL gl, int frameBuffer, int width, int height) {
        this.gl = gl;
        this.frameBuffer = frameBuffer;
        this.width = width;
        this.height = height;
        this.boundTarget = false;
    }

    /**
     * Retrieves a named ColorBuffer attachment
     * @param name the name of the attachment
     * @return a ColorBuffer
     */
    public ColorBuffer colorBuffer(String name) {
        return namedColorBuffers.get(name);
    }

    public int location(String name) {
        ColorBuffer colorBuffer = colorBuffer(name);
        for (int i = 0; i < colorBuffers.size(); ++i) {
            if (colorBuffers.get(i) == colorBuffer) {
                return i;
            }
        }
        throw new RNDRException("no colorbuffer named " + name);
    }

    /**
     * Attaches a ColorBuffer with a named attachment
     * @param name the name of the attachment
     * @param colorBuffer a colorBuffer
     * @return
     */
    public RenderTarget attach(String name, ColorBuffer colorBuffer) {

        if (namedColorBuffers.containsKey(name)) {
            throw new RNDRException("name in use:" + name);
        }
        attach(colorBuffer);
        namedColorBuffers.put(name, colorBuffer);
        return this;
    }

    /**
     * attaches a color buffer to the render target
     * @param colorBuffer a ColorBuffer instance that has the same height and width as the RenderTarget instance
     * @return
     */
    public RenderTarget attach(ColorBuffer colorBuffer) {

        boolean previouslyBound = boundTarget;

        if (!previouslyBound) {
            bindTarget();
        }

        GLError.check(gl);

        if (!(colorBuffer.width == width && colorBuffer.height == height)) {
            throw new IllegalArgumentException("buffer dimension mismatch. expected: (" + width + " x " + height + "), got: (" + colorBuffer.width + " x " + colorBuffer.height + ")");
        }


        gl.glFramebufferTexture2D(GL.GL_FRAMEBUFFER, GL.GL_COLOR_ATTACHMENT0 + colorBuffers.size(), colorBuffer.target, colorBuffer.texture, 0);
        GLError.debug(gl);
        int error;
        if (( error = gl.glGetError()) != GL.GL_NO_ERROR) {
            switch(error) {
                case GL.GL_INVALID_ENUM:
                    throw new RuntimeException("Invalid enum");
                case GL.GL_INVALID_OPERATION:
                    throw new RuntimeException("Invalid operation");
                default:
                    throw new RuntimeException("I dunno");
            }
        }
        colorBuffers.add(colorBuffer);

        checkFramebufferStatus();
        GLError.debug(gl);
        if (!previouslyBound) {
            unbindTarget();
        }

        return this;
    }

    /**
     * detaches all ColorBuffers
     * @return this
     */
    public RenderTarget detachColorBuffers() {
        GLError.debug(gl);

        boolean previouslyBound = boundTarget;
        if (!boundTarget)
            bindTarget();

        for (int i = 0; i < colorBuffers.size(); ++i) {
            gl.glFramebufferTexture2D(GL.GL_FRAMEBUFFER, GL.GL_COLOR_ATTACHMENT0 + i, GL.GL_TEXTURE_2D, 0, 0);
            GLError.debug(gl);
        }
        colorBuffers.clear();

        if (!previouslyBound) {
            unbindTarget();
        }

        return this;
    }

    /**
     * Attach a DepthBuffer
     * @param depthBuffer the DepthBuffer to attach
     * @return this
     */
    public RenderTarget attach(DepthBuffer depthBuffer) {

        boolean previouslyBound = boundTarget;

        if (!previouslyBound) {
            bindTarget();
        }

        if (!(depthBuffer.width == width && depthBuffer.height == height)) {
            throw new IllegalArgumentException("buffer dimension mismatch");
        }

        gl.glFramebufferTexture2D(GL.GL_FRAMEBUFFER, GL.GL_DEPTH_ATTACHMENT, GL.GL_TEXTURE_2D, depthBuffer.texture, 0);
        if (depthBuffer.hasStencil()) {
            gl.glFramebufferTexture2D(GL.GL_FRAMEBUFFER, GL.GL_STENCIL_ATTACHMENT, GL.GL_TEXTURE_2D, depthBuffer.texture, 0);
        }

        GLError.debug(gl);
        int error;
        if (( error = gl.glGetError()) != GL.GL_NO_ERROR) {
            switch(error) {
                case GL.GL_INVALID_ENUM:
                    throw new RuntimeException("Invalid enum");
                case GL.GL_INVALID_OPERATION:
                    throw new RuntimeException("Invalid operation");
            }
        }

        this.depthBuffer = depthBuffer;
        validated = false;

        if (!previouslyBound) {
            unbindTarget();
        }

        checkFramebufferStatus();

        return this;
    }

    /**
     * Clears a single color attachment
     * @param attachment the color attachment
     * @param color the color
     */
    public RenderTarget clearColor(int attachment, ColorRGBa color) {

        boolean bound = boundTarget;

        if (!bound) {
            bindTarget();
        }
        GL3 gl3 = gl.getGL3();
        gl3.glClearBufferfv(GL3.GL_COLOR, attachment, color.toFloatArray(), 0);

        if (!bound) {
            unbindTarget();
        }
        return this;
    }

    /**
     * Clears the depth attachment
     * @param depth the value to clear the depth attachment with
     * @return this
     */
    public RenderTarget clearDepth(double depth) {
        gl.glDepthMask(true);
        gl.glClearDepth(depth);
        gl.glClear(GL.GL_DEPTH_BUFFER_BIT);
        return this;
    }

    /** Clears the stencil part of the depth attachment
     *
      */
    public RenderTarget clearStencil(int value) {
        boolean bound = boundTarget;

        if (!bound) {
            bindTarget();
        }

        gl.glStencilMask(0xff);
        gl.glClearStencil(value);
        gl.glClear(GL.GL_STENCIL_BUFFER_BIT);
        if (!bound) {
            unbindTarget();
        }

        return this;
    }

    /**
     * Clears all color attachments
     * @param color the color
     */
    public RenderTarget clearColor(ColorRGBa color) {

        boolean bound = boundTarget;

        if (!bound) {
            bindTarget();
        }
        GL3 gl3 = gl.getGL3();
        for (int attachment = 0; attachment < colorBuffers.size(); ++attachment) {
            gl3.glClearBufferfv(GL3.GL_COLOR, attachment, color.toFloatArray(), 0);
            GLError.check(gl3);
        }

        if (!bound) {
            unbindTarget();
        }
        return this;
    }

    /**
     * Binds the RenderTarget as the active target
     * @return this
     */
    public RenderTarget bindTarget() {
        logger.trace("binding target {}", this);

        Stack<RenderTarget> stack = active.computeIfAbsent(GLContext.getCurrent(), k-> new Stack<>());
        stack.push(this);

        if (!boundTarget) {
            GLError.debug(gl);
            int bindings[] = new int[1];
            gl.glGetIntegerv(GL.GL_FRAMEBUFFER_BINDING, bindings, 0);
            GLError.debug(gl);
            previousRenderTarget = bindings[0];

            if (sRGB) {
                gl.glEnable(GL3.GL_FRAMEBUFFER_SRGB);
            } else {
                //gl.glDisable(GL3.GL_FRAMEBUFFER_SRGB);
            }

            gl.glBindFramebuffer(GL.GL_FRAMEBUFFER, frameBuffer);
            GLError.debug(gl);
            boundTarget = true;

            if (!validated && (colorBuffers.size() > 0 || depthBuffer != null)) {
                checkFramebufferStatus();
                validated = true;
            }

            gl.glGetIntegerv(GL.GL_VIEWPORT, viewport, 0);

            IntBuffer ib = IntBuffer.allocate(colorBuffers.size());
            int idx = GL.GL_COLOR_ATTACHMENT0;

            for (int i = 0; i < colorBuffers.size(); ++i) {
                ib.put(idx);
                idx++;
            }
            ib.rewind();

            gl.getGL2GL3().glDrawBuffers(colorBuffers.size(), ib);
            GLError.debug(gl);

            gl.glViewport(0, 0, width, height); 
        }
        else {
            throw new RNDRException("RenderTarget is already bound as target");
        }

        return this;
    }

    void checkFramebufferStatus() {
        int status = gl.glCheckFramebufferStatus(GL.GL_FRAMEBUFFER);
        if (status != GL.GL_FRAMEBUFFER_COMPLETE) {

            switch (status) {
                case GL2GL3.GL_FRAMEBUFFER_UNDEFINED:
                    throw new RNDRException("Framebuffer undefined");
                case GL2GL3.GL_FRAMEBUFFER_INCOMPLETE_ATTACHMENT:
                    throw new RNDRException("Attachment incomplete");
                case GL2GL3.GL_FRAMEBUFFER_INCOMPLETE_MISSING_ATTACHMENT:
                    throw new RNDRException("Attachment missing");
                case GL2GL3.GL_FRAMEBUFFER_INCOMPLETE_DRAW_BUFFER:
                    throw new RNDRException("Incomplete draw buffer");
            }

            throw new RNDRException("error creating framebuffer" + status);
        }
        GLError.debug(gl);
    }


    /**
     * Unbinds the RenderTarget as the active target
     * @return this
     */

    public RenderTarget unbindTarget() {
        Stack<RenderTarget> stack = active.computeIfAbsent(GLContext.getCurrent(), k-> new Stack<>());

        stack.pop();
        logger.trace("unbinding target {}", this);
        if (boundTarget) {
            GLError.debug(gl);
            gl.glBindFramebuffer(GL.GL_FRAMEBUFFER, previousRenderTarget);
            //gl.glDisable(GL3.GL_FRAMEBUFFER_SRGB);
            GLError.debug(gl);
            gl.glViewport(viewport[0], viewport[1], viewport[2], viewport[3]);
            boundTarget = false;
        }
        else {
            throw new RNDRException("RenderTarget is not bound as target");
        }
        return this;
    }

    /**
     * Returns the ColorBuffer at the given index
     * @param index
     * @return
     */
    public ColorBuffer colorBuffer(int index) {
        if (colorBuffers.size() > 0) {
            return colorBuffers.get(index);
        }
        else {
            throw new RNDRException("no color buffer attachments");
        }
    }

    /**
     * Destroy the RenderTarget
     * @return this
     */
    public RenderTarget destroy() {
        gl.glDeleteFramebuffers(1, new int[] { frameBuffer}, 0);
        GLError.check(gl);
        return this;
    }

    public DepthBuffer depthBuffer() {
        return depthBuffer;
    }

     @Override
    public String toString() {
        return "RenderTarget{" +
                "colorBuffers=" + colorBuffers +
                ", depthBuffer=" + depthBuffer +
                ", boundTarget=" + boundTarget +
                ", width=" + width +
                ", height=" + height +
                ", validated=" + validated +
                '}';
    }
}
