package net.lustlab.rndr.font.image;

import com.jogamp.opengl.GLContext;
import com.jogamp.opengl.util.texture.Texture;
import com.jogamp.opengl.util.texture.TextureIO;
import net.lustlab.rndr.RNDRException;
import net.lustlab.rndr.application.Platform;
import net.lustlab.rndr.font.FontDescriptor;
import net.lustlab.rndr.gl.GLError;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.awt.*;
import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;

public class FontImageMapManager {

    final static Logger logger = LogManager.getLogger(FontImageMapManager.class);

    final private static Map<FontDescriptor, FontImageMap> fontMaps = new HashMap<>();


    public static FontImageMap fromUrl(URL url, int size) {
        FontDescriptor descriptor = new FontDescriptor(url.toString(), size);


        FontImageMap fontImageMap = fontMaps.get(descriptor);
        if (fontImageMap == null) {
            logger.debug("loading font at size {} from {} ", new Object[] { size, url} );
            File cacheDir = new File(Platform.cacheDirectory("RNDR"),"fonts");
            if (!cacheDir.exists()) {
                logger.debug("creating font cache directory");
                cacheDir.mkdirs();
            }

            String basename =  descriptor.name.replace(":","@").replace("/", "_") + "@" + descriptor.size;
            File cachedFile = new File(cacheDir,basename + ".ifm");
            File cachedImageFile = new File(cacheDir, basename + ".png");
            boolean build = true;

            if (cachedFile.exists() && cachedImageFile.exists()) {
                logger.debug("loading font from cache (" + cachedFile + ")");

                try {
                    fontImageMap = loadFromCache(cachedFile, cachedImageFile);
                    build = false;
                } catch (RNDRException e) {
                    logger.warn("loading from cache failed; rebuilding font");
                }
            }
            if (build) {
                logger.debug("building font image map");
                try {
                    Font font = Font.createFont(Font.TRUETYPE_FONT, url.openStream());
                    fontImageMap = FontImageMapBuilder.buildFontMap(font, size);
                } catch (FontFormatException | IOException e) {
                    logger.error("could not load font " + url.toString());
                    throw new RNDRException("could not load font " + url.toString(), e);
                }
                logger.info("caching font");
                saveToCache(fontImageMap, cachedFile, cachedImageFile);
                logger.info("done caching font");
            }
            fontImageMap.getTexture().bind(GLContext.getCurrentGL());
            GLContext.getCurrentGL().glGenerateMipmap(fontImageMap.getTexture().getTarget());
            fontMaps.put(descriptor, fontImageMap);
        }


        return fontImageMap;
    }

    private static void saveToCache(FontImageMap fontImageMap, File ifmTarget, File pngTarget) {
        FontImageMapIO.save(fontImageMap, ifmTarget);
        try {

            TextureIO.write(fontImageMap.getTexture(), pngTarget);
            GLError.clear(GLContext.getCurrentGL());
        } catch (IOException e) {
            throw new RNDRException(e);
        }
    }

    private static FontImageMap loadFromCache(File ifmTarget, File pngTarget) {
        FontImageMap fontImageMap;
        FontImageMap temp = FontImageMapIO.load(ifmTarget);
        Texture texture;
        try {
            texture = TextureIO.newTexture(pngTarget, false);
            fontImageMap = new FontImageMap(temp, texture);
        } catch (IOException e) {
            throw new RNDRException(e);
        }
        return fontImageMap;
    }
}
