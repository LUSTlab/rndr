#version 330

${attributes! }
${uniforms! }
out vec2 v_texCoord0;

uniform mat4 modelViewMatrix;
uniform mat4 modelViewProjectionMatrix;
uniform mat4 normalMatrix;
uniform vec3 offset;
uniform float radius;

out vec3 v_boundsPosition;
out vec3 v_modelPosition;
out vec3 v_cameraPosition;

void main(void) {

    int instance = 0;

    vec3 x_position = position;

    {
        ${positionTransform !!}
    }

    v_boundsPosition = vec3(position.xy, 0);
    v_modelPosition = x_position * radius;
    v_cameraPosition = (modelViewMatrix * vec4( vec3(x_position.xy * radius, 0) + offset, 1.0)).xyz;

    gl_Position = modelViewProjectionMatrix * vec4( vec3(x_position.xy * radius, 0) + offset, 1.0);
    v_texCoord0 = texCoord0;
}