package net.lustlab.rndr.draw;

/**
 * Created by edwin on 05/06/17.
 */
public enum StencilOperation {
    KEEP,
    ZERO,
    REPLACE,
    INCREASE,
    INCREASE_WRAP,
    DECREASE,
    DECREASE_WRAP,
    INVERT
}
