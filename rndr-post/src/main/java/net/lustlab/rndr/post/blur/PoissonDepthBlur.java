package net.lustlab.rndr.post.blur;


import net.lustlab.rndr.buffers.ColorBuffer;
import net.lustlab.rndr.post.Filter;
import net.lustlab.rndr.shaders.FragmentShader;


public class PoissonDepthBlur extends Filter {
    ColorBuffer intermediate;

    public PoissonDepthBlur() {
        super(FragmentShader.fromUrl("cp:shaders/gl3/post/blur/poisson-depth-blur.frag"));

        map("tex1", "positions");

    }

    int passes = 4;
    double radius = 21.0;
    double amplitude = 1.0;

    @Override
    public Filter apply(ColorBuffer[] src, ColorBuffer[] dest) {

        if (intermediate != null && (intermediate.width() != dest[0].width() || intermediate.height() != dest[0].height())) {
            intermediate.destroy();
            intermediate = null;
        }

        if (intermediate == null) {
            intermediate = ColorBuffer.create(dest[0].width(), dest[0].height(), dest[0].format(), dest[0].type());
        }


        double activeRadius = radius;


        for (int pass = 0; pass < passes; ++pass) {
            parameter("offset", (float)(pass/10.0));
            parameter("amplitude", (float)amplitude);
            parameter("radius", (float)activeRadius);
            activeRadius*=0.9;
            super.apply(src, new ColorBuffer[]{intermediate});
            parameter("offset", (float)(pass/10.0+0.42871234));
            parameter("radius", (float)activeRadius);
            activeRadius*=0.9;
            super.apply(new ColorBuffer[]{intermediate}, dest);
        }

        return this;
    }

}
