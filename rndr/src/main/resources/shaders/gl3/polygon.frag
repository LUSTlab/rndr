#version 330
layout(origin_upper_left) in vec4 gl_FragCoord;

uniform vec4 fill;
${uniforms! }

// output variables
out vec4 o_color;

in vec3 v_boundsPosition;
in vec3 v_modelPosition;

void main(void) {

    int instance = 0;

    vec2 screenPosition = gl_FragCoord.xy;
    vec3 boundsPosition = v_boundsPosition;
    vec3 modelPosition = v_modelPosition;

    vec4 x_fill = fill;
    { ${fillTransform! } }

    o_color = x_fill;
}