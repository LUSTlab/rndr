#version 330

in vec2 v_texCoord0;
uniform sampler2D tex0;
uniform float bias;
uniform float gain;

out vec4 o_color;
void main() {

    vec2 s = textureSize(tex0, 0).xy;
    s = vec2(1.0/s.x, 1.0/s.y);

    vec4 sumx = vec4(0,0,0,0);
    sumx+=texture(tex0, v_texCoord0 + vec2(1,1) * s) * 0.25;
    sumx+=texture(tex0, v_texCoord0 + vec2(1,0) * s) * 0.5;
    sumx+=texture(tex0, v_texCoord0 + vec2(1,-1) * s) * 0.25;

    sumx-=texture(tex0, v_texCoord0 + vec2(-1,1) * s) * 0.25;
    sumx-=texture(tex0, v_texCoord0 + vec2(-1,0) * s) * 0.5;
    sumx-=texture(tex0, v_texCoord0 + vec2(-1,-1) * s) * 0.25;

    vec4 sumy = vec4(0,0,0,0);
    sumy+=texture(tex0, v_texCoord0 + vec2(1,1) * s) * 0.25;
    sumy+=texture(tex0, v_texCoord0 + vec2(0,1) * s) * 0.5;
    sumy+=texture(tex0, v_texCoord0 + vec2(-1,1) * s) * 0.25;

    sumy-=texture(tex0, v_texCoord0 + vec2(1,-1) * s) * 0.25;
    sumy-=texture(tex0, v_texCoord0 + vec2(0,-1) * s) * 0.5;
    sumy-=texture(tex0, v_texCoord0 + vec2(1,-1) * s) * 0.25;

    float magx = (sumx.r + sumx.g + sumx.b) / 3.0;
    float magy = (sumy.r + sumy.g + sumy.b) / 3.0;
    o_color.r = gain * magx + bias;
    o_color.g = gain * magy + bias;
    o_color.b = sqrt(magx*magx+magy*magy)/sqrt(2);
    o_color.a = 1.0;
}