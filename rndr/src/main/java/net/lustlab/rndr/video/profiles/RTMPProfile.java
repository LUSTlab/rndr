package net.lustlab.rndr.video.profiles;

import net.lustlab.rndr.video.VideoWriterProfile;


public class RTMPProfile extends VideoWriterProfile<RTMPProfile> {


    @Override
    public String[] arguments() {

        return new String[]{
                "-pix_fmt", "yuv420p", // this will produce videos that are playable by quicktime
                "-an",
                "-vcodec", "libx264",
                "-preset", "veryfast",
                "-maxrate", "3000k",
                "-bufsize", "6000k",
                "-g", "50",
                "-f", "flv"
        };
    }
}

