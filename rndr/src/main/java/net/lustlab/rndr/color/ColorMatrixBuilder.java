package net.lustlab.rndr.color;

import net.lustlab.rndr.math.Matrix44;
import net.lustlab.rndr.math.Vector4;

public class ColorMatrixBuilder {

    public class Mix {

        public Mix red() {
            return red;
        }

        public Mix blue() {
            return blue;
        }

        public Mix green() {
            return green;
        }

        public Matrix44 build() {
            return ColorMatrixBuilder.this.build();
        }

        double r;
        double g;
        double b;
        double c;

        public Mix plusRed(double red) {
            r += red;
            return this;
        }
        public Mix plusGreen(double green) {
            g += green;
            return this;
        }
        public Mix plusBlue(double blue) {
            b += blue;
            return this;
        }

        public Mix plusConstant(double constant) {
            c += constant;
            return this;
        }
    }

    Mix red = new Mix();
    Mix green = new Mix();
    Mix blue = new Mix();

    public Matrix44 build() {
        return Matrix44.fromColumnVectors(
                new Vector4(red.r, red.g, red.b, red.c),
                new Vector4(green.r, green.g, green.b, green.c),
                new Vector4(blue.r, blue.g, blue.b, blue.c),
                new Vector4(0, 0, 0, 1)
        );
    }

    public Mix red() {
        return red;
    }

    public Mix green() {
        return green;
    }

    public Mix blue() {
        return blue;
    }

}

