package net.lustlab.rndr.font;

import com.jogamp.opengl.GL;
import com.jogamp.opengl.GL3;
import net.lustlab.colorlib.ColorRGBa;
import net.lustlab.rndr.draw.BlendMode;
import net.lustlab.rndr.draw.ShadeStyle;
import net.lustlab.rndr.math.Matrix44;

import java.util.List;

/**
 * Generalized font map renderer interface
 */
public interface FontMapRenderer {


    void shadeStyle(ShadeStyle shadeStyle);

    public static class CharacterRectangle {
        char character;
        public double x;
        public double y;
        public double width;
        public double height;

        public CharacterRectangle(char character, double x, double y, double width, double height) {
            this.character = character;
            this.x = x;
            this.y = y;
            this.width = width;
            this.height = height;
        }
    }

    public double width(FontMap fontMap, String text);
    public List<CharacterRectangle> render(GL gl, FontMap fontMap, String text, double x, double y);
    public List<CharacterRectangle> render(GL gl, FontMap fontMap, String text, double x, double y, double tracking);

    public List<CharacterRectangle> render(GL3 gl, Matrix44 projection, Matrix44 modelView, FontMap fontMap, String text, double x, double y);
    public List<CharacterRectangle> render(GL3 gl, Matrix44 projection, Matrix44 modelView, FontMap fontMap, String text, double x, double y, double tracking);



    public void blend(BlendMode blendMode);

    public void clip(double left, double top, double width, double height);
    public void noClip();

    public void fill(ColorRGBa color);
    public void noFill();
    public void stroke(ColorRGBa color);
    public void noStroke();

    public void flush();

}
