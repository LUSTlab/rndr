package net.lustlab.rndr.noise;

import net.lustlab.rndr.math.Vector3;

/**
 * Created by voorbeeld on 6/15/14.
 */
public class NoiseSample {

    public double value;
    public Vector3 gradient;

    @Override
    public String toString() {
        return "NoiseSample{" +
                "value=" + value +
                ", gradient=" + gradient +
                '}';
    }
}
