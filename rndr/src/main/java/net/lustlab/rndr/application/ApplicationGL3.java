package net.lustlab.rndr.application;

import com.jogamp.nativewindow.util.DimensionImmutable;
import com.jogamp.newt.MonitorDevice;
import com.jogamp.newt.event.*;
import com.jogamp.newt.opengl.GLWindow;
import com.jogamp.opengl.*;
import com.jogamp.opengl.util.Animator;
import com.jogamp.opengl.util.AnimatorBase;
import com.jogamp.opengl.util.FPSAnimator;
import net.lustlab.colorlib.ColorRGBa;
import net.lustlab.rndr.Configuration;
import net.lustlab.rndr.draw.DrawThread;
import net.lustlab.rndr.draw.Drawer;
import net.lustlab.rndr.draw.gl3.DrawThreadGL3;
import net.lustlab.rndr.draw.gl3.DrawerGL3;
import net.lustlab.rndr.math.Vector2;
import net.lustlab.rndr.math.Vector3;
import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.core.config.Configurator;

import java.util.ArrayList;
import java.util.List;
import java.util.function.BiConsumer;
import java.util.function.Supplier;

public class ApplicationGL3 extends Application {

    private AnimatorBase animator;
    private boolean ready;
    private boolean setupCompleted;
    private long frame = 0;
    private int width;
    private int height;

    private static Logger logger = LogManager.getLogger(ApplicationGL3.class);

    private final List<MouseEvent> mouseEvents = new ArrayList<>();
    private final List<KeyEvent> keyEvents = new ArrayList<>();

    ApplicationGL3(Supplier<Program> supplier, Configuration configuration) {

        if (configuration.debug || Boolean.valueOf(System.getProperty("net.lustlab.rndr.debug", "false"))) {
            Configurator.setLevel("net.lustlab.rndr", Level.DEBUG);
        }

        System.setProperty("newt.window.icons", "cp:icons/rndr-icon-small.png cp:icons/rndr-icon-large.png");

        GLProfile glp = GLProfile.get(GLProfile.GL3);

        logger.debug("requesting OpenGL profile: {}", glp.getName());

        GLCapabilities caps = new GLCapabilities(glp);
        caps.setHardwareAccelerated(true);
        caps.setDepthBits(24);
        caps.setStencilBits(8);

        GLWindow window = GLWindow.create(caps);

        if (configuration.hideWindowDecorations) {
            window.setUndecorated(true);
        }

        window.addGLEventListener(new GLEventListener() {
            @Override
            public void init(GLAutoDrawable drawable) {
                GLContext.getCurrentGL().glClearColor(0, 0, 0, 1);
                GLContext.getCurrentGL().glClear(GL.GL_COLOR_BUFFER_BIT|GL.GL_DEPTH_BUFFER_BIT|GL.GL_STENCIL_BUFFER_BIT);
                window.swapBuffers();

                logger.debug("OpenGL init event");
                logger.debug("OpenGL version: " + drawable.getContext().getGLVersion());
                logger.debug("GLSL version: " + drawable.getContext().getGLSLVersionString());
                logger.debug("OpenGL extensions" + drawable.getContext().getGLExtensionsString());
                logger.debug("OpenGL debug message extensions {} ", drawable.getContext().getGLDebugMessageExtension());

                program = supplier.get();
                program.application = ApplicationGL3.this;

                program.start = System.currentTimeMillis();
                program.window = window;


                program.drawer = new DrawerGL3(drawable.getGL().getGL3(), configuration.width, configuration.height);
                try {
                    logger.debug("entering setup");
                    program.width = configuration.width;
                    program.height = configuration.height;
                    program.setup();
                    logger.debug("setup completed");
                    setupCompleted = true;
                } catch (Exception e) {
                    logger.error("caught unhandled exception");
                    printLimitedStackTrace(e);
                    //e.printStackTrace();
                    animator.stop();
                    System.exit(1);
                }
                program.start = System.currentTimeMillis();
            }

            @Override
            public void dispose(GLAutoDrawable glAutoDrawable) {

            }


            boolean inDisplay = false;

            @Override
            public void display(GLAutoDrawable drawable) {

                synchronized (this) {
                    if (inDisplay) {
                        System.out.println("warning already in display");
                        //return;
                    }
                    inDisplay = true;
                }

                if (ready) {
                    frame++;
                }


                if (ready && setupCompleted) {

                    synchronized (mouseEvents) {

                        for (MouseEvent event : mouseEvents) {
                            Vector2 position = new Vector2(event.getX(), event.getY());
                            switch (event.getEventType()) {
                                case MouseEvent.EVENT_MOUSE_MOVED:
                                    program.mouse.moved.trigger(Mouse.MouseEvent.move(position,
                                            mapMousePointers(event),
                                            event.getModifiers()));
                                    break;
                                case MouseEvent.EVENT_MOUSE_CLICKED:
                                    program.mouse.clicked.trigger(Mouse.MouseEvent.click(position, mapMousePointers(event), (int) event.getButton(), event.getModifiers()));
                                    break;
                                case MouseEvent.EVENT_MOUSE_DRAGGED:
                                    program.mouse.dragged.trigger(Mouse.MouseEvent.drag(position, mapMousePointers(event), (int) event.getButton(), event.getModifiers()));
                                    break;
                                case MouseEvent.EVENT_MOUSE_PRESSED:
                                    program.mouse.pressed.trigger(Mouse.MouseEvent.press(position, mapMousePointers(event), (int) event.getButton(), event.getModifiers()));
                                    break;
                                case MouseEvent.EVENT_MOUSE_RELEASED:
                                    program.mouse.released.trigger(Mouse.MouseEvent.release(position, mapMousePointers(event), (int) event.getButton(), event.getModifiers()));
                                    break;
                                case MouseEvent.EVENT_MOUSE_WHEEL_MOVED: {
                                    Vector3 rotation = new Vector3(event.getRotation()[0], event.getRotation()[1], event.getRotation()[2]);
                                    program.mouse.wheelMoved.trigger(Mouse.MouseEvent.wheelMove(position, mapMousePointers(event), rotation, event.getModifiers()));
                                }
                                break;
                            }
                            program.mouse.position = position;
                        }
                        mouseEvents.clear();

                    }

                    synchronized (keyEvents) {
                        for (KeyEvent event : keyEvents) {
                            switch (event.getEventType()) {

                                case KeyEvent.EVENT_KEY_PRESSED:
                                    program.keyboard.pressed.trigger(Keyboard.KeyboardEvent.press(event.getKeyChar(), event.getKeyCode(), event.getModifiers(), event.isPrintableKey()));
                                    break;
                                case KeyEvent.EVENT_KEY_RELEASED:
                                    program.keyboard.released.trigger(Keyboard.KeyboardEvent.release(event.getKeyChar(), event.getKeyCode(), event.getModifiers(), event.isPrintableKey()));
                                    break;
                            }
                        }
                        keyEvents.clear();
                    }


                    program.drawer.ortho(0, width, height, 0, -1, 1);
                    program.drawer.identity();

                    program.drawer.fill(ColorRGBa.BLACK);
                    program.drawer.stroke(ColorRGBa.WHITE);
                    program.drawer.strokeWeight(1);

                    drawable.getGL().glViewport(0, 0, width, height);
                    program.drawer.size(width, height);
                    try {
                        program.beforeDraw();


                        program.draw();
                        program.afterDraw();
                    } catch (Exception e) {
                        animator.stop();
                        e.printStackTrace();

                        printLimitedStackTrace(e);
                        System.exit(1);
                    }
                }
                synchronized (this) {
                    inDisplay = false;
                }

            }

            private List<Mouse.Pointer> mapMousePointers(MouseEvent event) {
                List<Mouse.Pointer> pointers = new ArrayList<>();
                for (int i = 0; i < event.getPointerCount(); ++i) {
                    Mouse.Pointer pointer = new Mouse.Pointer();
                    pointer.id = i;
                    pointer.idx = event.getPointerId(i);
                    pointer.position = new Vector2(event.getX(i), event.getY());
                    pointer.pressure = event.getPressure(i, true);
                    pointers.add(pointer);
                }
                return pointers;
            }

            @Override
            public void reshape(GLAutoDrawable glAutoDrawable, int x, int y, int width, int height) {

//                if (animator != null)
//                    animator.pause();

//                glAutoDrawable.getGL().glClear(GL.GL_COLOR_BUFFER_BIT);
//                glAutoDrawable.swapBuffers();
                float xScale = (float) window.getWidth() / (float) window.getSurfaceWidth();
                float yScale = (float) window.getHeight() / (float) window.getSurfaceHeight();
                window.setSurfaceScale(new float[]{xScale, yScale});

                logger.debug("window surface size: {}x{}", window.getSurfaceWidth(), window.getSurfaceHeight());
                logger.debug("window size: {}x{}", window.getWidth(), window.getHeight());
                logger.debug("reshaping window to: {}x{}", width * xScale, height * yScale);

                width = (int) (width * xScale);
                height = (int) (height * yScale);
                ApplicationGL3.this.width = width;
                ApplicationGL3.this.height = height;

                program.width = width;
                program.height = height;
                if (program.drawer != null) {
                    program.drawer.size(width, height);
                }

                program.delegates.forEach(p -> {
                    p.resize(program, program.width, program.height);
                });
//                if (animator != null)
//                    animator.resume();
            }
        });
        window.setTitle(configuration.title);


        if (configuration.width != -1 && configuration.height != -1) {
            logger.debug("setting window size to {}x{}", configuration.width, configuration.height);
            window.setSize(configuration.width, configuration.height);

            if (configuration.top != Integer.MIN_VALUE && configuration.left != Integer.MIN_VALUE) {
                window.setTopLevelPosition(configuration.left, configuration.top);
            }
        } else {
            logger.debug("setting window size to maximum");

            window.setVisible(true);
            MonitorDevice monitor = window.getMainMonitor();
            DimensionImmutable resolution = monitor.getCurrentMode().getSurfaceSize().getResolution();
            configuration.width = resolution.getWidth();
            configuration.height = resolution.getHeight();
            configuration.left = monitor.getViewport().getX();
            configuration.top = monitor.getViewport().getY();
            logger.debug("setting window size to {}x{}", configuration.width, configuration.height);


            if (configuration.top != Integer.MIN_VALUE && configuration.left != Integer.MIN_VALUE) {
                window.setTopLevelPosition(configuration.left, configuration.top);
            }
            window.setSize(configuration.width, configuration.height);

            //configuration.fullscreen = true;
        }
//        if (configuration.fullscreen && configuration.changeMode) {
//            final MonitorDevice monitor = window.getMainMonitor();
//            List<MonitorMode> monitorModes = monitor.getSupportedModes();
//            final Dimension dimension = new Dimension(configuration.width, configuration.height);
//            monitorModes = MonitorModeUtil.filterByResolution(monitorModes, dimension);
//            monitorModes = MonitorModeUtil.getHighestAvailableBpp(monitorModes);
//            monitorModes = MonitorModeUtil.getHighestAvailableRate(monitorModes);
//            monitor.setCurrentMode(monitorModes.get(0));
//        }
//

        window.setFullscreen(configuration.fullscreen);
        if (configuration.hideMouseCursor) {
            window.setPointerVisible(false);
        }

        window.addMouseListener(0, new MouseAdapter() {

            @Override
            public void mouseMoved(MouseEvent mouseEvent) {
                synchronized (mouseEvents) {
                    mouseEvents.add(mouseEvent);
                }
            }

            @Override
            public void mouseClicked(MouseEvent mouseEvent) {
                synchronized (mouseEvents) {
                    mouseEvents.add(mouseEvent);
                }
            }

            @Override
            public void mouseDragged(MouseEvent mouseEvent) {
                synchronized (mouseEvents) {
                    mouseEvents.add(mouseEvent);
                }
            }

            @Override
            public void mousePressed(MouseEvent mouseEvent) {
                synchronized (mouseEvents) {
                    mouseEvents.add(mouseEvent);
                }
            }

            @Override
            public void mouseReleased(MouseEvent mouseEvent) {
                synchronized (mouseEvents) {
                    mouseEvents.add(mouseEvent);
                }
            }

            @Override
            public void mouseWheelMoved(MouseEvent mouseEvent) {
                synchronized (mouseEvents) {
                    mouseEvents.add(mouseEvent);
                }
            }

        });
        window.addKeyListener(0, new KeyAdapter() {

            @Override
            public void keyPressed(KeyEvent keyEvent) {
                synchronized (keyEvents) {
                    keyEvents.add(keyEvent);
                }
            }

            @Override
            public void keyReleased(KeyEvent keyEvent) {
                synchronized (keyEvents) {
                    keyEvents.add(keyEvent);
                }
            }
        });
        window.requestFocus();

        window.setAutoSwapBufferMode(configuration.autoSwap);


        window.addWindowListener(new WindowAdapter() {
            @Override
            public void windowLostFocus(WindowEvent windowEvent) {
                super.windowLostFocus(windowEvent);
                if (configuration.defocusBehaviour == Configuration.DefocusBehaviour.SUSPEND) {
                    if (frame > 10) {
                        if (animator.isAnimating() && !animator.isPaused()) {
                            animator.pause();
                        }
                    }
                } else if (configuration.defocusBehaviour == Configuration.DefocusBehaviour.THROTTLE) {
                    if (frame > 10) {
                        ((ThrottledAnimator)animator).setThrottle(true);
                    }
                }
            }

            @Override
            public void windowGainedFocus(WindowEvent windowEvent) {
                if (animator instanceof ThrottledAnimator) {
                    ((ThrottledAnimator) animator).setThrottle(false);
                }

                if (animator.isPaused()) {
                    animator.resume();
                }
            }

        });

        window.addWindowListener(new WindowAdapter() {


            @Override
            public void windowDestroyNotify(WindowEvent event) {
                new Thread() {
                    @Override
                    public void run() {
                        logger.debug("stopping animator");
                        animator.setIgnoreExceptions(true);

                        animator.stop(); // stop the animator loop
                        logger.debug("exiting with error level 0");
                        System.exit(0);
                    }
                }.start();
            }
        });

        if (!(configuration.width == -1 && configuration.height == -1)) {
            logger.debug("setting window to visible");
            window.setVisible(false, true);
            window.display();

        }
        if (configuration.useFPSAnimator) {
            animator = new FPSAnimator(window, configuration.fps, true);

        } else {
            animator = new ThrottledAnimator();
            animator.add(window);


            animator.setUpdateFPSFrames(configuration.fps, null);
        }

        ready = true;
        animator.setIgnoreExceptions(false);
        animator.setUncaughtExceptionHandler((animator, drawable, cause) -> {
            logger.error("unhandled exception in animator", cause);
            printLimitedStackTrace(cause);
        });
        logger.debug("starting animator");
        animator.start();


    }

    private void printLimitedStackTrace(Throwable e) {
        System.err.println("Exception in thread \"RNDR\" " + e.getClass().getName() + ": " + e.getMessage());
        boolean foundSketch = false;
        for (StackTraceElement z : e.getStackTrace()) {
            try {
                if (foundSketch && !Program.class.isAssignableFrom(Class.forName(z.getClassName()))) {
                    break;
                }

                System.err.println("\tat " + z.getClassName() + "." + z.getMethodName() + "(" + z.getFileName() + ":" + z.getLineNumber() + ")");
                if (Program.class.isAssignableFrom(Class.forName(z.getClassName()))) {
                    foundSketch = true;
                }


            } catch (ClassNotFoundException e1) {
                e1.printStackTrace();
            }
        }
    }


    @Override
    public void quit() {
        animator.stop();
        System.exit(0);

    }

    public void error(int errorLevel) {
        animator.stop();
        System.exit(errorLevel);
    }

    @Override
    public DrawThread drawThread(BiConsumer<DrawThread, Drawer> toExecute) {
        logger.debug("spawning draw thread");
        DrawThread drawerThread = new DrawThreadGL3(toExecute);
        new Thread(drawerThread).start();
        return drawerThread;
    }

    @Override
    public void pause() {
        if (animator != null)
            animator.pause();

    }


    @Override
    public void resume() {
        if (animator != null)
            animator.resume();
    }
}
