package net.lustlab.rndr;

public class RNDRException extends RuntimeException{

	private static final long serialVersionUID = 1L;

    public RNDRException(Exception e) {
        super(e);
    }
	public RNDRException(String what) {
		super(what);
	}

    public RNDRException(String what, Exception e) {
        super(what, e);
    }
}
