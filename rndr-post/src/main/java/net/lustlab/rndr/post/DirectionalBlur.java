package net.lustlab.rndr.post;

import net.lustlab.rndr.buffers.ColorBuffer;
import net.lustlab.rndr.math.Vector2;
import net.lustlab.rndr.shaders.FragmentShader;

import static net.lustlab.rndr.math.Linear.vector2;

/**
 * Created by edwin on 09/10/16.
 */
public class DirectionalBlur extends Filter {




    public DirectionalBlur() {
        super(FragmentShader.fromUrl("cp:shaders/gl3/post/approximate-gaussian-blur.frag"));
        window(5);
        sigma(3);
        direction(new Vector2(1,0));
    }

    public DirectionalBlur sigma(double sigma) {
        parameter("sigma", (float) sigma);
        return this;
    }

    public DirectionalBlur window(int window) {
        parameter("window", window);
        return this;
    }

    public DirectionalBlur direction(Vector2 direction) {
        parameter("blurDirection", direction);
        return this;
    }



}
