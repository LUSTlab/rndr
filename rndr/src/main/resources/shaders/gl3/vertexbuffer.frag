#version 330

layout(origin_upper_left) in vec4 gl_FragCoord;
${outputs! }
${uniforms! }


uniform vec4 fill;
uniform vec4 stroke;

in vec3 v_modelPosition;
in vec3 v_viewPosition;
in mat4 v_normalMatrix;
out vec4 o_color;

${varyingIn! }

flat in int v_instance;
<#if attributes?contains("vec3 normal;")>
    in vec3 v_modelNormal;
</#if>

${preamble! }


void main(void) {
    <#if attributes?contains("vec3 normal;")>
        vec3 normal = normalize(v_normal);
        vec3 modelNormal = normalize(v_modelNormal);
    <#else>
        vec3 normal = vec3(0,0,1);
    </#if>


    int instance = v_instance;
    // -- shade style helpers
    vec2 screenPosition = gl_FragCoord.xy;
    vec3 boundsPosition = vec3(0);

    vec4 x_fill = fill;
    vec4 x_stroke = stroke;

    { ${fillTransform! } }
    { ${strokeTransform! } }
    vec4 x_color = x_fill;

    o_color = x_color;
}