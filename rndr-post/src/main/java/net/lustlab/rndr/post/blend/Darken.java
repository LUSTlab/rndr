package net.lustlab.rndr.post.blend;

import net.lustlab.rndr.shaders.FragmentShader;

public class Darken extends BlendFilter {
    public Darken() {
        super(FragmentShader.fromUrl("cp:shaders/gl3/post/blend/darken.frag"));
    }
}
