package net.lustlab.rndr.shape;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Stack;

/**
 * Composition class. A composition represents an hierarchical collection of shapes.
 */
public class Composition implements Serializable {

    /**
     * The root node
     */
    public CompositionNode root = new CompositionNode();



    /**
     * Returns a deep copy of the Composition
     */
    public Composition copy() {
        Composition copy = new Composition();
        copy.root = root.copy();
        return copy;
    }

    public CompositionNode node(String name) {
        Stack<CompositionNode> stack = new Stack<CompositionNode>();

        stack.push(root);

        while (!stack.empty()) {
            CompositionNode node = stack.pop();

            if (name.equals(node.id)) {
                return node;
            }

            node.children().forEach(stack::push);
        }
        return null;
    }

    /**
     * Returns the shape with given name
     * @param name
     * @return
     */
    public Shape shape(String name) {
        Stack<CompositionNode> stack = new Stack<CompositionNode>();

        stack.push(root);

        while (!stack.empty()) {
            CompositionNode node = stack.pop();


            if (name.equals(node.id) && node.shape != null) {
                return node.shape;
            }

            node.children().forEach(stack::push);
        }
        return null;
    }

    /**
     * Returns a list of all shapes in the composition
     * @return list containing all shapes in the composition
     */
    public List<Shape> shapes() {
        return shapes(".*");
    }

    public List<Shape> shapes(String regex) {
        List<Shape> result = new ArrayList<>();
        Stack<CompositionNode> stack = new Stack<>();

        stack.push(root);

        while (!stack.empty()) {
            CompositionNode node = stack.pop();

            String shapeId = node.id != null ? node.id : "";

            if (shapeId.matches(regex) && node.shape != null) {
                result.add(node.shape);
            }
            node.children().forEach(stack::push);
        }
        Collections.reverse(result);
        return result;
    }


    public List<CompositionNode> shapeNodes(String regex) {
        List<CompositionNode> result = new ArrayList<>();
        Stack<CompositionNode> stack = new Stack<>();

        stack.push(root);

        while (!stack.empty()) {
            CompositionNode node = stack.pop();

            String shapeId = node.id != null ? node.id : "";

            if (shapeId.matches(regex) && node.shape != null) {
                result.add(node);
            }
            node.children().forEach(stack::push);
        }
        Collections.reverse(result);
        return result;
    }

    private void outputPolygon(CompositionNode node, Figure figure, double distanceTolerance) {

        if (node.shape != null) {
            figure.polygons.add(node.shape.polygon());
        }
        for (CompositionNode child: node.children()) {
            outputPolygon(child, figure, distanceTolerance);
        }
    }

    public Figure figure() {
        return figure(-1);
    }

    public Figure figure(double distanceTolerance) {
        Figure figure = new Figure();

        outputPolygon(root, figure, distanceTolerance);
        return figure;
    }



}
