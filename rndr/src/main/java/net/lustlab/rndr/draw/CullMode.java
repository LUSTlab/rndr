package net.lustlab.rndr.draw;

import com.jogamp.opengl.GL;

public enum CullMode {
    NONE,
    FRONT,
    BACK,
    FRONT_AND_BACK;

    public int toGLCullFace() {
        switch (this) {
            case NONE:
                throw new RuntimeException("no gl cull face for NONE");
            case FRONT:
                return GL.GL_FRONT;
            case BACK:
                return GL.GL_BACK;
            case FRONT_AND_BACK:
                return GL.GL_FRONT_AND_BACK;
            default:
                throw new RuntimeException("unknown cull mode" + this);
        }
    }

}
