package net.lustlab.rndr.post.blend;

import net.lustlab.rndr.shaders.FragmentShader;

public class ColorDodge extends BlendFilter {
    public ColorDodge() {
        super(FragmentShader.fromUrl("cp:shaders/gl3/post/blend/color-dodge.frag"));
    }
}
