#version 330

layout(origin_upper_left) in vec4 gl_FragCoord;

// -- uniforms --
uniform sampler2D colors0;
uniform float uOffset;
uniform mat4 colorMatrix;
uniform vec4 tint;
uniform vec4 fill;
uniform vec4 stroke;

${uniforms! }

${preamble! }

// -- varying --
in vec2 v_texCoord0;

// -- outputs --
out vec4 o_color;

void main() {

    // -- shade style helper variables
    int instance = 0;
    vec2 screenPosition = gl_FragCoord.xy;
    vec4 c = ffmpegTexture2D(colors0, v_texCoord0);

    vec4 x_tint = tint;
    {
        ${tintTransform! }
    }

    vec4 color = colorMatrix * (vec4(c.rgb, 1.0) * x_tint);

    o_color = vec4(color);

}