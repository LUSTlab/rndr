package net.lustlab.rndr.buffers;

import com.jogamp.opengl.GL;
import com.jogamp.opengl.GL3;
import net.lustlab.rndr.RNDRException;

public enum ColorType {
    UINT8,
    UINT16,
    FLOAT16,
    FLOAT32;

    /**
     * Returns the size of a single component when stored in this ColorType
     * @return the size of a single component in bytes
     */
    public int componentSizeInBytes() {
        switch(this) {
            case UINT8:
                return 1;
            case UINT16:
                return 2;
            case FLOAT16:
                return 2;
            case FLOAT32:
                return 4;
            default:
                throw new RuntimeException("unsupported type");
        }
    }

    public static ColorType fromGLFormat(int format) {
        switch (format) {
            case GL.GL_RGBA8:
            case GL.GL_RGB8:
            case GL.GL_RG8:
            case GL.GL_R8:
            case GL.GL_SRGB8_ALPHA8:
            case GL.GL_SRGB:
                return UINT8;
            case GL.GL_RGBA16F:
            case GL.GL_RGB16F:
            case GL.GL_RG16F:
            case GL.GL_R16F:
                return FLOAT16;

            case GL.GL_RGBA32F:
            case GL.GL_RGB32F:
            case GL.GL_RG32F:
            case GL.GL_R32F:
                return FLOAT32;

            case GL3.GL_R16:
                return UINT16;

            default:
                throw new RNDRException("unknown format: " + format);

        }
    }

    public int toGLType() {
        switch(this) {
            case UINT8:
                return GL.GL_UNSIGNED_BYTE;
            case UINT16:
                return GL.GL_UNSIGNED_SHORT;

            case FLOAT32:
                return GL.GL_FLOAT;
            case FLOAT16:
                return GL.GL_HALF_FLOAT;
            default:
                throw new RNDRException("unknown type: " + this);
        }
    }

    public static ColorType fromGLType(int type) {
        switch(type) {
            case GL.GL_UNSIGNED_BYTE:
                return UINT8;
            case GL.GL_UNSIGNED_SHORT:
                return UINT16;
            case GL.GL_FLOAT:
                return FLOAT32;
            case GL.GL_HALF_FLOAT:
                return FLOAT16;
            default:
                throw new RNDRException("unknown type " + type);
        }
    }

}
