package net.lustlab.rndr.math.tests;

import net.lustlab.rndr.math.Matrix44;
import net.lustlab.rndr.math.Transforms;
import net.lustlab.rndr.math.Vector3;
import net.lustlab.rndr.math.Vector4;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class TestMatrix44 {

     static final double epsilon = 0.00001;

    @Test
    public void testDeterminant() {


        assertEquals(1.0, Matrix44.IDENTITY.determinant(), epsilon);
        assertEquals(16.0, Matrix44.IDENTITY.scale(2).determinant(), epsilon);

        assertEquals(1.0, Transforms.rotateX(10).determinant(), epsilon);
        assertEquals(1.0, Transforms.rotateY(10).determinant(), epsilon);
        assertEquals(1.0, Transforms.rotateZ(10).determinant(), epsilon);

        assertEquals(1.0, Transforms.translate(new Vector3(30,30,30)).determinant(), epsilon);
    }

    @Test
    public void testInverse() {

        Matrix44 inverseIdentity = Matrix44.IDENTITY.inverse();
        assertEquals(4.0, inverseIdentity.sum(), epsilon);
        assertEquals(0.0, inverseIdentity.minus(Matrix44.IDENTITY).sum(), epsilon);

        Matrix44 rot = Transforms.rotateY(1);
        Matrix44 irot = rot.inverse();
        assertEquals(0.0, rot.multiply(irot).minus(Matrix44.IDENTITY).sum(), epsilon);

        Matrix44 trans = Transforms.translate(new Vector3(1,5,4));
        Matrix44 itrans = trans.inverse();
        assertEquals(0.0, trans.multiply(itrans).minus(Matrix44.IDENTITY).sum(), epsilon);

    }

	@Test
	public void testMultiplication() {

		Matrix44 a = new Matrix44(1);
		Matrix44 b = a.multiply(a);



		assertEquals (1.0, b.m00, epsilon);
		assertEquals (0.0, b.m01, epsilon);
		assertEquals (0.0, b.m02 ,epsilon);
		assertEquals (0.0, b.m03,epsilon);

		assertEquals (0.0, b.m10, epsilon);
		assertEquals (1.0, b.m11, epsilon);
		assertEquals (0.0, b.m12, epsilon);
		assertEquals (0.0, b.m13, epsilon);

		assertEquals (0.0, b.m20, epsilon);
		assertEquals (0.0, b.m21, epsilon);
		assertEquals (1.0, b.m22, epsilon);
		assertEquals (0.0, b.m23, epsilon);

		assertEquals (0.0, b.m30, epsilon);
		assertEquals (0.0, b.m31, epsilon);
		assertEquals (0.0, b.m32, epsilon);
		assertEquals (1.0, b.m33, epsilon);

	}

    @Test
    public void testTranspose() {

        Matrix44 a = new Matrix44(
                0, 1, 2, 3,
                4, 5, 6, 7,
                8, 9, 10, 11,
                12, 13, 14, 15
        );

        Matrix44 b = a.transpose();
        assertEquals(a.m00, b.m00, epsilon);
        assertEquals(a.m01, b.m10, epsilon);
        assertEquals(a.m02, b.m20, epsilon);
        assertEquals(a.m03, b.m30, epsilon);
        assertEquals(a.m10, b.m01, epsilon);
        assertEquals(a.m11, b.m11, epsilon);
        assertEquals(a.m12, b.m21, epsilon);
        assertEquals(a.m13, b.m31, epsilon);
        assertEquals(a.m20, b.m02, epsilon);
        assertEquals(a.m21, b.m12, epsilon);
        assertEquals(a.m22, b.m22, epsilon);
        assertEquals(a.m23, b.m32, epsilon);
        assertEquals(a.m30, b.m03, epsilon);
        assertEquals(a.m31, b.m13, epsilon);
        assertEquals(a.m32, b.m23, epsilon);
        assertEquals(a.m33, b.m33, epsilon);
    }


	@Test
	public void testMultiplication2() {

		Matrix44 a = new Matrix44(1,2,3,4);
		Matrix44 b = a.multiply(a);

		assertEquals (1.0, b.m00, epsilon);
		assertEquals (0.0, b.m01, epsilon);
		assertEquals (0.0, b.m02, epsilon);
		assertEquals (0.0, b.m03, epsilon);

		assertEquals (0.0, b.m10, epsilon);
		assertEquals (4.0, b.m11, epsilon);
		assertEquals (0.0, b.m12, epsilon);
		assertEquals (0.0, b.m13, epsilon);

		assertEquals (0.0, b.m20, epsilon);
		assertEquals (0.0, b.m21, epsilon);
		assertEquals (9.0, b.m22, epsilon);
		assertEquals (0.0, b.m23, epsilon);

		assertEquals (0.0, b.m30, epsilon);
		assertEquals (0.0, b.m31, epsilon);
		assertEquals (0.0, b.m32, epsilon);
		assertEquals (16.0, b.m33, epsilon);

	}

    @Test
    public void testMultiplication3() {
        Matrix44 a = Transforms.translate(new Vector3(1,2,3));
        Matrix44 b = Matrix44.IDENTITY;

        Matrix44 ab = a.multiply(b); // a * b

        assertEquals (1.0, ab.m00, epsilon);
        assertEquals (0.0, ab.m01, epsilon);
        assertEquals (0.0, ab.m02, epsilon);
        assertEquals (0.0, ab.m03, epsilon);

        assertEquals (0.0, ab.m10, epsilon);
        assertEquals (1.0, ab.m11, epsilon);
        assertEquals (0.0, ab.m12, epsilon);
        assertEquals (0.0, ab.m13, epsilon);

        assertEquals (0.0, ab.m20, epsilon);
        assertEquals (0.0, ab.m21, epsilon);
        assertEquals (1.0, ab.m22, epsilon);
        assertEquals (0.0, ab.m23, epsilon);

        assertEquals (1.0, ab.m30, epsilon);
        assertEquals (2.0, ab.m31, epsilon);
        assertEquals (3.0, ab.m32, epsilon);
        assertEquals (1.0, ab.m33, epsilon);
    }

    @Test
    public void testMultiplication4() {
        Matrix44 a = new Matrix44(1,1,1,1,0,0,0,0,0,0,0,0,0,0,0,0);


        Matrix44 b = new Matrix44(1,0,0,0,1,0,0,0,1,0,0,0,1,0,0,0);

        Matrix44 ab = a.multiply(b);
        Matrix44 ba = b.multiply(a);

        assertEquals(ab.m00, 4.0, epsilon);
        assertEquals(ab.m01, 0.0, epsilon);
        assertEquals(ab.m02, 0.0, epsilon);
        assertEquals(ab.m03, 0.0, epsilon);
        assertEquals(ab.m10, 0.0, epsilon);
        assertEquals(ab.m11, 0.0, epsilon);
        assertEquals(ab.m12, 0.0, epsilon);
        assertEquals(ab.m13, 0.0, epsilon);
        assertEquals(ab.m20, 0.0, epsilon);
        assertEquals(ab.m21, 0.0, epsilon);
        assertEquals(ab.m22, 0.0, epsilon);
        assertEquals(ab.m23, 0.0, epsilon);
        assertEquals(ab.m30, 0.0, epsilon);
        assertEquals(ab.m31, 0.0, epsilon);
        assertEquals(ab.m32, 0.0, epsilon);
        assertEquals(ab.m33, 0.0, epsilon);

        assertEquals(ba.m00, 1.0, epsilon);
        assertEquals(ba.m01, 1.0, epsilon);
        assertEquals(ba.m02, 1.0, epsilon);
        assertEquals(ba.m03, 1.0, epsilon);
        assertEquals(ba.m10, 1.0, epsilon);
        assertEquals(ba.m11, 1.0, epsilon);
        assertEquals(ba.m12, 1.0, epsilon);
        assertEquals(ba.m13, 1.0, epsilon);
        assertEquals(ba.m20, 1.0, epsilon);
        assertEquals(ba.m21, 1.0, epsilon);
        assertEquals(ba.m22, 1.0, epsilon);
        assertEquals(ba.m23, 1.0, epsilon);
        assertEquals(ba.m30, 1.0, epsilon);
        assertEquals(ba.m31, 1.0, epsilon);
        assertEquals(ba.m32, 1.0, epsilon);
        assertEquals(ba.m33, 1.0, epsilon);


    }



    @Test
    public void testAdd() {
        Matrix44 a = new Matrix44(1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16);
        Matrix44 aa = a.plus(a);

        assertEquals(a.m00*2, aa.m00, epsilon);
        assertEquals(a.m01*2, aa.m01, epsilon);
        assertEquals(a.m02*2, aa.m02, epsilon);
        assertEquals(a.m03*2, aa.m03, epsilon);
        assertEquals(a.m10*2, aa.m10, epsilon);
        assertEquals(a.m11*2, aa.m11, epsilon);
        assertEquals(a.m12*2, aa.m12, epsilon);
        assertEquals(a.m13*2, aa.m13, epsilon);
        assertEquals(a.m20*2, aa.m20, epsilon);
        assertEquals(a.m21*2, aa.m21, epsilon);
        assertEquals(a.m22*2, aa.m22, epsilon);
        assertEquals(a.m23*2, aa.m23, epsilon);
        assertEquals(a.m30*2, aa.m30, epsilon);
        assertEquals(a.m31*2, aa.m31, epsilon);
        assertEquals(a.m32*2, aa.m32, epsilon);
        assertEquals(a.m33*2, aa.m33, epsilon);
    }

    @Test
    public void testSub() {
        Matrix44 a = new Matrix44(1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16);
        Matrix44 aa = a.minus(a);

        assertEquals(0, aa.m00, epsilon);
        assertEquals(0, aa.m01, epsilon);
        assertEquals(0, aa.m02, epsilon);
        assertEquals(0, aa.m03, epsilon);
        assertEquals(0, aa.m10, epsilon);
        assertEquals(0, aa.m11, epsilon);
        assertEquals(0, aa.m12, epsilon);
        assertEquals(0, aa.m13, epsilon);
        assertEquals(0, aa.m20, epsilon);
        assertEquals(0, aa.m21, epsilon);
        assertEquals(0, aa.m22, epsilon);
        assertEquals(0, aa.m23, epsilon);
        assertEquals(0, aa.m30, epsilon);
        assertEquals(0, aa.m31, epsilon);
        assertEquals(0, aa.m32, epsilon);
        assertEquals(0, aa.m33, epsilon);
    }
    @Test
    public void testColumns() {
        Matrix44 a = new Matrix44(1,2,3,4,
                                  5,6,7,8,
                                  9,10,11,12,
                                  13,14,15,16);
        Vector4 c0 = a.getColumn(0);

        assertEquals(c0.x, 1, epsilon);
        assertEquals(c0.y, 5, epsilon);
        assertEquals(c0.z, 9, epsilon);
        assertEquals(c0.w, 13, epsilon);

        Vector4 c1 = a.getColumn(1);

        assertEquals(c1.x, 2, epsilon);
        assertEquals(c1.y, 6, epsilon);
        assertEquals(c1.z, 10, epsilon);
        assertEquals(c1.w, 14, epsilon);


        Vector4 c2 = a.getColumn(2);

        assertEquals(c2.x, 3, epsilon);
        assertEquals(c2.y, 7, epsilon);
        assertEquals(c2.z, 11, epsilon);
        assertEquals(c2.w, 15, epsilon);

        Vector4 c3 = a.getColumn(3);

        assertEquals(c3.x, 4, epsilon);
        assertEquals(c3.y, 8, epsilon);
        assertEquals(c3.z, 12, epsilon);
        assertEquals(c3.w, 16, epsilon);

    }

    @Test
    public void testColumns2() {


        Matrix44 a = Matrix44.fromColumnVectors(
                new Vector4(1,5,9,13),
                new Vector4(2,6,10,14),
                new Vector4(3,7,11,15),
                new Vector4(4,8,12,16));


        Vector4 c0 = a.getColumn(0);

        assertEquals(c0.x, 1, epsilon);
        assertEquals(c0.y, 5, epsilon);
        assertEquals(c0.z, 9, epsilon);
        assertEquals(c0.w, 13, epsilon);

        Vector4 c1 = a.getColumn(1);

        assertEquals(c1.x, 2, epsilon);
        assertEquals(c1.y, 6, epsilon);
        assertEquals(c1.z, 10, epsilon);
        assertEquals(c1.w, 14, epsilon);


        Vector4 c2 = a.getColumn(2);

        assertEquals(c2.x, 3, epsilon);
        assertEquals(c2.y, 7, epsilon);
        assertEquals(c2.z, 11, epsilon);
        assertEquals(c2.w, 15, epsilon);

        Vector4 c3 = a.getColumn(3);

        assertEquals(c3.x, 4, epsilon);
        assertEquals(c3.y, 8, epsilon);
        assertEquals(c3.z, 12, epsilon);
        assertEquals(c3.w, 16, epsilon);

    }


}

