#version 330

${uniforms! }

uniform vec4 stroke;

uniform float strokeMult;
uniform float strokeThr;

uniform sampler2D tex;

in vec2 ftcoord;
in vec2 fpos;
in float v_offset;
out vec4 outColor;

float strokeMask() {
	return min(1.0, (1.0-abs(ftcoord.x*2.0-1.0))*strokeMult) * min(1.0, ftcoord.y);
}

void main(void) {

    float strokeOffset = v_offset;
	float strokeAlpha = strokeMask();

    vec4 x_stroke = stroke;
    { ${strokeTransform! } }

    vec4 color = x_stroke * vec4(1,1,1,strokeAlpha);
    vec4 result = color;

    if (strokeAlpha < strokeThr) {
	    discard;
	}
	outColor = result;

}