package net.lustlab.rndr.math;

import java.nio.FloatBuffer;

/**
 * Created by voorbeeld on 4/26/14.
 */
public class Matrix33 {

    final public double m00, m10, m20;
    final public double m01, m11, m21;
    final public double m02, m12, m22;

    final public static Matrix33 IDENTITY = new Matrix33(1,0,0, 0,1,0, 0,0,1);


    public Matrix33() {
        m00 = m10 = m20 = .0;
        m01 = m11 = m21 = .0;
        m02 = m12 = m22 = .0;

    }

    public Matrix33(double x00, double x10, double x20,
                    double x01, double x11, double x21,
                    double x02, double x12, double x22) {

        m00 = x00; m10 = x10; m20 = x20;
        m01 = x01; m11 = x11; m21 = x21;
        m02 = x02; m12 = x12; m22 = x22;
    }

    public Matrix33(final double diagonalValue) {
        m00 = m11 = m22 = diagonalValue;
        m10 = m20 = .0;
        m01 = m21 = .0;
        m02 = m12 = .0;
    }

    public Matrix33(final double dv0, final double dv1, final double dv2) {
        m00 = dv0; m11 = dv1; m22 =dv2;
        m01 = m02 = .0;
        m10 = m12 = .0;
        m20 = m21 = .0;
    }

    public Matrix33(final Vector3 col0, final Vector3 col1, final Vector3 col2) {
        this.m00 = col0.x; this.m10 = col1.x; this.m20 = col2.x;
        this.m01 = col0.y; this.m11 = col1.y; this.m21 = col2.y;
        this.m02 = col0.z; this.m12 = col1.z; this.m22 = col2.z;
    }

    public double determinant() {
        return m00 * (m11 * m22 - m12 * m21) - m01 * (m10 * m22 - m12 * m20) + m02 * (m10 * m21 - m11 * m20);
    }

    public Matrix33 inverse() {

        double det = determinant();

        if (det != 0) {

        double idet = 1.0 / det;

        double d00 = (m11 * m22 - m12 * m21) * idet;
        double d10 = (m20 * m12 - m22 * m10) * idet;
        double d20 = (m10 * m21 - m11 * m20) * idet;

        double d01 = (m21 * m02 - m22 * m01) * idet;
        double d11 = (m00 * m22 - m02 * m20) * idet;
        double d21 = (m20 * m01 - m21 * m00) * idet;

        double d02 = (m01 * m12 - m02 * m11) * idet;
        double d12 = (m10 * m02 - m12 * m00) * idet;
        double d22 = (m00 * m11 - m01 * m10) * idet;

        return new Matrix33(
                d00, d10, d20,
                d01, d11, d21,
                d02, d12, d22);
        }
        else throw new RuntimeException("determinant == 0");
    }

    public Matrix33 multiply(Matrix33 mat) {

        return new Matrix33(
                this.m00 * mat.m00 + this.m10 * mat.m01 + this.m20 * mat.m02,
                this.m00 * mat.m10 + this.m10 * mat.m11 + this.m20 * mat.m12,
                this.m00 * mat.m20 + this.m10 * mat.m21 + this.m20 * mat.m22,

                this.m01 * mat.m00 + this.m11 * mat.m01 + this.m21 * mat.m02,
                this.m01 * mat.m10 + this.m11 * mat.m11 + this.m21 * mat.m12,
                this.m01 * mat.m20 + this.m11 * mat.m21 + this.m21 * mat.m22,

                this.m02 * mat.m00 + this.m12 * mat.m01 + this.m22 * mat.m02,
                this.m02 * mat.m10 + this.m12 * mat.m11 + this.m22 * mat.m12,
                this.m02 * mat.m20 + this.m12 * mat.m21 + this.m22 * mat.m22);
    }

    public Vector3 column(int column) {
        if (column == 0) {
            return new Vector3(m00, m01, m02);
        }
        if (column == 1) {
            return new Vector3(m10, m11, m12);
        }
        if (column == 2) {
            return new Vector3(m20, m21, m22);
        }
        else throw new RuntimeException("invalid column");
    }


    public Matrix33 transpose() {
        return new Matrix33(
                m00, m01, m02,
                m10, m11, m12,
                m20, m21, m22);
    }

    @Override
    public String toString() {
        return "Matrix33{" +
                "m00=" + m00 +
                ", m10=" + m10 +
                ", m20=" + m20 +
                "\nm01=" + m01 +
                ", m11=" + m11 +
                ", m21=" + m21 +
                "\nm02=" + m02 +
                ", m12=" + m12 +
                ", m22=" + m22 +
                '}';
    }

    public Matrix33 toFloatBuffer(FloatBuffer fb) {
        fb.put((float)m00);
        fb.put((float)m01);
        fb.put((float)m02);

        fb.put((float)m10);
        fb.put((float)m11);
        fb.put((float)m12);

        fb.put((float)m20);
        fb.put((float)m21);
        fb.put((float)m22);

        return this;

    }

}
