package net.lustlab.rndr.application.internal;

import java.io.File;

public class WindowsPlatformDriver implements PlatformDriver {

    public static String randomID;

    public WindowsPlatformDriver() {
        char[] alphabet = new char[] { 'a','b','d','e','f','g','h','i','j','k','l','m','n','o','p','q','r','s','t','u','v','w','x','y','z'};

        String id = "";
        for (int i = 0; i < 8; ++i) {
            id += alphabet[i];
        }

        randomID = id;
    }

    @Override
    public File temporaryDirectory() {
        String directoryName = System.getProperty("java.io.tmpdir") + "RNDR-"+randomID;
        File file = new File(directoryName);
        if (!file.exists()) {
            file.mkdirs();
        }
        return file;
    }

    @Override
    public File cacheDirectory(String programName) {
        File f = new File(".\\cache");
        if (!f.exists()) {
            boolean result = f.mkdirs();
            if (!result) {
                throw new RuntimeException("could not create cache directory");
            }
        }
        return f;
    }

    @Override
    public File supportDirectory(String programName) {
        return new File(".");
    }

}
