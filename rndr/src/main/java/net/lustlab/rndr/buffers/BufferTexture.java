package net.lustlab.rndr.buffers;

import com.jogamp.opengl.GL;
import com.jogamp.opengl.GL3;
import com.jogamp.opengl.GLContext;
import net.lustlab.rndr.gl.GLError;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.nio.ByteBuffer;

public class BufferTexture {

    private final ColorFormat format;
    private final ColorType type;
    public int buffer;
    public int texture;
    GL3 gl3;
    int elementCount;

    BufferTextureShadow shadow;
    boolean bound = false;

    static Logger logger = LogManager.getLogger(BufferTexture.class);

    @Override
    public String toString() {
        return "BufferTexture{" +
                "format=" + format +
                ", type=" + type +
                ", elementCount=" + elementCount +
                ", destroyed=" + destroyed +
                '}';
    }

    public int elementCount() {
        return elementCount;
    }

    @Deprecated
    public static BufferTexture createStatic(GL3 gl3, ByteBuffer buffer, int size, int format) {
        throw new RuntimeException("not implemented");
//        int[] buffers = new int[1];
//        gl3.glGenBuffers(1, buffers, 0);
//
//        gl3.glBindBuffer(GL3.GL_TEXTURE_BUFFER, buffers[0]);
//        gl3.glBufferData(GL3.GL_TEXTURE_BUFFER, buffer.remaining(), buffer, GL3.GL_STATIC_DRAW);
//
//        int[] textures = new int[1];
//        gl3.glGenTextures(1, textures, 0);
//        gl3.glBindTexture(GL3.GL_TEXTURE_BUFFER, textures[0]);
//        gl3.glTexBuffer(GL3.GL_TEXTURE_BUFFER, format, buffers[0]);
//
//        gl3.glBindBuffer(GL3.GL_TEXTURE_BUFFER, 0);
//
//        return new BufferTexture(gl3, buffers[0], textures[0], ColorFormat.RGBA, ColorType.FLOAT32);
    }

    public static BufferTexture createStream(ColorFormat format, ColorType type, int elementCount) {

        logger.debug("creating BufferTexture {}, {} with {} elements", format, type, elementCount);
        int sizeInBytes = format.componentCount() * type.componentSizeInBytes() * elementCount;

        GL3 gl3 = GLContext.getCurrentGL().getGL3();
        int[] buffers = new int[1];
        gl3.glGenBuffers(1, buffers, 0);

        gl3.glBindBuffer(GL3.GL_TEXTURE_BUFFER, buffers[0]);
        gl3.glBufferData(GL3.GL_TEXTURE_BUFFER, sizeInBytes, null, GL3.GL_STREAM_DRAW);

        int[] textures = new int[1];
        gl3.glGenTextures(1, textures, 0);
        gl3.glBindTexture(GL3.GL_TEXTURE_BUFFER, textures[0]);
        gl3.glTexBuffer(GL3.GL_TEXTURE_BUFFER, ColorBuffer.toGLFormat(format, type), buffers[0]);
        gl3.glBindBuffer(GL3.GL_TEXTURE_BUFFER, 0);

        return new BufferTexture(gl3, buffers[0], textures[0], format, type, elementCount);
    }

    public static BufferTexture fromUrl(URL url) {

        int format = 0;

        if (url.getFile().endsWith(".floats")) {
            format = GL3.GL_FLOAT;
        } else if (url.getFile().endsWith(".ints")) {
            format = GL3.GL_INT;
        } else if (url.getFile().endsWith(".uints")) {
            format = GL3.GL_UNSIGNED_INT;
        } else if (url.getFile().endsWith(".bytes")) {
            format = GL3.GL_BYTE;
        } else if (url.getFile().endsWith(".ubytes")) {
            format = GL3.GL_UNSIGNED_BYTE;
        }


        final int bufferSize= 16384;
        byte[] buffer = new byte[bufferSize];

        try (InputStream is = url.openStream()) {
            ByteArrayOutputStream baos = new ByteArrayOutputStream();

            while (true) {
                int read = is.read(buffer);
                baos.write(buffer, 0, read);

                if (read < bufferSize) {
                    break;
                }
            }

            return BufferTexture.createStatic(GLContext.getCurrentGL().getGL3(), ByteBuffer.wrap(baos.toByteArray()), 0, format);

        } catch (IOException e) {
            throw new RuntimeException("failed to load BufferTexture from url");
        }

    }

    public BufferTexture write(ByteBuffer input) {
        write(input, input.remaining() / elementSize());
        return this;
    }

    public BufferTexture write(ByteBuffer input, int elements) {
        write(GLContext.getCurrentGL().getGL3(), input,  elementSize() * elements);
        GLError.debug(gl3);
        return this;
    }


    public BufferTexture write(ByteBuffer input, int elementCount, int targetElement) {
        gl3.glBindBuffer(GL3.GL_TEXTURE_BUFFER, buffer);
        GLError.debug(gl3);

        gl3.glBufferSubData(GL3.GL_TEXTURE_BUFFER, targetElement * elementSize(), elementCount * elementSize(), input);
        GLError.debug(gl3);

        gl3.glBindBuffer(GL3.GL_TEXTURE_BUFFER, 0);
        GLError.debug(gl3);
        return this;
    }

    BufferTexture write(GL3 gl3, ByteBuffer input, int size) {
        GLError.debug(gl3);

        gl3.glBindBuffer(GL3.GL_TEXTURE_BUFFER, buffer);
        GLError.debug(gl3);

        gl3.glBufferSubData(GL3.GL_TEXTURE_BUFFER, 0, size, input);
        GLError.debug(gl3);

        gl3.glBindBuffer(GL3.GL_TEXTURE_BUFFER, 0);
        GLError.debug(gl3);

        return this;
    }



    BufferTexture(GL3 gl3, int buffer, int texture, ColorFormat format, ColorType type, int elementCount) {
        this.buffer = buffer;
        this.texture = texture;
        this.gl3 = gl3;
        this.format = format;
        this.type = type;
        this.elementCount = elementCount;
    }

    public void bindTexture() {
        if (!bound) {
            gl3.glBindTexture(GL3.GL_TEXTURE_BUFFER, texture);
            //bound = true;
        }
        else {
            throw new RuntimeException("already bound");
        }
    }

    public void bindTexture(int textureUnit) {
        if (!bound) {
            gl3.glActiveTexture(GL.GL_TEXTURE0 + textureUnit);

            gl3.glBindTexture(GL3.GL_TEXTURE_BUFFER, texture);
            //bound = true;
        }
        else {
            throw new RuntimeException("already bound");
        }
    }

    public void unbindTexture() {
        if (true || bound) {
            gl3.glBindTexture(GL3.GL_TEXTURE_BUFFER, 0);
            bound = false;
        }
        else {
            throw new RuntimeException("texture not bound");
        }
    }

    boolean destroyed;
    public void destroy() {
        if (!destroyed) {
            logger.debug("destroying BufferTexture");
            gl3.glDeleteTextures(1, new int[]{texture}, 0);
            destroyed =true;
        }
    }

    public ColorFormat format() {
        return format;
    }

    public ColorType type() {
        return type;
    }

    public int elementSize() {
        return format.componentCount() * type.componentSizeInBytes();
    }

    public BufferTextureShadow shadow() {
        if (shadow == null) {
            shadow = new BufferTextureShadow(this);
        }
        return shadow;
    }

}
