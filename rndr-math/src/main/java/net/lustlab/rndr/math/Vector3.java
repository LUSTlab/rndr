package net.lustlab.rndr.math;

import java.io.Serializable;

public class Vector3 implements Serializable {

    static final long serialVersionUID = 1;

    public static Vector3 Zero = new Vector3();
	public static Vector3 UnitX = new Vector3(1, 0, 0);
	public static Vector3 UnitY = new Vector3(0, 1, 0);
	public static Vector3 UnitZ = new Vector3(0, 0, 1);

	public final double x;
	public final double y;
	public final double z;
	
	public Vector3() {
		x = 0;
		y = 0;
		z = 0;
	}

	public Vector3(double[] v) {
		this.x = v[0];
		this.y = v[1];
		this.z = v[2];
	}

	public Vector3(double x, double y, double z) {
		this.x = x;
		this.y = y;
		this.z = z;
	}

	public Vector3(double v) {
		this.x = v;
		this.y = v;
		this.z = v;
	}

	public Vector3 multiply(Matrix44 m) {
		return new Vector3(x * m.m00 + y * m.m10 + z * m.m20 + m.m30,
						   x * m.m01 + y * m.m11 + z * m.m21 + m.m31,
						   x * m.m02 + y * m.m12 + z * m.m22 + m.m32);
	}


	public Vector3 cross(Vector3 v) {
		return new Vector3(
                  y * v.z - z * v.y,
                -(x * v.z - z * v.x),
                  x * v.y - y * v.x);
	}

	public double[] array() {
		return new double[] {x,y,z,0,0,0};
	}

	public double squaredLength() {
		return x*x+y*y+z*z;
	}

	public double length() {
		return Math.sqrt(x*x+y*y+z*z);
	}


    public double get(int index) {
        switch (index) {
            case 0: return x;
            case 1: return y;
            case 2: return z;
        }
        throw new RuntimeException("invalid element");
    }

	public Vector3 times(double s) {
		return scale(s);
	}

	public Vector3 times(Vector3 v) {
		return new Vector3(x*v.x, y*v.y, z*v.z);
	}


	public Vector3 scale(double s) {
		return new Vector3(x*s, y*s, z*s);
	}

	public Vector3 scale(double xs, double ys, double zs) {
		return new Vector3(x*xs, y*ys, z*zs);
	}

	public Vector3 plus(Vector3 other) {
		return new Vector3(x+other.x, y+other.y, z+other.z);
	}

	public Vector3 minus(Vector3 other) {
		return new Vector3(x-other.x, y-other.y, z-other.z);
	}

	public Vector3 mask(Vector3 other) {
		return new Vector3(x*other.x, y*other.y, z*other.z);
	}

	public double dot(Vector3 other) {
		return x*other.x + y*other.y + z*other.z;
	}

	public Vector3 normalized() {
		double l = length();
		if (l == 0) {
			l=1;
		}
		return new Vector3(x/l,y/l,z/l);
	}


    //<editor-fold desc="Swizzlers">
    public Vector2 xy() { return new Vector2(x, y); }
    public Vector2 yx() { return new Vector2(y, x); }

    public Vector2 xx() { return new Vector2(x, x); }
    public Vector2 yy() { return new Vector2(y, y); }

	public Vector2 xz() { return new Vector2(x, z); }
	public Vector2 yz() { return new Vector2(y, z); }

	public Vector2 zx() { return new Vector2(z, x); }
	public Vector2 zy() { return new Vector2(z, y); }


    public Vector3 xxx() { return new Vector3(x, x, x); }
    public Vector3 xxy() { return new Vector3(x, x, y); }
    public Vector3 xxz() { return new Vector3(x, x, z); }
    public Vector3 xyx() { return new Vector3(x, y, x); }
    public Vector3 xyy() { return new Vector3(x, y, y); }
    public Vector3 xyz() { return new Vector3(x, y, z); }
    public Vector3 xzx() { return new Vector3(x, z, x); }
    public Vector3 xzy() { return new Vector3(x, z, y); }
    public Vector3 xzz() { return new Vector3(x, z, z); }

    public Vector3 yxx() { return new Vector3(y, x, x); }
    public Vector3 yxy() { return new Vector3(y, x, y); }
    public Vector3 yxz() { return new Vector3(y, x, z); }
    public Vector3 yyx() { return new Vector3(y, y, x); }
    public Vector3 yyy() { return new Vector3(y, y, y); }
    public Vector3 yyz() { return new Vector3(y, y, z); }
    public Vector3 yzx() { return new Vector3(y, z, x); }
    public Vector3 yzy() { return new Vector3(y, z, y); }
    public Vector3 yzz() { return new Vector3(y, z, z); }

    public Vector3 zxx() { return new Vector3(z, x, x); }
    public Vector3 zxy() { return new Vector3(z, x, y); }
    public Vector3 zxz() { return new Vector3(z, x, z); }
    public Vector3 zyx() { return new Vector3(z, y, x); }
    public Vector3 zyy() { return new Vector3(z, y, y); }
    public Vector3 zyz() { return new Vector3(z, y, z); }
    public Vector3 zzx() { return new Vector3(z, z, x); }
    public Vector3 zzy() { return new Vector3(z, z, y); }
    public Vector3 zzz() { return new Vector3(z, z, z); }


    public Vector4 xyz1() { return new Vector4(x,y, z, 1.0); }
    public Vector4 xyz0() { return new Vector4(x,y, z, 0.0); }
    //</editor-fold>

    @Override
	public String toString() {
		return "Vector3{" +
				"x=" + x +
				", y=" + y +
				", z=" + z +
				'}';
	}

    public Vector3 copy() {
        return new Vector3(x, y, z);
    }


}
