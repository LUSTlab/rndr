package net.lustlab.rndr.application;

import net.lustlab.horizon.Event;
import net.lustlab.rndr.math.Vector2;
import net.lustlab.rndr.math.Vector3;

import java.util.List;

public class Mouse {

    public Event.Filter<MouseEvent> button(int button) {
        return e -> e.button == button;
    }

    public static class Pointer {
        public Vector2 position;
        public double pressure;
        public int idx;
        public int id;
    }

    public static class MouseEvent {

        boolean propogationCancelled = false;

        public final void cancelPropagation() {
            propogationCancelled = true;
        }

        public final boolean isPropogationCancelled() {
            return propogationCancelled;
        }



        public enum EventType {
            MOVE,
            DRAG,
            CLICK,
            PRESS,
            RELEASE,
            WHEEL_MOVE,
        }

        public EventType eventType;
        public Vector2 position;
        public List<Pointer> pointers;
        public int modifiers;
        public int button;
        public Vector3 rotation;

        public MouseEvent() {

        }
        public MouseEvent(MouseEvent other) {
            eventType = other.eventType;
            position = other.position;
            modifiers = other.modifiers;
            button = other.button;
            rotation = other.rotation;
            pointers = other.pointers;
        }

        public static MouseEvent wheelMove(Vector2 position, List<Mouse.Pointer> pointers, Vector3 rotation, int modifiers) {
            MouseEvent me = new MouseEvent();
            me.eventType = EventType.WHEEL_MOVE;
            me.position = position;
            me.pointers = pointers;
            me.rotation = rotation;
            me.modifiers = modifiers;
            return me;
        }

        public static MouseEvent click(Vector2 position, List<Mouse.Pointer> pointers, int button, int modifiers) {
            MouseEvent me = new MouseEvent();
            me.eventType = EventType.CLICK;
            me.pointers = pointers;
            me.button = button;
            me.modifiers = modifiers;
            me.position = position;
            return me;
        }

        public static MouseEvent press(Vector2 position, List<Pointer> pointers, int button, int modifiers) {
            MouseEvent me = new MouseEvent();
            me.eventType = EventType.PRESS;
            me.pointers = pointers;
            me.button = button;
            me.modifiers = modifiers;
            me.position = position;
            return me;
        }

        public static MouseEvent release(Vector2 position, List<Pointer> pointers, int button, int modifiers) {
            MouseEvent me = new MouseEvent();
            me.eventType = EventType.RELEASE;
            me.pointers = pointers;
            me.button = button;
            me.modifiers = modifiers;
            me.position = position;
            return me;
        }

        public static MouseEvent move(Vector2 position, List<Pointer> pointers, int modifiers) {
            MouseEvent me = new MouseEvent();
            me.eventType = EventType.MOVE;
            me.pointers = pointers;
            me.modifiers = modifiers;
            me.position = position;
            return me;
        }

        public static MouseEvent drag(Vector2 position, List<Pointer> pointers, int button, int modifiers) {
            MouseEvent me = new MouseEvent();
            me.eventType = EventType.DRAG;
            me.pointers = pointers;
            me.modifiers = modifiers;
            me.button = button;
            me.position = position;
            return me;
        }

    }

    public final Event<MouseEvent> moved = new Event<MouseEvent>("mouse-moved").signature(MouseEvent.class);

    public final Event<MouseEvent> clicked = new Event<MouseEvent>("mouse-clicked").signature(MouseEvent.class);
    public final Event<MouseEvent> dragged = new Event<MouseEvent>("mouse-dragged").signature(MouseEvent.class);

    public final Event<MouseEvent> pressed = new Event<MouseEvent>("mouse-pressed").signature(MouseEvent.class);
    public final Event<MouseEvent> released = new Event<MouseEvent>("mouse-released").signature(MouseEvent.class);

    public final Event<MouseEvent> wheelMoved = new Event<MouseEvent>("mouse-wheel-moved").signature(MouseEvent.class);

    Vector2 position = new Vector2(0,0);
    public Vector2 position() {
        return position;
    }


}
