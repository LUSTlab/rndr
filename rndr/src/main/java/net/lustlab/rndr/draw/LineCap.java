package net.lustlab.rndr.draw;

/**
 * Created by voorbeeld on 4/10/16.
 */
public enum LineCap {
    SQUARE,
    ROUND,
    BUTT
}
