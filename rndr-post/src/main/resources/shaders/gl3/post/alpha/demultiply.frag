#version 150

in vec2 v_texCoord0;
uniform sampler2D tex0;
uniform sampler2D tex1;

out vec4 o_color;
void main() {
    o_color = texture(tex0, v_texCoord0);
    //o_color.a = max(0, min(1.0, o_color.a));
    if (o_color.a > 0) {
    o_color.rgb /= o_color.a;

    }

    //o_color.a = 1.0;
}