package net.lustlab.rndr.font.vector;

import com.jogamp.opengl.GL3;
import net.lustlab.colorlib.ColorRGBa;
import net.lustlab.rndr.RNDRException;

import net.lustlab.rndr.buffers.VertexFormat;
import net.lustlab.rndr.draw.*;
import net.lustlab.rndr.font.FontMap;
import net.lustlab.rndr.math.Intersections;
import net.lustlab.rndr.math.Vector2;
import net.lustlab.rndr.math.Vector4;
import net.lustlab.rndr.shaders.Shader;
import net.lustlab.rndr.buffers.BufferWriter;
import net.lustlab.rndr.buffers.VertexArray;
import net.lustlab.rndr.buffers.VertexBuffer;
import net.lustlab.rndr.shape.Composition;
import net.lustlab.rndr.shape.Figure;
import net.lustlab.rndr.shape.Vertex;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;


import java.io.Serializable;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.ByteBuffer;
import java.util.*;

public final class FontVectorMap implements Serializable, FontMap<FontVectorMap> {

    static Logger logger = LogManager.getLogger(FontVectorMap.class);

    public double fontHeight;
    public int leading;

    double ascenderLength;
    double descenderLength;

    VertexBuffer vbo;
    VertexArray vao;

    int size;
    String name;

    public int size() {
        return size;
    }
    public String name() {
        return name;
    }


    public static FontMap fromUrl(String s, int size) {
        try {
            return fromUrl(new URL(s), size);
        } catch (MalformedURLException e) {
            throw new RNDRException("failed to load font", e);
        }
    }

    static final class VBORange {
        long command;
        long offset;
        long vertices;
    }

    public static final class Entry implements Serializable {
        public Composition composition;
        public Figure figure;
        public Tessellation tessellation;
        public double width;
        public List<VBORange> ranges;
    }

    public static FontVectorMap fromUrl(URL url, int size) {
        return FontVectorMapManager.fromUrl(url, size);
    }

    FontVectorMap() {

    }

    FontVectorMap(String name, int size) {
        this.name = name;
        this.size = size;
    }

    public Map<Character, Entry> characters = new HashMap<Character, Entry>();

    /* --- */

    @Override
    public double fontHeight() {
        return fontHeight;
    }

    @Override
    public FontVectorMap fontHeight(double height) {
        this.fontHeight = height;
        return this;
    }


    @Override
    public double ascenderLength() {
        return ascenderLength;
    }

    @Override
    public double descenderLength() {
        return descenderLength;
    }

    @Override
    public FontVectorMap ascenderLength(double length) {
        this.ascenderLength = length;
        return this;
    }

    @Override
    public FontVectorMap descenderLength(double length) {
        this.descenderLength = length;
        return this;
    }

    @Override
    public double leading() {
        return leading;
    }


    public void bakeVBO(GL3 gl3, Shader shader) {

        VertexFormat format = new VertexFormat().color(4).position(3);
        logger.debug("baking vbo for {}", this);
        long size = 0;
        int vertexCount = 0;
        for (Entry entry: characters.values()) {
            for (Tessellation.PrimitiveData d: entry.tessellation.data) {
                if (d.command != GL3.GL_LINE_LOOP) {
                    size += d.vertices.size() * Tessellation.PrimitiveData.layout.size();
                    vertexCount += d.vertices.size();
                }
                else {
                    size+=d.vertices.size() * 6 * Tessellation.PrimitiveData.layout.size();
                    vertexCount += d.vertices.size() * 6;
                }
            }
        }

        logger.info("vertexCount: " + vertexCount );
        if (vertexCount == 0) {
            logger.warn("vertex count == 0");
        }

        ByteBuffer buffer = ByteBuffer.allocateDirect((int) size);
        BufferWriter bw = new BufferWriter(buffer);


        long offset = 0;
        for (Entry entry: characters.values()) {
            entry.ranges = new ArrayList<>();
            for (Tessellation.PrimitiveData d: entry.tessellation.data) {
                VBORange range = new VBORange();

                if (d.command != GL3.GL_LINE_LOOP) {
                    range.command = d.command;
                    range.offset = offset;
                    range.vertices = d.vertices.size();


                    for (Vertex v : d.vertices) {
                        bw.write(ColorRGBa.WHITE);
                        bw.write(v.position);
                        offset += 1;
                    }
                    entry.ranges.add(range);
                }
                else {

                    range.command = d.command;
                    range.offset = offset;
                    range.vertices = d.vertices.size() * 6;

                    boolean close = true;
                    int c = close ? 0 : 1;
                    Vector2 pi0 = null;
                    Vector2 pi1 = null;

                    int pc = d.vertices.size();
                    int vertices = 0;
                    for (int n = close?-1:0; n < pc - c; ++n) {
                        int n0 = ((n % pc) + pc) % pc;
                        int n1 = (n + 1) % pc;
                        int n2 = close ? (n+2) % pc : Math.min(d.vertices.size()-1,n + 2);

                        Vector2 p0 = d.vertices.get(n0).position.xy();
                        Vector2 p1 = d.vertices.get(n1).position.xy();
                        Vector2 p2 = d.vertices.get(n2).position.xy();

                        Vector2 d10 = p1.minus(p0).normalized();
                        Vector2 d21 = p2.minus(p1).normalized();

                        if (p1 == p2) {
                            d21 = d10;
                        }

                        double strokeWeight = 1.5;
                        Vector2 n10 = new Vector2(d10.y, -d10.x).scale(strokeWeight);
                        Vector2 n21 = new Vector2(d21.y, -d21.x).scale(strokeWeight);

                        Vector2 i0 = null;
                        Vector2 i1 = null;

                        double eps = 10.0;

                        if (p1 != p2) {
                            i0 = Intersections.intersect(p0, p1, p1, p2, eps);
                            i1 = Intersections.intersect(p0.plus(n10), p1.plus(n10), p1.plus(n21), p2.plus(n21), eps);
                        }
                        else {
                            i0 = p1;
                            i1 = p1.plus(n10);
                        }

                        boolean directionChanged = false;

                        if (i0 == Vector2.Infinity) {
                            i0 = p1;

                            if (n10.dot(n21) < 0) {
                                directionChanged = true;
                            }
                        }

                        if (i1 == Vector2.Infinity) {
                            i1 = p1.plus(n10);
                            if (n10.dot(n21) < 0) {
                                directionChanged = true;
                            }
                        }

                        if (pi0 == null) {
                            pi0 = p0;
                            pi1 = p0.plus(n10);
                        }


                        if (directionChanged) {
                            Vector2 t = i0;
                            i0 = i1;
                            i1 = t;
                        }


                        if (n >=0 ) {
                            Vector4 c00 = new Vector4(0,0,0,0);
                            Vector4 c01 = new Vector4(0,1,0,0);
                            Vector4 c11 = new Vector4(0,1,0,0);
                            Vector4 c10 = new Vector4(0,0,0,0);

                            Vector2 p00 = pi0;
                            Vector2 p01 = pi1;
                            Vector2 p10 = i0;
                            Vector2 p11 = i1;


                            bw .write(c00).write(p00).write(0) .write(c01).write(p01).write(0) .write(c11).write(p11).write(0);
                            bw .write(c11).write(p11).write(0) .write(c10).write(p10).write(0) .write(c00).write(p00).write(0);
                            vertices+=6;
                        }

                        pi0 = i0;
                        pi1 = i1;
                    }
                    range.vertices = vertices;
                    offset+=vertices;
                    entry.ranges.add(range);
                }

            }
        }
        buffer.rewind();
        vbo = VertexBuffer.createStatic(format, vertexCount, buffer);
        vao = VertexArray.create(gl3, vbo, Tessellation.PrimitiveData.layout, shader);

    }

}