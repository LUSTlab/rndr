package net.lustlab.rndr.post.antialias;

import net.lustlab.rndr.buffers.ColorBuffer;
import net.lustlab.rndr.post.Filter;
import net.lustlab.rndr.shaders.FragmentShader;

public class FXAA extends Filter {
    public FXAA() {
        super(FragmentShader.fromUrl("cp:shaders/gl3/post/antialias/fxaa.frag"));
    }

    public double lumaThreshold = 0.5;

    public double maxSpan = 8.0;
    public double directionReduceMultiplier = 0.0;
    public double directionReduceMinimum = 0.0;

    @Override
    public Filter apply(ColorBuffer[] src, ColorBuffer[] dest) {
        parameter("maxSpan", (float)maxSpan);
        parameter("lumaThreshold", (float)lumaThreshold);
        parameter("directionReduceMultiplier", (float)directionReduceMultiplier);
        parameter("directionReduceMinimum", (float)directionReduceMinimum);

        super.apply(src, dest);
        return this;
    }
}
