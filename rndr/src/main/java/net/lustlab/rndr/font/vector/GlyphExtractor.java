package net.lustlab.rndr.font.vector;

import net.lustlab.colorlib.ColorRGBa;
import net.lustlab.rndr.shape.Composition;
import net.lustlab.rndr.shape.CompositionNode;
import net.lustlab.rndr.shape.ShapeContour;
import net.lustlab.rndr.shape.Segment;
import net.lustlab.rndr.font.FontResolver;
import net.lustlab.rndr.math.Vector2;

import java.awt.*;
import java.awt.Shape;
import java.awt.font.FontRenderContext;
import java.awt.font.GlyphVector;
import java.awt.geom.AffineTransform;
import java.awt.geom.PathIterator;
import java.util.ArrayList;
import java.util.List;

public class GlyphExtractor {

    public Composition extractGlyphFromFile(String fontfile, int size, String text) {

            Font font = FontResolver.resolveFileFont(fontfile, size);
            return extractGlyph(font, text);

    }
    public Composition extractGlyph(Font font,  String text) {
        Composition composition = new Composition();
        FontRenderContext frc = new FontRenderContext(AffineTransform.getTranslateInstance(0,0), false, true);


        GlyphVector gv = font.createGlyphVector(frc, text);

        double coords[] = new double[6];
        List<net.lustlab.rndr.shape.Shape> shapes = new ArrayList<net.lustlab.rndr.shape.Shape>();

        Vector2 cursor = new Vector2();
        Vector2 anchor = new Vector2(0,0);


        for (int g = 0; g < gv.getNumGlyphs(); ++g) {
            Shape glyph = gv.getGlyphOutline(g);



            net.lustlab.rndr.shape.Shape activeShape = new net.lustlab.rndr.shape.Shape();
            ShapeContour activeContour = null;
            PathIterator pathIterator= glyph.getPathIterator(null);

            while (!pathIterator.isDone()) {

                int type = pathIterator.currentSegment(coords);

                switch(type) {

                    case PathIterator.SEG_MOVETO:
                        if (activeContour != null) {
                            activeShape.contours.add(activeContour);
                        }

                        activeContour = new ShapeContour();

                        cursor = new Vector2(coords[0], coords[1]);
                        anchor = new Vector2(coords[0], coords[1]);
                        break;
                    case PathIterator.SEG_LINETO:
                        activeContour.segments.add(new Segment(cursor.copy(), new Vector2(coords[0],coords[1])));
                        cursor = new Vector2(coords[0], coords[1]);
                        break;
                    case PathIterator.SEG_CLOSE:
                        activeContour.segments.add(new Segment(cursor.copy(), anchor.copy()));
                        activeContour.closed = true;
                        break;
                    case PathIterator.SEG_QUADTO:
                        activeContour.segments.add(new Segment(cursor.copy(), new Vector2(coords[0], coords[1]), new Vector2(coords[2],coords[3])));
                        cursor = new Vector2(coords[2], coords[3]);
                        break;
                    case PathIterator.SEG_CUBICTO:
                        activeContour.segments.add(new Segment(cursor.copy(), new Vector2(coords[0], coords[1]), new Vector2(coords[2],coords[3]),new Vector2(coords[4],coords[5])));
                        cursor = new Vector2(coords[4], coords[5]);
                        break;
                    default:
                        throw new RuntimeException("unsupported command");
                }

                pathIterator.next();
                if (pathIterator.isDone()) {
                    activeShape.contours.add(activeContour);
                    break;
                }

            }

            activeShape.fill = ColorRGBa.WHITE;
            shapes.add(activeShape);


            CompositionNode cn = new CompositionNode();
            cn.shape(activeShape);
            composition.root.append(cn);

        }


        return composition;
    }
}
