#version 330

${uniforms! }

in vec3 position;

uniform mat4 modelViewMatrix;
uniform mat4 modelViewProjectionMatrix;
uniform mat4 normalMatrix;

uniform vec4 color;


out vec4 v_color;

void main(void) {

    vec3 x_position = position;
    { ${positionTransform! } }

    gl_Position = modelViewProjectionMatrix * vec4(x_position.xyz, 1.0);
    v_color = color;
}