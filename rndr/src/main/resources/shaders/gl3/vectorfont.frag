#version 150

uniform vec4 clipArea;
uniform float viewHeight;
uniform vec4 color;

in vec4 v_color;

out vec4 o_color;
void main() {

    vec2 d = fwidth(v_color.xy);
    vec2 a3 = smoothstep(vec2(1.0), vec2(1.0)-d*1.20,v_color.xy);



	if (gl_FragCoord.x < clipArea.x
	    || gl_FragCoord.x > clipArea.y
	    || (viewHeight-gl_FragCoord.y) < clipArea.z
	    || (viewHeight-gl_FragCoord.y) > clipArea.w) {
	    discard;
	}

	float extra = 1.0;//- (1.0-v_color.z);
    o_color = vec4(color.r, color.g, color.b, max(v_color.z, a3.y) * color.a * extra);
}