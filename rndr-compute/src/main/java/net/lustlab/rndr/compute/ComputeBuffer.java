package net.lustlab.rndr.compute;

import com.jogamp.opencl.CLBuffer;

import java.nio.Buffer;
import java.nio.ByteBuffer;
import java.nio.IntBuffer;
import java.nio.ShortBuffer;

/**
 * Created by edwin on 22/04/17.
 */
public class ComputeBuffer {

    final CLBuffer buffer;
    public ComputeBuffer(CLBuffer buffer) {
        this.buffer = buffer;
    }

    public long elementCount() {
        return buffer.getCLSize();
    }

    public ByteBuffer byteBuffer() {
        Buffer b = buffer.getBuffer();
        return (ByteBuffer) buffer.getBuffer();
    }

    public IntBuffer intBuffer() {
        Buffer b = buffer.getBuffer();
        return (IntBuffer) buffer.getBuffer();
    }

    public ShortBuffer shortBuffer() {
        Buffer b = buffer.getBuffer();
        return (ShortBuffer) buffer.getBuffer();
    }



}
