package net.lustlab.rndr.draw.gl3;

import com.jogamp.opengl.GLContext;
import com.jogamp.opengl.GLDrawable;
import com.jogamp.opengl.GLDrawableFactory;
import com.jogamp.opengl.GLOffscreenAutoDrawable;
import net.lustlab.rndr.draw.DrawThread;
import net.lustlab.rndr.draw.Drawer;

import java.util.function.BiConsumer;

public class DrawThreadGL3 extends DrawThread {

    GLOffscreenAutoDrawable offscreen;

    BiConsumer<DrawThread, Drawer> toExecute;

    public DrawThreadGL3(BiConsumer<DrawThread, Drawer> toExecute) {

        GLDrawable drawable = GLContext.getCurrent().getGLDrawable();
        offscreen = GLDrawableFactory.getFactory(drawable.getGLProfile())
                .createOffscreenAutoDrawable(null, drawable.getChosenGLCapabilities(), null, 640, 480);
        offscreen.setSharedContext(GLContext.getCurrentGL().getContext());
        offscreen.display();
        this.toExecute = toExecute;
    }

    @Override
    public void run() {
        running = true;
        offscreen.getContext().makeCurrent();
        Drawer drawer = new DrawerGL3(offscreen.getGL().getGL3(), 640, 480);
        toExecute.accept(this, drawer);
        drawer.destroy();
        offscreen.destroy();
        running = false;
        done.trigger(this);
    }

    public void swap() {
        offscreen.swapBuffers();;
    }
}
