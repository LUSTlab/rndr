package net.lustlab.rndr.math;

import static java.lang.Math.abs;

public class Intersections {

    public static Vector2 intersect(Vector2 a0, Vector2 a1, Vector2 b0, Vector2 b1) {
        return intersect(a0, a1, b0, b1, 0.01);
    }

    public static Vector2 intersect(Vector2 a0, Vector2 a1, Vector2 b0, Vector2 b1, double eps) {


        double x0 = a0.x;
        double x1 = a1.x;
        double x2 = b0.x;
        double x3 = b1.x;

        double y0 = a0.y;
        double y1 = a1.y;
        double y2 = b0.y;
        double y3 = b1.y;


        double den = ((x0 - x1) * (y2 - y3) - (y0 - y1) * (x2 - x3));

        if (abs(den) > 0.0000001) {
            double px = ((x0 * y1 - y0 * x1) * (x2 - x3) - (x0 - x1) * (x2 * y3 - y2 * x3)) / den;
            double py = ((x0 * y1 - y0 * x1) * (y2 - y3) - (y0 - y1) * (x2 * y3 - y2 * x3)) / den;

            double s = (-(y1 - y0) * (x0 - x2) + (x1 - x0) * (y0 - y2)) / den;
            double t = ( (x3 - x2) * (y0 - y2) - (y3 - y2) * (x0 - x2)) / den;


            if (t >= 0-eps && t <= 1+eps && s >= 0-eps && s<= 1+eps) {
                return new Vector2(px, py);
            } else {
                return Vector2.Infinity;
            }
        } else {
            return Vector2.Infinity;
        }


    }
}
