package net.lustlab.rndr.font.image;

import net.lustlab.rndr.RNDRException;

import java.io.*;

public class FontImageMapIO {

    /**
     * Saves a FontImageMap to file
     * @param fontImageMap the FontImageMap to save
     * @param filename the filename to which the FontImageMap should be saved
     */

	public static void save(FontImageMap fontImageMap, File file) {
		try {
			FileOutputStream fos = new FileOutputStream(file);
			ObjectOutputStream oos = new ObjectOutputStream(fos);
			oos.writeObject(fontImageMap);
		} catch (IOException e) {
            throw new RNDRException(e);
		}
	}

    /**
     * Loads a FontImageMap from file
     * @param filename the filename from which the FontImageMap should loaded
     * @return a fully contructed FontImageMap
     * @throws RNDRException when the image map could not be loaded from the specified filename
     */
	public static FontImageMap load(File file) {
		try {
			FileInputStream fis = new FileInputStream(file);
			ObjectInputStream ois = new ObjectInputStream(fis);
			FontImageMap fm = (FontImageMap) ois.readObject();
			return fm;
		} catch (ClassNotFoundException | IOException e) {
		    throw new RNDRException(e);
		}
	}
}
