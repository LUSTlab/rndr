#version 330

layout(location=0) out vec4 o_vertical;
layout(location=1) out vec4 o_diagonal;

uniform vec2 vertical;
uniform vec2 diagonal;

in vec2 v_texCoord0;

uniform sampler2D image;

const int NUM_SAMPLES = 40;
const float PI = 3.14159265322f;


float saturate(float x) {
    return clamp(x, 0.0, 1.0);
}

vec4 BlurTexture(sampler2D tex, vec2 uv, vec2 direction)
{
    vec4 finalColor = vec4(0.0);
    float blurAmount = 0.0;

    uv += direction * 0.5;

    for (int i = 0; i < NUM_SAMPLES; ++i)
    {
        vec4 color = texture(tex, uv + direction * i);
        color *= color.a;
        blurAmount += color.a;
        finalColor += color;
    }

    return (finalColor / blurAmount);
}

void main() {
    vec2 viewport = textureSize(image, 0);
    vec2 invViewDims = 1.0/viewport;

    float coc = texture(image, v_texCoord0).a;

// CoC-weighted vertical blur.
    vec2 blurDir = (coc-1.0)/40.0 * invViewDims * vec2(cos(PI/2), sin(PI/2));
    vec4 color = BlurTexture(image, v_texCoord0, blurDir) * coc;

// CoC-weighted diagonal blur.
    vec2 blurDir2 = coc/40.0 * invViewDims * vec2(cos(-PI/6), sin(-PI/6));
    vec4 color2 = BlurTexture(image, v_texCoord0, blurDir2) * coc;

    o_vertical = vec4(color.rgb, coc);
    o_diagonal = vec4(color2.rgb + o_vertical.rgb, coc);

}