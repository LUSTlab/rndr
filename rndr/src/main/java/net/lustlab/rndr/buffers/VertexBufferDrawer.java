package net.lustlab.rndr.buffers;

import com.jogamp.opengl.GL;
import com.jogamp.opengl.GL3;
import net.lustlab.rndr.gl.GLError;
import net.lustlab.rndr.shaders.Shader;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.HashSet;
import java.util.Set;

/**
 * A utility class to draw vertex buffers.
 *
 * To draw a VertexBuffer a VertexBuffer and VertexFormat can be used
 */
public class VertexBufferDrawer {

    final static Logger logger = LogManager.getLogger(VertexBufferDrawer.class);

    public static class WarningInfo {

        public WarningInfo(String attribute, VertexFormat layout, Shader shader) {
            this.attribute = attribute;
            this.layout = layout;
            this.shader = shader;
        }

        String attribute;
        VertexFormat layout;
        Shader shader;

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;

            WarningInfo that = (WarningInfo) o;

            if (attribute != null ? !attribute.equals(that.attribute) : that.attribute != null) return false;
            if (layout != null ? !layout.equals(that.layout) : that.layout != null) return false;
            if (shader != null ? !shader.equals(that.shader) : that.shader != null) return false;

            return true;
        }

        @Override
        public int hashCode() {
            int result = attribute != null ? attribute.hashCode() : 0;
            result = 31 * result + (layout != null ? layout.hashCode() : 0);
            result = 31 * result + (shader != null ? shader.hashCode() : 0);
            return result;
        }
    }

    static Set<WarningInfo> issuedAttributeWarnings = new HashSet<>();

    public static void draw(GL3 gl, VertexArray vao, int primitive, int count) {
        vao.bind();
        //GLError.check(gl);

        gl.glDrawArrays(primitive, 0, count);
        //GLError.check(gl);

        vao.unbind();
//        GLError.check(gl);

    }

    /**
     * Draws a VertexBuffer.
     * @param gl the OpenGL interface to use
     * @param shader a shader. For GL2 contexts this can be null.
     * @param vbo the VertexBuffer instance to draw
     * @param layout the layout of the VertexBuffer
     * @param primitive the primitive to use. Valid values are: GL_TRIANGLES, GL_QUADS, GL_LINES, GL_POINTS
     * @param count the number of vertices to draw
     */
    public static void draw(GL gl, Shader shader, VertexBuffer vbo, VertexFormat layout, int primitive, int count, int offset) {

        if (gl.isGL3() && gl.getGL3() != null) {
            GLError.debug(gl.getGL3());
            vbo.bind();
            GLError.debug(gl.getGL3());

            setupLayoutGL3(gl.getGL3(), layout, shader);
            GLError.debug(gl.getGL3());

            gl.glDrawArrays(primitive, offset, count);
            GLError.debug(gl.getGL3());
            teardownLayoutGL3(gl.getGL3(), layout, shader);
            GLError.debug(gl.getGL3());

        }
    }

    public static void draw(GL gl, Shader shader, VertexBuffer vbo, VertexFormat layout, int primitive, int count) {
        draw(gl, shader, vbo, layout, primitive, count, 0);
    }



    public static void drawInstances(GL gl, Shader shader, VertexBuffer vbo, VertexFormat layout, int primitive, int primitiveCount, int instanceCount) {

        if (gl.isGL3()) {
            GL3 gl3 = gl.getGL3();

            setupLayoutGL3(gl3, layout, shader);
            GLError.debug(gl3);

            gl3.glDrawArraysInstanced(primitive, 0, primitiveCount, instanceCount);
            GLError.debug(gl3);

            teardownLayoutGL3(gl3, layout, shader);
            GLError.debug(gl3);
        }
        else {
            throw new RuntimeException("no GL3 context");
        }
    }

    public static void drawInstances(GL gl, VertexArray vao, int primitive, int primitiveCount, int instanceCount) {
        if (gl.isGL3()) {
            GL3 gl3 = gl.getGL3();
            vao.bind();
            gl3.glDrawArraysInstanced(primitive, 0, primitiveCount, instanceCount);
            vao.unbind();
        } else {
            throw new RuntimeException("no GL3 context");
        }
    }


//    public static void drawInstances(GL gl, Shader shader, VertexBuffer vbo, VertexFormat layout,
//                                     VertexBuffer attribute, VertexFormat attributeLayout,
//                                     int primitive, int primitiveCount, int instanceCount) {
//
//        if (gl.isGL3()) {
//            GL3 gl3 = gl.getGL3();
//            setupLayoutGL3(gl3, layout, shader);
//            setupLayoutGL3(gl3, attributeLayout, shader);
//            gl3.glVertexAttribDivisor(shader.attributeIndex(attributeLayout.items.get(0).attribute), 1);
//            GLError.check(gl3);
//            gl3.glDrawArraysInstanced(primitive, 0, primitiveCount, instanceCount);
//            GLError.check(gl3);
//            teardownLayoutGL3(gl3, layout, shader);
//            teardownLayoutGL3(gl3, attributeLayout, shader);
//        }
//        else {
//            throw new RuntimeException("no GL3 context");
//        }
//    }



    static void setupLayoutGL3(GL3 gl, VertexFormat layout, Shader shader) {
        for (VertexElement item: layout.items) {
            String attribute =  item.attribute;

            int attributeIndex = shader.attributeIndex(attribute);
            if (attributeIndex != -1) {
                gl.glEnableVertexAttribArray(attributeIndex);
                //GLError.check(gl);
                gl.glVertexAttribPointer(attributeIndex, item.count, item.type, false, layout.vertexSize, item.offset);
               // GLError.check(gl);
            }
            else {
                WarningInfo wi = new WarningInfo(attribute, layout, shader);
                if (!issuedAttributeWarnings.contains(wi)) {
                    logger.warn("problem with attribute:" + attribute + " " + shader + " " + layout);
                    issuedAttributeWarnings.add(wi);
                }
            }


        }
    }
    static void teardownLayoutGL3(GL3 gl, VertexFormat layout, Shader shader) {
        for (VertexElement item: layout.items) {
            // custom attribute
            int attributeIndex = shader.attributeIndex(item.attribute);
            if (attributeIndex != -1) {
                gl.glDisableVertexAttribArray(attributeIndex);
            }
        }
    }



}
