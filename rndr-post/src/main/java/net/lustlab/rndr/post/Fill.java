package net.lustlab.rndr.post;

import net.lustlab.rndr.shaders.FragmentShader;


public class Fill extends Filter {
    public Fill() {
        super(FragmentShader.fromUrl("cp:shaders/gl3/post/fill.frag"));
    }
}
