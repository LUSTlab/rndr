package net.lustlab.rndr.draw;

/**
 * Created by edwin on 05/06/17.
 */
public enum StencilTest {
    NEVER,
    LESS,
    LESS_OR_EQUAL,
    GREATER,
    GREATER_OR_EQUAL,
    EQUAL,
    NOT_EQUAL,
    ALWAYS
}
