package net.lustlab.rndr.font.image;

import com.jogamp.opengl.util.texture.Texture;
import net.lustlab.packer.PackNode;
import net.lustlab.rndr.font.EntityPairIndex;
import net.lustlab.rndr.font.FontMap;

import java.io.Serializable;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Map;


/**
 *
 */
public final class FontImageMap implements Serializable, FontMap<FontImageMap> {

	private transient final Texture texture;
	private final Map<Character, PackNode> map;
	private final int sanding;

	private final Map<EntityPairIndex, Float> kerningPairs;


    double ascenderLength;
    double descenderLength;


    String name;
    public int fontHeight;
    private int fontSize;
    private int leading;

    public static FontImageMap fromUrl(String s, int size) {
        try {
            return FontImageMapManager.fromUrl(new URL(s), size);
        } catch (MalformedURLException e) {
            throw new RuntimeException("malformed URL:" +s);
        }
    }

    public static FontImageMap fromUrl(URL url, int size) {
        return FontImageMapManager.fromUrl(url, size);
    }

    public FontImageMap(FontImageMap fontImageMap, Texture texture) {
		this.texture = texture;
		this.map = fontImageMap.map;
		this.sanding = fontImageMap.sanding;
        this.fontHeight = fontImageMap.fontHeight;
		this.kerningPairs = fontImageMap.kerningPairs;

		this.fontSize = fontImageMap.fontSize;
        this.leading = fontImageMap.leading;
        this.descenderLength = fontImageMap.descenderLength;
        this.ascenderLength = fontImageMap.ascenderLength;

	}

	public FontImageMap(
                        Texture texture,
                        Map<Character, PackNode> map,
                        Map<EntityPairIndex, Float> kerningPairs,
                        int sanding,
                        int fontHeight,
                        int fontSize,
                        int leading,
                        String name
    ) {

		this.texture = texture;
		this.map = map;
		this.sanding = sanding;
		this.kerningPairs = kerningPairs;
        this.fontHeight = fontHeight;
        this.fontSize = fontSize;
        this.leading = leading;
        this.name = name;
	}

	public Texture getTexture() {
		return texture;
	}

	public Map<Character, PackNode> getMap() {
		return map;
	}

    /**
     * Returns the width of the given character
     * @param character the character for which to determine the width
     * @return the width of the character in pixels or 0 if the map does not contain the character
     */
	public float characterWidth(char character) {
		PackNode node = map.get(character);
		if (node != null) {
            FontImageMapBuilder.Entry data = (FontImageMapBuilder.Entry) node.getData();
            return data.width;
		} else {
			return 0;
		}
	}


	public int getSanding() {
		return sanding;
	}

	public float kerning(char left, char right) {
		Float kerning = kerningPairs.get(new EntityPairIndex(left, right));
		if (kerning == null) {
			return 0;
		} else {
			return kerning;
		}
	}

    /**
     * Returns the size at which this font map is generated
     * @return the size in pixels?
     */
    public int size() {
        return fontSize;
    }


    public String name() {
        return name;
    }

    @Override
    public double fontHeight() {
        return fontHeight;
    }

    public FontImageMap fontHeight(double fontHeight) {
        this.fontHeight = (int) fontHeight;
        return this;
    }

    @Override
    public double leading() {
        return leading;
    }


    @Override
    public double ascenderLength() {
        return ascenderLength;
    }

    @Override
    public FontImageMap ascenderLength(double length) {
        this.ascenderLength = length;
        return this;
    }

    @Override
    public FontImageMap descenderLength(double length) {
        this.descenderLength = length;
        return this;
    }

    @Override
    public double descenderLength() {
        return this.descenderLength;
    }
}
