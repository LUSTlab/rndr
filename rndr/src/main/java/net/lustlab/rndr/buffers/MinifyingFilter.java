package net.lustlab.rndr.buffers;

import com.jogamp.opengl.GL;

public enum MinifyingFilter {
    NEAREST,
    LINEAR,
    NEAREST_MIPMAP_NEAREST,
    LINEAR_MIPMAP_NEAREST,
    NEAREST_MIPMAP_LINEAR,
    LINEAR_MIPMAP_LINEAR;

    public int toGLFilter() {
        switch(this) {
            case NEAREST:
                return GL.GL_NEAREST;
            case LINEAR:
                return GL.GL_LINEAR;
            case NEAREST_MIPMAP_NEAREST:
                return GL.GL_NEAREST_MIPMAP_NEAREST;
            case LINEAR_MIPMAP_NEAREST:
                return GL.GL_LINEAR_MIPMAP_NEAREST;
            case NEAREST_MIPMAP_LINEAR:
                return GL.GL_NEAREST_MIPMAP_LINEAR;
            case LINEAR_MIPMAP_LINEAR:
                return GL.GL_LINEAR_MIPMAP_LINEAR;
            default:
                throw new RuntimeException("unsupported filter");
        }

    }
}
