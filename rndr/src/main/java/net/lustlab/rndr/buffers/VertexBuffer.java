package net.lustlab.rndr.buffers;

import com.jogamp.opengl.GL;
import com.jogamp.opengl.GL2GL3;
import com.jogamp.opengl.GL3;
import com.jogamp.opengl.GLContext;
import net.lustlab.rndr.RNDRException;
import net.lustlab.rndr.gl.GLError;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.nio.ByteBuffer;
import java.nio.FloatBuffer;

/**
 * Vertex Buffer Object
 */
public class VertexBuffer {

    boolean destroyed;
    public final int vbo;
    private final GL gl;
    VertexFormat format;
    final long vertexCount;

    VertexBufferShadow shadow;
    static Logger logger = LogManager.getLogger(VertexBuffer.class);


    @Override
    public String toString() {
        return "VertexBuffer{" +
                "format=" + format +
                ", vertexCount=" + vertexCount +
                '}';
    }

    public long vertexCount() {
        return vertexCount;
    }


    public static VertexBuffer createStatic(VertexFormat format, long vertexCount, ByteBuffer data) {
        logger.debug("creating static VertexBuffer with vertex size of {} bytes with {} vertices", format.size(), vertexCount);
        GL gl = GLContext.getCurrentGL();
        if (gl.isGL2GL3()) {
            GL2GL3 gl23 = gl.getGL2GL3();
            GLError.check(gl23);
            int[] vbos = new int[1];
            gl.glGenBuffers(1, vbos, 0);
            GLError.check(gl23);

            gl23.glBindBuffer(GL.GL_ARRAY_BUFFER, vbos[0] );
            GLError.check(gl23);

            long sizeInBytes = format.vertexSize * vertexCount;
            gl23.glBufferData(GL.GL_ARRAY_BUFFER, sizeInBytes, data, GL2GL3.GL_STATIC_DRAW);

            GLError.check(gl23);
            return new VertexBuffer(gl, vbos[0], vertexCount).format(format);
        }
        else {
            throw new RNDRException("unsupported GL implementation");
        }
    }

    public static VertexBuffer createDynamic(VertexFormat format, long vertexCount) {
        logger.debug("creating dynamic VertexBuffer with vertex size of {} bytes with {} vertices", format.size(), vertexCount);

        GL gl = GLContext.getCurrentGL();
        if (gl.isGL2GL3()) {
            GL2GL3 gl23 = gl.getGL2GL3();
            GLError.check(gl23);
            int[] vbos = new int[1];
            gl.glGenBuffers(1, vbos, 0);
            GLError.check(gl23);

            gl23.glBindBuffer(GL.GL_ARRAY_BUFFER, vbos[0] );
            GLError.check(gl23);

            long sizeInBytes = format.vertexSize * vertexCount;
            gl23.glBufferData(GL.GL_ARRAY_BUFFER, sizeInBytes, null, GL2GL3.GL_STREAM_DRAW);
            GLError.check(gl23);

            return new VertexBuffer(gl, vbos[0], vertexCount).format(format);
        }

        else {
            throw new RNDRException("unsupported GL implementation");
        }

    }


    private VertexBuffer(GL gl, int vbo, long vertexCount) {
        this.vbo = vbo;
        this.gl = gl;
        this.vertexCount = vertexCount;
    }


    public VertexBuffer format(VertexFormat format) {
        this.format = format;
        return this;
    }

    public VertexFormat format() {
        return format;
    }

    /**
     * Binds the VertexBuffer to the GL_ARRAY_BUFFER
     */
    public VertexBuffer bind() {
        if (!destroyed) {
            gl.glBindBuffer(GL3.GL_ARRAY_BUFFER, vbo);
        } else {
            throw new RuntimeException("vertex buffer is destroyed");
        }
        return this;
    }

    /**
     * Unbinds the VertexBuffer from the GL_ARRAY_BUFFER
     */
    public VertexBuffer unbind() {
        gl.glBindBuffer(GL3.GL_ARRAY_BUFFER, 0);
        return this;
    }

    public VertexBuffer destroy() {
        if (!destroyed) {
            gl.glDeleteBuffers(1, new int[]{vbo}, 0);
            destroyed = true;
        }
        return this;
    }

    public void write(ByteBuffer data) {

        if (!destroyed) {
            bind();
            gl.glBufferSubData(GL.GL_ARRAY_BUFFER, 0, data.capacity(), data);
        } else {
            throw new RNDRException("vertex buffer is destroyed");
        }
    }

    public void write(ByteBuffer data, int offset, int size) {
        bind();
        gl.glBufferSubData(GL.GL_ARRAY_BUFFER, offset, size, data);
    }


    public VertexBufferShadow shadow() {
        if (shadow == null) {
            shadow = new VertexBufferShadow(this);
        }
        return shadow;
    }

    public void write(FloatBuffer data, int byteOffset, int elements) {
        if (!destroyed) {
            bind();
            gl.glBufferSubData(GL.GL_ARRAY_BUFFER, byteOffset, elements * 4, data);
        } else {
            throw new RNDRException("vertex buffer is destroyed");
        }

    }
}
