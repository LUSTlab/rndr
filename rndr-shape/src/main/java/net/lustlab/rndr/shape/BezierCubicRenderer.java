package net.lustlab.rndr.shape;

import net.lustlab.rndr.math.Vector2;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by voorbeeld on 12/19/13.
 */
public class BezierCubicRenderer {


    private final int recursionLimit = 8;

    private List<Vector2> points = new ArrayList<Vector2>();

    private static final double colinearityEpsilon = 1e-30;
    private static final double angleToleranceEpsilon = 0.01;

    private double distanceToleranceSquare;
    private double angleTolerance = 0.0;
    private double cuspLimit = 0.0;
    public double distanceTolerance = 0.5;

    public List<Vector2> render(Vector2 x1, Vector2 x2, Vector2 x3, Vector2 x4) {

        distanceToleranceSquare = distanceTolerance * distanceTolerance;

        points.clear();

        points.add(new Vector2(x1));
        render(x1, x2, x3, x4, 0);
        points.add(new Vector2(x4));

        return points;

    }

    private void render(Vector2 x1, Vector2 x2, Vector2 x3, Vector2 x4, int level) {

        if (level > recursionLimit) {
            return;
        }

        Vector2 x12 = x1.plus(x2).scale(0.5);
        Vector2 x23 = x2.plus(x3).scale(0.5);
        Vector2 x34 = x3.plus(x4).scale(0.5);

        Vector2 x123 = x12.plus(x23).scale(0.5);
        Vector2 x234 = x23.plus(x34).scale(0.5);

        Vector2 x1234 = x123.plus(x234).scale(0.5);


        double dx = x4.x - x1.x;
        double dy = x4.y - x1.y;

        double d2 = Math.abs(((x2.x - x4.x) * dy - (x2.y - x4.y) * dx));
        double d3 = Math.abs(((x3.x - x4.x) * dy - (x3.y - x4.y) * dx));

        boolean p1 = d2 > colinearityEpsilon;
        boolean p0 = d3 > colinearityEpsilon;
        int p = (p1 ? 2 : 0) + (p0 ? 1 : 0);


        double k, da1, da2;
        switch (p) {
            case 0:
                k = dx * dx + dy * dy;
                if (k == 0) {
                    d2 = squareDistance(x1.x, x1.y, x2.x, x2.y);
                    d3 = squareDistance(x4.x, x4.y, x3.x, x3.y);
                } else {
                    k = 1 / k;
                    da1 = x2.x - x1.x;
                    da2 = x2.y - x1.y;
                    d2 = k * (da1 * dx + da2 * dy);
                    da1 = x3.x - x1.x;
                    da2 = x3.y - x1.y;
                    d3 = k * (da1 * dx + da2 * dy);
                    if (d2 > 0 && d2 < 1 && d3 > 0 && d3 < 1) {
                        // Simple collinear case, 1---2---3---4
                        // We can leave just two endpoints
                        return;
                    }
                    if (d2 <= 0) d2 = squareDistance(x2.x, x2.y, x1.x, x1.y);
                    else if (d2 >= 1) d2 = squareDistance(x2.x, x2.y, x4.x, x4.y);
                    else d2 = squareDistance(x2.x, x2.y, x1.x + d2 * dx, x1.y + d2 * dy);

                    if (d3 <= 0) d3 = squareDistance(x3.x, x3.y, x1.x, x1.y);
                    else if (d3 >= 1) d3 = squareDistance(x3.x, x3.y, x4.x, x4.y);
                    else d3 = squareDistance(x3.x, x3.y, x1.x + d3 * dx, x1.y + d3 * dy);

                }
                if (d2 > d3) {
                    if (d2 < distanceToleranceSquare) {
                        points.add(new Vector2(x2.x, x2.y));
                        return;
                    }
                } else {
                    if (d3 < distanceToleranceSquare) {
                        points.add(new Vector2(x3.x, x3.y));
                        return;
                    }
                }
                break;
            case 1:
                // p1,p2,p4 are collinear, p3 is significant
                //----------------------
                if (d3 * d3 <= distanceToleranceSquare * (dx * dx + dy * dy)) {
                    if (angleTolerance < angleToleranceEpsilon) {
                        points.add(new Vector2(x23));
                        return;
                    }

                    // Angle Condition
                    //----------------------
                    da1 = Math.abs(Math.atan2(x4.y - x3.y, x4.x - x3.x) - Math.atan2(x3.y - x2.y, x3.x - x2.x));
                    if (da1 >= Math.PI) da1 = 2 * Math.PI - da1;

                    if (da1 < angleTolerance) {
                        points.add(new Vector2(x2));
                        points.add(new Vector2(x3));
                        return;
                    }

                    if (cuspLimit != 0.0) {
                        if (da1 > cuspLimit) {
                            points.add(new Vector2(x3));
                            return;
                        }
                    }
                }
                break;
            case 2:
                // p1,p3,p4 are collinear, p2 is significant
                //----------------------
                if (d2 * d2 <= distanceToleranceSquare * (dx * dx + dy * dy)) {
                    if (angleTolerance < angleToleranceEpsilon) {
                        points.add(new Vector2(x23));
                        return;
                    }

                    // Angle Condition
                    //----------------------
                    da1 = Math.abs(Math.atan2(x3.y - x2.y, x3.x - x2.x) - Math.atan2(x2.y - x1.y, x2.x - x1.x));
                    if (da1 >= Math.PI) da1 = 2 * Math.PI - da1;

                    if (da1 < angleTolerance) {
                        points.add(new Vector2(x2));
                        points.add(new Vector2(x3));
                        return;
                    }

                    if (cuspLimit != 0.0) {
                        if (da1 > cuspLimit) {
                            points.add(new Vector2(x2));
                            return;
                        }
                    }
                }
                break;
            case 3:
                // Regular case
                //-----------------
                if ((d2 + d3) * (d2 + d3) <= distanceToleranceSquare * (dx * dx + dy * dy)) {
                    // If the curvature doesn't exceed the distance_tolerance value
                    // we tend to finish subdivisions.
                    //----------------------
                    if (angleTolerance < angleToleranceEpsilon) {
                        points.add(new Vector2(x23));
                        return;
                    }

                    // Angle & Cusp Condition
                    //----------------------
                    k = Math.atan2(x3.y - x2.y, x3.x - x2.x);
                    da1 = Math.abs(k - Math.atan2(x2.y - x1.y, x2.x - x1.x));
                    da2 = Math.abs(Math.atan2(x4.y - x3.y, x4.x - x3.x) - k);
                    if (da1 >= Math.PI) da1 = 2 * Math.PI - da1;
                    if (da2 >= Math.PI) da2 = 2 * Math.PI - da2;

                    if (da1 + da2 < angleTolerance) {
                        // Finally we can stop the recursion
                        //----------------------
                        points.add(new Vector2(x23));
                        return;
                    }

                    if (cuspLimit != 0.0) {
                        if (da1 > cuspLimit) {
                            points.add(new Vector2(x2));
                            return;
                        }

                        if (da2 > cuspLimit) {
                            points.add(new Vector2(x3));
                            return;
                        }
                    }
                }
                break;

        }


        render(x1, x12, x123, x1234, level + 1);
        render(x1234, x234, x34, x4, level + 1);


    }

    private static double squareDistance(double x, double y, double x1, double y1) {
        double dx = x1 - x;
        double dy = y1 - y;
        return dx * dx + dy * dy;
    }


}
