package net.lustlab.rndr.draw.gl3;

import com.jogamp.opengl.GL;
import com.jogamp.opengl.GL3;

import net.lustlab.colorlib.ColorRGBa;
import net.lustlab.rndr.buffers.*;
import net.lustlab.rndr.draw.ShadeStyle;
import net.lustlab.rndr.math.*;
import net.lustlab.rndr.shaders.ShadeStyleManager;
import net.lustlab.rndr.shaders.Shader;

import java.nio.ByteBuffer;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class SolidsRenderer {


    Map<String, VertexBuffer> vbos = new HashMap<>();
    //Map<String, VertexArray> vaos = new HashMap<>();

    VertexFormat layout;
    BufferTexture bufferTexture;

    ShadeStyleManager solidManager;
    ShadeStyleManager solidsManager;



    public SolidsRenderer(GL3 gl3) {

        bufferTexture = BufferTexture.createStream(ColorFormat.RGBA, ColorType.FLOAT32, 10000000);
        layout = new VertexFormat().textureCoordinate(2).normal(3).position(3);
        solidManager = new ShadeStyleManager("cp:shaders/gl3/solid.vert", "cp:shaders/gl3/solid.frag");
        solidsManager = new ShadeStyleManager("cp:shaders/gl3/solids.vert", "cp:shaders/gl3/solids.frag");

        vbos.put("box", generateSolidBox(gl3));
        //vaos.put("box", VertexArray.create(gl3, vbos.get("box"), layout, defaultShader));
    }

    private VertexBuffer generateSolidBox(GL3 gl3) {
        ByteBuffer buffer = ByteBuffer.allocateDirect(layout.size() * 6 * 6);
        BufferWriter bw = new BufferWriter(buffer);

        Vector3 nl = new Vector3(-1,0,0);
        Vector3 nr = new Vector3(1,0,0);
        Vector3 nu = new Vector3(0,1,0);
        Vector3 nd = new Vector3(0,-1,0);
        Vector3 nf = new Vector3(0,0,-1);
        Vector3 nb = new Vector3(0,0,1);

        Vector2 uv00 = new Vector2(0,0);
        Vector2 uv01 = new Vector2(0,1);
        Vector2 uv11 = new Vector2(1,1);
        Vector2 uv10 = new Vector2(1,0);

        double w = 0.5;
        double h = 0.5;
        double d = 0.5;

        Vector3 p000 = new Vector3(-w,h,-d);
        Vector3 p001 = new Vector3(-w,h,d);
        Vector3 p010 = new Vector3(-w,-h,-d);
        Vector3 p011 = new Vector3(-w,-h,d);
        Vector3 p100 = new Vector3(w,h,-d);
        Vector3 p101 = new Vector3(w,h,d);
        Vector3 p110 = new Vector3(w,-h,-d);
        Vector3 p111 = new Vector3(w,-h,d);

        {   // left
            bw.write(uv11).write(nl).write(p011) .write(uv10).write(nl).write(p010) .write(uv00).write(nl).write(p000);
            bw.write(uv00).write(nl).write(p000) .write(uv01).write(nl).write(p001) .write(uv11).write(nl).write(p011);
        }
        {   // right
            bw.write(uv00).write(nr).write(p100) .write(uv10).write(nr).write(p110) .write(uv11).write(nr).write(p111);
            bw.write(uv11).write(nr).write(p111) .write(uv01).write(nr).write(p101) .write(uv00).write(nr).write(p100);
        }
        {   // facing the camera, backwards
            bw.write(uv00).write(nb).write(p001) .write(uv10).write(nb).write(p101) .write(uv11).write(nb).write(p111);
            bw.write(uv11).write(nb).write(p111) .write(uv01).write(nb).write(p011) .write(uv00).write(nb).write(p001);
        }
        {   // facing along the camera direction, forwards
            bw.write(uv11).write(nf).write(p110) .write(uv10).write(nf).write(p100) .write(uv00).write(nf).write(p000);
            bw.write(uv00).write(nf).write(p000) .write(uv01).write(nf).write(p010) .write(uv11).write(nf).write(p110);
        }
        {   // up
            bw.write(uv00).write(nu).write(p000) .write(uv10).write(nu).write(p100) .write(uv11).write(nu).write(p101);
            bw.write(uv11).write(nu).write(p101) .write(uv01).write(nu).write(p001) .write(uv00).write(nu).write(p000);
        }
        {   // down
            bw.write(uv11).write(nd).write(p111) .write(uv10).write(nd).write(p110) .write(uv00).write(nd).write(p010);
            bw.write(uv00).write(nd).write(p010) .write(uv01).write(nd).write(p011) .write(uv11).write(nd).write(p111);
        }

        buffer.rewind();
        VertexBuffer box = VertexBuffer.createStatic(layout, 6 * 6, buffer);
        return box;
    }

    void subdiv(BufferWriter bw, Vector3 normal, Vector3 p00, Vector3 p01, Vector3 p11, Vector3 p10, Vector2 uv00, Vector2 uv01, Vector2 uv11, Vector2 uv10, int subdiv) {


        Vector3 pdt = p10.minus(p00).scale(1.0/subdiv);
        Vector3 pds = p01.minus(p00).scale(1.0/subdiv);

        Vector2 uvdt = uv10.minus(uv00).scale(1.0/subdiv);
        Vector2 uvds = uv01.minus(uv00).scale(1.0/subdiv);


        for (int s = 0; s < subdiv; ++s) {
            for (int t =0; t < subdiv; ++t) {

                Vector3 ip00 = p00.plus(pdt.scale(t)).plus(pdt.scale(s));
                Vector3 ip10 = p00.plus(pdt.scale(t+1)).plus(pds.scale(s));
                Vector3 ip01 = p00.plus(pdt.scale(t).plus(pds.scale(s+1)));
                Vector3 ip11 = p00.plus(pdt.scale(t+1).plus( pds.scale((s+1))));

                Vector2 iuv00 = uv00.plus(uvdt.scale(t)).plus(uvdt.scale(s));
                Vector2 iuv10 = uv10.plus(uvdt.scale(t+1)).plus(uvds.scale(s));
                Vector2 iuv01 = uv01.plus(uvdt.scale(t).plus(uvds.scale(s+1)));
                Vector2 iuv11 = uv11.plus(uvdt.scale(t+1).plus( uvds.scale((s+1))));


                bw.write(iuv11).write(normal).write(ip11) .write(iuv10).write(normal).write(ip10) .write(iuv00).write(normal).write(ip00);
                bw.write(iuv00).write(normal).write(ip00) .write(iuv01).write(normal).write(ip01) .write(iuv11).write(normal).write(ip11);

            }
        }




    }

    private VertexBuffer generateSolidBox(GL3 gl3, int subdiv) {
        ByteBuffer buffer = ByteBuffer.allocateDirect(layout.size() * 6 * 6);
        BufferWriter bw = new BufferWriter(buffer);

        Vector3 nl = new Vector3(-1,0,0);
        Vector3 nr = new Vector3(1,0,0);
        Vector3 nu = new Vector3(0,1,0);
        Vector3 nd = new Vector3(0,-1,0);
        Vector3 nf = new Vector3(0,0,-1);
        Vector3 nb = new Vector3(0,0,1);

        Vector2 uv00 = new Vector2(0,0);
        Vector2 uv01 = new Vector2(0,1);
        Vector2 uv11 = new Vector2(1,1);
        Vector2 uv10 = new Vector2(1,0);

        double w = 0.5;
        double h = 0.5;
        double d = 0.5;

        Vector3 p000 = new Vector3(-w,h,-d);
        Vector3 p001 = new Vector3(-w,h,d);
        Vector3 p010 = new Vector3(-w,-h,-d);
        Vector3 p011 = new Vector3(-w,-h,d);
        Vector3 p100 = new Vector3(w,h,-d);
        Vector3 p101 = new Vector3(w,h,d);
        Vector3 p110 = new Vector3(w,-h,-d);
        Vector3 p111 = new Vector3(w,-h,d);

        {   // left
            bw.write(uv11).write(nl).write(p011) .write(uv10).write(nl).write(p010) .write(uv00).write(nl).write(p000);
            bw.write(uv00).write(nl).write(p000) .write(uv01).write(nl).write(p001) .write(uv11).write(nl).write(p011);
        }
        {   // right
            bw.write(uv00).write(nr).write(p100) .write(uv10).write(nr).write(p110) .write(uv11).write(nr).write(p111);
            bw.write(uv11).write(nr).write(p111) .write(uv01).write(nr).write(p101) .write(uv00).write(nr).write(p100);
        }
        {   // facing the camera, backwards
            bw.write(uv00).write(nb).write(p001) .write(uv10).write(nb).write(p101) .write(uv11).write(nb).write(p111);
            bw.write(uv11).write(nb).write(p111) .write(uv01).write(nb).write(p011) .write(uv00).write(nb).write(p001);
        }
        {   // facing along the camera direction, forwards
            bw.write(uv11).write(nf).write(p110) .write(uv10).write(nf).write(p100) .write(uv00).write(nf).write(p000);
            bw.write(uv00).write(nf).write(p000) .write(uv01).write(nf).write(p010) .write(uv11).write(nf).write(p110);
        }
        {   // up
            bw.write(uv00).write(nu).write(p000) .write(uv10).write(nu).write(p100) .write(uv11).write(nu).write(p101);
            bw.write(uv11).write(nu).write(p101) .write(uv01).write(nu).write(p001) .write(uv00).write(nu).write(p000);
        }
        {   // down
            bw.write(uv11).write(nd).write(p111) .write(uv10).write(nd).write(p110) .write(uv00).write(nd).write(p010);
            bw.write(uv00).write(nd).write(p010) .write(uv01).write(nd).write(p011) .write(uv11).write(nd).write(p111);
        }

        buffer.rewind();
        VertexBuffer box = VertexBuffer.createStatic(layout, 6 * 6, buffer);
        return box;
    }


    public void solid(GL gl, Matrix44 projection, Matrix44 modelView, ColorRGBa fill, String solid, Vector3 offset, Vector3 dimensions, ShadeStyle shadeStyle) {

        Shader shader = solidManager.shader(shadeStyle, vbos.get(solid).format());


        shader.begin();

        solidManager.setParameters(shader, shadeStyle);
        shader.setUniform("projectionMatrix", projection);
        shader.setUniform("modelViewMatrix", modelView);

        shader.setUniform("modelViewProjectionMatrix", projection.multiply(modelView));
        shader.setUniform("fill", fill);
        shader.setUniform("offset", offset);
        shader.setUniform("dimensions", dimensions);
        shader.setUniform("normalMatrix", Transforms.normal(modelView));


        vbos.get(solid).bind();
        //VertexBufferDrawer.draw(gl.getGL3(), vaos.get(solid), GL.GL_TRIANGLES, 6 * 6);
        VertexBufferDrawer.draw(gl.getGL3(), shader, vbos.get(solid), layout, GL.GL_TRIANGLES, 6*6);
        vbos.get(solid).unbind();
        shader.end();
    }

    public void solids(GL gl, Matrix44 projection, Matrix44 modelView, ColorRGBa fill, String solid, List<Vector3> positions, List<Vector3> dimensions, ShadeStyle shadeStyle) {


        if (positions.size() != dimensions.size()) {
            throw new RuntimeException("size mismatch");
        }


        Shader shader = solidsManager.shader(shadeStyle, vbos.get(solid).format());

        BufferWriter bw = bufferTexture.shadow().writer();

        shader.begin();

        shader.setUniform("projectionMatrix", projection);
        shader.setUniform("modelViewMatrix", modelView);

        shader.setUniform("modelViewProjectionMatrix", projection.multiply(modelView));
        shader.setUniform("fill", fill);
        //shader.setUniform("offset", offset);
        //shader.setUniform("dimensions", dimensions);
        shader.setUniform("normalMatrix", Transforms.normal(modelView));
        shader.setUniform1i("dataBuffer" ,0);
        solidsManager.setParameters(shader, shadeStyle);

        bufferTexture.shadow().buffer().rewind();
        for (int i = 0; i < positions.size(); ++i) {
            bw.write(positions.get(i)).write(0.0f);
            bw.write(dimensions.get(i)).write(0.0f);
        }
        bufferTexture.shadow().buffer().rewind();
        bufferTexture.write(bufferTexture.shadow().buffer());
        bufferTexture.bindTexture(0);
        //VertexBufferDrawer.draw(gl.getGL3(), vaos.get(solid), GL.GL_TRIANGLES, 6 * 6);

        vbos.get(solid).bind();
        VertexBufferDrawer.drawInstances(gl.getGL3(), shader, vbos.get(solid), layout, GL.GL_TRIANGLES, 6*6, positions.size());
        bufferTexture.unbindTexture();
        vbos.get(solid).unbind();
        shader.end();
    }


}
