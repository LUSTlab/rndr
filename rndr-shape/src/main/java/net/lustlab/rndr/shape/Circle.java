package net.lustlab.rndr.shape;

import net.lustlab.rndr.math.Vector2;

public class Circle {
    public Vector2 center;
    public double radius;

    public Circle(Vector2 center, double radius) {
        this.center = center;
        this.radius = radius;
    }

    static Circle fromPoints(Vector2 a, Vector2 b) {
        Vector2 center = a.plus(b).scale(0.5);
        return new Circle(center, b.minus(center).length());
    }

    
    public boolean contains(Vector2 point) {
        return (point.minus(center).squaredLength() < radius*radius);
    }
}
