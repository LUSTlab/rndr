#version 330

layout(origin_upper_left) in vec4 gl_FragCoord;

${uniforms! }

uniform vec4 stroke;

out vec4 o_color;

in vec2 v_centerDistance;
uniform float xSmoothBias;
uniform float smooth_;


in float v_contourOffset;
uniform float contourLength;
uniform vec4 bounds;
uniform float strokeWeight;
uniform float strokeBias;

in vec3 v_boundsPosition;

void main(void) {

    vec3 boundsPosition = v_boundsPosition;
    vec2 screenPosition = gl_FragCoord.xy;
    float contourOffset = v_contourOffset;
    float contourFraction = v_contourOffset / contourLength;


   float du = v_centerDistance.x;
   float dv = v_centerDistance.y* ((strokeWeight+strokeBias)/strokeWeight);
   float wv = fwidth(dv);
   float wu = fwidth(du);
   float a = smoothstep(0, abs(wv*1.25), 1.0-abs(dv));
   float b = smoothstep(0, abs(wu*1.25), 1.0-abs(du));


    float contourDistance = abs(v_centerDistance.y);
    float contourPosition = v_centerDistance.y;
    vec4 x_stroke = stroke;
    { ${strokeTransform! } }


    o_color = x_stroke;
    o_color.a *= smooth_ * a* max(xSmoothBias, b) + (1.0-smooth_);


}