package net.lustlab.rndr.shaders;

import net.lustlab.colorlib.ColorRGBa;
import net.lustlab.rndr.buffers.VertexFormat;
import net.lustlab.rndr.draw.ShadeStyle;
import net.lustlab.rndr.buffers.ColorBuffer;
import net.lustlab.rndr.buffers.BufferTexture;
import net.lustlab.rndr.math.Matrix44;
import net.lustlab.rndr.math.Vector2;
import net.lustlab.rndr.math.Vector3;
import net.lustlab.rndr.math.Vector4;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.HashMap;
import java.util.Map;

public class ShadeStyleManager {
    Map<ShadeStyle.Structure, Shader> shaders = new HashMap<>();

    static Logger logger = LogManager.getLogger(ShadeStyleManager.class);
    public String vertexShaderUrl;
    public String fragmentShaderUrl;
    public Shader defaultShader;

    public ShadeStyleManager(String vertexShaderUrl, String fragmentShaderUrl) {
        this.vertexShaderUrl = vertexShaderUrl;
        this.fragmentShaderUrl = fragmentShaderUrl;
    }

    public Shader shader(ShadeStyle style, VertexFormat format) {
        if (style == null) {
            if (defaultShader == null) {
                logger.debug("creating default shader");
                ShadeStyle.Structure structure = new ShadeStyle.Structure();
                structure.attributesFromFormat(format);
                structure.varyingInFromFormat(format);
                structure.varyingOutFromFormat(format);
                structure.varyingBridgeFromFormat(format);
                defaultShader = new ShaderPreprocessor().fromUrls(structure, vertexShaderUrl, fragmentShaderUrl);
                String name = String.format("(%s, %s, style:null)", vertexShaderUrl, fragmentShaderUrl);
                defaultShader.name(name);
            }
            return defaultShader;
        } else {
            ShadeStyle.Structure structure = style.structural(format);
            structure.varyingInFromFormat(format);
            structure.varyingOutFromFormat(format);
            structure.varyingBridgeFromFormat(format);

            if (!structure.equals(structure)) {
                throw new RuntimeException("WTF!?");
            }
            Shader shader = shaders.get(structure);
            if (shader == null) {

                try {
                    shader = new ShaderPreprocessor().fromUrls(structure, vertexShaderUrl, fragmentShaderUrl);
                    String name = String.format("(%s, %s, style:%s)", vertexShaderUrl, fragmentShaderUrl, style.toString());
                    shader.name(name);
                    shaders.put(structure, shader);
                } catch (ShaderException e) {
                    shader = shader(null, format);
                    shaders.put(structure, shader);
                }

            }
            return shader;
        }
    }

    public void setParameters(Shader shader, ShadeStyle shadeStyle) {
        int textureIndex = 2;
        if (shadeStyle != null) {
            for (Map.Entry<String, Object> e : shadeStyle.parameterValues.entrySet()) {
                if (e.getValue().getClass() == Matrix44.class) {
                    Matrix44 v = (Matrix44) e.getValue();
                    shader.setUniform("p_" + e.getKey(), v);
                }

                if (e.getValue().getClass() == Float.class) {
                    Float v = (Float) e.getValue();
                    shader.setUniform1f("p_" + e.getKey(), v.floatValue());
                } if (e.getValue().getClass() == Vector3.class) {
                    Vector3 v = (Vector3) e.getValue();
                    shader.setUniform("p_"+e.getKey(), v);
                } if (e.getValue().getClass() == Vector2.class) {
                    Vector2 v = (Vector2) e.getValue();
                    shader.setUniform("p_"+e.getKey(), v);
                } if (e.getValue().getClass() == Vector4.class) {
                    Vector4 v = (Vector4) e.getValue();
                    shader.setUniform("p_"+e.getKey(), v);
                } if (e.getValue().getClass() == ColorRGBa.class) {
                    ColorRGBa v = (ColorRGBa) e.getValue();
                    shader.setUniform("p_"+e.getKey(), v);
                } if (e.getValue().getClass() == ColorBuffer.class) {
                    shader.setUniform1i("p_"+e.getKey(), textureIndex);
                    ColorBuffer cb = (ColorBuffer) e.getValue();
                    cb.bindTexture(textureIndex);
                    textureIndex++;
                }
                if (e.getValue().getClass() == BufferTexture.class) {
                    shader.setUniform1i("p_"+e.getKey(), textureIndex);
                    BufferTexture bt = (BufferTexture) e.getValue();
                    bt.bindTexture(textureIndex);
                    textureIndex++;
                }
            }
        }

    }

    public void destroy() {
        if (defaultShader != null) {
            defaultShader.destroy();
        }
    }
}
