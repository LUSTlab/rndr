package net.lustlab.rndr.math;

import java.nio.FloatBuffer;

public class Matrix44 {
	final public double m00, m10, m20, m30;
	final public double m01, m11, m21, m31;
	final public double m02, m12, m22, m32;
	final public double m03, m13, m23, m33;

	public final static Matrix44 IDENTITY = new Matrix44(1);

    public static Matrix44 fromMatrix33(Matrix33 m) {
        return new Matrix44(m.m00, m.m10, m.m20, 0,
                     m.m01, m.m11, m.m21, 0,
                     m.m02, m.m12, m.m22, 0,
                     0, 0, 0, 1);
    }

	public Matrix44() {
		m00 = m10 = m20 = m30 = .0;
		m01 = m11 = m21 = m31 = .0;
		m02 = m12 = m22 = m32 = .0;
		m03 = m13 = m23 = m33 = .0;
	}


	public Matrix44(final double diagonalValue) {
		m00 = m11 = m22 = m33 = diagonalValue;
		m01 = m02 = m03 = .0;
		m10 = m12 = m13 = .0;
		m20 = m21 = m23 = .0;
		m30 = m31 = m32 = .0;
	}

	public Matrix44(final double dv0, final double dv1, final double dv2, final double dv3) {
		m00 = dv0; m11 = dv1; m22 =dv2; m33 = dv3;
		m01 = m02 = m03 = .0;
		m10 = m12 = m13 = .0;
		m20 = m21 = m23 = .0;
		m30 = m31 = m32 = .0;
	}


	public static Matrix44 fromColumnVectors(final Vector4 col0, final Vector4 col1, final Vector4 col2, final Vector4 col3) {
		return new Matrix44(col0, col1, col2, col3);
	}

	@Deprecated
	public Matrix44(final Vector3 col0, final Vector3 col1, final Vector3 col2, final Vector3 col3) {
		this.m00 = col0.x; this.m10 = col1.x; this.m20 = col2.x; this.m30 = col3.x;
		this.m01 = col0.y; this.m11 = col1.y; this.m21 = col2.y; this.m31 = col3.y;
		this.m02 = col0.z; this.m12 = col1.z; this.m22 = col2.z; this.m32 = col3.z;
		this.m03 = 0f;     this.m13 = 0f;     this.m23 = 0f;     this.m33 = 1f;
	}

	@Deprecated
	public Matrix44(final Vector4 col0, final Vector4 col1, final Vector4 col2, final Vector4 col3) {
		this.m00 = col0.x; this.m10 = col1.x; this.m20 = col2.x; this.m30 = col3.x;
		this.m01 = col0.y; this.m11 = col1.y; this.m21 = col2.y; this.m31 = col3.y;
		this.m02 = col0.z; this.m12 = col1.z; this.m22 = col2.z; this.m32 = col3.z;
		this.m03 = col0.w; this.m13 = col1.w; this.m23 = col2.w; this.m33 = col3.w;
	}

    public Matrix44 inverse() {
        double n00 = m12*m23*m31 - m13*m22*m31 + m13*m21*m32 - m11*m23*m32 - m12*m21*m33 + m11*m22*m33;
        double n01 = m03*m22*m31 - m02*m23*m31 - m03*m21*m32 + m01*m23*m32 + m02*m21*m33 - m01*m22*m33;
        double n02 = m02*m13*m31 - m03*m12*m31 + m03*m11*m32 - m01*m13*m32 - m02*m11*m33 + m01*m12*m33;
        double n03 = m03*m12*m21 - m02*m13*m21 - m03*m11*m22 + m01*m13*m22 + m02*m11*m23 - m01*m12*m23;
        double n10 = m13*m22*m30 - m12*m23*m30 - m13*m20*m32 + m10*m23*m32 + m12*m20*m33 - m10*m22*m33;
        double n11 = m02*m23*m30 - m03*m22*m30 + m03*m20*m32 - m00*m23*m32 - m02*m20*m33 + m00*m22*m33;
        double n12 = m03*m12*m30 - m02*m13*m30 - m03*m10*m32 + m00*m13*m32 + m02*m10*m33 - m00*m12*m33;
        double n13 = m02*m13*m20 - m03*m12*m20 + m03*m10*m22 - m00*m13*m22 - m02*m10*m23 + m00*m12*m23;
        double n20 = m11*m23*m30 - m13*m21*m30 + m13*m20*m31 - m10*m23*m31 - m11*m20*m33 + m10*m21*m33;
        double n21 = m03*m21*m30 - m01*m23*m30 - m03*m20*m31 + m00*m23*m31 + m01*m20*m33 - m00*m21*m33;
        double n22 = m01*m13*m30 - m03*m11*m30 + m03*m10*m31 - m00*m13*m31 - m01*m10*m33 + m00*m11*m33;
        double n23 = m03*m11*m20 - m01*m13*m20 - m03*m10*m21 + m00*m13*m21 + m01*m10*m23 - m00*m11*m23;
        double n30 = m12*m21*m30 - m11*m22*m30 - m12*m20*m31 + m10*m22*m31 + m11*m20*m32 - m10*m21*m32;
        double n31 = m01*m22*m30 - m02*m21*m30 + m02*m20*m31 - m00*m22*m31 - m01*m20*m32 + m00*m21*m32;
        double n32 = m02*m11*m30 - m01*m12*m30 - m02*m10*m31 + m00*m12*m31 + m01*m10*m32 - m00*m11*m32;
        double n33 = m01*m12*m20 - m02*m11*m20 + m02*m10*m21 - m00*m12*m21 - m01*m10*m22 + m00*m11*m22;

        double d = determinant();
        return new Matrix44(n00/d, n10/d, n20/d, n30/d,
                            n01/d, n11/d, n21/d, n31/d,
                            n02/d, n12/d, n22/d, n32/d,
                            n03/d, n13/d, n23/d, n33/d);
    }

    public double determinant() {
        return m03*m12*m21*m30 - m02*m13*m21*m30 - m03*m11*m22*m30 + m01*m13*m22*m30+
                m02*m11*m23*m30 - m01*m12*m23*m30 - m03*m12*m20*m31 + m02*m13*m20*m31+
                m03*m10*m22*m31 - m00*m13*m22*m31 - m02*m10*m23*m31 + m00*m12*m23*m31+
                m03*m11*m20*m32 - m01*m13*m20*m32 - m03*m10*m21*m32 + m00*m13*m21*m32+
                m01*m10*m23*m32 - m00*m11*m23*m32 - m02*m11*m20*m33 + m01*m12*m20*m33+
                m02*m10*m21*m33 - m00*m12*m21*m33 - m01*m10*m22*m33 + m00*m11*m22*m33;
    }

	public Matrix44(
			final double x00, final double x10, final double x20, final double x30,
			final double x01, final double x11, final double x21, final double x31,
			final double x02, final double x12, final double x22, final double x32,
			final double x03, final double x13, final double x23, final double x33) {
		// Col 1
		this.m00 = x00;
		this.m01 = x01;
		this.m02 = x02;
		this.m03 = x03;

		// Col 2
		this.m10 = x10;
		this.m11 = x11;
		this.m12 = x12;
		this.m13 = x13;

		// Col 3
		this.m20 = x20;
		this.m21 = x21;
		this.m22 = x22;
		this.m23 = x23;

		// Col 4
		this.m30 = x30;
		this.m31 = x31;
		this.m32 = x32;
		this.m33 = x33;
	}

	public Matrix44(final double[] mat) {
		assert mat.length >= 16 : "Invalid matrix array length";

		int i = 0;

		// Col 1
		m00 = mat[i++];
		m01 = mat[i++];
		m02 = mat[i++];
		m03 = mat[i++];

		// Col 2
		m10 = mat[i++];
		m11 = mat[i++];
		m12 = mat[i++];
		m13 = mat[i++];

		// Col 3
		m20 = mat[i++];
		m21 = mat[i++];
		m22 = mat[i++];
		m23 = mat[i++];

		// Col 4
		m30 = mat[i++];
		m31 = mat[i++];
		m32 = mat[i++];
		m33 = mat[i++];
	}

	public Matrix44(final Matrix44 mat) {
		this.m00 = mat.m00;
		this.m01 = mat.m01;
		this.m02 = mat.m02;
		this.m03 = mat.m03;

		this.m10 = mat.m10;
		this.m11 = mat.m11;
		this.m12 = mat.m12;
		this.m13 = mat.m13;

		this.m20 = mat.m20;
		this.m21 = mat.m21;
		this.m22 = mat.m22;
		this.m23 = mat.m23;

		this.m30 = mat.m30;
		this.m31 = mat.m31;
		this.m32 = mat.m32;
		this.m33 = mat.m33;
	}

	public Vector4 getColumn(final int columnIndex) {
		assert columnIndex < 4 : "Invalid column index = " + columnIndex;

		switch (columnIndex) {
			case 0:
				return new Vector4(m00, m01, m02, m03);
			case 1:
				return new Vector4(m10, m11, m12, m13);
			case 2:
				return new Vector4(m20, m21, m22, m23);
			case 3:
				return new Vector4(m30, m31, m32, m33);
			default:
				throw new IllegalArgumentException("Invalid column index = " + columnIndex);
		}
	}

    public Vector4 times(Vector4 v) {
        return v.multiply(this);
    }

    public Matrix44 times(Matrix44 mat) {
        return multiply(mat);
    }

    public Matrix44 times(double s) {
        return scale(s);
    }

	public Matrix44 multiply(Matrix44 mat) {

		return new Matrix44(
				this.m00 * mat.m00 + this.m10 * mat.m01 + this.m20 * mat.m02 + this.m30 * mat.m03, // m00
                this.m00 * mat.m10 + this.m10 * mat.m11 + this.m20 * mat.m12 + this.m30 * mat.m13, // m10
                this.m00 * mat.m20 + this.m10 * mat.m21 + this.m20 * mat.m22 + this.m30 * mat.m23, // m20
                this.m00 * mat.m30 + this.m10 * mat.m31 + this.m20 * mat.m32 + this.m30 * mat.m33, // m30


                this.m01 * mat.m00 + this.m11 * mat.m01 + this.m21 * mat.m02 + this.m31 * mat.m03, // m01
                this.m01 * mat.m10 + this.m11 * mat.m11 + this.m21 * mat.m12 + this.m31 * mat.m13, // m11
                this.m01 * mat.m20 + this.m11 * mat.m21 + this.m21 * mat.m22 + this.m31 * mat.m23, // m21
                this.m01 * mat.m30 + this.m11 * mat.m31 + this.m21 * mat.m32 + this.m31 * mat.m33, // m31

                this.m02 * mat.m00 + this.m12 * mat.m01 + this.m22 * mat.m02 + this.m32 * mat.m03, // m02
                this.m02 * mat.m10 + this.m12 * mat.m11 + this.m22 * mat.m12 + this.m32 * mat.m13, // m12
                this.m02 * mat.m20 + this.m12 * mat.m21 + this.m22 * mat.m22 + this.m32 * mat.m23, // m22
                this.m02 * mat.m30 + this.m12 * mat.m31 + this.m22 * mat.m32 + this.m32 * mat.m33, // m32

                this.m03 * mat.m00 + this.m13 * mat.m01 + this.m23 * mat.m02 + this.m33 * mat.m03, // m03
                this.m03 * mat.m10 + this.m13 * mat.m11 + this.m23 * mat.m12 + this.m33 * mat.m13, // m13
                this.m03 * mat.m20 + this.m13 * mat.m21 + this.m23 * mat.m22 + this.m33 * mat.m23, // m23
                this.m03 * mat.m30 + this.m13 * mat.m31 + this.m23 * mat.m32 + this.m33 * mat.m33 // m33
		);
	}

    public Matrix44 scale(double s) {
        return new Matrix44(s*m00, s*m10, s*m20, s*m30,
                            s*m01, s*m11, s*m21, s*m31,
                            s*m02, s*m12, s*m22, s*m32,
                            s*m03, s*m13, s*m23, s*m33);
    }

//	public Matrix44 translate(final Vector3 translation) {
//		Vector4 v0 = new Vector4(m00 * translation.x, m01 * translation.x, m02 * translation.x, m03 * translation.x);
//		Vector4 v1 = new Vector4(m10 * translation.y, m11 * translation.y, m12 * translation.y, m13 * translation.y);
//		Vector4 v2 = new Vector4(m20 * translation.z, m21 * translation.z, m22 * translation.z, m23 * translation.z);
//		Vector4 v3 = new Vector4(m30, m31, m32, m33);
//
//		Vector4 result = v0.plus(v1).plus(v2).plus(v3);
//
//		return new Matrix44(
//				m00, m01, m02, m03,
//				m10, m11, m12, m13,
//				m20, m21, m22, m23,
//				result.x, result.y, result.z, result.w
//		);
//	}

    public double sum() {
        return m00+m01+m02+m03+m10+m11+m12+m13+m20+m21+m22+m23+m30+m31+m32+m33;
    }


    public Matrix44 plus(Matrix44 o) {
        return new Matrix44(
                m00 + o.m00, m10 + o.m10, m20 + o.m20, m30 + o.m30,
                m01 + o.m01, m11 + o.m11, m21 + o.m21, m31 + o.m31,
                m02 + o.m02, m12 + o.m12, m22 + o.m22, m32 + o.m32,
                m03 + o.m03, m13 + o.m13, m23 + o.m23, m33 + o.m33);
    }

    public Matrix44 minus(Matrix44 o) {
        return new Matrix44(
                m00 - o.m00, m10 - o.m10, m20 - o.m20, m30 - o.m30,
                m01 - o.m01, m11 - o.m11, m21 - o.m21, m31 - o.m31,
                m02 - o.m02, m12 - o.m12, m22 - o.m22, m32 - o.m32,
                m03 - o.m03, m13 - o.m13, m23 - o.m23, m33 - o.m33);
    }


    public Matrix44 transpose() {
		return new Matrix44(
				m00, m01, m02, m03,
				m10, m11, m12, m13,
				m20, m21, m22, m23,
				m30, m31, m32, m33
		);
	}

	public Matrix44 toFloatBuffer(FloatBuffer fb) {
		fb.put((float)m00);
		fb.put((float)m01);
		fb.put((float)m02);
		fb.put((float)m03);

		fb.put((float)m10);
		fb.put((float)m11);
		fb.put((float)m12);
		fb.put((float)m13);

		fb.put((float)m20);
		fb.put((float)m21);
		fb.put((float)m22);
		fb.put((float)m23);

		fb.put((float)m30);
		fb.put((float)m31);
		fb.put((float)m32);
		fb.put((float)m33);

		return this;

	}

    @Override
    public String toString() {
        return "Matrix44{" +
                "m00=" + m00 +
                ", m10=" + m10 +
                ", m20=" + m20 +
                ", m30=" + m30 +
                "\n     m01=" + m01 +
                ",      m11=" + m11 +
                ",      m21=" + m21 +
                ",      m31=" + m31 +
                "\n     m02=" + m02 +
                ",      m12=" + m12 +
                ",      m22=" + m22 +
                ",      m32=" + m32 +
                "\n         m03=" + m03 +
                ",      m13=" + m13 +
                ",      m23=" + m23 +
                ",      m33=" + m33 +
                '}';
    }


}
