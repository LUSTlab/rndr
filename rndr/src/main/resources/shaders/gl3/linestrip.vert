#version 330

in vec3 position;
in vec3 normal;
in vec4 color;
in float offset;

in vec2 texCoord0;

out vec3 v_normal;
out vec4 v_color;
out mat4 v_modelViewMatrix;
out vec2 v_texCoord0;

out vec2 v_centerDistance;
out float v_contourOffset;

uniform mat4 modelViewMatrix;
uniform mat4 modelViewProjectionMatrix;
uniform mat4 normalMatrix;

uniform float strokeWeight;
uniform float contourLength;

uniform vec4 bounds;
out vec3 v_boundsPosition;

void main(void) {


    vec3 x_position = position;
    vec3 x_normal = normal;

    v_boundsPosition =( (vec3(position.xy - bounds.xy, 0 ) / vec3(bounds.zw , 1)) - vec3(0.5,0.5,0)) * 2.0;

    { ${positionTransform! } }
//
    v_centerDistance = vec2((texCoord0.x-0.5)*2, texCoord0.y);
    //v_centerDistance *= ((strokeWeight+3)/(strokeWeight));
    v_contourOffset = offset;

    gl_Position = modelViewProjectionMatrix * vec4(x_position, 1.0);
    v_normal = (normalMatrix * vec4(normal.xyz, 0.0)).xyz;
    v_texCoord0 = texCoord0;

}