//package net.lustlab.rndr.draw.gl3;
//
//import com.jogamp.opengl.GL;
//import com.jogamp.opengl.GL3;
//import com.jogamp.opengl.GLContext;
//import net.lustlab.colorlib.ColorRGBa;
//import net.lustlab.rndr.buffers.*;
//import net.lustlab.rndr.draw.DrawContext;
//import net.lustlab.rndr.draw.DrawStyle;
//import net.lustlab.rndr.draw.ShadeStyle;
//import net.lustlab.rndr.math.*;
//import net.lustlab.rndr.shaders.ShadeStyleManager;
//import net.lustlab.rndr.shaders.Shader;
//
//import java.nio.ByteBuffer;
//import java.util.List;
//
//public class LineRenderer {
//
//    VertexBuffer vbo;
//    ShadeStyleManager shaderManager;
//
//    public LineRenderer() {
//        // -- linestrip shader --
//        shaderManager = new ShadeStyleManager("cp:shaders/gl3/linestrip.vert", "cp:shaders/gl3/linestrip.frag");
//
//
//        vbo = VertexBuffer.createDynamic(
//                new VertexFormat().textureCoordinate(2).position(3).attribute("offset", 1, VertexElementType.FLOAT32),
//                10000 * 6);
//
//
//
//    }
//
//
//    public void drawLineStrip(DrawContext context, DrawStyle style, List<Vector2> points) {
//        drawLineStrip(context, style, points, false);
//    }
//
//    public void drawLineLoop(DrawContext context, DrawStyle style, List<Vector2> points) {
//        drawLineStrip(context, style, points, true);
//    }
//
//    public static double generateLineStrip(ByteBuffer buffer, List<Vector2> points, boolean close, double strokeWeight, boolean smooth) {
//
//        double strokeBias = smooth? 1: 0;
//        double strokeGain = smooth? 1: 0.5;
//
//        double effectiveStroke = (strokeWeight*strokeGain + strokeBias);
//
//
//        Vector2 pi0 = null;
//        Vector2 pi1 = null;
//        int pc = points.size();
//
//
//        buffer.rewind();
//        BufferWriter bw = new BufferWriter(buffer);
//
//        int c = close ? 0 : 1;
//
//
//        double offset = 0;
//
//
//
//        for (int n = close?-1:0; n < pc - c; ++n) {
//
//            int n0 = ((n % pc) + pc) % pc;
//            int n1 = (n + 1) % pc;
//            int n2 = close ? (n+2) % pc : Math.min(points.size()-1,n + 2);
//
//            Vector2 p0 = points.get(n0);
//            Vector2 p1 = points.get(n1);
//            Vector2 p2 = points.get(n2);
//
//            double length = p1.minus(p0).length();
//
//            Vector2 d10 = p1.minus(p0).normalized();
//            Vector2 d21 = p2.minus(p1).normalized();
//
//            if (p1 == p2) {
//                d21 = d10;
//            }
//
//
//            Vector2 n10 = new Vector2(-d10.y, d10.x).scale(effectiveStroke);
//            Vector2 n21 = new Vector2(-d21.y, d21.x).scale(effectiveStroke);
//
//            Vector2 i0;
//            Vector2 i1;
//
//
//            if (p1 != p2 && length > 0.01 /*&& d10.dot(d21) > -0.7*/) {
//
//
//                Vector2 tangent = d10.normalized().plus(d21.normalized()).normalized();
//                Vector2 perp = d10.normalized().perpendicular();
//                Vector2 miter = tangent.perpendicular();
//
//
//
//
//                double l = effectiveStroke / miter.dot(perp);
//                double eps = 10.0;
//
//                l = Math.min(effectiveStroke * Math.sqrt(4), l);
////                i0 = p1.minus(tangent.normalized().perpendicular().scale(l));
//                i0 = Intersections.intersect(p0.minus(n10), p1.minus(n10).plus(d10.scale(0)),p1.minus(n21).minus(d21.scale(0)), p2.minus(n21), eps);  //p1.plus(tangent.normalized().perpendicular().scale(l));
//                i1 = Intersections.intersect(p0.plus(n10), p1.plus(n10).plus(d10.scale(0)),p1.plus(n21).minus(d21.scale(0)), p2.plus(n21), eps);  //p1.plus(tangent.normalized().perpendicular().scale(l));
//
//
////                if (length < effectiveStroke) {
////                    i0 = p1.minus(tangent.normalized().perpendicular().scale(l));
////                    i1 = p1.plus(tangent.normalized().perpendicular().scale(l));
////                    //System.out.println("gr");
////                }
//
//
//                if (i0 == Vector2.Infinity) {
//                    i0 = p1.minus(tangent.normalized().perpendicular().scale(l));
//                }
//                if (i1 == Vector2.Infinity) {
//                    i1 = p1.plus(tangent.normalized().perpendicular().scale(l));
//                }
//            }
//            else {
//                i0 = p1.minus(n10);
//                i1 = p1.plus(n10);
//            }
//
//
//            if (pi0 == null) {
//                pi0 = p0.minus(n10);
//                pi1 = p0.plus(n10);
//            }
//
//
//
//            if (n >=0 ) {
//                float nn0 = (float) n0 / (points.size() - 1);
//                float nn1 = (float) n1 / (points.size() - 1);
//
//                bw.write(nn0, -1);
//                bw.write(pi0.x, pi0.y, 0);
//                bw.write((float) offset);
//
//                bw.write(nn1, -1);
//                bw.write(i0.x, i0.y, 0);
//                bw.write((float) (offset + length));
//
//                bw.write(nn1, 1);
//                bw.write(i1.x, i1.y, 0);
//                bw.write((float) (offset + length));
//
//                bw.write(nn0, -1);
//                bw.write(pi0.x, pi0.y, 0);
//                bw.write((float) offset);
//
//                bw.write(nn0, 1);
//                bw.write(pi1.x, pi1.y, 0);
//                bw.write((float) offset);
//
//                bw.write(nn1, 1);
//                bw.write(i1.x, i1.y, 0);
//                bw.write((float) (offset + length));
//
//            }
//
//            pi0 = i0;
//            pi1 = i1;
//            offset+=length;
//        }
//
//        return offset;
//
//    }
//
//    public void drawLineStrip(DrawContext context, DrawStyle style,List<Vector2> points, boolean close) {
//
//        StateTools.setupState(style);
//        double minX = Double.POSITIVE_INFINITY;
//        double minY = Double.POSITIVE_INFINITY;
//
//        double maxX = Double.NEGATIVE_INFINITY;
//        double maxY = Double.NEGATIVE_INFINITY;
//
//
//        for (Vector2 point: points) {
//            minX = Math.min(minX, point.x);
//            minY = Math.min(minY, point.y);
//            maxX = Math.max(maxX, point.x);
//            maxY = Math.max(maxY, point.y);
//        }
//
//
//        Shader lineStripShader = shaderManager.shader(style.shadeStyle, vbo.format());
//        lineStripShader.begin();
//        ShaderTools.setupShader(lineStripShader, context, style);
//        lineStripShader.setUniform1f("smooth_", style.smooth?1:0);
//        lineStripShader.setUniform1f("strokeBias", style.smooth?1:0);
//        lineStripShader.setUniform1f("xSmoothBias", close?1:0);
//        lineStripShader.setUniform("bounds", new Vector4(minX, minY, Math.max(1,maxX-minX), Math.max(1,maxY-minY)));
//
//        double offset = generateLineStrip(vbo.shadow().buffer(), points, close, style.strokeWeight, style.smooth);
//
//        int size = vbo.shadow().buffer().position();
//        vbo.shadow().buffer().rewind();
//        vbo.write(vbo.shadow().buffer(), 0, size);
//
//
//        shaderManager.setParameters(lineStripShader, style.shadeStyle);
//        lineStripShader.setUniform1f("contourLength", (float) offset);
//
//        GL3 gl3 = GLContext.getCurrentGL().getGL3();
//        VertexBufferDrawer.draw(gl3, lineStripShader, vbo, vbo.format(), GL.GL_TRIANGLES, size / vbo.format().size());
//
//        lineStripShader.end();
//
//    }
//
//    public void destroy() {
//        shaderManager.destroy();
//        vbo.destroy();
//    }
//
//}
//
