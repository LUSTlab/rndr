package net.lustlab.rndr.post.blur;

import net.lustlab.rndr.buffers.ColorBuffer;
import net.lustlab.rndr.post.Filter;
import net.lustlab.rndr.shaders.FragmentShader;


public class CocBlur extends Filter {

    public CocBlur() {
        super(FragmentShader.fromUrl("cp:shaders/gl3/post/blur/coc-blur.frag"));
    }

    public int window = 4;
    public double sigma = 4.0;


    public double focus = 10.0;

    @Override
    public Filter apply(ColorBuffer[] src, ColorBuffer[] dest) {


        parameter("window", window);
        parameter("sigma", (float)sigma);
        super.apply(src, dest);
        return this;
    }
}
