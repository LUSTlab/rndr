package net.lustlab.rndr.buffers;

import com.jogamp.opengl.GL;
import com.jogamp.opengl.GL3;
import com.jogamp.opengl.GLContext;

import java.nio.IntBuffer;

public final class DepthBuffer {

    public final int texture;
    public final int width;
    public final int height;

    boolean destroyed = false;
    public final DepthFormat format;

    @Override
    public String toString() {
        return "DepthBuffer{" +
                "width=" + width +
                ", height=" + height +
                ", format=" + format +
                '}';
    }

    public final boolean hasStencil() {
        return format == DepthFormat.DEPTH32F_STENCIL8
                || format == DepthFormat.DEPTH24_STENCIL8;
    }

    public static DepthBuffer create(GL gl, int width, int height) {
        return create(gl, width, height, DepthFormat.DEPTH32F_STENCIL8);
    }


    public static DepthBuffer create(int width, int height) {
        return create(GLContext.getCurrentGL(), width, height);
    }


    public static DepthBuffer create(int width, int height, DepthFormat format) {
        return create(GLContext.getCurrentGL(), width, height, format);
    }

    public static DepthBuffer create(GL gl, int width, int height, DepthFormat format) {
        IntBuffer textures = IntBuffer.allocate(1);
        gl.glGenTextures(1, textures);
        int texture = textures.get(0);
        gl.glBindTexture(GL.GL_TEXTURE_2D, texture);

        gl.glTexImage2D(GL.GL_TEXTURE_2D, 0, format.toGLFormat(), width, height, 0, GL3.GL_DEPTH_COMPONENT, GL.GL_UNSIGNED_BYTE, null);

        gl.glTexParameteri(GL.GL_TEXTURE_2D, GL.GL_TEXTURE_MIN_FILTER, GL.GL_LINEAR);
        gl.glTexParameteri(GL.GL_TEXTURE_2D, GL.GL_TEXTURE_MAG_FILTER, GL.GL_LINEAR);
        gl.glTexParameteri(GL.GL_TEXTURE_2D, GL.GL_TEXTURE_WRAP_S, GL.GL_CLAMP_TO_EDGE);
        gl.glTexParameteri(GL.GL_TEXTURE_2D, GL.GL_TEXTURE_WRAP_T, GL.GL_CLAMP_TO_EDGE);

        return new DepthBuffer(texture, width, height, format);
    }

    private DepthBuffer(int texture, int width, int height, DepthFormat format) {
        this.texture = texture;
        this.width = width;
        this.height = height;
        this.format = format;
    }

    public void bindTexture(GL gl) {
        gl.glBindTexture(GL.GL_TEXTURE_2D, texture);
    }


    public void destroy() {
        if (!destroyed) {
            GL gl = GLContext.getCurrent().getGL();
            gl.glDeleteTextures(1, new int[] {texture},0);

            destroyed = true;
        }
    }


    public final DepthFormat format() {
        return format;
    }
}
