package net.lustlab.rndr.shape;

import net.lustlab.colorlib.ColorRGBa;
import net.lustlab.rndr.math.Vector2;
import net.lustlab.rndr.math.Vector3;
import net.lustlab.rndr.shape.gpcj.Poly;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * Polygon
 */
public class Polygon implements Iterable<Contour>, Serializable {

    /**
     * The polygon contours. The first contour describes the polygon outline, the following contours describe holes.
     */
	public List<Contour> contours = new ArrayList<>();
	public ColorRGBa fill;
	public ColorRGBa stroke;


    public boolean containsPoint(Vector2 point) {
        int leftCount = 0;
        int rightCount = 0;

        for (Contour _contour: contours) {

            PolygonContour contour = (PolygonContour) _contour;

            for (int i = 0; i < contour.vertices.size(); ++i) {

                Vertex left = contour.vertices.get(i);
                Vertex right = contour.vertices.get((i + 1) % contour.vertices.size());

                if (left.position.y <= point.y && right.position.y >=point.y || right.position.y <= point.y && left.position.y >= point.y) {

                    double dy = left.position.y - right.position.y;
                    double dx = left.position.x - right.position.x;
                    double dpy = point.y - right.position.y;

                    double x;
                    if (dx == 0) {
                        x = right.position.x;
                    } else {
                        x = (dpy/dy) * dx + right.position.x;
                    }
                    if (x < point.x) {
                        leftCount++;
                    } else {
                        rightCount++;
                    }

                }

            }
            break;
        }
        return leftCount % 2 == 1 && rightCount %2 == 1;


    }

    public static Polygon fromPoints(List<Vector2> points) {

        if (points.size() >= 3) {

            Polygon polygon = new Polygon();

            PolygonContour contour = new PolygonContour();

            for (Vector2 v2 : points) {
                Vertex v = new Vertex();
                v.position = new Vector3(v2.x, v2.y, 0);
                contour.vertices.add(v);
            }

            polygon.contours.add(contour);


            return polygon;
        } else {
            throw new RuntimeException("can't construct polygons from fewer than 3 points (I got" + points.size() +")");
        }
    }

//    @Override
    public Rectangle bounds() {

        double minx = Double.POSITIVE_INFINITY;
        double maxx = Double.NEGATIVE_INFINITY;
        double miny = Double.POSITIVE_INFINITY;
        double maxy = Double.NEGATIVE_INFINITY;

//        Matrix44 m = transform.matrix;

        for (Contour _contour: contours) {

            PolygonContour contour = (PolygonContour) _contour;
            for (Vertex segment: contour.vertices) {

                Vector3 p = segment.position;
//                p = p.multiply(m);

                minx = Math.min(minx, p.x);
                maxx = Math.max(maxx, p.x);
                miny = Math.min(miny, p.y);
                maxy = Math.max(maxy, p.y);
            }

        }
        return new Rectangle(minx, miny, maxx-minx, maxy-miny);
    }

	public boolean contoursClosed() {
		boolean closed = true;
		for (Contour contour: contours) {
			if (!contour.closed()) {
				closed = false;
				break;
			}
		}
		return closed;
	}

    /**
     * Returns an iterator over the polygon contours
     * @return PolygonCounter iterator
     */
	@Override
	public Iterator<Contour> iterator() {
		return contours.iterator();
	}

    @Override
    public String toString() {
        return "Polygon{" +
                "contours=" + contours +
                ", fill=" + fill +
                ", stroke=" + stroke +
                "} " + super.toString();
    }


    public Polygon unite(Polygon other) {
        return PolygonTools.fromGPCPolygon(PolygonTools.toGPCPolygon(this).union(PolygonTools.toGPCPolygon(other)));
    }

    public Polygon minus(Polygon other) {
        return PolygonTools.fromGPCPolygon(PolygonTools.toGPCPolygon(this).difference(PolygonTools.toGPCPolygon(other)));
    }

    public boolean intersects(Polygon other) {
        return !PolygonTools.toGPCPolygon(this).intersection(PolygonTools.toGPCPolygon(other)).isEmpty();
    }


    public Polygon intersect(Polygon other) {
        return PolygonTools.fromGPCPolygon(PolygonTools.toGPCPolygon(this).intersection(PolygonTools.toGPCPolygon(other)));
    }

    public Polygon exclude(Polygon other) {
        return PolygonTools.fromGPCPolygon(PolygonTools.toGPCPolygon(this).xor(PolygonTools.toGPCPolygon(other)));
    }

}
