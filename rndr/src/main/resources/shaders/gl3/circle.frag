#version 330

layout(origin_upper_left) in vec4 gl_FragCoord;

in vec2 v_texCoord0;

${uniforms! }
// -- uniforms --
uniform vec4 fill;
uniform vec4 stroke;

uniform vec2 dimensions;
uniform float strokeWeight;
uniform sampler2D image;
uniform float radius;

out vec4 o_color;

in vec3 v_boundsPosition;
in vec3 v_modelPosition;
in vec3 v_cameraPosition;

void main(void) {

    // -- shade style support variables
    vec3 boundsPosition = v_boundsPosition;
    vec3 modelPosition = v_modelPosition;
    vec3 cameraPosition = v_cameraPosition;
    vec2 screenPosition = gl_FragCoord.xy;
    int instance = 0;

    vec4 x_fill = fill;

    vec4 x_stroke = stroke;
    { ${fillTransform! } }

    {
        float contourLength = radius * 3.1415926535 * 2;
        float contourOffset = atan(boundsPosition.y, boundsPosition.x) + 3.1415826535;
        float contourFraction = contourOffset / 3.1415926535;
        contourOffset *= radius;
        ${strokeTransform! }
    }

    float wd = fwidth(length(v_texCoord0 - vec2(0.5)));
    float d = length(v_texCoord0 - vec2(0.5)) * 2;

    float f = smoothstep(0, wd * 2.5, 1.0 - d);
    float b = (strokeWeight) / radius;
    float f2 = smoothstep(0, wd * 2.5, 1.0 - b - d) * f;

    o_color = vec4(x_fill.rgb*f2 + x_stroke.rgb*(1.0-f2), (x_fill.a*f) * f2 + (x_stroke.a*f) * (1.0-f2) );
}