package net.lustlab.rndr.font;

import java.io.Serializable;

public final class FontDescriptor implements Serializable {
	private static final long serialVersionUID = 1L;
	final public String name;
	final public int size;
	
	public FontDescriptor(String name, int size) {
		this.name = name;
		this.size = size;
	}
	
	@Override
	public int hashCode() {
		return (name+"@"+size).hashCode();
	}
	
	@Override
	public boolean equals(Object obj) {
		FontDescriptor descriptor = (FontDescriptor) obj;
		if (descriptor != null) {
			return (name.equals(descriptor.name) && size == (descriptor.size));
		}
		else {
			return false;
		}
	}
	
}


