package net.lustlab.rndr.shape;


import net.lustlab.rndr.shape.gpcj.Poly;
import net.lustlab.rndr.shape.gpcj.PolyDefault;
import net.lustlab.rndr.shape.gpcj.PolySimple;
import net.lustlab.rndr.math.Vector2;

class PolygonTools {

    static PolyDefault toGPCPolygon(PolygonContour contour) {

        if (contour.vertices.size() < 3) {
            throw new RuntimeException("contour has less than 3 vertices (" +  contour.vertices.size()+ ")");
        }

        PolyDefault simple = new PolyDefault();
        for (Vertex point: contour.vertices) {
            simple.add(point.position.x, point.position.y);
        }
        return simple;
    }

    static PolyDefault toGPCPolygon(Polygon polygon) {
        PolyDefault polyDefault = new PolyDefault();
        int i = 0;
        for (Contour contour: polygon) {
            PolyDefault c = toGPCPolygon((PolygonContour)contour);
            c.setIsHole(i > 0);
            c.setContributing(0, true);
            polyDefault.add(c);
            i++;
        }

        return polyDefault;
    }

    static PolySimple toGPCSimplePolygon(PolygonContour contour) {
        if (contour.vertices.size() < 3) {
            throw new RuntimeException("contour has less than 3 vertices (" +  contour.vertices.size()+ ")");
        }

        PolySimple simple = new PolySimple();
        for (Vertex point: contour.vertices) {
            simple.add(point.position.x, point.position.y);
        }
        return simple;
    }

    private final static Vector2[] extractPolyVertices(Poly poly) {
        int n = poly.getNumPoints();
        Vector2[] points = new Vector2[n];
        for (int i = 0; i < n; i++)
            points[i] = new Vector2(poly.getX(i), poly.getY(i));
        return points;
    }


    public static Polygon fromGPCPolygon(Poly poly) {

        int n = poly.getNumInnerPoly();
        Polygon polygon = new Polygon();

        // if the result is single, create a SimplePolygon
        if (n == 1) {
            PolygonContour contour = new PolygonContour();
            contour.closed = true;
            for (Vector2 point: extractPolyVertices(poly.getInnerPoly(0))) {
                contour.vertex(point);
            }

            polygon.contours.add(contour);

        }
        else {
            for (int i = 0; i < n; i++) {
                polygon.contours.add(fromGPCSimplePolygon(poly.getInnerPoly(i)));
            }

        }
        return polygon;
    }

    private static PolygonContour fromGPCSimplePolygon(Poly innerPoly) {
        PolygonContour contour = new PolygonContour();
        contour.closed = true;

        for (Vector2 point: extractPolyVertices(innerPoly)) {
            contour.vertex(point);
        }
        return contour;
    }
}
