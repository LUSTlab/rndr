package net.lustlab.rndr.draw;

import net.lustlab.colorlib.ColorRGBa;
import net.lustlab.rndr.buffers.*;
import net.lustlab.rndr.font.FontMap;
import net.lustlab.rndr.math.*;
import net.lustlab.rndr.shape.*;
import net.lustlab.rndr.text.Writer;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Stack;

import static net.lustlab.rndr.math.Linear.multiply;

@SuppressWarnings("UnusedDeclaration")
public abstract class Drawer {

    Map<String, ColorBuffer> svgImages = new HashMap<>();
    protected Stack<Matrix44> modelViewStack = new Stack<>();
    protected Stack<Matrix44> projectionStack = new Stack<>();

    protected Matrix44 modelViewMatrix = Matrix44.IDENTITY;
    protected Matrix44 projectionMatrix = Matrix44.IDENTITY;

    protected int width;
    protected int height;

    protected Stack<DrawStyle> styleStack = new Stack<>();

    public abstract DrawContext drawContext();

    protected DrawStyle style = new DrawStyle();

    public final DrawStyle drawStyle() {
        return style;
    }

    public Drawer this_() {
        //noinspection unchecked
        return this;
    }

    public int channelMask() {
        return style.channelMask;
    }

    public Drawer channelMask(int mask) {
        style.channelMask = mask;
        return this;
    }

    public Drawer channelMask(boolean red, boolean green, boolean blue, boolean alpha) {
        style.channelMask = (red?0:ChannelMask.RED) +
                (green?0:ChannelMask.GREEN) +
                (blue?0:ChannelMask.BLUE) +
                (alpha?0:ChannelMask.ALPHA);
        return this;
    }


    public final Drawer colorMatrix(Matrix44 colorMatrix) {
        style.colorMatrix = colorMatrix;
        return this_();
    }

    public final Matrix44 colorMatrix() {
        return style.colorMatrix;
    }


    public final ColorRGBa tint() {
        return drawStyle().tint;
    }

    public final Drawer tint(ColorRGBa tint) {
        drawStyle().tint = tint;
        return this_();
    }

    public final Drawer noTint() {
        drawStyle().tint = ColorRGBa.WHITE;
        return this_();
    }

    public final ColorRGBa fill() {
        return drawStyle().fill;
    }

    public final Drawer fill(ColorRGBa fill) {
        drawStyle().fill = fill;
        return this_();
    }

    public final Drawer noFill() {
        drawStyle().fill = null;
        return this_();
    }

    public final ColorRGBa stroke() {
        return drawStyle().stroke;
    }

    public final Drawer stroke(ColorRGBa stroke) {
        drawStyle().stroke = stroke;
        return this_();
    }

    public final Drawer noStroke() {
        drawStyle().stroke = null;
        return this_();
    }

    public final Drawer fill(double r, double g, double b) {
        return fill(r, g, b, 1);

    }

    public final Drawer fill(double r, double g, double b, double a) {
        return fill(new ColorRGBa(r, g, b, a));
    }




    public final Drawer stroke(double r, double g, double b) {
        return stroke(r, g, b, 1);
    }

    public final Drawer stroke(double r, double g, double b, double a) {
        return stroke(new ColorRGBa(r, g, b, a));
    }


    public final Drawer lineCap(LineCap lineCap) {
        style.lineCap = lineCap;
        return this;
    }

    public final Drawer lineJoin(LineJoin lineJoin) {
        style.lineJoin = lineJoin;
        return this;
    }

    public final Drawer rotateX(double angleInRadians) {
        modelViewMatrix = multiply(modelViewMatrix, Transforms.rotateX(angleInRadians));
        return this_();
    }

    public final Drawer rotateY(double angleInRadians) {
        modelViewMatrix = multiply(modelViewMatrix, Transforms.rotateY(angleInRadians));
        return this_();
    }

    public final Drawer rotateZ(double angleInRadians) {
        modelViewMatrix = multiply(modelViewMatrix, Transforms.rotateZ(angleInRadians));
        return this_();
    }
    public final Drawer rotate(double angleInRadians) {
        return rotateZ(angleInRadians);
    }

    public final Drawer translate(double x, double y) {
        return translate(new Vector3(x, y, 0));
    }

    public final Drawer translate(Vector2 translation) {
        return translate(translation.xy0());
    }

    public final Drawer translate(double x, double y, double z) {
        translate(new Vector3(x, y, z));
        return this_();
    }

    public final Drawer translate(Vector3 translation) {
        modelViewMatrix = multiply( modelViewMatrix,Transforms.translate(translation));
        return this_();
    }



    public Drawer lookAt(double ex, double ey, double ez, double tx, double ty, double tz, double ux, double uy, double uz) {
        modelViewMatrix = multiply( modelViewMatrix, Transforms.lookAt(
                new Vector3(ex, ey, ez),
                new Vector3(tx, ty, tz),
                new Vector3(ux, uy, uz)));
        return this_();

    }


    public final Drawer scale(double x, double y, double z) {
        modelViewMatrix = multiply(modelViewMatrix, Transforms.scale(x, y, z) );
        return this_();
    }

    public final Drawer scale(double scale) {
        return scale(scale, scale, scale);
    }


    public final Drawer scale(double x, double y) {
        return scale(x, y, 1);
    }

    public final Drawer pushProjection() {
        projectionStack.push(projectionMatrix);
        return this_();
    }

    public final Drawer popProjection() {
        projectionMatrix = projectionStack.pop();
        return this_();
    }

    public final Drawer pushMatrix() {
        modelViewStack.push(modelViewMatrix);
        return this_();
    }

    public final Drawer popMatrix() {
        modelViewMatrix = modelViewStack.pop();
        return this_();
    }

    public final Drawer multiplyMatrix(Matrix44 matrix) {
        modelViewMatrix = multiply(modelViewMatrix, matrix);
        return this_();
    }

    public final Matrix44 modelView() {
        return modelViewMatrix;
    }

    public final Matrix44 projection() {
        return projectionMatrix;
    }

    public final Drawer modelView(Matrix44 modelViewMatrix) {
        this.modelViewMatrix = modelViewMatrix;
        return this_();
    }

    public final Drawer projection(Matrix44 projectionMatrix) {
        this.projectionMatrix = projectionMatrix;
        return this_();
    }


    public final Drawer ortho(double left, double right, double bottom, double top, double near, double far) {
        projectionMatrix = Transforms.ortho(left, right, bottom, top, near, far);
        return this_();
    }

    public final Drawer ortho() {
        projectionMatrix = Transforms.ortho(0, width, height, 0, -1, 1);
        return this_();
    }

    public final Drawer ortho(RenderTarget sourcePatch) {
        if (sourcePatch != null) {
            ortho(0, sourcePatch.width(), sourcePatch.height(), 0, -1, 1);
        }
        return this_();
    }


    public final Drawer perspective(double fov, double aspectInDegrees, double near, double far) {
        projectionMatrix = Transforms.perspectiveDegrees(fov, aspectInDegrees, near, far);
        return this_();
    }

    public Drawer identity() {
        modelViewMatrix = Matrix44.IDENTITY;
        return this_();
    }


    public final boolean smooth() {
        return drawStyle().smooth;
    }

    public final Drawer smooth(boolean smooth) {
        drawStyle().smooth = smooth;
        return this_();
    }

    public final ShadeStyle shadeStyle() {
        return drawStyle().shadeStyle;
    }

    public final Drawer shadeStyle(ShadeStyle shadeStyle) {
        drawStyle().shadeStyle = shadeStyle;
        return this_();
    }

    public final Drawer noShadeStyle() {
        drawStyle().shadeStyle = null;
        return this_();
    }

    public final Drawer depthWrite(boolean enableDepthWrite) {
        drawStyle().depthWrite = enableDepthWrite;
        return this_();
    }




    public final DepthTestMode depthTest() {
        return drawStyle().depthTest;
    }
    public final Drawer depthTest(DepthTestMode depthTestMode) {
        drawStyle().depthTest = depthTestMode;
        return this_();
    }

    public final Drawer pushStyle() {
        styleStack.push(style.copy());
        return this_();
    }

    public final Drawer popStyle() {
        style = styleStack.pop().copy();
        return this_();
    }

    public final double strokeWeight() {
        return style.strokeWeight;
    }

    public final Drawer strokeWeight(double strokeWeight) {
        style.strokeWeight = strokeWeight;
        return this_();
    }



    public final BlendMode blend() {
        return drawStyle().blendMode;
    }

    public final Drawer blend(BlendMode blendMode) {
        drawStyle().blendMode = blendMode;
        return this_();
    }

    public final int width() {
        return width;
    }
    public final int height() {
        return height;
    }

    public abstract Drawer circle(Vector3 position, double radius);
    public final Drawer circle(Vector2 position, double radius) {
        return circle(position.xy0(), radius);
    }

    public final Drawer circle(double x, double y, double radius) {
        return circle(new Vector2(x,y), radius);
    }

    public final Drawer circle(Circle circle) {
        return circle(circle.center, circle.radius);
    }

    /**
     * Draw multiple circles with fixed radius
     * @param positions a list of positions
     * @param radius the radius to use
     */
    public abstract Drawer circles(List<Vector2> positions, double radius);
    /**
     * Draw multiple circles
     * @param positions a list of positions to use
     * @param radii the radii to use
     */
    public abstract Drawer circles(List<Vector2> positions, List<Double> radii);
    public abstract Drawer circles(List<Circle> circles);

    /**
     * Draw multiple circle with the positions encoded in a buffer texture
     * @param positionsAndRadii buffer texture that encodes positions and radii, encoding is xyzr
     * @return this for chaining
     */
    public abstract Drawer circles(BufferTexture positionsAndRadii, int count);
    public abstract Drawer circles(BufferTexture positions, double radius, int count);


    public abstract Drawer rectangle(Vector2 topLeft, double width, double height);
    public final Drawer rectangle(double left, double top, double width, double height) {
        return rectangle(new Vector2(left, top), width, height);
    }
    public final Drawer rectangle(Rectangle rectangle) {
        return rectangle(rectangle.x, rectangle.y, rectangle.width, rectangle.height);
    }
    public abstract Drawer rectangles(List<Rectangle> rectangles);

    public abstract Drawer rectangles(List<Vector2> positions, List<Vector2> dimensions);

    /**
     * Draw multiple rectangles with positions and dimensions encded in a buffer texture
     * @param positionsAndDimensions buffer texture that encodes positions and dimensions, encoding is xywh
     * @return this for chaining
     */
    public abstract Drawer rectangles(BufferTexture positionsAndDimensions, int count);


    /**
     * Draws a shape
     * @param shape the shape to be drawn
     */
    public abstract Drawer shape(Shape shape);
    public abstract Drawer shapes(List<Shape> shapes);

    /**
     * Draws a polygon
     * @param polygon the polygon to be drawn
     */
    public abstract Drawer polygon(Polygon polygon);
    public abstract Drawer polygons(List<Polygon> polygons);

    public abstract Drawer tesselation(Tessellation tessellation);

    public abstract Drawer lineLoop(List<Vector2> points);
    public abstract Drawer lineStrip(List<Vector2> points);
    public abstract Drawer line(Vector2 start, Vector2 end);
    public final Drawer line(double x0, double y0, double x1, double y1) {
        line(new Vector2(x0,y0), new Vector2(x1, y1));
        return this_();
    }


    public final Drawer bezier(Vector2 start, Vector2 cp, Vector2 end) {
        lineStrip(new BezierQuadraticRenderer().render(start, cp, end));
        return this_();
    }

    public final Drawer bezier(Vector2 start, Vector2 cp0, Vector2 cp1, Vector2 end) {
        lineStrip(new BezierCubicRenderer().render(start, cp0, cp1, end));
        return this_();
    }

    public final Drawer beziers(Vector2[][] points) {

        for (Vector2[] bezier: points) {
            if (bezier.length == 4) {
                new BezierCubicRenderer().render(bezier[0], bezier[1], bezier[2], bezier[3]);
            }
            else if (bezier.length == 3) {
                new BezierQuadraticRenderer().render(bezier[0], bezier[1], bezier[2]);
            }

        }
        return this_();
    }

    public final Drawer contour(Contour contour) {
        if (contour.closed()) {
            List<Vector2> samples = contour.adaptivePositions();

            return lineLoop(samples.subList(0, samples.size()-1));
        } else {
            return lineStrip(contour.adaptivePositions());
        }
    }

    public final Drawer segment(Segment segment) {
        if (segment.control.length == 2) {
            return bezier(segment.start, segment.control[0], segment.control[1], segment.end);
        } else if (segment.control.length == 1) {
            return bezier(segment.start, segment.control[0], segment.end);
        } else if (segment.isLinear()) {
            return line(segment.start, segment.end);
        }
        return this_();
    }


    public final Drawer image(ColorBuffer colorBuffer, double x, double y) {
        return image(colorBuffer, x, y, colorBuffer.width(), colorBuffer.height());
    }

    public final Drawer image(ColorBuffer colorBuffer, double x, double y, double width, double height) {
        return image(colorBuffer,  0, 0, colorBuffer.width(), colorBuffer.height(),x, y, width, height);
    }

    public abstract Drawer image(ColorBuffer colorBuffer, double sourceX, double sourceY, double sourceWidth, double sourceHeight,
                            double destX, double destY, double destWidth, double destHeight);

    public final Drawer image(ColorBufferProxy proxy, double x, double y) {
        proxy.queue();
        proxy.colorBuffer().ifPresent(b -> image(proxy, x, y, b.width(), b.height()));
        return this_();
    }

    public final Drawer image(ColorBufferProxy proxy, double x, double y, double width, double height) {
        proxy.queue();
        if (proxy.colorBuffer().isPresent()) {
            return image(proxy, 0, 0, proxy.colorBuffer().get().width(), proxy.colorBuffer().get().height(), x, y, width, height);
        }
        return this_();
    }

    public final Drawer image(ColorBufferProxy proxy, double sourceX, double sourceY, double sourceWidth, double sourceHeight,
                            double destX, double destY, double destWidth, double destHeight) {
        proxy.queue();
        if (proxy.colorBuffer().isPresent()) {
            image(proxy.colorBuffer().get(), sourceX, sourceY, sourceWidth, sourceHeight, destX, destY, destWidth, destHeight);
        }
        return this_();
    }


    public abstract Drawer vertexBuffer(VertexBuffer vertexBuffer, Primitive primitive, long vertexCount);

    public abstract Drawer vertexBuffers(VertexBuffer vertexBuffer, Primitive primitive, long vertexCount, long instances);

    // -- 3d primitives
    /**
     * Draws a 3d box.
     * @param x the x-coordinate of the center
     * @param y the y-coordinate of the center
     * @param z the z-coordinate of the center
     * @param width the width of the box
     * @param height the height of the box
     * @param depth the depth of the box
     */

    public abstract Drawer box(double x, double y, double z, double width, double height, double depth);
    public abstract Drawer boxes(List<Vector3> positions, List<Vector3> dimensions);


    public final Drawer background(ColorRGBa background) {
        background(background.r, background.g, background.b, background.a);
        return this_();
    }

    public abstract Drawer background(double r, double g, double b, double a);



    // are these still needed?
    public final Drawer quickPerspective() {
        double fov = Math.PI/3.0;
        double cameraZ = (height/2.0) / Math.tan(fov/2.0);
        projectionMatrix = Transforms.perspectiveDegrees((fov/Math.PI)*180.0, width / height, cameraZ / 10.0, cameraZ * 10.0);
        modelViewMatrix = Transforms.translate(new Vector3(-width / 2, -height / 2, -cameraZ));
        return this_();
    }

    public final Drawer quickPerspective(double xOff, double yOff) {
        double fov = Math.PI/3.0;
        double cameraZ = (height/2.0) / Math.tan(fov/2.0);

        double aspect = (double) width / (double) height;
        double fH = Math.tan( (180.0*(fov/Math.PI) ) / 360 * Math.PI ) * cameraZ/10.0;
        double fW = fH * aspect;


        projectionMatrix = Transforms.frustum(
                -fW - xOff/10.0,
                fW - xOff/10.0,
                -fH + yOff /10.0,
                fH + yOff/10.0,
                cameraZ/10.0,
                cameraZ*10.0);

        modelViewMatrix = multiply(Transforms.scale(1, -1, 1), Transforms.translate(new Vector3(-width / 2.0, -height / 2.0, -cameraZ)));

        return this_();

    }

    public final Drawer composition(Composition composition) {
        compositionNode(composition.root);
        return this_();
    }


    private void compositionNode(CompositionNode node) {

        pushMatrix();
        multiplyMatrix(node.transform);
        if (node.shape() != null) {
            fill(node.shape().fill);
            stroke(node.shape().stroke);
            shape(node.shape());
        }
        if (node.image() != null) {

            ColorBuffer image = svgImages.computeIfAbsent(node.image().url, k-> ColorBuffer.fromUrl(k));
            image(image, 0.0, 0.0, node.image().width, node.image().height);
        }
        node.children().forEach(this::compositionNode);
        popMatrix();

    }

    public final Drawer text(FontMap fontMap, double x, double y, String text) {
        Writer writer = new Writer(this);
        writer.blend(style.blendMode);

        writer.font(fontMap);
        writer.fill(style.fill);
        writer.cursor(x,y);
        writer.text(text);


        return this_();
    }

    public final Drawer size(int width, int height) {
        this.width = width;
        this.height = height;
        return this_();
    }

    public final Drawer cull(CullMode cullMode) {
        style.cullMode = cullMode;
        return this_();
    }

    public final Drawer noCull() {
        style.cullMode = CullMode.NONE;
        return this_();
    }

    /**
     * Returns a shallow copy of this Drawer. Transforms, styles and their associated stacks are copied.
     * @return a shallow copy of this Drawer
     */
    public abstract Drawer copy();

    public abstract void destroy();

    public final Drawer stencilTestReference(int value) {
        style.stencilTestReference = value;
        return this;
    }

    public final Drawer stencilTestMask(int value) {
        style.stencilTestMask = value;
        return this;
    }

    public final Drawer stencilWriteMask(int value) {
        style.stencilWriteMask = value;
        return this;
    }
    public final Drawer stencilTest(StencilTest test) {
        style.stencilTest = test;
        return this;
    }

    public final Drawer depthPassOperation(StencilOperation operation) {
        style.depthPassOperation = operation;
        return this;
    }

    public final Drawer depthFailOperation(StencilOperation operation) {
        style.depthFailOperation = operation;
        return this;
    }

    public final Drawer stencilFailOperation(StencilOperation operation) {
        style.stencilFailOperation = operation;
        return this;
    }


}
