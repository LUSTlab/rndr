package net.lustlab.rndr.svg;

import net.lustlab.rndr.shape.Composition;

import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;

public class SVGComposition extends Composition {
    public static Composition fromFile(File file) {
        try {
            return fromUrl(file.toURI().toURL());
        } catch (MalformedURLException e) {
            throw new RuntimeException("malformed URL:" + file);
        }
    }

    public static Composition fromUrl(String urlString) {
        try {
            URL url = new URL(urlString);
            return fromUrl(url);
        } catch (MalformedURLException e) {
            throw new RuntimeException("malformed URL:" + urlString);
        }
    }

    public static Composition fromUrl(URL url) {
        try {
            return new SVGLoader().loadSVG(url.toString(),url).composition();
        } catch (IOException e) {
            throw new RuntimeException("failed to load SVG from url: " + url);
        }
    }

}
