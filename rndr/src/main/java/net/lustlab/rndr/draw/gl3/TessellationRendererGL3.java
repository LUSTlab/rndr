package net.lustlab.rndr.draw.gl3;


import com.jogamp.opengl.GL3;

import net.lustlab.colorlib.ColorRGBa;
import net.lustlab.rndr.draw.ShadeStyle;
import net.lustlab.rndr.draw.Tessellation;
import net.lustlab.rndr.math.Matrix44;
import net.lustlab.rndr.shaders.ShadeStyleManager;
import net.lustlab.rndr.shaders.Shader;
import net.lustlab.rndr.buffers.VertexBufferDrawer;

import java.nio.FloatBuffer;

public class TessellationRendererGL3 {

    static ShadeStyleManager shadeStyleManager = new ShadeStyleManager("cp:shaders/gl3/tessellation.vert", "cp:shaders/gl3/tessellation.frag");


    public static void drawTessellationSmooth(GL3 gl3, ShadeStyle shadeStyle, Matrix44 projection, Matrix44 modelView, Tessellation ts){
        drawTessellationSmooth(gl3, shadeStyle, projection, modelView, ts, null, null);
    }

    public static void drawTessellationSmooth(GL3 gl3, ShadeStyle shadeStyle, Matrix44 projection, Matrix44 modelView, Tessellation ts, ColorRGBa fill, ColorRGBa stroke) {

        Shader shader = shadeStyleManager.shader(shadeStyle, ts.data.get(0).vbo.format());

        gl3.glEnable(GL3.GL_LINE_SMOOTH);
        gl3.glEnable(GL3.GL_BLEND);
        gl3.glBlendFunc(GL3.GL_SRC_ALPHA, GL3.GL_ONE_MINUS_SRC_ALPHA);

        shader.begin();

        shadeStyleManager.setParameters(shader, shadeStyle);


        for (Tessellation.PrimitiveData pd: ts.data) {
            boolean ok = true;
            Matrix44 transform = Matrix44.IDENTITY; //modelView.multiply(pd.polygon.transform.matrix);
            Matrix44 worldViewProjection = projection.multiply(transform);

            shader.setUniform("modelViewProjectionMatrix", worldViewProjection);

            if (pd.colorSource == Tessellation.PrimitiveData.ColorSource.Fill) {
                if (fill != null) {
                    shader.setUniform("color", fill);
                } else if (pd.polygon.fill != null) {
                    shader.setUniform("color", pd.polygon.fill);
                } else {
//                    shader.setUniform("color", new ColorRGBa(1,0,1,1));
                    ok = false;
                }
            }
            else if (pd.colorSource == Tessellation.PrimitiveData.ColorSource.Stroke) {
                if (stroke != null) {
                    shader.setUniform("color", stroke);
                } else if (pd.polygon.stroke != null) {
                    shader.setUniform("color", pd.polygon.stroke);
                } else if (pd.polygon.fill != null) {
                    shader.setUniform("color", pd.polygon.fill);
                } else {
//                    shader.setUniform("color", new ColorRGBa(1,0,1,1));
                    ok = false;
                }
            }

            if (pd.vbo == null) {
                pd.createVBO(gl3, shader);
            }

            pd.vbo.bind();
            //VertexBufferDrawer.draw(gl2, shader, pd.vbo, Tessellation.PrimitiveData.layout, pd.command, pd.vertices.size());
            if (ok) {

//                if (pd.command == GL3.GL_LINES) {
                   VertexBufferDrawer.draw(gl3, pd.vao, pd.command, pd.vertices.size());
//                }
            }

        }

        shader.end();
    }

}
